<!DOCTYPE html>
<html>
	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	
	<body>
		<div id="app">
			<p v-bind:title="title">Hover !</p>

			<input type="text" v-model="val" :placeholder="placeholder">

			<button type="button" @click="insert()">Insert !</button>
			<button type="button" @click="remove()">Remove !</button>
			<button type="button" v-bind:id="abc" v-on:click="alertMessage">Alert !</button>

			<p v-for="(x, i) in arr" v-bind:key="i">
				{{ x.val }}
			</p>

			<box></box>
		</div>
	</body>

	<script type="text/javascript">
		Vue.component('box', {
			template: '<p>This is a box</p>'
		});

		let app = new Vue({
			el: '#app',
			data: {
				title: 'This is para !',
				val: '',
				val2: 10,
				abc: '',
				placeholder: 'Insert...',
				arr: [
					{val: 1},
					{val: 2},
					{val: 3}
				]
			},
			methods: {
				alertMessage : function(){
					console.log(this.arr);
				}
			},
			beforeCreate: function(){
				console.log(this.val2);
			}
		});

		function insert(){
			app.val = 'Hello World !';
		}

		function remove(){
			app.val = '';
		}
	</script>
</html>