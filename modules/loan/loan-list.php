<?php 
    require_once('../session.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RRM Inventory by RRM Gorup | Loan</title>

        <?php 
            require_once('../../sub-page-header.php');

            // PERMISSION
            $user_categories = [1, 3, 4];
            if(!in_array($user_category, $user_categories)){
                header('location: ../dashboard');
            }
        ?>

        <style type="text/css">
            .select2-container .select2-selection--single{
                height: 34px;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered{
                line-height: 34px;
            }
            .select2-container .select2-selection--single .select2-selection__arrow{
                height: 0px !important;
                top: 18px;
            }

            .consumable-table > thead > tr > th{
                vertical-align: middle;
            }
            .spare-table > tbody > tr > td{
                vertical-align: middle;
            }

            .hr-custom-style{
                border-top: 1px solid #808080;
            }
        </style>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">
            <!-- Topbar Start -->
            <?php include('../../topbar-for-sub-page.php'); ?>
            <!-- end Topbar -->

            <?php include('../../navbar-for-sub-page.php'); ?>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="wrapper full-width-background">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                    <li class="breadcrumb-item active">Loan List</li>
                                </ol>
                            </div>
                            <h4 class="page-title"><i class="mdi mdi-swap-vertical-variant"></i> Loan List</h4>
                        </div>
                    </div>
                </div>     
                <!-- end page title -->

                <!-- Start Modals For Create / Update Loan -->
                <div class="modal fade full-width-modal" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="full-width-modalLabel">Loan Parts</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>

                            <div class="modal-body" style="overflow-y: auto; height: 500px;">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-content mb-0 b-0 pt-0">
                                            <div class="tab-pane" id="first">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="consumable_table" style="overflow-x: auto;">
                                                            <table class="table table-bordered table-responsive-md table-striped text-center mb-0 consumable-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="align-middle text-center" style="min-width: 50px;">SL.</th>
                                                                        <th class="align-middle text-center" style="min-width: 120px;">Add to List<br><div title="Add all" class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-all-to-list" id="add_all_to_list" onclick="add_all_to_list()"><label class="custom-control-label" for="add_all_to_list"></label></div></th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Required For</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Parts Name</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Quantity</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Where to Use</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Price</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Party Name</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Gate No.</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Challan No.</th>
                                                                        <th class="align-middle text-center" style="min-width: 250px;">Challan Photo</th>
                                                                        <th class="align-middle text-center" style="min-width: 250px;">Bill Photo</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Remarks</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Loan Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="consumable_table_body">
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="mt-3 text-center proceed-div">
                                                            <button type="button" class="btn btn-success waves-effect waves-light" onclick="proceed_consumable()"><span class="btn-label"><i class="fas fa-arrow-right"></i></span>Proceed</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="second">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="spare_table" style="overflow-x: auto;">
                                                            <table class="table table-bordered table-responsive-md table-striped text-center mb-0 spare-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="align-middle text-center" style="min-width: 50px;">SL.</th>
                                                                        <th class="align-middle text-center" style="min-width: 120px;">Add to List<br><div title="Add all" class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-all-to-list-2" id="add_all_to_list_2" onclick="add_all_to_list_2()"><label class="custom-control-label" for="add_all_to_list_2"></label></div></th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Required For</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Parts Name</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Quantity</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Old Spares Details</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Status</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Price</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Party Name</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Gate No.</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Challan No.</th>
                                                                        <th class="align-middle text-center" style="min-width: 250px;">Challan Photo</th>
                                                                        <th class="align-middle text-center" style="min-width: 250px;">Bill Photo</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Remarks</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Loan Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="mt-3 text-center proceed-div-2">
                                                            <button type="button" class="btn btn-success waves-effect waves-light" onclick="proceed_spare()"><span class="btn-label"><i class="fas fa-arrow-right"></i></span>Proceed</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- tab-content -->
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button title="Scroll to left" type="button" class="btn btn-xs btn-info waves-effect waves-light scroll-left"><i class="fas fa-long-arrow-alt-left"></i></button>
                                <button title="Scroll to right" type="button" class="btn btn-xs btn-info waves-effect waves-light mr-2 scroll-right"><i class="fas fa-long-arrow-alt-right"></i></button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End Modals For Create Loan -->

                <!-- Start Modals For LOAN VIEW -->
                <div class="modal fade full-width-modal-2" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="full-width-modalLabel">View / Update Loan</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>

                            <div class="modal-body" style="overflow-y: auto; height: 500px;">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div style="overflow-x: auto;">
                                                    <table class="table table-bordered table-responsive-md table-striped text-center mb-0">
                                                        <thead>
                                                            <th class="text-center" style="min-width: 50px;">Sl.</th>
                                                            <th class="text-center" style="min-width: 200px;">Quantity</th>
                                                            <th class="text-center" style="min-width: 200px;">Price</th>
                                                            <th class="text-center" style="min-width: 250px;">Party</th>
                                                            <th class="text-center" style="min-width: 150px;">Gate No.</th>
                                                            <th class="text-center" style="min-width: 150px;">Challan No.</th>
                                                            <th class="text-center" style="min-width: 250px;">Challan Photo</th>
                                                            <th class="text-center" style="min-width: 250px;">Bill Photo</th>
                                                            <th class="text-center" style="min-width: 200px;">Loan Date</th>
                                                            <th class="text-center" style="min-width: 150px;">Action</th>
                                                        </thead>
                                                        <tbody class="loan-records">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row -->
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End Modals For LOAN VIEW -->
                
                <!-- LOAN LIST -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div id="rootwizard">
                                    <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                                        <li class="nav-item">
                                            <a href="#third" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2 active">
                                                <i class="fas fa-fire mr-1"></i>
                                                <span class="d-none d-sm-inline">CONSUMABLE</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#forth" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="fas fa-wrench mr-1"></i>
                                                <span class="d-none d-sm-inline">SPARE</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#fifth" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="fas fa-search mr-1"></i>
                                                <span class="d-none d-sm-inline">FILTERED</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#sixth" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="fas fa-undo mr-1"></i>
                                                <span class="d-none d-sm-inline">Loan Repay</span>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content mb-0 b-0 pt-0">
                                        <div class="tab-pane active" id="third" style="overflow-x: auto; overflow-y: auto;">
                                            <table id="basic-datatable2" class="table w-100 nowrap cell-border">
                                                <thead style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Loan Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="third_body">
                                                    <?php 
                                                        $con_requisition_query = mysqli_query($conn, "SELECT *, u1.user_fullname AS requisitioned_by, u2.user_fullname AS accepted_by FROM rrmsteel_con_requisition r INNER JOIN rrmsteel_user u1 ON u1.user_id = r.requisition_by INNER JOIN rrmsteel_user u2 ON u2.user_id = r.p_approved_by WHERE r.p_approval_status = 1 ORDER BY r.requisition_id DESC");

                                                        if(mysqli_num_rows($con_requisition_query) > 0){
                                                            $i = 1;

                                                            while($row = mysqli_fetch_assoc($con_requisition_query)){
                                                                $con_requisition_data_info = mysqli_fetch_assoc(mysqli_query($conn, "SELECT COUNT(*) AS tot_rec FROM rrmsteel_con_requisition_data WHERE requisition_id = '".$row['requisition_id']."' AND loan = 1"));

                                                                if($con_requisition_data_info['tot_rec'] > 0){
                                                    ?>
                                                                    <tr>
                                                                        <td class="align-middle text-center"><?= $i++; ?></td>
                                                                        <td class="align-middle text-center"><?= 'RRM\CONSUMABLE-REQUISITION\\' . date('Y', $row['requisition_created']) . '-' . $row['requisition_id']; ?></td>
                                                                        <td class="align-middle text-center"><?= $row['requisitioned_by']; ?></td>
                                                                        <td class="align-middle text-center"><?= $row['accepted_by']; ?></td>
                                                                        <td class="align-middle text-center"><?= date('d M, Y', $row['requisition_created']); ?></td>
                                                                        <td class="align-middle text-center">
                                                                            <?php 
                                                                                $loan_info = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM rrmsteel_con_loan WHERE requisition_id = '".$row['requisition_id']."' LIMIT 1"));

                                                                                if(isset($loan_info)){
                                                                                    $requisitioned_parts = $loan_info['requisitioned_parts'];
                                                                                    $borrowed_parts = $loan_info['borrowed_parts'];

                                                                                    if($borrowed_parts >= $requisitioned_parts){
                                                                                        $con_loan_status = 1;

                                                                                        echo '<span class="text-success font-weight-bold">Borrowed</span>';
                                                                                    } else{
                                                                                        $con_loan_status = 0;

                                                                                        echo '<div class="progress mb-2 progress-sm w-100">
                                                                                            <div class="progress-bar bg-info" role="progressbar" style="width: '.(($borrowed_parts / $requisitioned_parts) * 100).'%;" aria-valuenow="'.(($borrowed_parts / $requisitioned_parts) * 100).'" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                        </div>';
                                                                                        echo '<span class="text-warning font-weight-bold">' . ($requisitioned_parts - $borrowed_parts) . ' of ' . $requisitioned_parts . ' requisitioned parts are pending</span>';
                                                                                    }
                                                                                } else{
                                                                                    $con_loan_status = 0;
                                                                                    echo '<span class="text-warning font-weight-bold">Pending</span>';
                                                                                }
                                                                            ?>
                                                                        </td>
                                                                        <td class="align-middle text-center">
                                                                            <?php 
                                                                                $con_loan_query = mysqli_query($conn, "SELECT * FROM rrmsteel_con_loan WHERE requisition_id = '".$row['requisition_id']."'");

                                                                                $con_loan_status = 0;

                                                                                if(mysqli_num_rows($con_loan_query) > 0){
                                                                                    $loan_info = mysqli_fetch_assoc($con_loan_query);

                                                                                    $loan_id = $loan_info['loan_id'];
                                                                                    $requisitioned_parts = $loan_info['requisitioned_parts'];
                                                                                    $borrowed_parts = $loan_info['borrowed_parts'];

                                                                                    if($requisitioned_parts == $borrowed_parts)
                                                                                        $con_loan_status = 1;
                                                                                }

                                                                                if((($user_category == 1 && $row['approval_status'] == 1) || ($user_category == 3 && $row['p_approval_status'] == 1) || ($user_category == 4 && $row['p_approval_status'] == 1)) && (mysqli_num_rows($con_loan_query) == 0 || $con_loan_status == 0)){
                                                                                    if(isset($loan_id)){
                                                                            ?>
                                                                                        <a title="Mark as Borrowed" href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="mark_loan(<?= $loan_id ?>)"><i class="mdi mdi-check"></i></a>
                                                                            <?php 
                                                                                    }
                                                                            ?>

                                                                                    <a title="Loan" href="javascript:void(0)" class="btn btn-xs btn-success" data-toggle="modal" data-target=".full-width-modal" data-id="<?= $row['requisition_id'] ?>" onclick="loan_con(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-cart"></i></a>
                                                                            <?php 
                                                                                }

                                                                                if(mysqli_num_rows($con_loan_query) > 0){
                                                                            ?>
                                                                                    <a title="View / Update" href="javascript:void(0)" class="btn btn-xs btn-info" data-toggle="modal" data-target=".full-width-modal-2" data-id="<?= $row['requisition_id'] ?>" onclick="view_loan_con(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-eye"></i></a>
                                                                            <?php 
                                                                                }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                    <?php 
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Loan Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="forth" style="overflow-x: auto; overflow-y: auto;">
                                            <table id="basic-datatable" class="table w-100 nowrap cell-border">
                                                <thead style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Loan Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="forth_body">
                                                    <?php 
                                                        $spr_requisition_query = mysqli_query($conn, "SELECT *, u1.user_fullname AS requisitioned_by, u2.user_fullname AS accepted_by FROM rrmsteel_spr_requisition r INNER JOIN rrmsteel_user u1 ON u1.user_id = r.requisition_by INNER JOIN rrmsteel_user u2 ON u2.user_id = r.p_approved_by WHERE r.p_approval_status = 1 ORDER BY r.requisition_id DESC");

                                                        if(mysqli_num_rows($spr_requisition_query) > 0){
                                                            $i = 1;

                                                            while($row = mysqli_fetch_assoc($spr_requisition_query)){
                                                                $spr_requisition_data_info = mysqli_fetch_assoc(mysqli_query($conn, "SELECT COUNT(*) AS tot_rec FROM rrmsteel_spr_requisition_data WHERE requisition_id = '".$row['requisition_id']."' AND loan = 1"));
                                                            
                                                                if($spr_requisition_data_info['tot_rec'] > 0){
                                                    ?>
                                                                    <tr>
                                                                        <td class="align-middle text-center"><?= $i++; ?></td>
                                                                        <td class="align-middle text-center"><?= 'RRM\SPARE-REQUISITION\\' . date('Y', $row['requisition_created']) . '-' . $row['requisition_id']; ?></td>
                                                                        <td class="align-middle text-center"><?= $row['requisitioned_by']; ?></td>
                                                                        <td class="align-middle text-center"><?= $row['accepted_by']; ?></td>
                                                                        <td class="align-middle text-center"><?= date('d M, Y', $row['requisition_created']); ?></td>
                                                                        <td class="align-middle text-center">
                                                                            <?php 
                                                                                $loan_info = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM rrmsteel_spr_loan WHERE requisition_id = '".$row['requisition_id']."' LIMIT 1"));
                                                                                if(isset($loan_info)){
                                                                                    $requisitioned_parts = $loan_info['requisitioned_parts'];
                                                                                    $borrowed_parts = $loan_info['borrowed_parts'];

                                                                                    if($borrowed_parts >= $requisitioned_parts){
                                                                                        $spr_loan_status = 1;

                                                                                        echo '<span class="text-success font-weight-bold">Borrowed</span>';
                                                                                    } else{
                                                                                        $spr_loan_status = 0;

                                                                                        echo '<div class="progress mb-2 progress-sm w-100">
                                                                                            <div class="progress-bar bg-info" role="progressbar" style="width: '.(($borrowed_parts / $requisitioned_parts) * 100).'%;" aria-valuenow="'.(($borrowed_parts / $requisitioned_parts) * 100).'" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                        </div>';
                                                                                        echo '<span class="text-warning font-weight-bold">' . ($requisitioned_parts - $borrowed_parts) . ' of ' . $requisitioned_parts . ' requisitioned parts are pending</span>';
                                                                                    }
                                                                                } else{
                                                                                    $spr_loan_status = 0;
                                                                                    echo '<span class="text-warning font-weight-bold">Pending</span>';
                                                                                }
                                                                            ?>
                                                                        </td>
                                                                        <td class="align-middle text-center">
                                                                            <?php 
                                                                                $spr_loan_query = mysqli_query($conn, "SELECT * FROM rrmsteel_spr_loan WHERE requisition_id = '".$row['requisition_id']."'");

                                                                                $spr_loan_status = 0;

                                                                                if(mysqli_num_rows($spr_loan_query) > 0){
                                                                                    $loan_info = mysqli_fetch_assoc($spr_loan_query);

                                                                                    $loan_id_2 = $loan_info['loan_id'];
                                                                                    $requisitioned_parts = $loan_info['requisitioned_parts'];
                                                                                    $borrowed_parts = $loan_info['borrowed_parts'];

                                                                                    if($requisitioned_parts == $borrowed_parts)
                                                                                        $spr_loan_status = 1;
                                                                                }

                                                                                if((($user_category == 1 && $row['approval_status'] == 1) || ($user_category == 3 && $row['p_approval_status'] == 1) || ($user_category == 4 && $row['p_approval_status'] == 1)) && (mysqli_num_rows($spr_loan_query) == 0 || $spr_loan_status == 0)){
                                                                                    if(isset($loan_id_2)){
                                                                            ?>
                                                                                        <a title="Mark as Borrowed" href="javascript:void(0)" class="btn btn-xs btn-primary" onclick="mark_loan2(<?= $loan_id_2 ?>)"><i class="mdi mdi-check"></i></a>
                                                                            <?php 
                                                                                    }
                                                                            ?>

                                                                                    <a title="Loan" href="javascript:void(0)" class="btn btn-xs btn-success" data-toggle="modal" data-target=".full-width-modal" data-id="<?= $row['requisition_id'] ?>" onclick="loan_spr(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-cart"></i></a>
                                                                            <?php 
                                                                                }

                                                                                if(mysqli_num_rows($spr_loan_query) > 0){
                                                                            ?>
                                                                                    <a title="View" href="javascript:void(0)" class="btn btn-xs btn-info" data-toggle="modal" data-target=".full-width-modal-2" data-id="<?= $row['requisition_id'] ?>" onclick="view_loan_spr(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-eye"></i></a>
                                                                            <?php 
                                                                                }
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                    <?php 
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Loan Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="fifth" style="overflow-x: auto; overflow-y: auto;">
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="type">Status</label>
                                                        <select data-placeholder="Choose" class="form-control select-b type">
                                                            <option value="">Choose</option>
                                                            <option value="1">Borrowed</option>
                                                            <option value="2" selected>Pending</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="type">Party</label>
                                                        <select data-placeholder="Choose" class="form-control select-b party">
                                                            <option value="">Choose</option>
                                                            <?php 
                                                                $party_query = mysqli_query($conn, "SELECT * FROM rrmsteel_party");

                                                                if(mysqli_num_rows($party_query) > 0){
                                                                    while($row = mysqli_fetch_assoc($party_query)){
                                                            ?>
                                                                        <option value="<?= $row['party_id'] ?>"><?= $row['party_name'] ?></option>
                                                            <?php 
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label for="parts">Parts</label>
                                                        <select data-placeholder="Choose" class="form-control select-b parts">
                                                            <option value="">Choose</option>
                                                            <?php 
                                                                $parts_query = mysqli_query($conn, "SELECT parts_id, parts_name FROM rrmsteel_parts");

                                                                if(mysqli_num_rows($parts_query) > 0){
                                                                    while($row = mysqli_fetch_assoc($parts_query)){
                                                            ?>
                                                                        <option value="<?= $row['parts_id'] ?>"><?= $row['parts_name'] ?></option>
                                                            <?php 
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-2">
                                                    <div class="row">
                                                        <label for="." style="visibility: hidden;">.</label>
                                                    </div>
                                                    <div class="row">
                                                        <button type="button" class="btn btn-success waves-effect waves-light filter"><span class="btn-label"><i class="mdi mdi-filter-outline"></span></i>Filter Loan</button>
                                                    </div>
                                                </div>
                                            </div>

                                            <table id="basic-datatable3" class="table w-100 nowrap cell-border text-center">
                                                <thead style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Required For</th>
                                                        <th class="align-middle text-center">Parts Name</th>
                                                        <th class="align-middle text-center" style="min-width: 130px">Qty.</th>
                                                        <th class="align-middle text-center" style="min-width: 100px">Price</th>
                                                        <th class="align-middle text-center" style="min-width: 200px">Party Name</th>
                                                        <th class="align-middle text-center">Gate No.</th>
                                                        <th class="align-middle text-center">Challan No.</th>
                                                        <th class="align-middle text-center">Loan Date</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="fifth_body">
                                                </tbody>
                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Required For</th>
                                                        <th class="align-middle text-center">Parts Name</th>
                                                        <th class="align-middle text-center">Qty.</th>
                                                        <th class="align-middle text-center">Price</th>
                                                        <th class="align-middle text-center">Party Name</th>
                                                        <th class="align-middle text-center">Gate No.</th>
                                                        <th class="align-middle text-center">Challan No.</th>
                                                        <th class="align-middle text-center">Loan Date</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="sixth" style="overflow-x: auto; overflow-y: auto;">
                                            <table id="basic-datatable4" class="table w-100 nowrap cell-border text-center">
                                                <thead style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Parts Name</th>
                                                        <th class="align-middle text-center">Parts Unit</th>
                                                        <th class="align-middle text-center">Stock Quantity</th>
                                                        <th class="align-middle text-center">Borrowed Quantity</th>
                                                        <th class="align-middle text-center">Repaid Quantity</th>
                                                        <th class="align-middle text-center">Average Rate</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $con_arr = []; $spr_arr = [];

                                                        $con_requisition_data_query = mysqli_query($conn, "SELECT parts_id, SUM(parts_qty) AS borrowed_qty, SUM(repay_qty) AS repaid_qty FROM rrmsteel_con_loan_data GROUP BY parts_id");

                                                        if(mysqli_num_rows($con_requisition_data_query) > 0){
                                                            while($row = mysqli_fetch_assoc($con_requisition_data_query)){
                                                                array_push(
                                                                    $con_arr,
                                                                    (object) [
                                                                        'parts_id' => $row['parts_id'],
                                                                        'borrowed_qty' => $row['borrowed_qty'],
                                                                        'repaid_qty' => $row['repaid_qty']
                                                                    ]
                                                                );
                                                            }
                                                        }

                                                        $spr_requisition_data_query = mysqli_query($conn, "SELECT parts_id, SUM(parts_qty) AS borrowed_qty, SUM(repay_qty) AS repaid_qty FROM rrmsteel_spr_loan_data GROUP BY parts_id");

                                                        if(mysqli_num_rows($spr_requisition_data_query) > 0){
                                                            while($row = mysqli_fetch_assoc($spr_requisition_data_query)){
                                                                array_push(
                                                                    $spr_arr,
                                                                    (object) [
                                                                        'parts_id' => $row['parts_id'],
                                                                        'borrowed_qty' => $row['borrowed_qty'],
                                                                        'repaid_qty' => $row['repaid_qty']
                                                                    ]
                                                                );
                                                            }
                                                        }

                                                        $parts_data = array_column(array_merge($con_arr, $spr_arr), NULL, 'parts_id');
                                                        ksort($parts_data);
                                                        $parts_data = array_values($parts_data);

                                                        if(count($parts_data) > 0){
                                                            $parts_id_str = implode(',', array_map(function($p){
                                                                return $p->parts_id;
                                                            }, $parts_data));

                                                            $inv_summary_query = mysqli_query($conn, "SELECT i.*, p.parts_name, p.unit, p.category FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.parts_id IN (".$parts_id_str.")");

                                                            if(mysqli_num_rows($inv_summary_query) > 0){
                                                                $i = 0;

                                                                while($row = mysqli_fetch_assoc($inv_summary_query)){
                                                    ?>
                                                                    <tr>
                                                                        <td><?= $i+1; ?></td>
                                                                        <td><?= $row['parts_name']; ?></td>
                                                                        <td>
                                                                            <?php 
                                                                                if($row['unit'] == 1) echo 'Bag';
                                                                                elseif($row['unit'] == 2) echo 'Box';
                                                                                elseif($row['unit'] == 3) echo 'Box/Pcs';
                                                                                elseif($row['unit'] == 4) echo 'Bun';
                                                                                elseif($row['unit'] == 5) echo 'Bundle';
                                                                                elseif($row['unit'] == 6) echo 'Can';
                                                                                elseif($row['unit'] == 7) echo 'Cartoon';
                                                                                elseif($row['unit'] == 8) echo 'Challan';
                                                                                elseif($row['unit'] == 9) echo 'Coil';
                                                                                elseif($row['unit'] == 10) echo 'Drum';
                                                                                elseif($row['unit'] == 11) echo 'Feet';
                                                                                elseif($row['unit'] == 12) echo 'Gallon';
                                                                                elseif($row['unit'] == 13) echo 'Item';
                                                                                elseif($row['unit'] == 14) echo 'Job';
                                                                                elseif($row['unit'] == 15) echo 'Kg';
                                                                                elseif($row['unit'] == 16) echo 'Kg/Bundle';
                                                                                elseif($row['unit'] == 17) echo 'Kv';
                                                                                elseif($row['unit'] == 18) echo 'Lbs';
                                                                                elseif($row['unit'] == 19) echo 'Ltr';
                                                                                elseif($row['unit'] == 20) echo 'Mtr';
                                                                                elseif($row['unit'] == 21) echo 'Pack';
                                                                                elseif($row['unit'] == 22) echo 'Pack/Pcs';
                                                                                elseif($row['unit'] == 23) echo 'Pair';
                                                                                elseif($row['unit'] == 24) echo 'Pcs';
                                                                                elseif($row['unit'] == 25) echo 'Pound';
                                                                                elseif($row['unit'] == 26) echo 'Qty';
                                                                                elseif($row['unit'] == 27) echo 'Roll';
                                                                                elseif($row['unit'] == 28) echo 'Set';
                                                                                elseif($row['unit'] == 29) echo 'Truck';
                                                                                elseif($row['unit'] == 30) echo 'Unit';
                                                                                elseif($row['unit'] == 31) echo 'Yeard';
                                                                                elseif($row['unit'] == 32) echo '(Unit Unknown)';
                                                                                elseif($row['unit'] == 33) echo 'SFT';
                                                                                elseif($row['unit'] == 34) echo 'RFT';
                                                                                elseif($row['unit'] == 35) echo 'CFT';
                                                                            ?>
                                                                        </td>
                                                                        <td><?= $row['parts_qty']; ?></td>
                                                                        <td><?= $parts_data[$i]->borrowed_qty ?></td>
                                                                        <td><?= $parts_data[$i]->repaid_qty ?></td>
                                                                        <td><?= $row['parts_avg_rate']; ?></td>
                                                                        <td>
                                                                            <?php 
                                                                                if($parts_data[$i]->repaid_qty < $parts_data[$i]->borrowed_qty){
                                                                            ?>
                                                                                    <a title="Parts Loan" href="javascript:void(0)" class="btn btn-xs btn-success" data-toggle="modal" data-target=".bs-example-modal-lg3" data-id="<?= $row['parts_id'] ?>" onclick="parts_loan(<?= $row['parts_id'] ?>, <?= $row['category'] ?>, this)"><i class="mdi mdi-swap-vertical-variant"></i></a>
                                                                            <?php 
                                                                                }
                                                                            ?>

                                                                            <a title="Loan Repay History" href="javascript:void(0)" class="btn btn-xs btn-info" data-toggle="modal" data-target=".bs-example-modal-lg4" data-id="<?= $row['parts_id'] ?>" onclick="loan_repay_history(<?= $row['parts_id'] ?>, <?= $row['category'] ?>, this)"><i class="mdi mdi-format-list-bulleted-type"></i></a>
                                                                        </td>
                                                                    </tr>
                                                    <?php 
                                                                    $i++;
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Parts Name</th>
                                                        <th class="align-middle text-center">Parts Unit</th>
                                                        <th class="align-middle text-center">Stock Quantity</th>
                                                        <th class="align-middle text-center">Borrowed Quantity</th>
                                                        <th class="align-middle text-center">Repaid Quantity</th>
                                                        <th class="align-middle text-center">Average Rate</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->

                <!-- Start Modals For Parts Loan -->
                <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title col-md-10">
                                    Parts Loan Data: <span class="loan-repay-title"></span>

                                    <br>

                                    <span id="test" class="badge badge-secondary mt-1">
                                        Available Stock Qty.:

                                        <span class="badge badge-light stock-qty"></span>
                                    </span>
                                </h4>

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            
                            <div class="modal-body" style="overflow: scroll; height: 500px;">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="card" style="margin-bottom: 0">
                                            <div class="card-body">
                                                <div style="overflow-x: auto; overflow-y: auto;">
                                                    <table id="basic-datatable5" class="table w-100 nowrap text-center cell-border">
                                                        <thead style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Required For</th>
                                                                <th>Requisitioned Qty.</th>
                                                                <th>Borrowed Qty.</th>
                                                                <th>Repay Date</th>
                                                                <th>Repay Qty.</th>
                                                                <th>Price</th>
                                                                <th>Party Name</th>
                                                                <th>Borrow Date</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="loan-repay-records">
                                                        </tbody>
                                                        <tfoot style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Required For</th>
                                                                <th>Requisitioned Qty.</th>
                                                                <th>Borrowed Qty.</th>
                                                                <th>Repay Date</th>
                                                                <th>Repay Qty.</th>
                                                                <th>Price</th>
                                                                <th>Party Name</th>
                                                                <th>Borrow Date</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div> <!-- end card-body-->
                                        </div> <!-- end card-->
                                    </div> <!-- end col -->                  
                                </div> 
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End Modals For Parts Loan -->

                <!-- Start Modals For Loan Repay History -->
                <div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title col-md-10">
                                    Loan Repay History: <span class="loan-repay-history-title"></span>
                                </h4>

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            
                            <div class="modal-body" style="overflow: scroll; height: 500px;">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="card" style="margin-bottom: 0">
                                            <div class="card-body">
                                                <div style="overflow-x: auto; overflow-y: auto;">
                                                    <table id="basic-datatable6" class="table w-100 nowrap text-center cell-border">
                                                        <thead style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Parts Name</th>
                                                                <th>Parts Unit</th>
                                                                <th>Repaid Qty.</th>
                                                                <th>Repay Date</th>
                                                                <th>Party Name</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="loan-repay-history-records">
                                                        </tbody>
                                                        <tfoot style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Parts Name</th>
                                                                <th>Parts Unit</th>
                                                                <th>Repaid Qty.</th>
                                                                <th>Repay Date</th>
                                                                <th>Party Name</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div> <!-- end card-body-->
                                        </div> <!-- end card-->
                                    </div> <!-- end col -->                  
                                </div> 
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End Modals For Loan Repay History -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
        
        <!-- Footer Start -->
        <?php require_once('../../footer-for-sub-page.php'); ?>
        <!-- end Footer -->

        <!-- Custom js -->
        <script type="text/javascript">
            $(document).ready(function(){
                let table = $('#basic-datatable2, #basic-datatable3, #basic-datatable4').DataTable({
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                    }
                });

                $('.full-width-modal').on('shown.bs.modal', function(){
                    $('.modal-body').animate({
                        scrollTop: 0
                    }, 'faster');

                    $('#consumable_table, #spare_table').animate({
                        scrollLeft: 0
                    }, 'faster');
                });

                $('.scroll-left').click(function(){
                    $('#consumable_table, #spare_table').animate({scrollLeft: '-=400'}, 'faster');
                });

                $('.scroll-right').click(function(){
                    $('#consumable_table, #spare_table').animate({scrollLeft: '+=400'}, 'faster');
                });

                $('.party').prop('disabled', true);
                
                $.ajax({
                    url: '../../api/loan',
                    method: 'post',
                    data: {
                        loan_data_type: 'fetch_filtered_loan',
                        type: 2,
                        party_id: null,
                        parts_id: null
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let table = $('#basic-datatable3').DataTable();
                                table_data = '',
                                loan_info = _.sortBy(data.Reply, 'loan_date').reverse();

                            table.destroy();

                            $('#basic-datatable3 thead tr, #basic-datatable3 tfoot tr').find('th:eq(5), th:eq(6), th:eq(7), th:eq(8), th:eq(10)').addClass('d-none');
                            $('#basic-datatable3 thead tr, #basic-datatable3 tfoot tr').find('th:eq(9)').html('Requisition / Loan Date');

                            let party_id_arr = [];

                            $.each(loan_info, function(i, borrowed_party_id){
                                party_id_arr.push(borrowed_party_id.party_id);
                            });

                            let party_name_option = '';

                            $.ajax({
                                url: '../../api/party',
                                method: 'post',
                                data: {
                                    party_data_type: 'fetch_all'
                                },
                                dataType: 'json',
                                cache: false,
                                async: false,
                                success: function(data2){
                                    if(data2.Type == 'success'){
                                        $.each(data2.Reply, function(i, party_item){
                                            let selected = (jQuery.inArray(party_item.party_id, party_id_arr) !== -1) ? 'selected' : '';

                                            party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'" ' + selected + '>'+party_item.party_name+'</option>';
                                        });
                                    } else if(data.Type == 'error'){
                                        Swal.fire({
                                            title: 'Error',
                                            text: data.Reply,
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please try again.'
                                        });
                                    } else{
                                        Swal.fire({
                                            title: 'Info',
                                            text: 'Server is under maintenance. Please try again later!',
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        });
                                    }

                                    return false;
                                }
                            });

                            $.each(loan_info, function(i, data){
                                table_data += '<tr>';
                                    table_data += '<td class="align-middle">' + (i+1) + '</td>';
                                    table_data += '<td class="align-middle">' + data.reference + '</td>';
                                    table_data += '<td class="align-middle">' + data.required_for + '</td>';
                                    table_data += '<td class="align-middle">' + data.parts_name + '</td>';
                                    table_data += '<td class="align-middle">' + data.parts_qty + ' ' + data.parts_unit + '</td>';
                                    table_data += '<td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td>';
                                    table_data += '<td class="align-middle">' + data.date + '</td>';
                                    table_data += '<td class="d-none"></td>';
                                table_data += '</tr>';
                            });

                            $('#fifth_body').empty().append(table_data);

                            $('.select-b').select2({
                                width: '100%',
                                placeholder: 'Choose'
                            });

                            $('#basic-datatable3').DataTable({
                                language: {
                                    paginate: {
                                        previous: '<i class="mdi mdi-chevron-left">',
                                        next: '<i class="mdi mdi-chevron-right">'
                                    }
                                },
                                drawCallback: function(){
                                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                }
                            });
                        }
                    }
                });

                $('.type').on('change', function(){
                    if($(this).val() == 1)
                        $('.party').prop('disabled', false);
                    else
                        $('.party').prop('disabled', true);
                });

                $('.filter').click(function(){
                    let type = $('.type').val(),
                        party_id = $('.party').val(),
                        parts_id = $('.parts').val();

                    if(!type){
                        Swal.fire({
                            title: 'Error',
                            text: 'Empty required data!',
                            type: 'error',
                            width: 450,
                            showCloseButton: true,
                            confirmButtonColor: '#5cb85c',
                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                            footer: 'Please choose party or parts.'
                        });
                    } else{
                        let t;

                        Swal.fire({
                            title: 'Fetching Filtered Loan Data',
                            text: 'Please wait...',
                            timer: 100,
                            allowOutsideClick: false,
                            onBeforeOpen: function(){
                                Swal.showLoading(), t = setInterval(function(){
                                }, 100);
                            }
                        }).then(function(){
                            $.ajax({
                                url: '../../api/loan',
                                method: 'post',
                                data: {
                                    loan_data_type: 'fetch_filtered_loan',
                                    type: type,
                                    party_id: party_id,
                                    parts_id: parts_id
                                },
                                dataType: 'json',
                                cache: false,
                                async: false,
                                success: function(data){
                                    if(data.Type == 'success'){
                                        let table = $('#basic-datatable3').DataTable();
                                            table_data = '',
                                            loan_info = _.sortBy(data.Reply, 'loan_date').reverse();

                                        table.destroy();

                                        if(type == 1){
                                            $('#basic-datatable3 thead tr, #basic-datatable3 tfoot tr').find('th:eq(5), th:eq(6), th:eq(7), th:eq(8), th:eq(10)').removeClass('d-none');
                                            $('#basic-datatable3 thead tr, #basic-datatable3 tfoot tr').find('th:eq(9)').html('Loan Date');
                                        } else{
                                            $('#basic-datatable3 thead tr, #basic-datatable3 tfoot tr').find('th:eq(5), th:eq(6), th:eq(7), th:eq(8), th:eq(10)').addClass('d-none');
                                            $('#basic-datatable3 thead tr, #basic-datatable3 tfoot tr').find('th:eq(9)').html('Requisition / Loan Date');
                                        }

                                        let party_id_arr = [];

                                        $.each(loan_info, function(i, borrowed_party_id){
                                            party_id_arr.push(borrowed_party_id.party_id);
                                        });

                                        let party_name_option = '';
                                        
                                        $.ajax({
                                            url: '../../api/party',
                                            method: 'post',
                                            data: {
                                                party_data_type: 'fetch_all'
                                            },
                                            dataType: 'json',
                                            cache: false,
                                            async: false,
                                            success: function(data2){
                                                if(data2.Type == 'success'){
                                                    $.each(data2.Reply, function(i, party_item){
                                                        let selected = (jQuery.inArray(party_item.party_id, party_id_arr) !== -1) ? 'selected' : '';

                                                        party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'" ' + selected + '>'+party_item.party_name+'</option>';
                                                    });
                                                } else if(data.Type == 'error'){
                                                    Swal.fire({
                                                        title: 'Error',
                                                        text: data.Reply,
                                                        type: 'error',
                                                        width: 450,
                                                        showCloseButton: true,
                                                        confirmButtonColor: '#5cb85c',
                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                        footer: 'Please try again.'
                                                    });
                                                } else{
                                                    Swal.fire({
                                                        title: 'Info',
                                                        text: 'Server is under maintenance. Please try again later!',
                                                        type: 'info',
                                                        width: 450,
                                                        showCloseButton: true,
                                                        confirmButtonColor: '#5cb85c',
                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                    });
                                                }

                                                return false;
                                            }
                                        });

                                        $.each(loan_info, function(i, data){
                                            table_data += '<tr>';
                                                table_data += '<td class="align-middle">' + (i+1) + '</td>';
                                                table_data += '<td class="align-middle">' + data.reference + '</td>';
                                                table_data += '<td class="align-middle">' + data.required_for + '</td>';
                                                table_data += '<td class="align-middle">' + data.parts_name + '</td>';

                                                if(type == 1){
                                                    table_data += '<td class="align-middle">';
                                                        table_data += '<span class="data-span">' + data.parts_qty + ' ' + data.parts_unit + '</span>';

                                                        table_data += '<div class="input-group data-input d-none">';
                                                            table_data += '<input type="number" class="form-control" placeholder="Insert" value="'+data.parts_qty+'" oninput="qty_3(this, ' + data.parts_qty + ')">';
                                                            table_data += '<div class="input-group-prepend">';
                                                                table_data += '<div class="input-group-text">' + data.parts_unit + '</div>';
                                                            table_data += '</div>';
                                                        table_data += '</div>';
                                                    table_data += '</td>';

                                                    table_data += '<td class="align-middle">';
                                                        table_data += '<span class="data-span"><i class="mdi mdi-currency-bdt"></i>' + data.price + '</span>';

                                                        table_data += '<input type="number" class="form-control data-input d-none" placeholder="Insert" value="'+data.price+'" oninput="price_2(this, ' + data.price + ')">';
                                                    table_data += '</td>';

                                                    table_data += '<td class="align-middle">';
                                                        table_data += '<span class="data-span">' + data.party_name + '</span>';

                                                        table_data += '<div class="data-input d-none">';
                                                            table_data += '<select class="select-b" onchange="party_name_2(this)">';
                                                                table_data += '<option value="">Choose</option>';
                                                                table_data += party_name_option;
                                                            table_data += '</select>';
                                                            table_data += '<span class="float-left mt-1 text-primary remarks"></span>';
                                                        table_data += '</div>';    
                                                    table_data += '</td>';

                                                    table_data += '<td class="align-middle text-center">';
                                                        table_data += '<span class="data-span">' + data.gate_no + '</span>';

                                                        table_data += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+data.gate_no+'">';
                                                    table_data += '</td>';

                                                    table_data += '<td class="align-middle">';
                                                        table_data += '<span class="data-span">' + data.challan_no + '</span>';

                                                        table_data += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+data.challan_no+'">';
                                                    table_data += '</td>';
                                                    
                                                    table_data += '<td class="align-middle">';
                                                        table_data += '<span class="data-span">' + data.loan_date + '</span>';

                                                        table_data += '<input type="text" class="form-control data-input d-none" oninput="loan_date_2(this)" data-id="'+ data.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" value="'+data.loan_date+'">';
                                                    table_data += '</td>';

                                                    table_data += '<td class="align-middle">';
                                                        table_data += '<a title="Edit" href="javascript:void(0)" class="btn btn-xs btn-info mr-1 edt-btn" onclick="edit_btn(this)"><i class="mdi mdi-pencil"></i></a>';
                                                        table_data += '<a title="Cancel" href="javascript:void(0)" class="btn btn-xs btn-warning d-none mr-1 cncl-btn" onclick="cancel_btn(this)"><i class="mdi mdi-cancel"></i></a>';
                                                        
                                                        let req_for = 0;

                                                        if(data.required_for == 'BCP-CCM')
                                                            req_for = 1;
                                                        else if(data.required_for == 'BCP-Furnace')
                                                            req_for = 2;
                                                        else if(data.required_for == 'Concast-CCM')
                                                            req_for = 3;
                                                        else if(data.required_for == 'Concast-Furnace')
                                                            req_for = 4;
                                                        else if(data.required_for == 'HRM')
                                                            req_for = 5;
                                                        else if(data.required_for == 'HRM Unit-2')
                                                            req_for = 6;
                                                        else if(data.required_for == 'Lal Masjid')
                                                            req_for = 7;
                                                        else if(data.required_for == 'Sonargaon')
                                                            req_for = 8;
                                                        else if(data.required_for == 'General')
                                                            req_for = 9;

                                                        if(data.type === 'Consumable'){
                                                            table_data += '<a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none mr-1 upd-btn" onclick="update_loan_data_con_2(' + data.loan_data_id + ', this, ' + data.loan_id + ', ' + data.parts_id + ', ' + req_for + ')"><i class="mdi mdi-arrow-up-bold-outline"></i></a>';
                                                            table_data += '<a title="Delete" href="javascript:void(0)" class="btn btn-xs btn-danger dlt-btn" onclick="delete_loan_data_con_2(' + data.loan_data_id + ', this, ' + data.parts_id + ', ' + req_for + ', ' + data.loan_id + ')"><i class="mdi mdi-delete"></i></a>';
                                                        } else{
                                                            table_data += '<a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none mr-1 upd-btn" onclick="update_loan_data_spr_2(' + data.loan_data_id + ', this, ' + data.loan_id + ', ' + data.parts_id + ', ' + req_for + ')"><i class="mdi mdi-arrow-up-bold-outline"></i></a>';
                                                            table_data += '<a title="Delete" href="javascript:void(0)" class="btn btn-xs btn-danger dlt-btn" onclick="delete_loan_data_spr_2(' + data.loan_data_id + ', this, ' + data.parts_id + ', ' + req_for + ', ' + data.loan_id + ')"><i class="mdi mdi-delete"></i></a>';
                                                        }
                                                    table_data += '</td>';
                                                } else{
                                                    table_data += '<td class="align-middle">' + data.parts_qty + ' ' + data.parts_unit + '</td>';
                                                    table_data += '<td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td>';
                                                    table_data += '<td class="align-middle">' + data.date + '</td>';
                                                    table_data += '<td class="d-none"></td>';
                                                }
                                            table_data += '</tr>';
                                        });

                                        $('#fifth_body').empty().append(table_data);

                                        $('.select-b').select2({
                                            width: '100%',
                                            placeholder: 'Choose'
                                        });

                                        $('#basic-datatable3').DataTable({
                                            language: {
                                                paginate: {
                                                    previous: '<i class="mdi mdi-chevron-left">',
                                                    next: '<i class="mdi mdi-chevron-right">'
                                                }
                                            },
                                            drawCallback: function(){
                                                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                });
            });

            // GET PARTS QTY
            function qty(ele, ele2){
                if(ele == 1){
                    $('#consumable_table tbody tr').each(function(){
                        if($(this).find('td:eq(4)').find('.qty').val() <= 0)
                            $(this).find('td:eq(4)').find('.qty').val('');
                        // else if($(this).find('td:eq(4)').find('.qty').val() > ele2)
                            // $(this).find('td:eq(4)').find('.qty').val(ele2);
                    });
                } else{
                    $('#spare_table tbody tr').each(function(){
                        if($(this).find('td:eq(4)').find('.qty-2').val() <= 0)
                            $(this).find('td:eq(4)').find('.qty-2').val('');
                        // else if($(this).find('td:eq(4)').find('.qty-2').val() > ele2)
                            // $(this).find('td:eq(4)').find('.qty-2').val(ele2);
                    });
                }
            }

            // GET PARTS QTY 2
            function qty_2(ele, ele2, ele3, ele4, ele5){
                /*let tot_borrowed = 0;

                $('.loan-records tr').each(function(){
                    if($(this).index() >= ele4 && $(this).index() <= ele5)
                        tot_borrowed += parseInt($(this).find('td:eq(1)').find('input').val()) ? parseInt($(this).find('td:eq(1)').find('input').val()) : 0;
                });

                if(parseInt($(ele).val()) <= 0 || tot_borrowed > ele3)*/

                if(parseInt($(ele).val()) <= 0)
                    $(ele).closest('tr').find('td:eq(1)').find('input').val(ele2);
            }

            // GET PARTS QTY 3
            function qty_3(ele, ele2){
                if(parseInt($(ele).val()) <= 0)
                    $(ele).closest('tr').find('td:eq(4)').find('input').val(ele2);
            }

            // GET PARTS QTY 4
            function qty_4(ele, ele2){
                let repay_date = $(ele).closest('tr').find('td:eq(4)').find('input').val(),
                    repay_qty = $(ele).val();

                if(!repay_qty){
                    $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                } else if(parseFloat(repay_qty) <= 0){
                    $(ele).closest('tr').find('td:eq(5)').find('input').val('');
                    $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                } else if(parseFloat($(ele).val()) > parseFloat(ele2)){
                    $(ele).closest('tr').find('td:eq(5)').find('input').val(ele2);
                } else{
                    if(repay_date){
                        $(ele).closest('tr').find('td:eq(9)').find('a').removeClass('disabled');
                    } else{
                        $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                    }
                }
            }

            // GET PRICE
            function price(ele){
                if(ele == 1){
                    $('#consumable_table tbody tr').each(function(){
                        if($(this).find('td:eq(6)').find('.price').val() <= 0)
                            $(this).find('td:eq(6)').find('.price').val('');
                    });
                } else{
                    $('#spare_table tbody tr').each(function(){
                        if($(this).find('td:eq(7)').find('.price-2').val() <= 0)
                            $(this).find('td:eq(7)').find('.price-2').val('');
                    });
                }
            }

            function price_2(ele, ele2){
                if(parseInt($(ele).val()) <= 0)
                    $(ele).closest('tr').find('td:eq(2)').find('input').val(ele2.toFixed(2));
            }

            // GET PARTY NAME
            function party_name(ele, ele2, ele3){
                let party_arr = ele.split('|'),
                    remarks = party_arr[1];

                if(ele2 == 1){
                    if(remarks)
                        $('#consumable_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(7)').find('.remarks').html('Remarks: ' + remarks);
                    else
                        $('#consumable_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(7)').find('.remarks').html('');
                } else{
                    if(remarks)
                        $('#spare_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(8)').find('.remarks-2').html('Remarks: ' + remarks);
                    else
                        $('#spare_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(8)').find('.remarks-2').html('');
                }
            }

            function party_name_2(ele){
                let party_arr = $(ele).val().split('|'),
                    remarks = party_arr[1];

                if(remarks)
                    $(ele).closest('tr').find('td:eq(3)').find('.remarks').html('Remarks: ' + remarks);
                else
                    $(ele).closest('tr').find('td:eq(3)').find('.remarks').html('');
            }

            // VALIDATE LOAN DATE FIELD
            function loan_date(ele, ele2){
                if(ele == 1)
                    $('#consumable_table tbody').find('tr:nth-child(' + ele2 + ')').find('td:eq(13)').find('input').val('');
                else
                    $('#spare_table tbody').find('tr:nth-child(' + ele2 + ')').find('td:eq(14)').find('input').val('');
            }

            function loan_date_2(ele){
                $(ele).closest('tr').find('td:eq(8)').find('input').val('');
            }

            // VALIDATE LOAN REPAY DATE FIELD
            function loan_repay_date(ele){
                $(ele).closest('tr').find('td:eq(4)').find('input').val('');
            }

            // ADD ALL TO CON LIST
            function add_all_to_list(){
                $('.add-to-list').each(function(){
                    if($(this).attr('disabled') == undefined){
                        if($('.add-all-to-list').is(':checked'))
                            $(this).prop('checked', true);
                        else
                            $(this).prop('checked', false);
                    }
                });
            }

            // ADD ALL TO SPARE LIST
            function add_all_to_list_2(){
                $('.add-to-list-2').each(function(){
                    if($(this).attr('disabled') == undefined){
                        if($('.add-all-to-list-2').is(':checked'))
                            $(this).prop('checked', true);
                        else
                            $(this).prop('checked', false);
                    }
                });
            }

            // ADD TO CON LIST
            function add_to_list(){
                let flag = 1;

                $('.add-to-list').each(function(i){
                    if(!$(this).is(':checked')){
                        flag = 0;

                        return false;
                    }
                });

                if(flag == 1)
                    $('.add-all-to-list').prop('checked', true);
                else
                    $('.add-all-to-list').prop('checked', false);
            }

            // ADD TO SPARE LIST
            function add_to_list_2(){
                let flag = 1;

                $('.add-to-list-2').each(function(i){
                    if(!$(this).is(':checked')){
                        flag = 0;

                        return false;
                    }
                });

                if(flag == 1)
                    $('.add-all-to-list-2').prop('checked', true);
                else
                    $('.add-all-to-list-2').prop('checked', false);
            }

            function edit_btn(ele){
                $(ele).closest('tr').find('.data-span').addClass('d-none');
                $(ele).closest('tr').find('.data-input').removeClass('d-none');
                
                $(ele).closest('tr').find('.edt-btn').addClass('d-none');
                $(ele).closest('tr').find('.cncl-btn').removeClass('d-none');
                $(ele).closest('tr').find('.upd-btn').removeClass('d-none');
            }

            function cancel_btn(ele){
                $(ele).closest('tr').find('.data-span').removeClass('d-none');
                $(ele).closest('tr').find('.data-input').addClass('d-none');
                
                $(ele).closest('tr').find('.edt-btn').removeClass('d-none');
                $(ele).closest('tr').find('.cncl-btn').addClass('d-none');
                $(ele).closest('tr').find('.upd-btn').addClass('d-none');
            }

            let requisition_data = '';

            // CONSUMABLE LOAN
            function loan_con(ele){
                $('#first').addClass('active');
                $('#second').removeClass('fade active show');

                let t;

                Swal.fire({
                    title: 'Fetching Loan Data',
                    text: 'Please wait...',
                    timer: 100,
                    allowOutsideClick: false,
                    onBeforeOpen: function(){
                        Swal.showLoading(), t = setInterval(function(){
                        }, 100);
                    }
                }).then(function(){
                    let requisition_id = ele;

                    $.ajax({
                        url: '../../api/loan',
                        method: 'post',
                        data: {
                            loan_data_type: 'fetch_requisition_con',
                            requisition_id: requisition_id
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                requisition_data = data;
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let parts_id_arr = [],
                        parts_name_arr = [];

                    $.each(requisition_data.Reply, function(i, requisitioned_parts_id){
                        parts_id_arr.push(requisitioned_parts_id.parts_id);
                    });

                    $.ajax({
                        url: '../../api/parts',
                        method: 'post',
                        data: {
                            parts_data_type: 'fetch_all'
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                $.each(data.Reply, function(i, parts_item){
                                    if(parts_item.parts_category <= 2 && jQuery.inArray(parts_item.parts_id, parts_id_arr) !== -1){
                                        for(let j=0; j<parts_id_arr.length; j++){
                                            if(parts_id_arr[j] == parts_item.parts_id)
                                                parts_name_arr.push(parts_item.parts_name);
                                        }
                                    }
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let party_name_option = '';
                    $.ajax({
                        url: '../../api/party',
                        method: 'post',
                        data: {
                            party_data_type: 'fetch_all'
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                $.each(data.Reply, function(i, party_item){
                                    party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'">'+party_item.party_name+'</option>';
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let trHTML = '';

                    $.each(requisition_data.Reply, function(i, requisition_item){
                        trHTML += '<tr '+ ((requisition_item.parts_id == requisition_item.borrowed_parts_id && parseFloat(requisition_item.borrowed_parts_qty) >= parseFloat(requisition_item.r_qty)) ? 'style=" background: #cfe8cf;"' : '') +'>';
                            trHTML += '<td class="align-middle">' + (i+1) + ' <span class="text-danger d-none">*</span></td>';

                            let checked = '',
                                disabled = '';

                            if(requisition_item.parts_id == requisition_item.borrowed_parts_id && parseFloat(requisition_item.borrowed_parts_qty) >= parseFloat(requisition_item.r_qty)){
                                checked = 'checked';
                                disabled = 'disabled';
                            }

                            trHTML += '<td class="align-middle"><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-to-list" id="add_to_list'+i+'" onclick="add_to_list()" '+ checked + ' ' + disabled +'><label class="custom-control-label" for="add_to_list'+i+'"></label></div></td>';

                            if(requisition_item.required_for == 1)
                                required_for = 'BCP-CCM';
                            else if(requisition_item.required_for == 2)
                                required_for = 'BCP-Furnace';
                            else if(requisition_item.required_for == 3)
                                required_for = 'Concast-CCM';
                            else if(requisition_item.required_for == 4)
                                required_for = 'Concast-Furnace';
                            else if(requisition_item.required_for == 5)
                                required_for = 'HRM';
                            else if(requisition_item.required_for == 6)
                                required_for = 'HRM Unit-2';
                            else if(requisition_item.required_for == 7)
                                required_for = 'Lal Masjid';
                            else if(requisition_item.required_for == 8)
                                required_for = 'Sonargaon';
                            else if(requisition_item.required_for == 9)
                                required_for = 'General';

                            trHTML += '<td class="text-center align-middle">' + required_for + '</td>';
                            trHTML += '<td class="text-center align-middle">' + parts_name_arr[i] + '</td>';

                            trHTML += '<td class="align-middle text-center">';
                                trHTML += '<div class="input-group">';
                                    trHTML += '<input type="number" class="form-control qty" placeholder="Insert" oninput="qty(1, '+ (requisition_item.r_qty - requisition_item.borrowed_parts_qty) +')" '+ disabled +'>';
                                    trHTML += '<div class="input-group-prepend">';
                                        let parts_unit = '';

                                        if(requisition_item.parts_unit == 1)
                                            parts_unit = 'Bag';
                                        else if(requisition_item.parts_unit == 2)
                                            parts_unit = 'Box';
                                        else if(requisition_item.parts_unit == 3)
                                            parts_unit = 'Box/Pcs';
                                        else if(requisition_item.parts_unit == 4)
                                            parts_unit = 'Bun';
                                        else if(requisition_item.parts_unit == 5)
                                            parts_unit = 'Bundle';
                                        else if(requisition_item.parts_unit == 6)
                                            parts_unit = 'Can';
                                        else if(requisition_item.parts_unit == 7)
                                            parts_unit = 'Cartoon';
                                        else if(requisition_item.parts_unit == 8)
                                            parts_unit = 'Challan';
                                        else if(requisition_item.parts_unit == 9)
                                            parts_unit = 'Coil';
                                        else if(requisition_item.parts_unit == 10)
                                            parts_unit = 'Drum';
                                        else if(requisition_item.parts_unit == 11)
                                            parts_unit = 'Feet';
                                        else if(requisition_item.parts_unit == 12)
                                            parts_unit = 'Gallon';
                                        else if(requisition_item.parts_unit == 13)
                                            parts_unit = 'Item';
                                        else if(requisition_item.parts_unit == 14)
                                            parts_unit = 'Job';
                                        else if(requisition_item.parts_unit == 15)
                                            parts_unit = 'Kg';
                                        else if(requisition_item.parts_unit == 16)
                                            parts_unit = 'Kg/Bundle';
                                        else if(requisition_item.parts_unit == 17)
                                            parts_unit = 'Kv';
                                        else if(requisition_item.parts_unit == 18)
                                            parts_unit = 'Lbs';
                                        else if(requisition_item.parts_unit == 19)
                                            parts_unit = 'Ltr';
                                        else if(requisition_item.parts_unit == 20)
                                            parts_unit = 'Mtr';
                                        else if(requisition_item.parts_unit == 21)
                                            parts_unit = 'Pack';
                                        else if(requisition_item.parts_unit == 22)
                                            parts_unit = 'Pack/Pcs';
                                        else if(requisition_item.parts_unit == 23)
                                            parts_unit = 'Pair';
                                        else if(requisition_item.parts_unit == 24)
                                            parts_unit = 'Pcs';
                                        else if(requisition_item.parts_unit == 25)
                                            parts_unit = 'Pound';
                                        else if(requisition_item.parts_unit == 26)
                                            parts_unit = 'Qty';
                                        else if(requisition_item.parts_unit == 27)
                                            parts_unit = 'Roll';
                                        else if(requisition_item.parts_unit == 28)
                                            parts_unit = 'Set';
                                        else if(requisition_item.parts_unit == 29)
                                            parts_unit = 'Truck';
                                        else if(requisition_item.parts_unit == 30)
                                            parts_unit = 'Unit';
                                        else if(requisition_item.parts_unit == 31)
                                            parts_unit = 'Yeard';
                                        else if(requisition_item.parts_unit == 32)
                                            parts_unit = '(Unit Unknown)';
                                        else if(requisition_item.parts_unit == 33)
                                            parts_unit = 'SFT';
                                        else if(requisition_item.parts_unit == 34)
                                            parts_unit = 'RFT';
                                        else if(requisition_item.parts_unit == 35)
                                            parts_unit = 'CFT';

                                        trHTML += '<div class="input-group-text"><span id="unit">' + parts_unit + '</span></div>';
                                    trHTML += '</div>';
                                trHTML += '</div>';

                                trHTML += '<span class="float-left mt-1 text-primary w-100 in-stock">In Stock: <strong>' + requisition_item.i_qty + '</strong> ' + parts_unit + '</span><span class="float-left text-info w-100 borrowed">Borrowed: <strong>' + requisition_item.borrowed_parts_qty + '</strong> of <strong>' + requisition_item.r_qty + '</strong> ' + parts_unit + '</span>';
                            trHTML += '</td>';

                            trHTML += '<td class="text-center align-middle">' + requisition_item.parts_usage + '</td>';
                            
                            trHTML += '<td class="align-middle"><input type="number" class="form-control price" placeholder="Insert" oninput="price(1)" '+ disabled +'></td>';
                                
                            trHTML += '<td class="align-middle">';
                                trHTML += '<select class="select-b party-name" onchange="party_name(this.value, 1, ' + (i+1) + ')" '+ disabled +'>';
                                    trHTML += '<option value="">Choose</option>';
                                    trHTML += party_name_option;
                                trHTML += '</select>';
                                trHTML += '<span class="float-left mt-1 text-primary remarks"></span>';
                            trHTML += '</td>';

                            trHTML += '<td class="align-middle"><input type="text" class="form-control gate-no" placeholder="Insert" '+ disabled +'></td>';
                            trHTML += '<td class="align-middle"><input type="text" class="form-control challan-no" placeholder="Insert" '+ disabled +'></td>';
                            trHTML += '<td class="align-middle"><input type="file" class="form-control challan-photo" accept="image/*" '+ disabled +'></td>';
                            trHTML += '<td class="align-middle"><input type="file" class="form-control bill-photo" accept="image/*" '+ disabled +'></td>';

                            trHTML += '<td class="text-center align-middle">' + requisition_item.remarks + '</td>';

                            trHTML += '<td class="align-middle"><input type="text" class="form-control loan-date" style="width: 150px;" oninput="loan_date(1, '+ (i+1) +')" data-id="'+ requisition_item.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" '+ disabled +'></td>';
                        trHTML += '</tr>';

                        $('#consumable_table').find('table tbody').empty();
                        $('#consumable_table').find('table').append(trHTML);

                        $('.select-b').select2({
                            width: '100%',
                            placeholder: 'Choose'
                        });
                    });
                });
            }

            // PROCEED CONSUMABLE
            function proceed_consumable(){
                $('#consumable_table tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(6)').find('input').val(),
                        party = $(this).find('td:eq(7)').find('select').val(),
                        gate_no = $(this).find('td:eq(8)').find('input').val(),
                        challan_no = $(this).find('td:eq(9)').find('input').val(),
                        loan_date = $(this).find('td:eq(13)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if(listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === ''))
                            $(this).find('td:eq(0)').find('span').removeClass('d-none');
                        else
                           $(this).find('td:eq(0)').find('span').addClass('d-none');
                    }
                });

                let flag = 2;

                // GET SELECTED ROW
                $('#consumable_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                    if(listed_disabled != 'disabled'){
                        if(listed == 1){
                            flag = 2;

                            return false;
                        } else{
                            flag = 0;
                        }
                    }
                });

                // GET SELECTED ROW DATA
                $('#consumable_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(6)').find('input').val(),
                        party = $(this).find('td:eq(7)').find('select').val(),
                        gate_no = $(this).find('td:eq(8)').find('input').val(),
                        challan_no = $(this).find('td:eq(9)').find('input').val(),
                        loan_date = $(this).find('td:eq(13)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if(listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === '')){
                            flag = 1;

                            return false;
                        }
                    }
                });

                if(flag == 0){
                    Swal.fire({
                        title: 'Error',
                        text: 'Please select a row!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else if(flag == 1){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    let table_data = '';

                    let table_object = $('#consumable_table tbody tr').map(function(i){
                        let row = '',
                            listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                            listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                let challan_photo = $(this).find('td:eq(10)').find('input').prop('files')[0],
                                    bill_photo = $(this).find('td:eq(11)').find('input').prop('files')[0],
                                    challan_file_data = '',
                                    bill_file_data = '';

                                if(challan_photo != undefined)
                                    challan_file_data = challan_photo.name;

                                if(bill_photo != undefined)
                                    bill_file_data = bill_photo.name;

                                row = {
                                    'listed': listed,
                                    'required_for': $(this).find('td:eq(2)').html(),
                                    'parts': $(this).find('td:eq(3)').html(),
                                    'qty': $(this).find('td:eq(4)').find('input').val(),
                                    'loan': $(this).find('td:eq(4)').find('.borrowed').find('strong:eq(0)').html(),
                                    'req': $(this).find('td:eq(4)').find('.borrowed').find('strong:eq(1)').html(),
                                    'use': $(this).find('td:eq(5)').html(),
                                    'price': $(this).find('td:eq(6)').find('input').val(),
                                    'party': $(this).find('td:eq(7)').find('select').val(),
                                    'gate_no': $(this).find('td:eq(8)').find('input').val(),
                                    'challan_no': $(this).find('td:eq(9)').find('input').val(),
                                    'challan_photo': challan_file_data,
                                    'bill_photo': bill_file_data,
                                    'remarks': $(this).find('td:eq(12)').html(),
                                    'loan_date': $(this).find('td:eq(13)').find('input').val()
                                };
                            } else{
                                return;
                            }
                        } else{
                            return;
                        }
                        
                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                table_data += '<tr>';
                                    table_data += '<td>' + (i+1) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(2)').html() + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(3)').html() + '</td>';
                                    table_data += '<td>' + parseFloat(row.qty).toFixed(3) + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + parseFloat($(this).find('td:eq(4)').find('.in-stock').html().replace(/[^0-9.-]+/g, '')).toFixed(3) + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + row.use + '</td>';
                                    table_data += '<td><i class="mdi mdi-currency-bdt"></i>' + parseFloat(row.price).toFixed(2) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(7)').find('.party-name option:selected').html() + '</td>';
                                    table_data += '<td>' + row.gate_no + '</td>';
                                    table_data += '<td>' + row.challan_no + '</td>';
                                    table_data += '<td>' + row.remarks + '</td>';
                                    table_data += '<td>' + row.loan_date + '</td>';
                                table_data += '</tr>';
                            }
                        }

                        return row;
                    }).get();

                    let table = '<table class="table table-responsive table-striped table-bordered consumable-table">';
                        table += '<thead>';
                            table += '<tr>';
                                table += '<th>SL.</td>';
                                table += '<th>Required For</td>';
                                table += '<th>Parts</td>';
                                table += '<th>Quantity</td>';
                                table += '<th>In Stock</td>';
                                table += '<th>Where to Use</td>';
                                table += '<th>Price</td>';
                                table += '<th>Party</td>';
                                table += '<th>Gate No.</td>';
                                table += '<th>Challan No.</td>';
                                table += '<th>Remarks</td>';
                                table += '<th>Loan Date</td>';
                            table += '</tr>';
                        table += '</thead>';
                        table += '<tbody id="consumable_data">';
                            table += table_data;
                        table += '</tbody>';
                    table += '</table>';

                    let t;

                    Swal.fire({
                        title: 'Proceeding',
                        text: 'Please wait...',
                        timer: 100,
                        allowOutsideClick: false,
                        onBeforeOpen: function(){
                            Swal.showLoading(), t = setInterval(function(){
                            }, 100);
                        }
                    }).then(function(){
                        Swal.fire({
                            title: 'Loan Data',
                            html: table,
                            type: 'info',
                            width: 'auto',
                            showCancelButton: true,
                            showCloseButton: true,
                            confirmButtonColor: '#5cb85c',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Submit'
                        }).then(function(result){
                            if(result.value){
                                let loan_table_data = JSON.stringify(table_object);
                                
                                let requisition_id = 0;
                                if(requisition_data)
                                    requisition_id = requisition_data.Reply[0].requisition_id;

                                let flag = 1,
                                    msg = '';

                                // GET SELECTED ROWS IMAGE DATA
                                $('#consumable_table tbody tr').each(function(){
                                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                                    if(listed_disabled != 'disabled'){
                                        if(listed == 1){
                                            let challan_photo = $(this).find('td:eq(10)').find('input').prop('files')[0],
                                                bill_photo = $(this).find('td:eq(11)').find('input').prop('files')[0];
                                                
                                            if(challan_photo != undefined){
                                                let form_data = new FormData();
                                                form_data.append('file', challan_photo);

                                                $.ajax({
                                                    url: '../../api/uploadImage',
                                                    method: 'post',
                                                    data: form_data,
                                                    dataType: 'json',
                                                    contentType : false,
                                                    processData: false,
                                                    cache: false,
                                                    async: false,
                                                    success: function(data){
                                                        if(data.Type === 'error'){
                                                            flag = 0;
                                                            msg = data.Reply;
                                                        }
                                                    }
                                                });

                                                if(flag == 0)
                                                    return false;
                                            }

                                            if(bill_photo != undefined){
                                                let form_data = new FormData();
                                                form_data.append('file', bill_photo);

                                                $.ajax({
                                                    url: '../../api/uploadImage',
                                                    method: 'post',
                                                    data: form_data,
                                                    dataType: 'json',
                                                    contentType : false,
                                                    processData: false,
                                                    cache: false,
                                                    async: false,
                                                    success: function(data){
                                                        if(data.Type === 'error'){
                                                            flag = 0;
                                                            msg = data.Reply;
                                                        }
                                                    }
                                                });

                                                if(flag == 0)
                                                    return false;
                                            }
                                        }
                                    }
                                });

                                if(flag == 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: msg,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Please insert valid image.'
                                    });
                                } else{
                                    let borrowed_parts = 0;

                                    $.each(table_object, function(i, obj_data){
                                        if((parseFloat(obj_data.qty) + parseFloat(obj_data.loan)) >= parseFloat(obj_data.req))
                                            borrowed_parts++;
                                    });

                                    $.ajax({
                                        url: '../../api/interactionController',
                                        method: 'post',
                                        data: {
                                            interact_type: 'add',
                                            interact: 'loan_con',
                                            requisition_id: requisition_id,
                                            requisitioned_parts: $('#consumable_table tbody tr').length,
                                            borrowed_parts: borrowed_parts,
                                            loan_data: loan_table_data
                                        },
                                        dataType: 'json',
                                        cache: false,
                                        success: function(data){
                                            if(data.Type == 'success'){
                                                let t;

                                                Swal.fire({
                                                    title: 'Adding Loan Data',
                                                    text: 'Please wait...',
                                                    timer: 100,
                                                    allowOutsideClick: false,
                                                    onBeforeOpen: function(){
                                                        Swal.showLoading(), t = setInterval(function(){
                                                        }, 100);
                                                    }
                                                }).then(function(){
                                                    Swal.fire({
                                                        title: 'Success',
                                                        text: data.Reply,
                                                        type: 'success',
                                                        width: 450,
                                                        showCloseButton: false,
                                                        allowOutsideClick: false,
                                                        confirmButtonColor: '#5cb85c',
                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                    }).then((result) => {
                                                        if(result.value){
                                                            // window.location.reload(true);

                                                            $('#third_body').load(' #third_body > *');
                                                            $('.full-width-modal').modal('hide');
                                                        }
                                                    });
                                                });
                                            } else if(data.Type == 'error'){
                                                Swal.fire({
                                                    title: 'Error',
                                                    text: data.Reply,
                                                    type: 'error',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                    footer: 'Please insert valid data.'
                                                });
                                            } else{
                                                Swal.fire({
                                                    title: 'Info',
                                                    text: 'Server is under maintenance. Please try again later!',
                                                    type: 'info',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                });
                                            }

                                            return false;
                                          },
                                    });
                                }
                            }
                        });
                    });
                }
            }

            let requisition_data2 = '';

            // SPARE LOAN
            function loan_spr(ele){
                $('#first').removeClass('active');
                $('#second').addClass('fade active show');

                let t;

                Swal.fire({
                    title: 'Fetching Loan Data',
                    text: 'Please wait...',
                    timer: 100,
                    allowOutsideClick: false,
                    onBeforeOpen: function(){
                        Swal.showLoading(), t = setInterval(function(){
                        }, 100);
                    }
                }).then(function(){
                    loan_date = '';

                    let requisition_id = ele;

                    $.ajax({
                        url: '../../api/loan',
                        method: 'post',
                        data: {
                            loan_data_type: 'fetch_requisition_spr',
                            requisition_id: requisition_id
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                requisition_data2 = data;
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let parts_id_arr = [],
                        parts_name_arr = [];

                    $.each(requisition_data2.Reply, function(i, requisitioned_parts_id){
                        parts_id_arr.push(requisitioned_parts_id.parts_id);
                    });

                    $.ajax({
                        url: '../../api/parts',
                        method: 'post',
                        data: {
                            parts_data_type: 'fetch_all'
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                $.each(data.Reply, function(i, parts_item){
                                    if(parts_item.parts_category <= 1 && jQuery.inArray(parts_item.parts_id, parts_id_arr) !== -1){
                                        for(let j=0; j<parts_id_arr.length; j++){
                                            if(parts_id_arr[j] == parts_item.parts_id)
                                                parts_name_arr.push(parts_item.parts_name);
                                        }
                                    }
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let party_name_option = '';
                    $.ajax({
                        url: '../../api/party',
                        method: 'post',
                        data: {
                            party_data_type: 'fetch_all'
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                $.each(data.Reply, function(i, party_item){
                                    party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'">'+party_item.party_name+'</option>';
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let trHTML = '';

                    $.each(requisition_data2.Reply, function(i, requisition_item){
                        trHTML += '<tr '+ ((requisition_item.parts_id == requisition_item.borrowed_parts_id && parseFloat(requisition_item.borrowed_parts_qty) >= parseFloat(requisition_item.r_qty)) ? 'style=" background: #cfe8cf;"' : '') +'>';
                            trHTML += '<td class="align-middle">' + (i+1) + ' <span class="text-danger d-none">*</span></td>';

                            let checked = '',
                                disabled = '';

                            if(requisition_item.parts_id == requisition_item.borrowed_parts_id && parseFloat(requisition_item.borrowed_parts_qty) >= parseFloat(requisition_item.r_qty)){
                                checked = 'checked';
                                disabled = 'disabled';
                            }
                            
                            trHTML += '<td class="align-middle"><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-to-list-2" id="add_to_list_2'+i+'" onclick="add_to_list_2()" '+ checked + ' ' + disabled +'><label class="custom-control-label" for="add_to_list_2'+i+'"></label></div></td>';

                            if(requisition_item.required_for == 1)
                                required_for = 'BCP-CCM';
                            else if(requisition_item.required_for == 2)
                                required_for = 'BCP-Furnace';
                            else if(requisition_item.required_for == 3)
                                required_for = 'Concast-CCM';
                            else if(requisition_item.required_for == 4)
                                required_for = 'Concast-Furnace';
                            else if(requisition_item.required_for == 5)
                                required_for = 'HRM';
                            else if(requisition_item.required_for == 6)
                                required_for = 'HRM Unit-2';
                            else if(requisition_item.required_for == 7)
                                required_for = 'Lal Masjid';
                            else if(requisition_item.required_for == 8)
                                required_for = 'Sonargaon';
                            else if(requisition_item.required_for == 9)
                                required_for = 'General';

                            trHTML += '<td class="text-center align-middle">' + required_for + '</td>';
                            trHTML += '<td class="text-center align-middle">' + parts_name_arr[i] + '</td>';

                            trHTML += '<td class="align-middle text-center">';
                                trHTML += '<div class="input-group">';
                                    trHTML += '<input type="number" class="form-control qty-2" placeholder="Insert" oninput="qty(2, '+ (requisition_item.r_qty - requisition_item.borrowed_parts_qty) +')" '+ disabled +'>';
                                    trHTML += '<div class="input-group-prepend">';
                                        let parts_unit = '';

                                        if(requisition_item.parts_unit == 1)
                                            parts_unit = 'Bag';
                                        else if(requisition_item.parts_unit == 2)
                                            parts_unit = 'Box';
                                        else if(requisition_item.parts_unit == 3)
                                            parts_unit = 'Box/Pcs';
                                        else if(requisition_item.parts_unit == 4)
                                            parts_unit = 'Bun';
                                        else if(requisition_item.parts_unit == 5)
                                            parts_unit = 'Bundle';
                                        else if(requisition_item.parts_unit == 6)
                                            parts_unit = 'Can';
                                        else if(requisition_item.parts_unit == 7)
                                            parts_unit = 'Cartoon';
                                        else if(requisition_item.parts_unit == 8)
                                            parts_unit = 'Challan';
                                        else if(requisition_item.parts_unit == 9)
                                            parts_unit = 'Coil';
                                        else if(requisition_item.parts_unit == 10)
                                            parts_unit = 'Drum';
                                        else if(requisition_item.parts_unit == 11)
                                            parts_unit = 'Feet';
                                        else if(requisition_item.parts_unit == 12)
                                            parts_unit = 'Gallon';
                                        else if(requisition_item.parts_unit == 13)
                                            parts_unit = 'Item';
                                        else if(requisition_item.parts_unit == 14)
                                            parts_unit = 'Job';
                                        else if(requisition_item.parts_unit == 15)
                                            parts_unit = 'Kg';
                                        else if(requisition_item.parts_unit == 16)
                                            parts_unit = 'Kg/Bundle';
                                        else if(requisition_item.parts_unit == 17)
                                            parts_unit = 'Kv';
                                        else if(requisition_item.parts_unit == 18)
                                            parts_unit = 'Lbs';
                                        else if(requisition_item.parts_unit == 19)
                                            parts_unit = 'Ltr';
                                        else if(requisition_item.parts_unit == 20)
                                            parts_unit = 'Mtr';
                                        else if(requisition_item.parts_unit == 21)
                                            parts_unit = 'Pack';
                                        else if(requisition_item.parts_unit == 22)
                                            parts_unit = 'Pack/Pcs';
                                        else if(requisition_item.parts_unit == 23)
                                            parts_unit = 'Pair';
                                        else if(requisition_item.parts_unit == 24)
                                            parts_unit = 'Pcs';
                                        else if(requisition_item.parts_unit == 25)
                                            parts_unit = 'Pound';
                                        else if(requisition_item.parts_unit == 26)
                                            parts_unit = 'Qty';
                                        else if(requisition_item.parts_unit == 27)
                                            parts_unit = 'Roll';
                                        else if(requisition_item.parts_unit == 28)
                                            parts_unit = 'Set';
                                        else if(requisition_item.parts_unit == 29)
                                            parts_unit = 'Truck';
                                        else if(requisition_item.parts_unit == 30)
                                            parts_unit = 'Unit';
                                        else if(requisition_item.parts_unit == 31)
                                            parts_unit = 'Yeard';
                                        else if(requisition_item.parts_unit == 32)
                                            parts_unit = '(Unit Unknown)';
                                        else if(requisition_item.parts_unit == 33)
                                            parts_unit = 'SFT';
                                        else if(requisition_item.parts_unit == 34)
                                            parts_unit = 'RFT';
                                        else if(requisition_item.parts_unit == 35)
                                            parts_unit = 'CFT';

                                        trHTML += '<div class="input-group-text"><span id="unit_2">' + parts_unit + '</span></div>';
                                    trHTML += '</div>';
                                trHTML += '</div>';

                                trHTML += '<span class="float-left mt-1 text-primary w-100 in-stock-2">In Stock: <strong>' + requisition_item.i_qty + '</strong> ' + parts_unit + '</span><span class="float-left text-info w-100 borrowed-2">Borrowed: <strong>' + requisition_item.borrowed_parts_qty + '</strong> of <strong>' + requisition_item.r_qty + '</strong> ' + parts_unit + '</span>';
                            trHTML += '</td>';

                            trHTML += '<td class="text-center align-middle">' + requisition_item.old_spare_details + '</td>';
                            trHTML += '<td class="text-center align-middle">' + ((requisition_item.status == 1) ? 'Repairable' : 'Unusual') + '</td>';
                            trHTML += '<td class="align-middle"><input type="number" class="form-control price-2" placeholder="Insert" oninput="price(2)" '+ disabled +'></td>';
                                
                            trHTML += '<td class="align-middle">';
                                trHTML += '<select class="select-b party-name-2" onchange="party_name(this.value, 2, ' + (i+1) + ')" '+ disabled +'>';
                                    trHTML += '<option value="">Choose</option>';
                                    trHTML += party_name_option;
                                trHTML += '</select>';
                                trHTML += '<span class="float-left mt-1 text-primary remarks-2"></span>';
                            trHTML += '</td>';

                            trHTML += '<td class="align-middle"><input type="text" class="form-control gate-no-2" placeholder="Insert" '+ disabled +'></td>';
                            trHTML += '<td class="align-middle"><input type="text" class="form-control challan-no-2" placeholder="Insert" '+ disabled +'></td>';
                            trHTML += '<td class="align-middle"><input type="file" class="form-control challan-photo-2" accept="image/*" '+ disabled +'></td>';
                            trHTML += '<td class="align-middle"><input type="file" class="form-control bill-photo-2" accept="image/*" '+ disabled +'></td>';
                            trHTML += '<td class="text-center align-middle">' + requisition_item.remarks + '</td>';
                            trHTML += '<td class="align-middle"><input type="text" class="form-control loan-date-2" style="width: 150px;" oninput="loan_date(2, '+ (i+1) +')" data-id="'+ requisition_item.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" '+ disabled +'></td>';
                        trHTML += '</tr>';
                            
                        $('#spare_table').find('table tbody').empty();
                        $('#spare_table').find('table').append(trHTML);

                        $('.select-b').select2({
                            width: '100%',
                            placeholder: 'Choose'
                        });
                    });
                });
            }

            // PROCEED SPARE
            function proceed_spare(){
                $('#spare_table tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(7)').find('input').val(),
                        party = $(this).find('td:eq(8)').find('select').val(),
                        gate_no = $(this).find('td:eq(9)').find('input').val(),
                        challan_no = $(this).find('td:eq(10)').find('input').val(),
                        loan_date = $(this).find('td:eq(14)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if(listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === ''))
                            $(this).find('td:eq(0)').find('span').removeClass('d-none');
                        else
                           $(this).find('td:eq(0)').find('span').addClass('d-none');
                    }
                });

                let flag = 2;

                // GET SELECTED ROW
                $('#spare_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                    if(listed_disabled != 'disabled'){
                        if(listed == 1){
                            flag = 2;

                            return false;
                        } else{
                            flag = 0;
                        }
                    }
                });

                // GET SELECTED ROW DATA
                $('#spare_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(7)').find('input').val(),
                        party = $(this).find('td:eq(8)').find('select').val(),
                        gate_no = $(this).find('td:eq(9)').find('input').val(),
                        challan_no = $(this).find('td:eq(10)').find('input').val(),
                        loan_date = $(this).find('td:eq(14)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if(listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === '')){
                            flag = 1;

                            return false;
                        }
                    }
                });

                if(flag == 0){
                    Swal.fire({
                        title: 'Error',
                        text: 'Please select a row!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else if(flag == 1){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    let table_data = '';

                    let table_object = $('#spare_table tbody tr').map(function(i){
                        let row = '',
                            listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                            listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                let challan_photo = $(this).find('td:eq(11)').find('input').prop('files')[0],
                                    bill_photo = $(this).find('td:eq(12)').find('input').prop('files')[0],
                                    challan_file_data = '',
                                    bill_file_data = '';

                                if(challan_photo != undefined)
                                    challan_file_data = challan_photo.name;

                                if(bill_photo != undefined)
                                    bill_file_data = bill_photo.name;

                                row = {
                                    'listed': listed,
                                    'required_for': $(this).find('td:eq(2)').html(),
                                    'parts': $(this).find('td:eq(3)').html(),
                                    'qty': $(this).find('td:eq(4)').find('input').val(),
                                    'loan': $(this).find('td:eq(4)').find('.borrowed-2').find('strong:eq(0)').html(),
                                    'req': $(this).find('td:eq(4)').find('.borrowed-2').find('strong:eq(1)').html(),
                                    'old': $(this).find('td:eq(5)').html(),
                                    'status': $(this).find('td:eq(6)').html(),
                                    'price': $(this).find('td:eq(7)').find('input').val(),
                                    'party': $(this).find('td:eq(8)').find('select').val(),
                                    'gate_no': $(this).find('td:eq(9)').find('input').val(),
                                    'challan_no': $(this).find('td:eq(10)').find('input').val(),
                                    'challan_photo': challan_file_data,
                                    'bill_photo': bill_file_data,
                                    'remarks': $(this).find('td:eq(13)').html(),
                                    'loan_date': $(this).find('td:eq(14)').find('input').val()
                                };
                            } else{
                                return;
                            }
                        } else{
                            return;
                        }
                        
                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                table_data += '<tr>';
                                    table_data += '<td>' + (i+1) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(2)').html() + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(3)').html() + '</td>';
                                    table_data += '<td>' + parseFloat(row.qty).toFixed(3) + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + parseFloat($(this).find('td:eq(4)').find('.in-stock-2').html().replace(/[^0-9.-]+/g, '')).toFixed(3) + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + row.old + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(6)').html() + '</td>';
                                    table_data += '<td><i class="mdi mdi-currency-bdt"></i>' + parseFloat(row.price).toFixed(2) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(8)').find('.party-name-2 option:selected').html() + '</td>';
                                    table_data += '<td>' + row.gate_no + '</td>';
                                    table_data += '<td>' + row.challan_no + '</td>';
                                    table_data += '<td>' + row.remarks + '</td>';
                                    table_data += '<td>' + row.loan_date + '</td>';
                                table_data += '</tr>';
                            }
                        }

                        return row;
                    }).get();

                    let table = '<table class="table table-responsive table-striped table-bordered spare-table">';
                        table += '<thead>';
                            table += '<tr>';
                                table += '<th>SL.</td>';
                                table += '<th>Required For</td>';
                                table += '<th>Parts</td>';
                                table += '<th>Quantity</td>';
                                table += '<th>In Stock</td>';
                                table += '<th>Old Spares Details</td>';
                                table += '<th>Status</td>';
                                table += '<th>Price</td>';
                                table += '<th>Party</td>';
                                table += '<th>Gate No.</td>';
                                table += '<th>Challan No.</td>';
                                table += '<th>Remarks</td>';
                                table += '<th>Loan Date</td>';
                            table += '</tr>';
                        table += '</thead>';
                        table += '<tbody id="spare_data">';
                            table += table_data;
                        table += '</tbody>';
                    table += '</table>';

                    let t;

                    Swal.fire({
                        title: 'Proceeding',
                        text: 'Please wait...',
                        timer: 100,
                        allowOutsideClick: false,
                        onBeforeOpen: function(){
                            Swal.showLoading(), t = setInterval(function(){
                            }, 100);
                        }
                    }).then(function(){
                        Swal.fire({
                            title: 'Loan Data',
                            html: table,
                            type: 'info',
                            width: 'auto',
                            showCancelButton: true,
                            showCloseButton: true,
                            confirmButtonColor: '#5cb85c',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Submit'
                        }).then(function(result){
                            if(result.value){
                                let loan_table_data = JSON.stringify(table_object);
                                
                                let requisition_id = 0;
                                if(requisition_data2)
                                    requisition_id = requisition_data2.Reply[0].requisition_id;

                                let flag = 1,
                                    msg = '';

                                // GET SELECTED ROWS IMAGE DATA
                                $('#spare_table tbody tr').each(function(){
                                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                                    if(listed_disabled != 'disabled'){
                                        if(listed == 1){
                                            let challan_photo = $(this).find('td:eq(11)').find('input').prop('files')[0],
                                                bill_photo = $(this).find('td:eq(12)').find('input').prop('files')[0];
                                                
                                            if(challan_photo != undefined){
                                                let form_data = new FormData();
                                                form_data.append('file', challan_photo);

                                                $.ajax({
                                                    url: '../../api/uploadImage',
                                                    method: 'post',
                                                    data: form_data,
                                                    dataType: 'json',
                                                    contentType : false,
                                                    processData: false,
                                                    cache: false,
                                                    async: false,
                                                    success: function(data){
                                                        if(data.Type === 'error'){
                                                            flag = 0;
                                                            msg = data.Reply;
                                                        }
                                                    }
                                                });

                                                if(flag == 0)
                                                    return false;
                                            }

                                            if(bill_photo != undefined){
                                                let form_data = new FormData();
                                                form_data.append('file', bill_photo);

                                                $.ajax({
                                                    url: '../../api/uploadImage',
                                                    method: 'post',
                                                    data: form_data,
                                                    dataType: 'json',
                                                    contentType : false,
                                                    processData: false,
                                                    cache: false,
                                                    async: false,
                                                    success: function(data){
                                                        if(data.Type === 'error'){
                                                            flag = 0;
                                                            msg = data.Reply;
                                                        }
                                                    }
                                                });

                                                if(flag == 0)
                                                    return false;
                                            }
                                        }
                                    }
                                });

                                if(flag == 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: msg,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Please insert valid image.'
                                    });
                                } else{
                                    let borrowed_parts = 0;

                                    $.each(table_object, function(i, obj_data){
                                        if((parseFloat(obj_data.qty) + parseFloat(obj_data.loan)) >= parseFloat(obj_data.req))
                                            borrowed_parts++;
                                    });

                                    $.ajax({
                                        url: '../../api/interactionController',
                                        method: 'post',
                                        data: {
                                            interact_type: 'add',
                                            interact: 'loan_spr',
                                            requisition_id: requisition_id,
                                            requisitioned_parts: $('#spare_table tbody tr').length,
                                            borrowed_parts: borrowed_parts,
                                            loan_data: loan_table_data
                                        },
                                        dataType: 'json',
                                        cache: false,
                                        success: function(data){
                                            if(data.Type == 'success'){
                                                let t;

                                                Swal.fire({
                                                    title: 'Adding Loan Data',
                                                    text: 'Please wait...',
                                                    timer: 100,
                                                    allowOutsideClick: false,
                                                    onBeforeOpen: function(){
                                                        Swal.showLoading(), t = setInterval(function(){
                                                        }, 100);
                                                    }
                                                }).then(function(){
                                                    Swal.fire({
                                                        title: 'Success',
                                                        text: data.Reply,
                                                        type: 'success',
                                                        width: 450,
                                                        showCloseButton: false,
                                                        allowOutsideClick: false,
                                                        confirmButtonColor: '#5cb85c',
                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                    }).then((result) => {
                                                        if(result.value){
                                                            // window.location.reload(true);

                                                            $('#forth_body').load(' #forth_body > *');
                                                            $('.full-width-modal').modal('hide');
                                                        }
                                                    });
                                                });
                                            } else if(data.Type == 'error'){
                                                Swal.fire({
                                                    title: 'Error',
                                                    text: data.Reply,
                                                    type: 'error',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                    footer: 'Please insert valid data.'
                                                });
                                            } else{
                                                Swal.fire({
                                                    title: 'Info',
                                                    text: 'Server is under maintenance. Please try again later!',
                                                    type: 'info',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                });
                                            }

                                            return false;
                                          },
                                    });
                                }
                            }
                        });
                    });
                }
            }

            // VIEW CONSUMABLE LOAN
            function view_loan_con(ele){
                let requisition_id = ele;

                $.ajax({
                    url: '../../api/loan',
                    method: 'post',
                    data: {
                        loan_data_type: 'fetch_loan_con',
                        requisition_id: requisition_id
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Loan Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            }).then(function(){
                                let trHTML = '',
                                    required_for = '',
                                    parts_unit = '';

                                $.each(data.Reply1, function(i, loan_item_1){
                                    if(loan_item_1.required_for == 1)
                                        required_for = 'BCP-CCM';
                                    else if(loan_item_1.required_for == 2)
                                        required_for = 'BCP-Furnace';
                                    else if(loan_item_1.required_for == 3)
                                        required_for = 'Concast-CCM';
                                    else if(loan_item_1.required_for == 4)
                                        required_for = 'Concast-Furnace';
                                    else if(loan_item_1.required_for == 5)
                                        required_for = 'HRM';
                                    else if(loan_item_1.required_for == 6)
                                        required_for = 'HRM Unit-2';
                                    else if(loan_item_1.required_for == 7)
                                        required_for = 'Lal Masjid';
                                    else if(loan_item_1.required_for == 8)
                                        required_for = 'Sonargaon';
                                    else if(loan_item_1.required_for == 9)
                                        required_for = 'General';

                                    if(loan_item_1.parts_unit == 1)
                                        parts_unit = 'Bag';
                                    else if(loan_item_1.parts_unit == 2)
                                        parts_unit = 'Box';
                                    else if(loan_item_1.parts_unit == 3)
                                        parts_unit = 'Box/Pcs';
                                    else if(loan_item_1.parts_unit == 4)
                                        parts_unit = 'Bun';
                                    else if(loan_item_1.parts_unit == 5)
                                        parts_unit = 'Bundle';
                                    else if(loan_item_1.parts_unit == 6)
                                        parts_unit = 'Can';
                                    else if(loan_item_1.parts_unit == 7)
                                        parts_unit = 'Cartoon';
                                    else if(loan_item_1.parts_unit == 8)
                                        parts_unit = 'Challan';
                                    else if(loan_item_1.parts_unit == 9)
                                        parts_unit = 'Coil';
                                    else if(loan_item_1.parts_unit == 10)
                                        parts_unit = 'Drum';
                                    else if(loan_item_1.parts_unit == 11)
                                        parts_unit = 'Feet';
                                    else if(loan_item_1.parts_unit == 12)
                                        parts_unit = 'Gallon';
                                    else if(loan_item_1.parts_unit == 13)
                                        parts_unit = 'Item';
                                    else if(loan_item_1.parts_unit == 14)
                                        parts_unit = 'Job';
                                    else if(loan_item_1.parts_unit == 15)
                                        parts_unit = 'Kg';
                                    else if(loan_item_1.parts_unit == 16)
                                        parts_unit = 'Kg/Bundle';
                                    else if(loan_item_1.parts_unit == 17)
                                        parts_unit = 'Kv';
                                    else if(loan_item_1.parts_unit == 18)
                                        parts_unit = 'Lbs';
                                    else if(loan_item_1.parts_unit == 19)
                                        parts_unit = 'Ltr';
                                    else if(loan_item_1.parts_unit == 20)
                                        parts_unit = 'Mtr';
                                    else if(loan_item_1.parts_unit == 21)
                                        parts_unit = 'Pack';
                                    else if(loan_item_1.parts_unit == 22)
                                        parts_unit = 'Pack/Pcs';
                                    else if(loan_item_1.parts_unit == 23)
                                        parts_unit = 'Pair';
                                    else if(loan_item_1.parts_unit == 24)
                                        parts_unit = 'Pcs';
                                    else if(loan_item_1.parts_unit == 25)
                                        parts_unit = 'Pound';
                                    else if(loan_item_1.parts_unit == 26)
                                        parts_unit = 'Qty';
                                    else if(loan_item_1.parts_unit == 27)
                                        parts_unit = 'Roll';
                                    else if(loan_item_1.parts_unit == 28)
                                        parts_unit = 'Set';
                                    else if(loan_item_1.parts_unit == 29)
                                        parts_unit = 'Truck';
                                    else if(loan_item_1.parts_unit == 30)
                                        parts_unit = 'Unit';
                                    else if(loan_item_1.parts_unit == 31)
                                        parts_unit = 'Yeard';
                                    else if(loan_item_1.parts_unit == 32)
                                        parts_unit = '(Unit Unknown)';
                                    else if(loan_item_1.parts_unit == 33)
                                        parts_unit = 'SFT';
                                    else if(loan_item_1.parts_unit == 34)
                                        parts_unit = 'RFT';
                                    else if(loan_item_1.parts_unit == 35)
                                        parts_unit = 'CFT';

                                    trHTML += '<tr>';
                                        trHTML += '<td class="text-left alert-info" colspan="10">\
                                                        <i class="mdi mdi-drag-variant"></i> Required For: ' + required_for + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Parts Name: ' + loan_item_1.parts_name + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Old Spares Details:  ' + loan_item_1.parts_usage + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Remarks: ' + loan_item_1.remarks + '\
                                                        \
                                                        <span class="float-right">(' + loan_item_1.borrowed_parts_qty + '/' + loan_item_1.requisitioned_parts_qty + ' Borrowed)</span>\
                                                        <div class="progress mt-1 mr-1 progress-sm w-25 float-right" style="background: #fff;">\
                                                            <div class="progress-bar bg-primary" role="progressbar" style="width: ' + ((loan_item_1.borrowed_parts_qty / loan_item_1.requisitioned_parts_qty) * 100) + '%;" aria-valuenow="' + ((loan_item_1.borrowed_parts_qty / loan_item_1.requisitioned_parts_qty) * 100) + '" aria-valuemin="0" aria-valuemax="100"></div>\
                                                        </div>\
                                                        \
                                                    </td>';
                                    trHTML += '</tr>';

                                    $.each(data.Reply2, function(j, loan_item_2){
                                        if(loan_item_2.parts_id == loan_item_1.parts_id){
                                            trHTML += '<tr>';
                                                trHTML += '<td class="align-middle text-center">' + (j+1) + '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.parts_qty + ' ' + parts_unit + '</span>';

                                                    trHTML += '<div class="input-group data-input d-none">';
                                                        trHTML += '<input type="number" class="form-control" placeholder="Insert" value="'+loan_item_2.parts_qty+'" oninput="qty_2(this, ' + loan_item_2.parts_qty + ', ' + loan_item_1.requisitioned_parts_qty + ', ' + loan_item_1.loan_indx_f + ', ' + loan_item_1.loan_indx_l + ')">';
                                                        trHTML += '<div class="input-group-prepend">';
                                                            trHTML += '<div class="input-group-text">' + parts_unit + '</div>';
                                                        trHTML += '</div>';
                                                    trHTML += '</div>';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span"><i class="mdi mdi-currency-bdt"></i>' + loan_item_2.price + '</span>';

                                                    trHTML += '<input type="number" class="form-control data-input d-none" placeholder="Insert" value="'+loan_item_2.price+'" oninput="price_2(this, ' + loan_item_2.price + ')">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.party_name + '</span>';

                                                    trHTML += '<div class="data-input d-none">';
                                                        let party_name_option = '';
                                                        $.ajax({
                                                            url: '../../api/party',
                                                            method: 'post',
                                                            data: {
                                                                party_data_type: 'fetch_all'
                                                            },
                                                            dataType: 'json',
                                                            cache: false,
                                                            async: false,
                                                            success: function(data){
                                                                if(data.Type == 'success'){
                                                                    $.each(data.Reply, function(i, party_item){
                                                                        let selected = (party_item.party_id == loan_item_2.party_id) ? 'selected' : '';

                                                                        party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'" ' + selected + '>'+party_item.party_name+'</option>';
                                                                    });
                                                                } else if(data.Type == 'error'){
                                                                    Swal.fire({
                                                                        title: 'Error',
                                                                        text: data.Reply,
                                                                        type: 'error',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                                        footer: 'Please try again.'
                                                                    });
                                                                } else{
                                                                    Swal.fire({
                                                                        title: 'Info',
                                                                        text: 'Server is under maintenance. Please try again later!',
                                                                        type: 'info',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                                    });
                                                                }

                                                                return false;
                                                            }
                                                        });

                                                        trHTML += '<select class="select-b" onchange="party_name_2(this)">';
                                                            trHTML += '<option value="">Choose</option>';
                                                            trHTML += party_name_option;
                                                        trHTML += '</select>';
                                                        trHTML += '<span class="float-left mt-1 text-primary remarks"></span>';
                                                    trHTML += '</div>';    
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.gate_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+loan_item_2.gate_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.challan_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+loan_item_2.challan_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(loan_item_2.challan_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + loan_item_2.challan_photo + '" target="_blank">' + loan_item_2.challan_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';
                                                
                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(loan_item_2.bill_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + loan_item_2.bill_photo + '" target="_blank">' + loan_item_2.bill_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.loan_date + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" oninput="loan_date_2(this)" data-id="'+ loan_item_2.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" value="'+loan_item_2.loan_date+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<a title="Edit" href="javascript:void(0)" class="btn btn-xs btn-info mr-1 edt-btn" onclick="edit_btn(this)"><i class="mdi mdi-pencil"></i></a>';
                                                    trHTML += '<a title="Cancel" href="javascript:void(0)" class="btn btn-xs btn-warning d-none mr-1 cncl-btn" onclick="cancel_btn(this)"><i class="mdi mdi-cancel"></i></a>';
                                                    trHTML += '<a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none mr-1 upd-btn" onclick="update_loan_data_con(' + loan_item_2.loan_data_id + ', this, ' + loan_item_1.loan_id + ', ' + loan_item_2.parts_id + ', ' + loan_item_1.required_for + ')"><i class="mdi mdi-arrow-up-bold-outline"></i></a>';
                                                    trHTML += '<a title="Delete" href="javascript:void(0)" class="btn btn-xs btn-danger dlt-btn" onclick="delete_loan_data_con(' + loan_item_2.loan_data_id + ', this, ' + loan_item_2.parts_id + ', ' + loan_item_1.required_for + ', ' + loan_item_1.loan_id + ')"><i class="mdi mdi-delete"></i></a>';
                                                trHTML += '</td>';
                                            trHTML += '</tr>';
                                        }
                                    });
                                });

                                $('.loan-records').empty().append(trHTML);

                                $('.select-b').select2({
                                    width: '100%',
                                    placeholder: 'Choose'
                                });
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });
            }

            // UPDATE CONSUMABLE LOAN DATA
            function update_loan_data_con(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    parts_id = ele4,
                    req_for = ele5,
                    qty = $(ele2).closest('tr').find('td:eq(1)').find('input').val(),
                    price = $(ele2).closest('tr').find('td:eq(2)').find('input').val(),
                    party = $(ele2).closest('tr').find('td:eq(3)').find('select').val(),
                    gate_no = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    challan_no = $(ele2).closest('tr').find('td:eq(5)').find('input').val(),
                    challan_photo = $(ele2).closest('tr').find('td:eq(6)').find('input').prop('files')[0],
                    bill_photo = $(ele2).closest('tr').find('td:eq(7)').find('input').prop('files')[0],
                    challan_file_data = '',
                    bill_file_data = '',
                    loan_date = $(ele2).closest('tr').find('td:eq(8)').find('input').val();

                if(qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === ''){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    if(challan_photo != undefined)
                        challan_file_data = challan_photo.name;

                    if(bill_photo != undefined)
                        bill_file_data = bill_photo.name;

                    if(challan_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', challan_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    if(bill_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', bill_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    $.ajax({
                        url: '../../api/interactionController',
                        method: 'post',
                        data: {
                            interact_type: 'update',
                            interact: 'loan_data',
                            id: loan_data_id,
                            loan_id: ele3,
                            parts_id: ele4,
                            req_for: ele5,
                            qty: qty,
                            price: price,
                            party: party,
                            gate_no: gate_no,
                            challan_no: challan_no,
                            challan_photo: challan_file_data,
                            bill_photo: bill_file_data,
                            loan_date: loan_date
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let t;

                                Swal.fire({
                                    title: 'Updating Loan Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    Swal.fire({
                                        title: 'Success',
                                        text: data.Reply,
                                        type: 'success',
                                        width: 450,
                                        showCloseButton: false,
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    }).then((result) => {
                                        if(result.value){
                                            window.location.reload(true);
                                        }
                                    });
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Something went wrong.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });
                }
            }

            function update_loan_data_con_2(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    parts_id = ele4,
                    req_for = ele5,
                    qty = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    price = $(ele2).closest('tr').find('td:eq(5)').find('input').val(),
                    party = $(ele2).closest('tr').find('td:eq(6)').find('select').val(),
                    gate_no = $(ele2).closest('tr').find('td:eq(7)').find('input').val(),
                    challan_no = $(ele2).closest('tr').find('td:eq(8)').find('input').val(),
                    loan_date = $(ele2).closest('tr').find('td:eq(9)').find('input').val();

                if(qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === ''){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    $.ajax({
                        url: '../../api/interactionController',
                        method: 'post',
                        data: {
                            interact_type: 'update',
                            interact: 'loan_data',
                            id: loan_data_id,
                            loan_id: ele3,
                            qty: qty,
                            price: price,
                            party: party,
                            gate_no: gate_no,
                            challan_no: challan_no,
                            challan_photo: '',
                            bill_photo: '',
                            loan_date: loan_date
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let t;

                                Swal.fire({
                                    title: 'Updating Loan Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    Swal.fire({
                                        title: 'Success',
                                        text: data.Reply,
                                        type: 'success',
                                        width: 450,
                                        showCloseButton: false,
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    }).then((result) => {
                                        if(result.value){
                                            window.location.reload(true);
                                        }
                                    });
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Something went wrong.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });
                }
            }

            // DELETE CONSUMABLE LOAN DATA
            function delete_loan_data_con(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    qty = $(ele2).closest('tr').find('td:eq(1)').find('input').val(),
                    loan_date = $(ele2).closest('tr').find('td:eq(8)').find('input').val(),
                    parts_id = ele3,
                    req_for = ele4;

                Swal.fire({
                    title: 'Are you sure you want to delete?',
                    text: 'The loan data will be deleted permanently!',
                    type: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#5cb85c',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class="fas fa-check"></i>&nbsp;&nbsp; Yes',
                    cancelButtonText: '<i class="fas fa-times"></i>&nbsp;&nbsp; No'
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: '../../api/interactionController',
                            method: 'post',
                            data: {
                                interact_type: 'delete',
                                interact: 'loan',
                                id: loan_data_id,
                                loan_id: ele5,
                                parts_id: parts_id,
                                req_for: req_for,
                                qty: qty,
                                loan_date: loan_date
                            },
                            dataType: 'json',
                            cache: false,
                            success: function(data){
                                if(data.Type == 'success'){
                                    let t;

                                    Swal.fire({
                                        title: 'Deleting Loan Data',
                                        text: 'Please wait...',
                                        timer: 100,
                                        allowOutsideClick: false,
                                        onBeforeOpen: function(){
                                            Swal.showLoading(), t = setInterval(function(){
                                            }, 100);
                                        }
                                    }).then(function(){
                                        Swal.fire({
                                            title: 'Success',
                                            text: data.Reply,
                                            type: 'success',
                                            width: 450,
                                            showCloseButton: false,
                                            allowOutsideClick: false,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        }).then((result) => {
                                            if(result.value){
                                                window.location.reload(true);
                                            }
                                        });
                                    });
                                } else if(data.Type == 'error'){
                                    Swal.fire({
                                        title: 'Error',
                                        text: data.Reply,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Something went wrong.'
                                    });
                                } else{
                                    Swal.fire({
                                        title: 'Info',
                                        text: 'Server is under maintenance. Please try again later!',
                                        type: 'info',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    });
                                }

                                return false;
                            }
                        });
                    }
                });
            }

            function delete_loan_data_con_2(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    qty = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    loan_date = $(ele2).closest('tr').find('td:eq(9)').find('input').val(),
                    parts_id = ele3,
                    req_for = ele4;

                Swal.fire({
                    title: 'Are you sure you want to delete?',
                    text: 'The loan data will be deleted permanently!',
                    type: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#5cb85c',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class="fas fa-check"></i>&nbsp;&nbsp; Yes',
                    cancelButtonText: '<i class="fas fa-times"></i>&nbsp;&nbsp; No'
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: '../../api/interactionController',
                            method: 'post',
                            data: {
                                interact_type: 'delete',
                                interact: 'loan',
                                id: loan_data_id,
                                loan_id: ele5,
                                parts_id: parts_id,
                                req_for: req_for,
                                qty: qty,
                                loan_date: loan_date
                            },
                            dataType: 'json',
                            cache: false,
                            success: function(data){
                                if(data.Type == 'success'){
                                    let t;

                                    Swal.fire({
                                        title: 'Deleting Loan Data',
                                        text: 'Please wait...',
                                        timer: 100,
                                        allowOutsideClick: false,
                                        onBeforeOpen: function(){
                                            Swal.showLoading(), t = setInterval(function(){
                                            }, 100);
                                        }
                                    }).then(function(){
                                        Swal.fire({
                                            title: 'Success',
                                            text: data.Reply,
                                            type: 'success',
                                            width: 450,
                                            showCloseButton: false,
                                            allowOutsideClick: false,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        }).then((result) => {
                                            if(result.value){
                                                window.location.reload(true);
                                            }
                                        });
                                    });
                                } else if(data.Type == 'error'){
                                    Swal.fire({
                                        title: 'Error',
                                        text: data.Reply,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Something went wrong.'
                                    });
                                } else{
                                    Swal.fire({
                                        title: 'Info',
                                        text: 'Server is under maintenance. Please try again later!',
                                        type: 'info',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    });
                                }

                                return false;
                            }
                        });
                    }
                });
            }

            // VIEW SPARE LOAN
            function view_loan_spr(ele){
                let requisition_id = ele;

                $.ajax({
                    url: '../../api/loan',
                    method: 'post',
                    data: {
                        loan_data_type: 'fetch_loan_spr',
                        requisition_id: requisition_id
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Loan Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            }).then(function(){
                                let trHTML = '',
                                    required_for = '',
                                    parts_unit = '';

                                $.each(data.Reply1, function(i, loan_item_1){
                                    if(loan_item_1.required_for == 1)
                                        required_for = 'BCP-CCM';
                                    else if(loan_item_1.required_for == 2)
                                        required_for = 'BCP-Furnace';
                                    else if(loan_item_1.required_for == 3)
                                        required_for = 'Concast-CCM';
                                    else if(loan_item_1.required_for == 4)
                                        required_for = 'Concast-Furnace';
                                    else if(loan_item_1.required_for == 5)
                                        required_for = 'HRM';
                                    else if(loan_item_1.required_for == 6)
                                        required_for = 'HRM Unit-2';
                                    else if(loan_item_1.required_for == 7)
                                        required_for = 'Lal Masjid';
                                    else if(loan_item_1.required_for == 8)
                                        required_for = 'Sonargaon';
                                    else if(loan_item_1.required_for == 9)
                                        required_for = 'General';

                                    if(loan_item_1.parts_unit == 1)
                                        parts_unit = 'Bag';
                                    else if(loan_item_1.parts_unit == 2)
                                        parts_unit = 'Box';
                                    else if(loan_item_1.parts_unit == 3)
                                        parts_unit = 'Box/Pcs';
                                    else if(loan_item_1.parts_unit == 4)
                                        parts_unit = 'Bun';
                                    else if(loan_item_1.parts_unit == 5)
                                        parts_unit = 'Bundle';
                                    else if(loan_item_1.parts_unit == 6)
                                        parts_unit = 'Can';
                                    else if(loan_item_1.parts_unit == 7)
                                        parts_unit = 'Cartoon';
                                    else if(loan_item_1.parts_unit == 8)
                                        parts_unit = 'Challan';
                                    else if(loan_item_1.parts_unit == 9)
                                        parts_unit = 'Coil';
                                    else if(loan_item_1.parts_unit == 10)
                                        parts_unit = 'Drum';
                                    else if(loan_item_1.parts_unit == 11)
                                        parts_unit = 'Feet';
                                    else if(loan_item_1.parts_unit == 12)
                                        parts_unit = 'Gallon';
                                    else if(loan_item_1.parts_unit == 13)
                                        parts_unit = 'Item';
                                    else if(loan_item_1.parts_unit == 14)
                                        parts_unit = 'Job';
                                    else if(loan_item_1.parts_unit == 15)
                                        parts_unit = 'Kg';
                                    else if(loan_item_1.parts_unit == 16)
                                        parts_unit = 'Kg/Bundle';
                                    else if(loan_item_1.parts_unit == 17)
                                        parts_unit = 'Kv';
                                    else if(loan_item_1.parts_unit == 18)
                                        parts_unit = 'Lbs';
                                    else if(loan_item_1.parts_unit == 19)
                                        parts_unit = 'Ltr';
                                    else if(loan_item_1.parts_unit == 20)
                                        parts_unit = 'Mtr';
                                    else if(loan_item_1.parts_unit == 21)
                                        parts_unit = 'Pack';
                                    else if(loan_item_1.parts_unit == 22)
                                        parts_unit = 'Pack/Pcs';
                                    else if(loan_item_1.parts_unit == 23)
                                        parts_unit = 'Pair';
                                    else if(loan_item_1.parts_unit == 24)
                                        parts_unit = 'Pcs';
                                    else if(loan_item_1.parts_unit == 25)
                                        parts_unit = 'Pound';
                                    else if(loan_item_1.parts_unit == 26)
                                        parts_unit = 'Qty';
                                    else if(loan_item_1.parts_unit == 27)
                                        parts_unit = 'Roll';
                                    else if(loan_item_1.parts_unit == 28)
                                        parts_unit = 'Set';
                                    else if(loan_item_1.parts_unit == 29)
                                        parts_unit = 'Truck';
                                    else if(loan_item_1.parts_unit == 30)
                                        parts_unit = 'Unit';
                                    else if(loan_item_1.parts_unit == 31)
                                        parts_unit = 'Yeard';
                                    else if(loan_item_1.parts_unit == 32)
                                        parts_unit = '(Unit Unknown)';
                                    else if(loan_item_1.parts_unit == 33)
                                        parts_unit = 'SFT';
                                    else if(loan_item_1.parts_unit == 34)
                                        parts_unit = 'RFT';
                                    else if(loan_item_1.parts_unit == 35)
                                        parts_unit = 'CFT';

                                    trHTML += '<tr>';
                                        trHTML += '<td class="text-left alert-info" colspan="10">\
                                                        <i class="mdi mdi-drag-variant"></i> Required For: ' + required_for + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Parts Name: ' + loan_item_1.parts_name + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Old Spares Details:  ' + loan_item_1.old_spare_details + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Status:  ' + loan_item_1.status + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Remarks: ' + loan_item_1.remarks + '\
                                                        \
                                                        <span class="float-right">(' + loan_item_1.borrowed_parts_qty + '/' + loan_item_1.requisitioned_parts_qty + ' borrowed)</span>\
                                                        <div class="progress mt-1 mr-1 progress-sm w-25 float-right" style="background: #fff;">\
                                                            <div class="progress-bar bg-primary" role="progressbar" style="width: ' + ((loan_item_1.borrowed_parts_qty / loan_item_1.requisitioned_parts_qty) * 100) + '%;" aria-valuenow="' + ((loan_item_1.borrowed_parts_qty / loan_item_1.requisitioned_parts_qty) * 100) + '" aria-valuemin="0" aria-valuemax="100"></div>\
                                                        </div>\
                                                        \
                                                    </td>';
                                    trHTML += '</tr>';

                                    $.each(data.Reply2, function(j, loan_item_2){
                                        if(loan_item_2.parts_id == loan_item_1.parts_id){
                                            trHTML += '<tr>';
                                                trHTML += '<td class="align-middle text-center">' + (j+1) + '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.parts_qty + ' ' + parts_unit + '</span>';

                                                    trHTML += '<div class="input-group data-input d-none">';
                                                        trHTML += '<input type="number" class="form-control" placeholder="Insert" value="'+loan_item_2.parts_qty+'" oninput="qty_2(this, ' + loan_item_2.parts_qty + ', ' + loan_item_1.requisitioned_parts_qty + ', ' + loan_item_1.loan_indx_f + ', ' + loan_item_1.loan_indx_l + ')">';
                                                        trHTML += '<div class="input-group-prepend">';
                                                            trHTML += '<div class="input-group-text">' + parts_unit + '</div>';
                                                        trHTML += '</div>';
                                                    trHTML += '</div>';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span"><i class="mdi mdi-currency-bdt"></i>' + loan_item_2.price + '</span>';

                                                    trHTML += '<input type="number" class="form-control data-input d-none" placeholder="Insert" value="'+loan_item_2.price+'" oninput="price_2(this, ' + loan_item_2.price + ')">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.party_name + '</span>';

                                                    trHTML += '<div class="data-input d-none">';
                                                        let party_name_option = '';
                                                        $.ajax({
                                                            url: '../../api/party',
                                                            method: 'post',
                                                            data: {
                                                                party_data_type: 'fetch_all'
                                                            },
                                                            dataType: 'json',
                                                            cache: false,
                                                            async: false,
                                                            success: function(data){
                                                                if(data.Type == 'success'){
                                                                    $.each(data.Reply, function(i, party_item){
                                                                        let selected = (party_item.party_id == loan_item_2.party_id) ? 'selected' : '';

                                                                        party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'" ' + selected + '>'+party_item.party_name+'</option>';
                                                                    });
                                                                } else if(data.Type == 'error'){
                                                                    Swal.fire({
                                                                        title: 'Error',
                                                                        text: data.Reply,
                                                                        type: 'error',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                                        footer: 'Please try again.'
                                                                    });
                                                                } else{
                                                                    Swal.fire({
                                                                        title: 'Info',
                                                                        text: 'Server is under maintenance. Please try again later!',
                                                                        type: 'info',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                                    });
                                                                }

                                                                return false;
                                                            }
                                                        });

                                                        trHTML += '<select class="select-b" onchange="party_name_2(this)">';
                                                            trHTML += '<option value="">Choose</option>';
                                                            trHTML += party_name_option;
                                                        trHTML += '</select>';
                                                        trHTML += '<span class="float-left mt-1 text-primary remarks"></span>';
                                                    trHTML += '</div>';    
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.gate_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+loan_item_2.gate_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.challan_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+loan_item_2.challan_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(loan_item_2.challan_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + loan_item_2.challan_photo + '" target="_blank">' + loan_item_2.challan_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';
                                                
                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(loan_item_2.bill_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + loan_item_2.bill_photo + '" target="_blank">' + loan_item_2.bill_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + loan_item_2.loan_date + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" oninput="loan_date_2(this)" data-id="'+ loan_item_2.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" value="'+loan_item_2.loan_date+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<a title="Edit" href="javascript:void(0)" class="btn btn-xs btn-info mr-1 edt-btn" onclick="edit_btn(this)"><i class="mdi mdi-pencil"></i></a>';
                                                    trHTML += '<a title="Cancel" href="javascript:void(0)" class="btn btn-xs btn-warning d-none mr-1 cncl-btn" onclick="cancel_btn(this)"><i class="mdi mdi-cancel"></i></a>';
                                                    trHTML += '<a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none mr-1 upd-btn" onclick="update_loan_data_spr(' + loan_item_2.loan_data_id + ', this, ' + loan_item_1.loan_id + ', ' + loan_item_2.parts_id + ', ' + loan_item_1.required_for + ')"><i class="mdi mdi-arrow-up-bold-outline"></i></a>';
                                                    trHTML += '<a title="Delete" href="javascript:void(0)" class="btn btn-xs btn-danger dlt-btn" onclick="delete_loan_data_spr(' + loan_item_2.loan_data_id + ', this, ' + loan_item_2.parts_id + ', ' + loan_item_1.required_for + ', ' + loan_item_1.loan_id + ')"><i class="mdi mdi-delete"></i></a>';
                                                trHTML += '</td>';
                                            trHTML += '</tr>';
                                        }
                                    });
                                });

                                $('.loan-records').empty().append(trHTML);

                                $('.select-b').select2({
                                    width: '100%',
                                    placeholder: 'Choose'
                                });
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });
            }

            // UPDATE SPARE LOAN DATA
            function update_loan_data_spr(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    parts_id = ele4,
                    req_for = ele5,
                    qty = $(ele2).closest('tr').find('td:eq(1)').find('input').val(),
                    price = $(ele2).closest('tr').find('td:eq(2)').find('input').val(),
                    party = $(ele2).closest('tr').find('td:eq(3)').find('select').val(),
                    gate_no = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    challan_no = $(ele2).closest('tr').find('td:eq(5)').find('input').val(),
                    challan_photo = $(ele2).closest('tr').find('td:eq(6)').find('input').prop('files')[0],
                    bill_photo = $(ele2).closest('tr').find('td:eq(7)').find('input').prop('files')[0],
                    challan_file_data = '',
                    bill_file_data = '',
                    loan_date = $(ele2).closest('tr').find('td:eq(8)').find('input').val();

                if(qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === ''){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    if(challan_photo != undefined)
                        challan_file_data = challan_photo.name;

                    if(bill_photo != undefined)
                        bill_file_data = bill_photo.name;

                    if(challan_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', challan_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    if(bill_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', bill_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    $.ajax({
                        url: '../../api/interactionController',
                        method: 'post',
                        data: {
                            interact_type: 'update',
                            interact: 'loan_data2',
                            id: loan_data_id,
                            loan_id: ele3,
                            parts_id: ele4,
                            req_for: ele5,
                            qty: qty,
                            price: price,
                            party: party,
                            gate_no: gate_no,
                            challan_no: challan_no,
                            challan_photo: challan_file_data,
                            bill_photo: bill_file_data,
                            loan_date: loan_date
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let t;

                                Swal.fire({
                                    title: 'Updating Loan Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    Swal.fire({
                                        title: 'Success',
                                        text: data.Reply,
                                        type: 'success',
                                        width: 450,
                                        showCloseButton: false,
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    }).then((result) => {
                                        if(result.value){
                                            window.location.reload(true);
                                        }
                                    });
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Something went wrong.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });
                }
            }

            function update_loan_data_spr_2(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    parts_id = ele4,
                    req_for = ele5,
                    qty = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    price = $(ele2).closest('tr').find('td:eq(5)').find('input').val(),
                    party = $(ele2).closest('tr').find('td:eq(6)').find('select').val(),
                    gate_no = $(ele2).closest('tr').find('td:eq(7)').find('input').val(),
                    challan_no = $(ele2).closest('tr').find('td:eq(8)').find('input').val(),
                    loan_date = $(ele2).closest('tr').find('td:eq(9)').find('input').val();

                if(qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || loan_date === ''){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    $.ajax({
                        url: '../../api/interactionController',
                        method: 'post',
                        data: {
                            interact_type: 'update',
                            interact: 'loan_data2',
                            id: loan_data_id,
                            loan_id: ele3,
                            parts_id: ele4,
                            req_for: ele5,
                            qty: qty,
                            price: price,
                            party: party,
                            gate_no: gate_no,
                            challan_no: challan_no,
                            challan_photo: '',
                            bill_photo: '',
                            loan_date: loan_date
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let t;

                                Swal.fire({
                                    title: 'Updating Loan Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    Swal.fire({
                                        title: 'Success',
                                        text: data.Reply,
                                        type: 'success',
                                        width: 450,
                                        showCloseButton: false,
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    }).then((result) => {
                                        if(result.value){
                                            window.location.reload(true);
                                        }
                                    });
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Something went wrong.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });
                }
            }

            // DELETE SPARE LOAN DATA
            function delete_loan_data_spr(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    qty = $(ele2).closest('tr').find('td:eq(1)').find('input').val(),
                    loan_date = $(ele2).closest('tr').find('td:eq(8)').find('input').val(),
                    parts_id = ele3,
                    req_for = ele4;

                Swal.fire({
                    title: 'Are you sure you want to delete?',
                    text: 'The loan data will be deleted permanently!',
                    type: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#5cb85c',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class="fas fa-check"></i>&nbsp;&nbsp; Yes',
                    cancelButtonText: '<i class="fas fa-times"></i>&nbsp;&nbsp; No'
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: '../../api/interactionController',
                            method: 'post',
                            data: {
                                interact_type: 'delete',
                                interact: 'loan2',
                                id: loan_data_id,
                                loan_id: ele5,
                                parts_id: parts_id,
                                req_for: req_for,
                                qty: qty,
                                loan_date: loan_date
                            },
                            dataType: 'json',
                            cache: false,
                            success: function(data){
                                if(data.Type == 'success'){
                                    let t;

                                    Swal.fire({
                                        title: 'Deleting Loan Data',
                                        text: 'Please wait...',
                                        timer: 100,
                                        allowOutsideClick: false,
                                        onBeforeOpen: function(){
                                            Swal.showLoading(), t = setInterval(function(){
                                            }, 100);
                                        }
                                    }).then(function(){
                                        Swal.fire({
                                            title: 'Success',
                                            text: data.Reply,
                                            type: 'success',
                                            width: 450,
                                            showCloseButton: false,
                                            allowOutsideClick: false,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        }).then((result) => {
                                            if(result.value){
                                                window.location.reload(true);
                                            }
                                        });
                                    });
                                } else if(data.Type == 'error'){
                                    Swal.fire({
                                        title: 'Error',
                                        text: data.Reply,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Something went wrong.'
                                    });
                                } else{
                                    Swal.fire({
                                        title: 'Info',
                                        text: 'Server is under maintenance. Please try again later!',
                                        type: 'info',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    });
                                }

                                return false;
                            }
                        });
                    }
                });
            }

            function delete_loan_data_spr_2(ele, ele2, ele3, ele4, ele5){
                let loan_data_id = ele,
                    qty = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    loan_date = $(ele2).closest('tr').find('td:eq(9)').find('input').val(),
                    parts_id = ele3,
                    req_for = ele4;

                Swal.fire({
                    title: 'Are you sure you want to delete?',
                    text: 'The loan data will be deleted permanently!',
                    type: 'warning',
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#5cb85c',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class="fas fa-check"></i>&nbsp;&nbsp; Yes',
                    cancelButtonText: '<i class="fas fa-times"></i>&nbsp;&nbsp; No'
                }).then(function(result){
                    if(result.value){
                        $.ajax({
                            url: '../../api/interactionController',
                            method: 'post',
                            data: {
                                interact_type: 'delete',
                                interact: 'loan2',
                                id: loan_data_id,
                                loan_id: ele5,
                                parts_id: parts_id,
                                req_for: req_for,
                                qty: qty,
                                loan_date: loan_date
                            },
                            dataType: 'json',
                            cache: false,
                            success: function(data){
                                if(data.Type == 'success'){
                                    let t;

                                    Swal.fire({
                                        title: 'Deleting Loan Data',
                                        text: 'Please wait...',
                                        timer: 100,
                                        allowOutsideClick: false,
                                        onBeforeOpen: function(){
                                            Swal.showLoading(), t = setInterval(function(){
                                            }, 100);
                                        }
                                    }).then(function(){
                                        Swal.fire({
                                            title: 'Success',
                                            text: data.Reply,
                                            type: 'success',
                                            width: 450,
                                            showCloseButton: false,
                                            allowOutsideClick: false,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        }).then((result) => {
                                            if(result.value){
                                                window.location.reload(true);
                                            }
                                        });
                                    });
                                } else if(data.Type == 'error'){
                                    Swal.fire({
                                        title: 'Error',
                                        text: data.Reply,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Something went wrong.'
                                    });
                                } else{
                                    Swal.fire({
                                        title: 'Info',
                                        text: 'Server is under maintenance. Please try again later!',
                                        type: 'info',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    });
                                }

                                return false;
                            }
                        });
                    }
                });
            }

            // PARTS LOAN
            function parts_loan(ele, ele2, ele3){
                $('.loan-repay-title').html('<strong>' + $(ele3).closest('tr').find('td:eq(1)').html() + '</strong>');
                $('.stock-qty').html('<strong>' + $(ele3).closest('tr').find('td:eq(3)').html() + ' ' + $(ele3).closest('tr').find('td:eq(2)').html() + '</strong>');

                let table = $('#basic-datatable5').DataTable();
                table.destroy();
                $('.loan-repay-records').empty();

                let trHTML = '';

                $.ajax({
                    url: '../../api/loan',
                    method: 'post',
                    data: {
                        loan_data_type: 'fetch_parts_loan_data',
                        parts_id: ele,
                        parts_cat: ele2
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Loan Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            });

                            $.each(data.Reply, function(i, item){
                                let tr_style = (item.parts_qty == item.repay_qty) ? 'style="background: #cfe8cf;"' : '',
                                    disabled = (item.parts_qty == item.repay_qty) ? 'disabled' : '';

                                trHTML += '<tr '+ tr_style +'>';
                                    trHTML += '<td class="align-middle text-center">' + (i+1) + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.required_for + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.req_qty + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.parts_qty + '</td>';
                                    trHTML += '<td class="align-middle text-center" width="13%"><input type="text" class="form-control loan-repay-date" oninput="loan_repay_date(this)" onchange="action_date(this, '+ item.parts_id +')" data-id="'+ item.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" ' + disabled + '></td>';
                                    trHTML += '<td class="align-middle text-center" width="13%">';
                                        trHTML += '<div class="input-group">';
                                            let repay_qty = ((item.parts_qty - item.repay_qty) < parseFloat($(ele3).closest('tr').find('td:eq(3)').html())) ? (item.parts_qty - item.repay_qty) : parseFloat($(ele3).closest('tr').find('td:eq(3)').html());

                                            trHTML += '<input type="number" class="form-control" placeholder="Insert" oninput="qty_4(this, ' + repay_qty + ')" ' + disabled + ' readonly>';
                                            trHTML += '<div class="input-group-prepend">';
                                                trHTML += '<div class="input-group-text">' + item.parts_unit + '</div>';
                                            trHTML += '</div>';
                                        trHTML += '</div>';
                                        trHTML += '<span class="float-left w-100 mt-1 text-primary">Repaid Qty.: <strong>' + item.repay_qty + '</strong> ' + item.parts_unit + '.</span>';
                                    trHTML += '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.price + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.party_name + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.loan_date + '</td>';
                                    trHTML += '<td class="align-middle text-center">';
                                        if(item.parts_qty != item.repay_qty){
                                            let borrow_date = "'" + item.loan_date + "'";

                                            if(item.parts_cat == 1){
                                                trHTML += '<a title="Repay" href="javascript:void(0)" class="btn btn-xs btn-success disabled" onclick="loan_repay2(' + item.parts_id + ', ' + item.req_for + ', this, ' + item.party_id + ', ' + borrow_date + ', ' + item.req_qty + ', ' + item.parts_qty + ', ' + item.loan_id + ', ' + item.loan_data_id + ');"><i class="mdi mdi-undo"></i></a>';
                                            } else{
                                                trHTML += '<a title="Repay" href="javascript:void(0)" class="btn btn-xs btn-success disabled" onclick="loan_repay(' + item.parts_id + ', ' + item.req_for + ', this, ' + item.party_id + ', ' + borrow_date + ', ' + item.req_qty + ', ' + item.parts_qty + ', ' + item.loan_id + ', ' + item.loan_data_id + ');"><i class="mdi mdi-undo"></i></a>';
                                            }
                                        }
                                    trHTML += '</td>';
                                trHTML += '</tr>';
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });

                $('.loan-repay-records').append(trHTML);

                $('#basic-datatable5').DataTable({
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                    }
                });
            }

            // PARTS LOAN REPAY HISTORY
            function loan_repay_history(ele, ele2, ele3){
                $('.loan-repay-history-title').html('<strong>' + $(ele3).closest('tr').find('td:eq(1)').html() + '</strong>');

                let table = $('#basic-datatable6').DataTable();
                table.destroy();
                $('.loan-repay-history-records').empty();

                let trHTML = '';

                $.ajax({
                    url: '../../api/loan',
                    method: 'post',
                    data: {
                        loan_data_type: 'fetch_parts_loan_repay_history_data',
                        parts_id: ele,
                        parts_cat: ele2
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Loan Repay History Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            });

                            $.each(data.Reply, function(i, item){
                                trHTML += '<tr>';
                                    trHTML += '<td class="align-middle text-center">' + (i+1) + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.parts_name + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.parts_unit + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.repaid_qty + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.repay_date + '</td>';
                                    trHTML += '<td class="align-middle text-center">' + item.party_name + '</td>';
                                trHTML += '</tr>';
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });

                $('.loan-repay-history-records').append(trHTML);

                $('#basic-datatable6').DataTable({
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                    }
                });
            }

            // LOAN REPAY ACTION DATE
            function action_date(ele, ele2){
                let req_for = $(ele).closest('tr').find('td:eq(1)').html(),
                    parts_id = ele2;

                if(req_for == 'BCP-CCM')
                    req_for = 1;
                else if(req_for == 'BCP-Furnace')
                    req_for = 2;
                else if(req_for == 'Concast-CCM')
                    req_for = 3;
                else if(req_for == 'Concast-Furnace')
                    req_for = 4;
                else if(req_for == 'HRM')
                    req_for = 5;
                else if(req_for == 'HRM Unit-2')
                    req_for = 6;
                else if(req_for == 'Lal Masjid')
                    req_for = 7;
                else if(req_for == 'Sonargaon')
                    req_for = 8;
                else if(req_for == 'General')
                    req_for = 9;

                if($(ele).val()){
                    if(new Date($(ele).val()) < new Date($(ele).closest('tr').find('td:eq(8)').html())){
                        Swal.fire({
                            title: 'Error',
                            text: 'No loan record found!',
                            type: 'error',
                            width: 450,
                            showCloseButton: true,
                            confirmButtonColor: '#5cb85c',
                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                            footer: 'Repay date should be greater than borrow date.'
                        }).then(function(){
                            $(ele).closest('tr').find('td:eq(5)').find('input').prop('readonly', true).val('');
                            $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                        });
                    } else{
                        $.ajax({
                            url: '../../api/inventory',
                            method: 'post',
                            data: {
                                parts_id: parts_id,
                                required_for: req_for,
                                action_date: $(ele).val(),
                                inventory_data_type: 'inventory_parts_issue'
                            },
                            dataType: 'json',
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type == 'success'){
                                    if(data.Reply[0].parts_qty == null){
                                        Swal.fire({
                                            title: 'Error',
                                            text: 'No inventory record found!',
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please receive some before repay.'
                                        }).then(function(){
                                            $(ele).closest('tr').find('td:eq(5)').find('input').prop('readonly', true).val('');
                                            $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                                        });
                                    } else if(data.Reply[0].parts_qty <= 0){
                                        Swal.fire({
                                            title: 'Error',
                                            text: 'All received parts have been issued / repaid!',
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please receive some to repay.'
                                        }).then(function(){
                                            $(ele).closest('tr').find('td:eq(5)').find('input').prop('readonly', true).val('');
                                            $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                                        });
                                    } else{
                                        $(ele).closest('tr').find('td:eq(5)').find('input').prop('readonly', false).val('');
                                        $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                                    }
                                }
                            }
                        });
                    }
                } else{
                    $(ele).closest('tr').find('td:eq(5)').find('input').prop('readonly', true).val('');
                    $(ele).closest('tr').find('td:eq(9)').find('a').addClass('disabled');
                }
            }
        </script>
    </body>
</html>