<?php 
    require_once('../session.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RRM Inventory by RRM Gorup | Purchase</title>

        <?php 
            require_once('../../sub-page-header.php');

            // PERMISSION
            $user_categories = [1, 3, 4];
            if(!in_array($user_category, $user_categories)){
                header('location: ../dashboard');
            }
        ?>

        <style type="text/css">
            .select2-container .select2-selection--single{
                height: 34px;
            }
            .select2-container--default .select2-selection--single .select2-selection__rendered{
                line-height: 34px;
            }
            .select2-container .select2-selection--single .select2-selection__arrow{
                height: 0px !important;
                top: 18px;
            }

            .consumable-table > thead > tr > th{
                vertical-align: middle;
            }
            .spare-table > tbody > tr > td{
                vertical-align: middle;
            }

            .hr-custom-style{
                border-top: 1px solid #808080;
            }
        </style>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">
            <!-- Topbar Start -->
            <?php include('../../topbar-for-sub-page.php'); ?>
            <!-- end Topbar -->

            <?php include('../../navbar-for-sub-page.php'); ?>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="wrapper full-width-background">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                    <li class="breadcrumb-item active">Purchase List</li>
                                </ol>
                            </div>
                            <h4 class="page-title"><i class="mdi mdi-currency-bdt"></i> Purchase List</h4>
                        </div>
                    </div>
                </div>     
                <!-- end page title -->

                <!-- Start Modals For Create / Update Purchase -->
                <div class="modal fade full-width-modal" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="full-width-modalLabel">Purchase Parts</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>

                            <div class="modal-body" style="overflow: scroll; height: 700px;">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="tab-content mb-0 b-0 pt-0">
                                            <div class="tab-pane" id="first">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="consumable_table" style="overflow-x: auto;">
                                                            <table class="table table-bordered table-responsive-md table-striped text-center mb-0 consumable-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="align-middle text-center" style="min-width: 50px;">SL.</th>
                                                                        <th class="align-middle text-center" style="min-width: 120px;">Add to List<br><div title="Add all" class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-all-to-list" id="add_all_to_list" onclick="add_all_to_list()"><label class="custom-control-label" for="add_all_to_list"></label></div></th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Required For</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Parts Name</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Quantity</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Where to Use</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Price</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Party Name</th>
                                                                        <?php 
                                                                            if($user_category < 4){
                                                                        ?>
                                                                                <th class="align-middle text-center" style="min-width: 150px;">Gate No.</th>
                                                                                <th class="align-middle text-center" style="min-width: 150px;">Challan No.</th>
                                                                                <th class="align-middle text-center" style="min-width: 250px;">Challan Photo</th>
                                                                                <th class="align-middle text-center" style="min-width: 250px;">Bill Photo</th>
                                                                        <?php 
                                                                            }
                                                                        ?>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Remarks</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Purchase Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="mt-3 text-center proceed-div">
                                                            <button type="button" class="btn btn-success waves-effect waves-light" onclick="proceed_consumable()"><span class="btn-label"><i class="fas fa-arrow-right"></i></span>Proceed</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="second">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="spare_table" style="overflow-x: auto;">
                                                            <table class="table table-bordered table-responsive-md table-striped text-center mb-0 spare-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="align-middle text-center" style="min-width: 50px;">SL.</th>
                                                                        <th class="align-middle text-center" style="min-width: 120px;">Add to List<br><div title="Add all" class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-all-to-list-2" id="add_all_to_list_2" onclick="add_all_to_list_2()"><label class="custom-control-label" for="add_all_to_list_2"></label></div></th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Required For</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Parts Name</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Quantity</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Old Spares Details</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Status</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Price</th>
                                                                        <th class="align-middle text-center" style="min-width: 200px;">Party Name</th>
                                                                        <?php 
                                                                            if($user_category < 4){
                                                                        ?>
                                                                                <th class="align-middle text-center" style="min-width: 150px;">Gate No.</th>
                                                                                <th class="align-middle text-center" style="min-width: 150px;">Challan No.</th>
                                                                                <th class="align-middle text-center" style="min-width: 250px;">Challan Photo</th>
                                                                                <th class="align-middle text-center" style="min-width: 250px;">Bill Photo</th>
                                                                        <?php 
                                                                            }
                                                                        ?>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Remarks</th>
                                                                        <th class="align-middle text-center" style="min-width: 150px;">Purchase Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="mt-3 text-center proceed-div-2">
                                                            <button type="button" class="btn btn-success waves-effect waves-light" onclick="proceed_spare()"><span class="btn-label"><i class="fas fa-arrow-right"></i></span>Proceed</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- tab-content -->
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End Modals For Create Purchase -->

                <!-- Start Modals For PURCHASE VIEW -->
                <div class="modal fade full-width-modal-2" role="dialog" aria-labelledby="full-width-modalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-full modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="full-width-modalLabel">View / Update Purchase</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>

                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div style="overflow-x: auto;">
                                                    <table class="table table-bordered table-responsive-md table-striped text-center mb-0 purchase-view-table">
                                                        <thead>
                                                            <th class="text-center" style="min-width: 50px;">Sl.</th>
                                                            <th class="text-center" style="min-width: 200px;">Quantity</th>
                                                            <th class="text-center" style="min-width: 200px;">Price</th>
                                                            <th class="text-center" style="min-width: 250px;">Party</th>
                                                            <th class="text-center" style="min-width: 150px;">Gate No.</th>
                                                            <th class="text-center" style="min-width: 150px;">Challan No.</th>
                                                            <th class="text-center" style="min-width: 250px;">Challan Photo</th>
                                                            <th class="text-center" style="min-width: 250px;">Bill Photo</th>
                                                            <th class="text-center" style="min-width: 200px;">Purchase Date</th>
                                                            <?php 
                                                                if($user_category != 3){
                                                            ?>
                                                                    <th class="text-center" style="min-width: 120px;">Action</th>
                                                            <?php 
                                                                }
                                                            ?>
                                                        </thead>
                                                        <tbody class="purchase-records">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div>
                                <!-- end row -->
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <!-- End Modals For PURCHASE VIEW -->
                
                <!-- PURCHASE LIST -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div id="rootwizard">
                                    <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-3">
                                        <li class="nav-item">
                                            <a href="#third" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2 active">
                                                <i class="fas fa-fire mr-1"></i>
                                                <span class="d-none d-sm-inline">CONSUMABLE</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#forth" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                                <i class="fas fa-wrench mr-1"></i>
                                                <span class="d-none d-sm-inline">SPARE</span>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="tab-content mb-0 b-0 pt-0">
                                        <div class="tab-pane active" id="third" style="overflow-x: auto; overflow-y: auto;">
                                            <table id="basic-datatable2" class="table w-100 nowrap cell-border">
                                                <thead style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Purchase Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        if($user_category == 1 || $user_category == 3){
                                                            $con_requisition_query = mysqli_query($conn, "SELECT *, u1.user_fullname AS requisitioned_by, u2.user_fullname AS accepted_by FROM rrmsteel_con_requisition r INNER JOIN rrmsteel_user u1 ON u1.user_id = r.requisition_by INNER JOIN rrmsteel_user u2 ON u2.user_id = r.approved_by WHERE r.approval_status = 1 ORDER BY r.requisition_id DESC");
                                                        } else{
                                                            $con_requisition_query = mysqli_query($conn, "SELECT *, u1.user_fullname AS requisitioned_by, u2.user_fullname AS accepted_by FROM rrmsteel_con_requisition r INNER JOIN rrmsteel_user u1 ON u1.user_id = r.requisition_by INNER JOIN rrmsteel_user u2 ON u2.user_id = r.p_approved_by WHERE r.p_approval_status = 1 AND r.p_approved_by = '$user_id' ORDER BY r.requisition_id DESC");
                                                        }

                                                        if(mysqli_num_rows($con_requisition_query) > 0){
                                                            $i = 1;

                                                            while($row = mysqli_fetch_assoc($con_requisition_query)){
                                                    ?>
                                                                <tr>
                                                                    <td class="align-middle text-center"><?= $i++; ?></td>
                                                                    <td class="align-middle text-center"><?= 'RRM\CONSUMABLE-REQUISITION\\' . date('Y', $row['requisition_created']) . '-' . $row['requisition_id']; ?></td>
                                                                    <td class="align-middle text-center"><?= $row['requisitioned_by']; ?></td>
                                                                    <td class="align-middle text-center"><?= $row['accepted_by']; ?></td>
                                                                    <td class="align-middle text-center"><?= date('d M, Y', $row['requisition_created']); ?></td>
                                                                    <td class="align-middle text-center">
                                                                        <?php 
                                                                            $purchase_info = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM rrmsteel_con_purchase WHERE requisition_id = '".$row['requisition_id']."' LIMIT 1"));
                                                                            if(isset($purchase_info)){
                                                                                $requisitioned_parts = $purchase_info['requisitioned_parts'];
                                                                                $purchased_parts = $purchase_info['purchased_parts'];

                                                                                if($requisitioned_parts == $purchased_parts){
                                                                                    $con_purchase_status = 1;

                                                                                    echo '<span class="text-success font-weight-bold">Purchased</span>';
                                                                                } else{
                                                                                    $con_purchase_status = 0;

                                                                                    echo '<div class="progress mb-2 progress-sm w-100">
                                                                                        <div class="progress-bar bg-info" role="progressbar" style="width: '.(($purchased_parts / $requisitioned_parts) * 100).'%;" aria-valuenow="'.(($purchased_parts / $requisitioned_parts) * 100).'" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                    </div>';
                                                                                    echo '<span class="text-warning font-weight-bold">' . ($requisitioned_parts - $purchased_parts) . ' of ' . $requisitioned_parts . ' requisitioned parts are pending</span>';
                                                                                }
                                                                            } else{
                                                                                $con_purchase_status = 0;
                                                                                echo '<span class="text-warning font-weight-bold">Pending</span>';
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                    <td class="align-middle text-center">
                                                                        <?php 
                                                                            $con_purchase_query = mysqli_query($conn, "SELECT * FROM rrmsteel_con_purchase WHERE requisition_id = '".$row['requisition_id']."'");

                                                                            $con_purchase_status = 0;

                                                                            if(mysqli_num_rows($con_purchase_query) > 0){
                                                                                $purchase_info = mysqli_fetch_assoc($con_purchase_query);

                                                                                $requisitioned_parts = $purchase_info['requisitioned_parts'];
                                                                                $purchased_parts = $purchase_info['purchased_parts'];

                                                                                if($requisitioned_parts == $purchased_parts)
                                                                                    $con_purchase_status = 1;
                                                                            }

                                                                            if((($user_category == 1 && $row['approval_status'] == 1) || ($user_category == 4 && $row['p_approval_status'] == 1)) && (mysqli_num_rows($con_purchase_query) == 0 || $con_purchase_status == 0)){
                                                                        ?>
                                                                                <a title="Purchase" href="javascript:void(0)" class="btn btn-xs btn-success" data-toggle="modal" data-target=".full-width-modal" data-id="<?= $row['requisition_id'] ?>" onclick="purchase_con(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-cart"></i></a>
                                                                        <?php 
                                                                            }

                                                                            if(mysqli_num_rows($con_purchase_query) > 0){
                                                                        ?>
                                                                                <a title="View / Update" href="javascript:void(0)" class="btn btn-xs btn-info" data-toggle="modal" data-target=".full-width-modal-2" data-id="<?= $row['requisition_id'] ?>" onclick="view_purchase_con(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-eye"></i></a>
                                                                        <?php 
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                    <?php 
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Purchase Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="forth" style="overflow-x: auto; overflow-y: auto;">
                                            <table id="basic-datatable" class="table w-100 nowrap cell-border">
                                                <thead style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Purchase Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        if($user_category == 1 || $user_category == 3){
                                                            $spr_requisition_query = mysqli_query($conn, "SELECT *, u1.user_fullname AS requisitioned_by, u2.user_fullname AS accepted_by FROM rrmsteel_spr_requisition r INNER JOIN rrmsteel_user u1 ON u1.user_id = r.requisition_by INNER JOIN rrmsteel_user u2 ON u2.user_id = r.approved_by WHERE r.approval_status = 1 ORDER BY r.requisition_id DESC");
                                                        } else{
                                                            $spr_requisition_query = mysqli_query($conn, "SELECT *, u1.user_fullname AS requisitioned_by, u2.user_fullname AS accepted_by FROM rrmsteel_spr_requisition r INNER JOIN rrmsteel_user u1 ON u1.user_id = r.requisition_by INNER JOIN rrmsteel_user u2 ON u2.user_id = r.p_approved_by WHERE r.p_approval_status = 1 AND r.p_approved_by = '$user_id' ORDER BY r.requisition_id DESC");
                                                        }

                                                        if(mysqli_num_rows($spr_requisition_query) > 0){
                                                            $i = 1;

                                                            while($row = mysqli_fetch_assoc($spr_requisition_query)){
                                                    ?>
                                                                <tr>
                                                                    <td class="align-middle text-center"><?= $i++; ?></td>
                                                                    <td class="align-middle text-center"><?= 'RRM\SPARE-REQUISITION\\' . date('Y', $row['requisition_created']) . '-' . $row['requisition_id']; ?></td>
                                                                    <td class="align-middle text-center"><?= $row['requisitioned_by']; ?></td>
                                                                    <td class="align-middle text-center"><?= $row['accepted_by']; ?></td>
                                                                    <td class="align-middle text-center"><?= date('d M, Y', $row['requisition_created']); ?></td>
                                                                    <td class="align-middle text-center">
                                                                        <?php 
                                                                            $purchase_info = mysqli_fetch_assoc(mysqli_query($conn, "SELECT * FROM rrmsteel_spr_purchase WHERE requisition_id = '".$row['requisition_id']."' LIMIT 1"));
                                                                            if(isset($purchase_info)){
                                                                                $requisitioned_parts = $purchase_info['requisitioned_parts'];
                                                                                $purchased_parts = $purchase_info['purchased_parts'];

                                                                                if($requisitioned_parts == $purchased_parts){
                                                                                    $spr_purchase_status = 1;

                                                                                    echo '<span class="text-success font-weight-bold">Purchased</span>';
                                                                                } else{
                                                                                    $spr_purchase_status = 0;

                                                                                    echo '<div class="progress mb-2 progress-sm w-100">
                                                                                        <div class="progress-bar bg-info" role="progressbar" style="width: '.(($purchased_parts / $requisitioned_parts) * 100).'%;" aria-valuenow="'.(($purchased_parts / $requisitioned_parts) * 100).'" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                    </div>';
                                                                                    echo '<span class="text-warning font-weight-bold">' . ($requisitioned_parts - $purchased_parts) . ' of ' . $requisitioned_parts . ' requisitioned parts are pending</span>';
                                                                                }
                                                                            } else{
                                                                                $spr_purchase_status = 0;
                                                                                echo '<span class="text-warning font-weight-bold">Pending</span>';
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                    <td class="align-middle text-center">
                                                                        <?php 
                                                                            $spr_purchase_query = mysqli_query($conn, "SELECT * FROM rrmsteel_spr_purchase WHERE requisition_id = '".$row['requisition_id']."'");

                                                                            $spr_purchase_status = 0;

                                                                            if(mysqli_num_rows($spr_purchase_query) > 0){
                                                                                $purchase_info = mysqli_fetch_assoc($spr_purchase_query);

                                                                                $requisitioned_parts = $purchase_info['requisitioned_parts'];
                                                                                $purchased_parts = $purchase_info['purchased_parts'];

                                                                                if($requisitioned_parts == $purchased_parts)
                                                                                    $spr_purchase_status = 1;
                                                                            }

                                                                            if((($user_category == 1 && $row['approval_status'] == 1) || ($user_category == 4 && $row['p_approval_status'] == 1)) && (mysqli_num_rows($spr_purchase_query) == 0 || $spr_purchase_status == 0)){
                                                                        ?>
                                                                                <a title="Purchase" href="javascript:void(0)" class="btn btn-xs btn-success" data-toggle="modal" data-target=".full-width-modal" data-id="<?= $row['requisition_id'] ?>" onclick="purchase_spr(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-cart"></i></a>
                                                                        <?php 
                                                                            }

                                                                            if(mysqli_num_rows($spr_purchase_query) > 0){
                                                                        ?>
                                                                                <a title="View" href="javascript:void(0)" class="btn btn-xs btn-info" data-toggle="modal" data-target=".full-width-modal-2" data-id="<?= $row['requisition_id'] ?>" onclick="view_purchase_spr(<?= $row['requisition_id'] ?>)"><i class="mdi mdi-eye"></i></a>
                                                                        <?php 
                                                                            }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                    <?php 
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                    <tr>
                                                        <th class="align-middle text-center">SL.</th>
                                                        <th class="align-middle text-center">Reference</th>
                                                        <th class="align-middle text-center">Requisitioned By</th>
                                                        <th class="align-middle text-center">Accepted By</th>
                                                        <th class="align-middle text-center">Requisition Date</th>
                                                        <th class="align-middle text-center">Purchase Status</th>
                                                        <th class="align-middle text-center">Action</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
        
        <!-- Footer Start -->
        <?php require_once('../../footer-for-sub-page.php'); ?>
        <!-- end Footer -->

        <!-- Custom js -->
        <script type="text/javascript">
            $(document).ready(function(){
                $('#basic-datatable2').DataTable({
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                    }
                });
            });

            // GET PARTS QTY
            function qty(ele, ele2){
                if(ele == 1){
                    $('#consumable_table tbody tr').each(function(){
                        if($(this).find('td:eq(4)').find('.qty').val() <= 0)
                            $(this).find('td:eq(4)').find('.qty').val('');
                        // else if($(this).find('td:eq(4)').find('.qty').val() > ele2)
                            // $(this).find('td:eq(4)').find('.qty').val(ele2);
                    });
                } else{
                    $('#spare_table tbody tr').each(function(){
                        if($(this).find('td:eq(4)').find('.qty-2').val() <= 0)
                            $(this).find('td:eq(4)').find('.qty-2').val('');
                        // else if($(this).find('td:eq(4)').find('.qty-2').val() > ele2)
                            // $(this).find('td:eq(4)').find('.qty-2').val(ele2);
                    });
                }
            }

            // GET PARTS QTY 2
            function qty_2(ele, ele2, ele3, ele4, ele5){
                let tot_purchased = 0;

                $('.purchase-records tr').each(function(){
                    if($(this).index() >= ele4 && $(this).index() <= ele5)
                        tot_purchased += parseInt($(this).find('td:eq(1)').find('input').val()) ? parseInt($(this).find('td:eq(1)').find('input').val()) : 0;
                });

                // if(parseInt($(ele).val()) <= 0 || tot_purchased > ele3)
                if(parseInt($(ele).val()) <= 0)
                    $(ele).closest('tr').find('td:eq(1)').find('input').val(ele2);
            }

            // GET PRICE
            function price(ele){
                if(ele == 1){
                    $('#consumable_table tbody tr').each(function(){
                        if($(this).find('td:eq(6)').find('.price').val() <= 0)
                            $(this).find('td:eq(6)').find('.price').val('');
                    });
                } else{
                    $('#spare_table tbody tr').each(function(){
                        if($(this).find('td:eq(7)').find('.price-2').val() <= 0)
                            $(this).find('td:eq(7)').find('.price-2').val('');
                    });
                }
            }

            function price_2(ele, ele2){
                if(parseInt($(ele).val()) <= 0)
                    $(ele).closest('tr').find('td:eq(2)').find('input').val(ele2.toFixed(2));
            }

            // GET PARTY NAME
            function party_name(ele, ele2, ele3){
                let party_arr = ele.split('|'),
                    remarks = party_arr[1];

                if(ele2 == 1){
                    if(remarks)
                        $('#consumable_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(7)').find('.remarks').html('Remarks: ' + remarks);
                    else
                        $('#consumable_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(7)').find('.remarks').html('');
                } else{
                    if(remarks)
                        $('#spare_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(8)').find('.remarks-2').html('Remarks: ' + remarks);
                    else
                        $('#spare_table tbody').find('tr:nth-child(' + ele3 + ')').find('td:eq(8)').find('.remarks-2').html('');
                }
            }

            function party_name_2(ele){
                let party_arr = $(ele).val().split('|'),
                    remarks = party_arr[1];

                if(remarks)
                    $(ele).closest('tr').find('td:eq(3)').find('.remarks').html('Remarks: ' + remarks);
                else
                    $(ele).closest('tr').find('td:eq(3)').find('.remarks').html('');
            }

            // VALIDATE PURCHASE DATE FIELD
            function purchase_date(ele, ele2){
                if(ele == 1)
                    $('#consumable_table tbody').find('tr:nth-child(' + ele2 + ')').find('td:eq(13)').find('input').val('');
                else
                    $('#spare_table tbody').find('tr:nth-child(' + ele2 + ')').find('td:eq(14)').find('input').val('');
            }

            function purchase_date_2(ele){
                $(ele).closest('tr').find('td:eq(8)').find('input').val('');
            }

            // ADD ALL TO CON LIST
            function add_all_to_list(){
                $('.add-to-list').each(function(){
                    if($(this).attr('disabled') == undefined){
                        if($('.add-all-to-list').is(':checked'))
                            $(this).prop('checked', true);
                        else
                            $(this).prop('checked', false);
                    }
                });
            }

            // ADD ALL TO SPARE LIST
            function add_all_to_list_2(){
                $('.add-to-list-2').each(function(){
                    if($(this).attr('disabled') == undefined){
                        if($('.add-all-to-list-2').is(':checked'))
                            $(this).prop('checked', true);
                        else
                            $(this).prop('checked', false);
                    }
                });
            }

            // ADD TO CON LIST
            function add_to_list(){
                let flag = 1;

                $('.add-to-list').each(function(i){
                    if(!$(this).is(':checked')){
                        flag = 0;

                        return false;
                    }
                });

                if(flag == 1)
                    $('.add-all-to-list').prop('checked', true);
                else
                    $('.add-all-to-list').prop('checked', false);
            }

            // ADD TO SPARE LIST
            function add_to_list_2(){
                let flag = 1;

                $('.add-to-list-2').each(function(i){
                    if(!$(this).is(':checked')){
                        flag = 0;

                        return false;
                    }
                });

                if(flag == 1)
                    $('.add-all-to-list-2').prop('checked', true);
                else
                    $('.add-all-to-list-2').prop('checked', false);
            }

            function edit_btn(ele){
                $(ele).closest('tr').find('.data-span').addClass('d-none');
                $(ele).closest('tr').find('.data-input').removeClass('d-none');
                
                $(ele).closest('tr').find('.edt-btn').addClass('d-none');
                $(ele).closest('tr').find('.cncl-btn').removeClass('d-none');
                $(ele).closest('tr').find('.upd-btn').removeClass('d-none');
            }

            function cancel_btn(ele){
                $(ele).closest('tr').find('.data-span').removeClass('d-none');
                $(ele).closest('tr').find('.data-input').addClass('d-none');
                
                $(ele).closest('tr').find('.edt-btn').removeClass('d-none');
                $(ele).closest('tr').find('.cncl-btn').addClass('d-none');
                $(ele).closest('tr').find('.upd-btn').addClass('d-none');
            }

            let requisition_data = '';

            // CONSUMABLE PURCHASE
            function purchase_con(ele){
                let user_category = '<?= $user_category ?>';

                $('#first').addClass('active');
                $('#second').removeClass('fade active show');

                let t;

                Swal.fire({
                    title: 'Fetching Purchase Data',
                    text: 'Please wait...',
                    timer: 100,
                    allowOutsideClick: false,
                    onBeforeOpen: function(){
                        Swal.showLoading(), t = setInterval(function(){
                        }, 100);
                    }
                }).then(function(){
                    let requisition_id = ele;

                    $.ajax({
                        url: '../../api/purchase',
                        method: 'post',
                        data: {
                            purchase_data_type: 'fetch_requisition_con',
                            requisition_id: requisition_id
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                requisition_data = data;
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let trHTML = '';

                    $.each(requisition_data.Reply, function(i, requisition_item){
                        trHTML += '<tr '+ ((requisition_item.parts_id == requisition_item.purchased_parts_id && requisition_item.purchased_parts_qty == requisition_item.r_qty) ? 'style=" background: #cfe8cf;"' : '') +'>';
                            trHTML += '<td class="align-middle">' + (i+1) + ' <span class="text-danger d-none">*</span></td>';

                            let checked = '',
                                disabled = '';

                            if(requisition_item.parts_id == requisition_item.purchased_parts_id && requisition_item.purchased_parts_qty == requisition_item.r_qty){
                                checked = 'checked';
                                disabled = 'disabled';
                            }

                            trHTML += '<td class="align-middle"><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-to-list" id="add_to_list'+i+'" onclick="add_to_list()" '+ checked + ' ' + disabled +'><label class="custom-control-label" for="add_to_list'+i+'"></label></div></td>';

                            if(requisition_item.required_for == 1)
                                required_for = 'BCP-CCM';
                            else if(requisition_item.required_for == 2)
                                required_for = 'BCP-Furnace';
                            else if(requisition_item.required_for == 3)
                                required_for = 'Concast-CCM';
                            else if(requisition_item.required_for == 4)
                                required_for = 'Concast-Furnace';
                            else if(requisition_item.required_for == 5)
                                required_for = 'HRM';
                            else if(requisition_item.required_for == 6)
                                required_for = 'HRM Unit-2';
                            else if(requisition_item.required_for == 7)
                                required_for = 'Lal Masjid';
                            else if(requisition_item.required_for == 8)
                                required_for = 'Sonargaon';
                            else if(requisition_item.required_for == 9)
                                required_for = 'General';

                            trHTML += '<td class="text-center align-middle">' + required_for + '</td>';

                            $.ajax({
                                url: '../../api/parts',
                                method: 'post',
                                data: {
                                    parts_data_type: 'fetch_all'
                                },
                                dataType: 'json',
                                cache: false,
                                async: false,
                                success: function(data){
                                    if(data.Type == 'success'){
                                        $.each(data.Reply, function(i, parts_item){
                                            if(parts_item.parts_category == 2 && requisition_item.parts_id == parts_item.parts_id){
                                                trHTML += '<td class="text-center align-middle">' + parts_item.parts_name + '</td>';

                                                return false;
                                            }
                                        });
                                    } else if(data.Type == 'error'){
                                        Swal.fire({
                                            title: 'Error',
                                            text: data.Reply,
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please try again.'
                                        });
                                    } else{
                                        Swal.fire({
                                            title: 'Info',
                                            text: 'Server is under maintenance. Please try again later!',
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        });
                                    }

                                    return false;
                                }
                            });

                            trHTML += '<td class="align-middle text-center">';
                                trHTML += '<div class="input-group">';
                                    trHTML += '<input type="number" class="form-control qty" placeholder="Insert" oninput="qty(1, '+ (requisition_item.r_qty - requisition_item.purchased_parts_qty) +')" '+ disabled +'>';
                                    trHTML += '<div class="input-group-prepend">';
                                        let parts_unit = '';

                                        if(requisition_item.parts_unit == 1)
                                            parts_unit = 'Bag';
                                        else if(requisition_item.parts_unit == 2)
                                            parts_unit = 'Box';
                                        else if(requisition_item.parts_unit == 3)
                                            parts_unit = 'Box/Pcs';
                                        else if(requisition_item.parts_unit == 4)
                                            parts_unit = 'Bun';
                                        else if(requisition_item.parts_unit == 5)
                                            parts_unit = 'Bundle';
                                        else if(requisition_item.parts_unit == 6)
                                            parts_unit = 'Can';
                                        else if(requisition_item.parts_unit == 7)
                                            parts_unit = 'Cartoon';
                                        else if(requisition_item.parts_unit == 8)
                                            parts_unit = 'Challan';
                                        else if(requisition_item.parts_unit == 9)
                                            parts_unit = 'Coil';
                                        else if(requisition_item.parts_unit == 10)
                                            parts_unit = 'Drum';
                                        else if(requisition_item.parts_unit == 11)
                                            parts_unit = 'Feet';
                                        else if(requisition_item.parts_unit == 12)
                                            parts_unit = 'Gallon';
                                        else if(requisition_item.parts_unit == 13)
                                            parts_unit = 'Item';
                                        else if(requisition_item.parts_unit == 14)
                                            parts_unit = 'Job';
                                        else if(requisition_item.parts_unit == 15)
                                            parts_unit = 'Kg';
                                        else if(requisition_item.parts_unit == 16)
                                            parts_unit = 'Kg/Bundle';
                                        else if(requisition_item.parts_unit == 17)
                                            parts_unit = 'Kv';
                                        else if(requisition_item.parts_unit == 18)
                                            parts_unit = 'Lbs';
                                        else if(requisition_item.parts_unit == 19)
                                            parts_unit = 'Ltr';
                                        else if(requisition_item.parts_unit == 20)
                                            parts_unit = 'Mtr';
                                        else if(requisition_item.parts_unit == 21)
                                            parts_unit = 'Pack';
                                        else if(requisition_item.parts_unit == 22)
                                            parts_unit = 'Pack/Pcs';
                                        else if(requisition_item.parts_unit == 23)
                                            parts_unit = 'Pair';
                                        else if(requisition_item.parts_unit == 24)
                                            parts_unit = 'Pcs';
                                        else if(requisition_item.parts_unit == 25)
                                            parts_unit = 'Pound';
                                        else if(requisition_item.parts_unit == 26)
                                            parts_unit = 'Qty';
                                        else if(requisition_item.parts_unit == 27)
                                            parts_unit = 'Roll';
                                        else if(requisition_item.parts_unit == 28)
                                            parts_unit = 'Set';
                                        else if(requisition_item.parts_unit == 29)
                                            parts_unit = 'Truck';
                                        else if(requisition_item.parts_unit == 30)
                                            parts_unit = 'Unit';
                                        else if(requisition_item.parts_unit == 31)
                                            parts_unit = 'Yeard';
                                        else if(requisition_item.parts_unit == 32)
                                            parts_unit = '(Unit Unknown)';
                                        else if(requisition_item.parts_unit == 33)
                                            parts_unit = 'SFT';
                                        else if(requisition_item.parts_unit == 34)
                                            parts_unit = 'RFT';
                                        else if(requisition_item.parts_unit == 35)
                                            parts_unit = 'CFT';

                                        trHTML += '<div class="input-group-text"><span id="unit">' + parts_unit + '</span></div>';
                                    trHTML += '</div>';
                                trHTML += '</div>';

                                trHTML += '<span class="float-left mt-1 text-primary w-100 in-stock">In Stock: <strong>' + requisition_item.i_qty + '</strong> ' + parts_unit + '</span><span class="float-left text-info w-100 purchased">Purchased: <strong>' + requisition_item.purchased_parts_qty + '</strong> of <strong>' + requisition_item.r_qty + '</strong> ' + parts_unit + '</span>';
                            trHTML += '</td>';

                            trHTML += '<td class="text-center align-middle">' + requisition_item.parts_usage + '</td>';
                            
                            trHTML += '<td class="align-middle"><input type="number" class="form-control price" placeholder="Insert" oninput="price(1)" '+ disabled +'></td>';

                            let party_name_option = '';
                            $.ajax({
                                url: '../../api/party',
                                method: 'post',
                                data: {
                                    party_data_type: 'fetch_all'
                                },
                                dataType: 'json',
                                cache: false,
                                async: false,
                                success: function(data){
                                    if(data.Type == 'success'){
                                        $.each(data.Reply, function(i, party_item){
                                            party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'">'+party_item.party_name+'</option>';
                                        });
                                    } else if(data.Type == 'error'){
                                        Swal.fire({
                                            title: 'Error',
                                            text: data.Reply,
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please try again.'
                                        });
                                    } else{
                                        Swal.fire({
                                            title: 'Info',
                                            text: 'Server is under maintenance. Please try again later!',
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        });
                                    }

                                    return false;
                                }
                            });
                                
                            trHTML += '<td class="align-middle">';
                                trHTML += '<select class="select-b party-name" onchange="party_name(this.value, 1, ' + (i+1) + ')" '+ disabled +'>';
                                    trHTML += '<option value="">Choose</option>';
                                    trHTML += party_name_option;
                                trHTML += '</select>';
                                trHTML += '<span class="float-left mt-1 text-primary remarks"></span>';
                            trHTML += '</td>';

                            if(parseInt(user_category) < 4){
                                trHTML += '<td class="align-middle"><input type="text" class="form-control gate-no" placeholder="Insert" '+ disabled +'></td>';
                                trHTML += '<td class="align-middle"><input type="text" class="form-control challan-no" placeholder="Insert" '+ disabled +'></td>';
                                trHTML += '<td class="align-middle"><input type="file" class="form-control challan-photo" accept="image/*" '+ disabled +'></td>';
                                trHTML += '<td class="align-middle"><input type="file" class="form-control bill-photo" accept="image/*" '+ disabled +'></td>';
                            }

                            trHTML += '<td class="text-center align-middle">' + requisition_item.remarks + '</td>';

                            trHTML += '<td class="align-middle"><input type="text" class="form-control purchase-date" style="width: 150px;" oninput="purchase_date(1, '+ (i+1) +')" data-id="'+ requisition_item.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" '+ disabled +'></td>';
                        trHTML += '</tr>';

                        $('#consumable_table').find('table tbody').empty();
                        $('#consumable_table').find('table').append(trHTML);

                        $('.select-b').select2({
                            width: '100%',
                            placeholder: 'Choose'
                        });
                    });
                });
            }

            // PROCEED CONSUMABLE
            function proceed_consumable(){
                let user_category = '<?= $user_category ?>';

                $('#consumable_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(6)').find('input').val(),
                        party = $(this).find('td:eq(7)').find('select').val(),
                        gate_no = $(this).find('td:eq(8)').find('input').val(),
                        challan_no = $(this).find('td:eq(9)').find('input').val(),
                        purchase_date = (parseInt(user_category) < 4) ? $(this).find('td:eq(13)').find('input').val() : $(this).find('td:eq(9)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if((parseInt(user_category) < 4 && listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || purchase_date === '')) || (parseInt(user_category) == 4 && listed == 1 && (qty === '' || price === '' || party === '' || purchase_date === '')))
                            $(this).find('td:eq(0)').find('span').removeClass('d-none');
                        else
                           $(this).find('td:eq(0)').find('span').addClass('d-none');
                    }
                });

                let flag = 2;

                // GET SELECTED ROW
                $('#consumable_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                    if(listed_disabled != 'disabled'){
                        if(listed == 1){
                            flag = 2;

                            return false;
                        } else{
                            flag = 0;
                        }
                    }
                });

                // GET SELECTED ROW DATA
                $('#consumable_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(6)').find('input').val(),
                        party = $(this).find('td:eq(7)').find('select').val(),
                        gate_no = $(this).find('td:eq(8)').find('input').val(),
                        challan_no = $(this).find('td:eq(9)').find('input').val(),
                        purchase_date = (parseInt(user_category) < 4) ? $(this).find('td:eq(13)').find('input').val() : $(this).find('td:eq(9)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if((parseInt(user_category) < 4 && listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || purchase_date === '')) || (parseInt(user_category) == 4 && listed == 1 && (qty === '' || price === '' || party === '' || purchase_date === ''))){
                            flag = 1;

                            return false;
                        }
                    }
                });

                if(flag == 0){
                    Swal.fire({
                        title: 'Error',
                        text: 'Please select a row!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else if(flag == 1){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    let table_data = '';

                    let table_object = $('#consumable_table tbody tr').map(function(i){
                        let row = '',
                            listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                            listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                let challan_file_data = '',
                                    bill_file_data = '';

                                if(parseInt(user_category) < 4){
                                    let challan_photo = $(this).find('td:eq(10)').find('input').prop('files')[0],
                                        bill_photo = $(this).find('td:eq(11)').find('input').prop('files')[0];

                                    if(challan_photo != undefined)
                                        challan_file_data = challan_photo.name;

                                    if(bill_photo != undefined)
                                        bill_file_data = bill_photo.name;
                                }

                                row = {
                                    'listed': listed,
                                    'required_for': $(this).find('td:eq(2)').html(),
                                    'parts': $(this).find('td:eq(3)').html(),
                                    'qty': $(this).find('td:eq(4)').find('input').val(),
                                    'pur': $(this).find('td:eq(4)').find('.purchased').find('strong:eq(0)').html(),
                                    'req': $(this).find('td:eq(4)').find('.purchased').find('strong:eq(1)').html(),
                                    'use': $(this).find('td:eq(5)').html(),
                                    'price': $(this).find('td:eq(6)').find('input').val(),
                                    'party': $(this).find('td:eq(7)').find('select').val(),
                                    'gate_no': (parseInt(user_category) < 4) ? $(this).find('td:eq(8)').find('input').val() : '',
                                    'challan_no': (parseInt(user_category) < 4) ? $(this).find('td:eq(9)').find('input').val() : '',
                                    'challan_photo': challan_file_data,
                                    'bill_photo': bill_file_data,
                                    'remarks': (parseInt(user_category) < 4) ? $(this).find('td:eq(12)').html() : $(this).find('td:eq(8)').html(),
                                    'purchase_date': (parseInt(user_category) < 4) ? $(this).find('td:eq(13)').find('input').val() : $(this).find('td:eq(9)').find('input').val()
                                };
                            } else{
                                return;
                            }
                        } else{
                            return;
                        }
                        
                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                table_data += '<tr>';
                                    table_data += '<td>' + (i+1) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(2)').html() + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(3)').html() + '</td>';
                                    table_data += '<td>' + row.qty + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + parseInt($(this).find('td:eq(4)').find('.in-stock').html().replace(/[^0-9]/gi, ''), 10) + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + row.use + '</td>';
                                    table_data += '<td><i class="mdi mdi-currency-bdt"></i>' + parseFloat(row.price).toFixed(2) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(7)').find('.party-name option:selected').html() + '</td>';

                                    if(parseInt(user_category) < 4){
                                        table_data += '<td>' + row.gate_no + '</td>';
                                        table_data += '<td>' + row.challan_no + '</td>';
                                    }

                                    table_data += '<td>' + row.remarks + '</td>';
                                    table_data += '<td>' + row.purchase_date + '</td>';
                                table_data += '</tr>';
                            }
                        }

                        return row;
                    }).get();

                    let table = '<table class="table table-responsive table-striped table-bordered consumable-table">';
                        table += '<thead>';
                            table += '<tr>';
                                table += '<th>SL.</td>';
                                table += '<th>Required For</td>';
                                table += '<th>Parts</td>';
                                table += '<th>Quantity</td>';
                                table += '<th>In Stock</td>';
                                table += '<th>Where to Use</td>';
                                table += '<th>Price</td>';
                                table += '<th>Party</td>';

                                if(parseInt(user_category) < 4){
                                    table += '<th>Gate No.</td>';
                                    table += '<th>Challan No.</td>';
                                }

                                table += '<th>Remarks</td>';
                                table += '<th>Purchase Date</td>';
                            table += '</tr>';
                        table += '</thead>';
                        table += '<tbody id="consumable_data">';
                            table += table_data;
                        table += '</tbody>';
                    table += '</table>';

                    let t;

                    Swal.fire({
                        title: 'Proceeding',
                        text: 'Please wait...',
                        timer: 100,
                        allowOutsideClick: false,
                        onBeforeOpen: function(){
                            Swal.showLoading(), t = setInterval(function(){
                            }, 100);
                        }
                    }).then(function(){
                        Swal.fire({
                            title: 'Purchase Data',
                            html: table,
                            type: 'info',
                            width: 'auto',
                            showCancelButton: true,
                            showCloseButton: true,
                            confirmButtonColor: '#5cb85c',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Submit'
                        }).then(function(result){
                            if(result.value){
                                let purchase_table_data = JSON.stringify(table_object);
                                
                                let requisition_id = 0;
                                if(requisition_data)
                                    requisition_id = requisition_data.Reply[0].requisition_id;

                                let flag = 1,
                                    msg = '';

                                // GET SELECTED ROWS IMAGE DATA
                                if(parseInt(user_category) < 4){
                                    $('#consumable_table tbody tr').each(function(){
                                        let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                                            listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                                        if(listed_disabled != 'disabled'){
                                            if(listed == 1){
                                                let challan_photo = $(this).find('td:eq(10)').find('input').prop('files')[0],
                                                    bill_photo = $(this).find('td:eq(11)').find('input').prop('files')[0];
                                                    
                                                if(challan_photo != undefined){
                                                    let form_data = new FormData();
                                                    form_data.append('file', challan_photo);

                                                    $.ajax({
                                                        url: '../../api/uploadImage',
                                                        method: 'post',
                                                        data: form_data,
                                                        dataType: 'json',
                                                        contentType : false,
                                                        processData: false,
                                                        cache: false,
                                                        async: false,
                                                        success: function(data){
                                                            if(data.Type === 'error'){
                                                                flag = 0;
                                                                msg = data.Reply;
                                                            }
                                                        }
                                                    });

                                                    if(flag == 0)
                                                        return false;
                                                }

                                                if(bill_photo != undefined){
                                                    let form_data = new FormData();
                                                    form_data.append('file', bill_photo);

                                                    $.ajax({
                                                        url: '../../api/uploadImage',
                                                        method: 'post',
                                                        data: form_data,
                                                        dataType: 'json',
                                                        contentType : false,
                                                        processData: false,
                                                        cache: false,
                                                        async: false,
                                                        success: function(data){
                                                            if(data.Type === 'error'){
                                                                flag = 0;
                                                                msg = data.Reply;
                                                            }
                                                        }
                                                    });

                                                    if(flag == 0)
                                                        return false;
                                                }
                                            }
                                        }
                                    });
                                }

                                if(flag == 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: msg,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Please insert valid image.'
                                    });
                                } else{
                                    let purchased_parts = 0;

                                    $.each(table_object, function(i, obj_data){
                                        if((parseInt(obj_data.qty) + parseInt(obj_data.pur)) == parseInt(obj_data.req))
                                            purchased_parts++;
                                    });

                                    $.ajax({
                                        url: '../../api/interactionController',
                                        method: 'post',
                                        data: {
                                            interact_type: 'add',
                                            interact: 'purchase_con',
                                            requisition_id: requisition_id,
                                            requisitioned_parts: $('#consumable_table tbody tr').length,
                                            purchased_parts: purchased_parts,
                                            purchase_data: purchase_table_data
                                        },
                                        dataType: 'json',
                                        cache: false,
                                        success: function(data){
                                            if(data.Type == 'success'){
                                                let t;

                                                Swal.fire({
                                                    title: 'Adding Purchase Data',
                                                    text: 'Please wait...',
                                                    timer: 100,
                                                    allowOutsideClick: false,
                                                    onBeforeOpen: function(){
                                                        Swal.showLoading(), t = setInterval(function(){
                                                        }, 100);
                                                    }
                                                }).then(function(){
                                                    Swal.fire({
                                                        title: 'Success',
                                                        text: data.Reply,
                                                        type: 'success',
                                                        width: 450,
                                                        showCloseButton: false,
                                                        allowOutsideClick: false,
                                                        confirmButtonColor: '#5cb85c',
                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                    }).then((result) => {
                                                        if(result.value){
                                                            window.location.reload(true);
                                                        }
                                                    });
                                                });
                                            } else if(data.Type == 'error'){
                                                Swal.fire({
                                                    title: 'Error',
                                                    text: data.Reply,
                                                    type: 'error',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                    footer: 'Please insert valid data.'
                                                });
                                            } else{
                                                Swal.fire({
                                                    title: 'Info',
                                                    text: 'Server is under maintenance. Please try again later!',
                                                    type: 'info',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                });
                                            }

                                            return false;
                                          },
                                    });
                                }
                            }
                        });
                    });
                }
            }

            let requisition_data2 = '';

            // SPARE PURCHASE
            function purchase_spr(ele){
                let user_category = '<?= $user_category ?>';

                $('#first').removeClass('active');
                $('#second').addClass('fade active show');

                let t;

                Swal.fire({
                    title: 'Fetching Purchase Data',
                    text: 'Please wait...',
                    timer: 100,
                    allowOutsideClick: false,
                    onBeforeOpen: function(){
                        Swal.showLoading(), t = setInterval(function(){
                        }, 100);
                    }
                }).then(function(){
                    purchase_date = '';

                    let requisition_id = ele;

                    $.ajax({
                        url: '../../api/purchase',
                        method: 'post',
                        data: {
                            purchase_data_type: 'fetch_requisition_spr',
                            requisition_id: requisition_id
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                requisition_data2 = data;
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Please try again.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });

                    let trHTML = '';

                    $.each(requisition_data2.Reply, function(i, requisition_item){
                        trHTML += '<tr '+ ((requisition_item.parts_id == requisition_item.purchased_parts_id && requisition_item.purchased_parts_qty == requisition_item.r_qty) ? 'style=" background: #cfe8cf;"' : '') +'>';
                            trHTML += '<td class="align-middle">' + (i+1) + ' <span class="text-danger d-none">*</span></td>';

                            let checked = '',
                                disabled = '';

                            if(requisition_item.parts_id == requisition_item.purchased_parts_id && requisition_item.purchased_parts_qty == requisition_item.r_qty){
                                checked = 'checked';
                                disabled = 'disabled';
                            }
                            
                            trHTML += '<td class="align-middle"><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input add-to-list-2" id="add_to_list_2'+i+'" onclick="add_to_list_2()" '+ checked + ' ' + disabled +'><label class="custom-control-label" for="add_to_list_2'+i+'"></label></div></td>';

                            if(requisition_item.required_for == 1)
                                required_for = 'BCP-CCM';
                            else if(requisition_item.required_for == 2)
                                required_for = 'BCP-Furnace';
                            else if(requisition_item.required_for == 3)
                                required_for = 'Concast-CCM';
                            else if(requisition_item.required_for == 4)
                                required_for = 'Concast-Furnace';
                            else if(requisition_item.required_for == 5)
                                required_for = 'HRM';
                            else if(requisition_item.required_for == 6)
                                required_for = 'HRM Unit-2';
                            else if(requisition_item.required_for == 7)
                                required_for = 'Lal Masjid';
                            else if(requisition_item.required_for == 8)
                                required_for = 'Sonargaon';
                            else if(requisition_item.required_for == 9)
                                required_for = 'General';

                            trHTML += '<td class="text-center align-middle">' + required_for + '</td>';

                            $.ajax({
                                url: '../../api/parts',
                                method: 'post',
                                data: {
                                    parts_data_type: 'fetch_all'
                                },
                                dataType: 'json',
                                cache: false,
                                async: false,
                                success: function(data){
                                    if(data.Type == 'success'){
                                        $.each(data.Reply, function(i, parts_item){
                                            if(parts_item.parts_category == 1 && requisition_item.parts_id == parts_item.parts_id){
                                                trHTML += '<td class="text-center align-middle">' + parts_item.parts_name + '</td>';

                                                return false;
                                            }
                                        });
                                    } else if(data.Type == 'error'){
                                        Swal.fire({
                                            title: 'Error',
                                            text: data.Reply,
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please try again.'
                                        });
                                    } else{
                                        Swal.fire({
                                            title: 'Info',
                                            text: 'Server is under maintenance. Please try again later!',
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        });
                                    }

                                    return false;
                                }
                            });

                            trHTML += '<td class="align-middle text-center">';
                                trHTML += '<div class="input-group">';
                                    trHTML += '<input type="number" class="form-control qty-2" placeholder="Insert" oninput="qty(2, '+ (requisition_item.r_qty - requisition_item.purchased_parts_qty) +')" '+ disabled +'>';
                                    trHTML += '<div class="input-group-prepend">';
                                        let parts_unit = '';

                                        if(requisition_item.parts_unit == 1)
                                            parts_unit = 'Bag';
                                        else if(requisition_item.parts_unit == 2)
                                            parts_unit = 'Box';
                                        else if(requisition_item.parts_unit == 3)
                                            parts_unit = 'Box/Pcs';
                                        else if(requisition_item.parts_unit == 4)
                                            parts_unit = 'Bun';
                                        else if(requisition_item.parts_unit == 5)
                                            parts_unit = 'Bundle';
                                        else if(requisition_item.parts_unit == 6)
                                            parts_unit = 'Can';
                                        else if(requisition_item.parts_unit == 7)
                                            parts_unit = 'Cartoon';
                                        else if(requisition_item.parts_unit == 8)
                                            parts_unit = 'Challan';
                                        else if(requisition_item.parts_unit == 9)
                                            parts_unit = 'Coil';
                                        else if(requisition_item.parts_unit == 10)
                                            parts_unit = 'Drum';
                                        else if(requisition_item.parts_unit == 11)
                                            parts_unit = 'Feet';
                                        else if(requisition_item.parts_unit == 12)
                                            parts_unit = 'Gallon';
                                        else if(requisition_item.parts_unit == 13)
                                            parts_unit = 'Item';
                                        else if(requisition_item.parts_unit == 14)
                                            parts_unit = 'Job';
                                        else if(requisition_item.parts_unit == 15)
                                            parts_unit = 'Kg';
                                        else if(requisition_item.parts_unit == 16)
                                            parts_unit = 'Kg/Bundle';
                                        else if(requisition_item.parts_unit == 17)
                                            parts_unit = 'Kv';
                                        else if(requisition_item.parts_unit == 18)
                                            parts_unit = 'Lbs';
                                        else if(requisition_item.parts_unit == 19)
                                            parts_unit = 'Ltr';
                                        else if(requisition_item.parts_unit == 20)
                                            parts_unit = 'Mtr';
                                        else if(requisition_item.parts_unit == 21)
                                            parts_unit = 'Pack';
                                        else if(requisition_item.parts_unit == 22)
                                            parts_unit = 'Pack/Pcs';
                                        else if(requisition_item.parts_unit == 23)
                                            parts_unit = 'Pair';
                                        else if(requisition_item.parts_unit == 24)
                                            parts_unit = 'Pcs';
                                        else if(requisition_item.parts_unit == 25)
                                            parts_unit = 'Pound';
                                        else if(requisition_item.parts_unit == 26)
                                            parts_unit = 'Qty';
                                        else if(requisition_item.parts_unit == 27)
                                            parts_unit = 'Roll';
                                        else if(requisition_item.parts_unit == 28)
                                            parts_unit = 'Set';
                                        else if(requisition_item.parts_unit == 29)
                                            parts_unit = 'Truck';
                                        else if(requisition_item.parts_unit == 30)
                                            parts_unit = 'Unit';
                                        else if(requisition_item.parts_unit == 31)
                                            parts_unit = 'Yeard';
                                        else if(requisition_item.parts_unit == 32)
                                            parts_unit = '(Unit Unknown)';
                                        else if(requisition_item.parts_unit == 33)
                                            parts_unit = 'SFT';
                                        else if(requisition_item.parts_unit == 34)
                                            parts_unit = 'RFT';
                                        else if(requisition_item.parts_unit == 35)
                                            parts_unit = 'CFT';

                                        trHTML += '<div class="input-group-text"><span id="unit_2">' + parts_unit + '</span></div>';
                                    trHTML += '</div>';
                                trHTML += '</div>';

                                trHTML += '<span class="float-left mt-1 text-primary w-100 in-stock-2">In Stock: <strong>' + requisition_item.i_qty + '</strong> ' + parts_unit + '</span><span class="float-left text-info w-100 purchased-2">Purchased: <strong>' + requisition_item.purchased_parts_qty + '</strong> of <strong>' + requisition_item.r_qty + '</strong> ' + parts_unit + '</span>';
                            trHTML += '</td>';

                            trHTML += '<td class="text-center align-middle">' + requisition_item.old_spare_details + '</td>';
                            trHTML += '<td class="text-center align-middle">' + ((requisition_item.status == 1) ? 'Repairable' : 'Unusual') + '</td>';
                            trHTML += '<td class="align-middle"><input type="number" class="form-control price-2" placeholder="Insert" oninput="price(2)" '+ disabled +'></td>';

                            let party_name_option = '';
                            $.ajax({
                                url: '../../api/party',
                                method: 'post',
                                data: {
                                    party_data_type: 'fetch_all'
                                },
                                dataType: 'json',
                                cache: false,
                                async: false,
                                success: function(data){
                                    if(data.Type == 'success'){
                                        $.each(data.Reply, function(i, party_item){
                                            party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'">'+party_item.party_name+'</option>';
                                        });
                                    } else if(data.Type == 'error'){
                                        Swal.fire({
                                            title: 'Error',
                                            text: data.Reply,
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please try again.'
                                        });
                                    } else{
                                        Swal.fire({
                                            title: 'Info',
                                            text: 'Server is under maintenance. Please try again later!',
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        });
                                    }

                                    return false;
                                }
                            });
                                
                            trHTML += '<td class="align-middle">';
                                trHTML += '<select class="select-b party-name-2" onchange="party_name(this.value, 2, ' + (i+1) + ')" '+ disabled +'>';
                                    trHTML += '<option value="">Choose</option>';
                                    trHTML += party_name_option;
                                trHTML += '</select>';
                                trHTML += '<span class="float-left mt-1 text-primary remarks-2"></span>';
                            trHTML += '</td>';

                            if(parseInt(user_category) < 4){
                                trHTML += '<td class="align-middle"><input type="text" class="form-control gate-no-2" placeholder="Insert" '+ disabled +'></td>';
                                trHTML += '<td class="align-middle"><input type="text" class="form-control challan-no-2" placeholder="Insert" '+ disabled +'></td>';
                                trHTML += '<td class="align-middle"><input type="file" class="form-control challan-photo-2" accept="image/*" '+ disabled +'></td>';
                                trHTML += '<td class="align-middle"><input type="file" class="form-control bill-photo-2" accept="image/*" '+ disabled +'></td>';
                            }

                            trHTML += '<td class="text-center align-middle">' + requisition_item.remarks + '</td>';
                            trHTML += '<td class="align-middle"><input type="text" class="form-control purchase-date-2" style="width: 150px;" oninput="purchase_date(2, '+ (i+1) +')" data-id="'+ requisition_item.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" '+ disabled +'></td>';
                        trHTML += '</tr>';
                            
                        $('#spare_table').find('table tbody').empty();
                        $('#spare_table').find('table').append(trHTML);

                        $('.select-b').select2({
                            width: '100%',
                            placeholder: 'Choose'
                        });
                    });
                });
            }

            // PROCEED SPARE
            function proceed_spare(){
                let user_category = '<?= $user_category ?>';

                $('#spare_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(7)').find('input').val(),
                        party = $(this).find('td:eq(8)').find('select').val(),
                        gate_no = $(this).find('td:eq(9)').find('input').val(),
                        challan_no = $(this).find('td:eq(10)').find('input').val(),
                        purchase_date = (parseInt(user_category) < 4) ? $(this).find('td:eq(14)').find('input').val() : $(this).find('td:eq(9)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if((parseInt(user_category) < 4 && listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || purchase_date === '')) || (parseInt(user_category) == 4 && listed == 1 && (qty === '' || price === '' || party === '' || purchase_date === '')))
                            $(this).find('td:eq(0)').find('span').removeClass('d-none');
                        else
                           $(this).find('td:eq(0)').find('span').addClass('d-none');
                    }
                });

                let flag = 2;

                // GET SELECTED ROW
                $('#spare_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                    if(listed_disabled != 'disabled'){
                        if(listed == 1){
                            flag = 2;

                            return false;
                        } else{
                            flag = 0;
                        }
                    }
                });

                // GET SELECTED ROW DATA
                $('#spare_table tbody tr').each(function(){
                    let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                        listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled'),
                        qty = $(this).find('td:eq(4)').find('input').val(),
                        price = $(this).find('td:eq(7)').find('input').val(),
                        party = $(this).find('td:eq(8)').find('select').val(),
                        gate_no = $(this).find('td:eq(9)').find('input').val(),
                        challan_no = $(this).find('td:eq(10)').find('input').val(),
                        purchase_date = (parseInt(user_category) < 4) ? $(this).find('td:eq(14)').find('input').val() : $(this).find('td:eq(9)').find('input').val();

                    if(listed_disabled != 'disabled'){
                        if((parseInt(user_category) < 4 && listed == 1 && (qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || purchase_date === '')) || (parseInt(user_category) == 4 && listed == 1 && (qty === '' || price === '' || party === '' || purchase_date === ''))){
                            flag = 1;

                            return false;
                        }
                    }
                });

                if(flag == 0){
                    Swal.fire({
                        title: 'Error',
                        text: 'Please select a row!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else if(flag == 1){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    let table_data = '';

                    let table_object = $('#spare_table tbody tr').map(function(i){
                        let row = '',
                            listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                            listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                let challan_file_data = '',
                                    bill_file_data = '';

                                if(parseInt(user_category) < 4){
                                    let challan_photo = $(this).find('td:eq(11)').find('input').prop('files')[0],
                                        bill_photo = $(this).find('td:eq(12)').find('input').prop('files')[0];

                                    if(challan_photo != undefined)
                                        challan_file_data = challan_photo.name;

                                    if(bill_photo != undefined)
                                        bill_file_data = bill_photo.name;
                                }

                                row = {
                                    'listed': listed,
                                    'required_for': $(this).find('td:eq(2)').html(),
                                    'parts': $(this).find('td:eq(3)').html(),
                                    'qty': $(this).find('td:eq(4)').find('input').val(),
                                    'pur': $(this).find('td:eq(4)').find('.purchased-2').find('strong:eq(0)').html(),
                                    'req': $(this).find('td:eq(4)').find('.purchased-2').find('strong:eq(1)').html(),
                                    'old': $(this).find('td:eq(5)').html(),
                                    'status': $(this).find('td:eq(6)').html(),
                                    'price': $(this).find('td:eq(7)').find('input').val(),
                                    'party': $(this).find('td:eq(8)').find('select').val(),
                                    'gate_no': (parseInt(user_category) < 4) ? $(this).find('td:eq(9)').find('input').val() : '',
                                    'challan_no': (parseInt(user_category) < 4) ? $(this).find('td:eq(10)').find('input').val() : '',
                                    'challan_photo': challan_file_data,
                                    'bill_photo': bill_file_data,
                                    'remarks': (parseInt(user_category) < 4) ? $(this).find('td:eq(13)').html() : $(this).find('td:eq(9)').html(),
                                    'purchase_date': (parseInt(user_category) < 4) ? $(this).find('td:eq(14)').find('input').val() : $(this).find('td:eq(10)').find('input').val()
                                };
                            } else{
                                return;
                            }
                        } else{
                            return;
                        }
                        
                        if(listed_disabled != 'disabled'){
                            if(listed == 1){
                                table_data += '<tr>';
                                    table_data += '<td>' + (i+1) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(2)').html() + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(3)').html() + '</td>';
                                    table_data += '<td>' + row.qty + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + parseInt($(this).find('td:eq(4)').find('.in-stock-2').html().replace(/[^0-9]/gi, ''), 10) + ' ' + $(this).find('td:eq(4)').find('span').html() + '</td>';
                                    table_data += '<td>' + row.old + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(6)').html() + '</td>';
                                    table_data += '<td><i class="mdi mdi-currency-bdt"></i>' + parseFloat(row.price).toFixed(2) + '</td>';
                                    table_data += '<td>' + $(this).find('td:eq(8)').find('.party-name-2 option:selected').html() + '</td>';
                                    
                                    if(parseInt(user_category) < 4){
                                        table_data += '<td>' + row.gate_no + '</td>';
                                        table_data += '<td>' + row.challan_no + '</td>';
                                    }

                                    table_data += '<td>' + row.remarks + '</td>';
                                    table_data += '<td>' + row.purchase_date + '</td>';
                                table_data += '</tr>';
                            }
                        }

                        return row;
                    }).get();

                    let table = '<table class="table table-responsive table-striped table-bordered spare-table">';
                        table += '<thead>';
                            table += '<tr>';
                                table += '<th>SL.</td>';
                                table += '<th>Required For</td>';
                                table += '<th>Parts</td>';
                                table += '<th>Quantity</td>';
                                table += '<th>In Stock</td>';
                                table += '<th>Old Spares Details</td>';
                                table += '<th>Status</td>';
                                table += '<th>Price</td>';
                                table += '<th>Party</td>';
                                
                                if(parseInt(user_category) < 4){
                                    table += '<th>Gate No.</td>';
                                    table += '<th>Challan No.</td>';
                                }
                                
                                table += '<th>Remarks</td>';
                                table += '<th>Purchase Date</td>';
                            table += '</tr>';
                        table += '</thead>';
                        table += '<tbody id="spare_data">';
                            table += table_data;
                        table += '</tbody>';
                    table += '</table>';

                    let t;

                    Swal.fire({
                        title: 'Proceeding',
                        text: 'Please wait...',
                        timer: 100,
                        allowOutsideClick: false,
                        onBeforeOpen: function(){
                            Swal.showLoading(), t = setInterval(function(){
                            }, 100);
                        }
                    }).then(function(){
                        Swal.fire({
                            title: 'Purchase Data',
                            html: table,
                            type: 'info',
                            width: 'auto',
                            showCancelButton: true,
                            showCloseButton: true,
                            confirmButtonColor: '#5cb85c',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Submit'
                        }).then(function(result){
                            if(result.value){
                                let purchase_table_data = JSON.stringify(table_object);
                                
                                let requisition_id = 0;
                                if(requisition_data2)
                                    requisition_id = requisition_data2.Reply[0].requisition_id;

                                let flag = 1,
                                    msg = '';

                                // GET SELECTED ROWS IMAGE DATA
                                if(parseInt(user_category)){
                                    $('#spare_table tbody tr').each(function(){
                                        let listed = $(this).find('td:eq(1)').find('input').is(':checked') ? 1 : 0,
                                            listed_disabled = $(this).find('td:eq(1)').find('input').attr('disabled');

                                        if(listed_disabled != 'disabled'){
                                            if(listed == 1){
                                                let challan_photo = $(this).find('td:eq(11)').find('input').prop('files')[0],
                                                    bill_photo = $(this).find('td:eq(12)').find('input').prop('files')[0];
                                                    
                                                if(challan_photo != undefined){
                                                    let form_data = new FormData();
                                                    form_data.append('file', challan_photo);

                                                    $.ajax({
                                                        url: '../../api/uploadImage',
                                                        method: 'post',
                                                        data: form_data,
                                                        dataType: 'json',
                                                        contentType : false,
                                                        processData: false,
                                                        cache: false,
                                                        async: false,
                                                        success: function(data){
                                                            if(data.Type === 'error'){
                                                                flag = 0;
                                                                msg = data.Reply;
                                                            }
                                                        }
                                                    });

                                                    if(flag == 0)
                                                        return false;
                                                }

                                                if(bill_photo != undefined){
                                                    let form_data = new FormData();
                                                    form_data.append('file', bill_photo);

                                                    $.ajax({
                                                        url: '../../api/uploadImage',
                                                        method: 'post',
                                                        data: form_data,
                                                        dataType: 'json',
                                                        contentType : false,
                                                        processData: false,
                                                        cache: false,
                                                        async: false,
                                                        success: function(data){
                                                            if(data.Type === 'error'){
                                                                flag = 0;
                                                                msg = data.Reply;
                                                            }
                                                        }
                                                    });

                                                    if(flag == 0)
                                                        return false;
                                                }
                                            }
                                        }
                                    });
                                }

                                if(flag == 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: msg,
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: 'Please insert valid image.'
                                    });
                                } else{
                                    let purchased_parts = 0;

                                    $.each(table_object, function(i, obj_data){
                                        if((parseInt(obj_data.qty) + parseInt(obj_data.pur)) == parseInt(obj_data.req))
                                            purchased_parts++;
                                    });

                                    $.ajax({
                                        url: '../../api/interactionController',
                                        method: 'post',
                                        data: {
                                            interact_type: 'add',
                                            interact: 'purchase_spr',
                                            requisition_id: requisition_id,
                                            requisitioned_parts: $('#spare_table tbody tr').length,
                                            purchased_parts: purchased_parts,
                                            purchase_data: purchase_table_data
                                        },
                                        dataType: 'json',
                                        cache: false,
                                        success: function(data){
                                            if(data.Type == 'success'){
                                                let t;

                                                Swal.fire({
                                                    title: 'Adding Purchase Data',
                                                    text: 'Please wait...',
                                                    timer: 100,
                                                    allowOutsideClick: false,
                                                    onBeforeOpen: function(){
                                                        Swal.showLoading(), t = setInterval(function(){
                                                        }, 100);
                                                    }
                                                }).then(function(){
                                                    Swal.fire({
                                                        title: 'Success',
                                                        text: data.Reply,
                                                        type: 'success',
                                                        width: 450,
                                                        showCloseButton: false,
                                                        allowOutsideClick: false,
                                                        confirmButtonColor: '#5cb85c',
                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                    }).then((result) => {
                                                        if(result.value){
                                                            window.location.reload(true);
                                                        }
                                                    });
                                                });
                                            } else if(data.Type == 'error'){
                                                Swal.fire({
                                                    title: 'Error',
                                                    text: data.Reply,
                                                    type: 'error',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                    footer: 'Please insert valid data.'
                                                });
                                            } else{
                                                Swal.fire({
                                                    title: 'Info',
                                                    text: 'Server is under maintenance. Please try again later!',
                                                    type: 'info',
                                                    width: 450,
                                                    showCloseButton: true,
                                                    confirmButtonColor: '#5cb85c',
                                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                });
                                            }

                                            return false;
                                          },
                                    });
                                }
                            }
                        });
                    });
                }
            }

            // VIEW CONSUMABLE PURCHASE
            function view_purchase_con(ele){
                let requisition_id = ele;

                $.ajax({
                    url: '../../api/purchase',
                    method: 'post',
                    data: {
                        purchase_data_type: 'fetch_purchase_con',
                        requisition_id: requisition_id
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Purchase Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            }).then(function(){
                                let trHTML = '',
                                    required_for = '',
                                    parts_unit = '',
                                    user_category = '<?= $user_category ?>';

                                $.each(data.Reply1, function(i, purchase_item_1){
                                    if(purchase_item_1.required_for == 1)
                                        required_for = 'BCP-CCM';
                                    else if(purchase_item_1.required_for == 2)
                                        required_for = 'BCP-Furnace';
                                    else if(purchase_item_1.required_for == 3)
                                        required_for = 'Concast-CCM';
                                    else if(purchase_item_1.required_for == 4)
                                        required_for = 'Concast-Furnace';
                                    else if(purchase_item_1.required_for == 5)
                                        required_for = 'HRM';
                                    else if(purchase_item_1.required_for == 6)
                                        required_for = 'HRM Unit-2';
                                    else if(purchase_item_1.required_for == 7)
                                        required_for = 'Lal Masjid';
                                    else if(purchase_item_1.required_for == 8)
                                        required_for = 'Sonargaon';
                                    else if(purchase_item_1.required_for == 9)
                                        required_for = 'General';

                                    if(purchase_item_1.parts_unit == 1)
                                        parts_unit = 'Bag';
                                    else if(purchase_item_1.parts_unit == 2)
                                        parts_unit = 'Box';
                                    else if(purchase_item_1.parts_unit == 3)
                                        parts_unit = 'Box/Pcs';
                                    else if(purchase_item_1.parts_unit == 4)
                                        parts_unit = 'Bun';
                                    else if(purchase_item_1.parts_unit == 5)
                                        parts_unit = 'Bundle';
                                    else if(purchase_item_1.parts_unit == 6)
                                        parts_unit = 'Can';
                                    else if(purchase_item_1.parts_unit == 7)
                                        parts_unit = 'Cartoon';
                                    else if(purchase_item_1.parts_unit == 8)
                                        parts_unit = 'Challan';
                                    else if(purchase_item_1.parts_unit == 9)
                                        parts_unit = 'Coil';
                                    else if(purchase_item_1.parts_unit == 10)
                                        parts_unit = 'Drum';
                                    else if(purchase_item_1.parts_unit == 11)
                                        parts_unit = 'Feet';
                                    else if(purchase_item_1.parts_unit == 12)
                                        parts_unit = 'Gallon';
                                    else if(purchase_item_1.parts_unit == 13)
                                        parts_unit = 'Item';
                                    else if(purchase_item_1.parts_unit == 14)
                                        parts_unit = 'Job';
                                    else if(purchase_item_1.parts_unit == 15)
                                        parts_unit = 'Kg';
                                    else if(purchase_item_1.parts_unit == 16)
                                        parts_unit = 'Kg/Bundle';
                                    else if(purchase_item_1.parts_unit == 17)
                                        parts_unit = 'Kv';
                                    else if(purchase_item_1.parts_unit == 18)
                                        parts_unit = 'Lbs';
                                    else if(purchase_item_1.parts_unit == 19)
                                        parts_unit = 'Ltr';
                                    else if(purchase_item_1.parts_unit == 20)
                                        parts_unit = 'Mtr';
                                    else if(purchase_item_1.parts_unit == 21)
                                        parts_unit = 'Pack';
                                    else if(purchase_item_1.parts_unit == 22)
                                        parts_unit = 'Pack/Pcs';
                                    else if(purchase_item_1.parts_unit == 23)
                                        parts_unit = 'Pair';
                                    else if(purchase_item_1.parts_unit == 24)
                                        parts_unit = 'Pcs';
                                    else if(purchase_item_1.parts_unit == 25)
                                        parts_unit = 'Pound';
                                    else if(purchase_item_1.parts_unit == 26)
                                        parts_unit = 'Qty';
                                    else if(purchase_item_1.parts_unit == 27)
                                        parts_unit = 'Roll';
                                    else if(purchase_item_1.parts_unit == 28)
                                        parts_unit = 'Set';
                                    else if(purchase_item_1.parts_unit == 29)
                                        parts_unit = 'Truck';
                                    else if(purchase_item_1.parts_unit == 30)
                                        parts_unit = 'Unit';
                                    else if(purchase_item_1.parts_unit == 31)
                                        parts_unit = 'Yeard';
                                    else if(purchase_item_1.parts_unit == 32)
                                        parts_unit = '(Unit Unknown)';
                                    else if(purchase_item_1.parts_unit == 33)
                                        parts_unit = 'SFT';
                                    else if(purchase_item_1.parts_unit == 34)
                                        parts_unit = 'RFT';
                                    else if(purchase_item_1.parts_unit == 35)
                                        parts_unit = 'CFT';

                                    trHTML += '<tr>';
                                        trHTML += '<td class="text-left alert-info" colspan="10">\
                                                        <i class="mdi mdi-drag-variant"></i> Required For: ' + required_for + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Parts Name: ' + purchase_item_1.parts_name + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Old Spares Details:  ' + purchase_item_1.parts_usage + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Remarks: ' + purchase_item_1.remarks + '\
                                                        \
                                                        <span class="float-right">(' + purchase_item_1.purchased_parts_qty + '/' + purchase_item_1.requisitioned_parts_qty + ' Purchased)</span>\
                                                        <div class="progress mt-1 mr-1 progress-sm w-25 float-right" style="background: #fff;">\
                                                            <div class="progress-bar bg-primary" role="progressbar" style="width: ' + ((purchase_item_1.purchased_parts_qty / purchase_item_1.requisitioned_parts_qty) * 100) + '%;" aria-valuenow="' + ((purchase_item_1.purchased_parts_qty / purchase_item_1.requisitioned_parts_qty) * 100) + '" aria-valuemin="0" aria-valuemax="100"></div>\
                                                        </div>\
                                                        \
                                                    </td>';
                                    trHTML += '</tr>';

                                    $.each(data.Reply2, function(j, purchase_item_2){
                                        if(purchase_item_2.parts_id == purchase_item_1.parts_id){
                                            trHTML += '<tr>';
                                                trHTML += '<td class="align-middle text-center">' + (j+1) + '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.parts_qty + ' ' + parts_unit + '</span>';

                                                    trHTML += '<div class="input-group data-input d-none">';
                                                        trHTML += '<input type="number" class="form-control" placeholder="Insert" value="'+purchase_item_2.parts_qty+'" oninput="qty_2(this, ' + purchase_item_2.parts_qty + ', ' + purchase_item_1.requisitioned_parts_qty + ', ' + purchase_item_1.purchase_indx_f + ', ' + purchase_item_1.purchase_indx_l + ')">';
                                                        trHTML += '<div class="input-group-prepend">';
                                                            trHTML += '<div class="input-group-text">' + parts_unit + '</div>';
                                                        trHTML += '</div>';
                                                    trHTML += '</div>';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span"><i class="mdi mdi-currency-bdt"></i>' + purchase_item_2.price + '</span>';

                                                    trHTML += '<input type="number" class="form-control data-input d-none" placeholder="Insert" value="'+purchase_item_2.price+'" oninput="price_2(this, ' + purchase_item_2.price + ')">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.party_name + '</span>';

                                                    trHTML += '<div class="data-input d-none">';
                                                        let party_name_option = '';
                                                        $.ajax({
                                                            url: '../../api/party',
                                                            method: 'post',
                                                            data: {
                                                                party_data_type: 'fetch_all'
                                                            },
                                                            dataType: 'json',
                                                            cache: false,
                                                            async: false,
                                                            success: function(data){
                                                                if(data.Type == 'success'){
                                                                    $.each(data.Reply, function(i, party_item){
                                                                        let selected = (party_item.party_id == purchase_item_2.party_id) ? 'selected' : '';

                                                                        party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'" ' + selected + '>'+party_item.party_name+'</option>';
                                                                    });
                                                                } else if(data.Type == 'error'){
                                                                    Swal.fire({
                                                                        title: 'Error',
                                                                        text: data.Reply,
                                                                        type: 'error',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                                        footer: 'Please try again.'
                                                                    });
                                                                } else{
                                                                    Swal.fire({
                                                                        title: 'Info',
                                                                        text: 'Server is under maintenance. Please try again later!',
                                                                        type: 'info',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                                    });
                                                                }

                                                                return false;
                                                            }
                                                        });

                                                        trHTML += '<select class="select-b" onchange="party_name_2(this)">';
                                                            trHTML += '<option value="">Choose</option>';
                                                            trHTML += party_name_option;
                                                        trHTML += '</select>';
                                                        trHTML += '<span class="float-left mt-1 text-primary remarks"></span>';
                                                    trHTML += '</div>';    
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.gate_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+purchase_item_2.gate_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.challan_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+purchase_item_2.challan_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(purchase_item_2.challan_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + purchase_item_2.challan_photo + '" target="_blank">' + purchase_item_2.challan_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';
                                                
                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(purchase_item_2.bill_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + purchase_item_2.bill_photo + '" target="_blank">' + purchase_item_2.bill_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.purchase_date + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" oninput="purchase_date_2(this)" data-id="'+ purchase_item_2.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" value="'+purchase_item_2.purchase_date+'">';
                                                trHTML += '</td>';

                                                if(user_category != 3){
                                                    trHTML += '<td class="align-middle text-center">';
                                                        trHTML += '<a title="Edit" href="javascript:void(0)" class="btn btn-xs btn-info edt-btn" style="margin-right: 5px;" onclick="edit_btn(this)"><i class="mdi mdi-pencil"></i></a>';
                                                        trHTML += '<a title="Cancel" href="javascript:void(0)" class="btn btn-xs btn-danger d-none cncl-btn" style="margin-right: 5px;" onclick="cancel_btn(this)"><i class="mdi mdi-cancel"></i></a>';
                                                        trHTML += '<a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none upd-btn" onclick="update_purchase_data_con(' + purchase_item_2.purchase_data_id + ', this, ' + purchase_item_1.purchase_id + ')"><i class="mdi mdi-arrow-up-bold-outline"></i></a>';
                                                    trHTML += '</td>';
                                                }
                                            trHTML += '</tr>';
                                        }
                                    });
                                });

                                $('.purchase-records').empty().append(trHTML);

                                $('.select-b').select2({
                                    width: '100%',
                                    placeholder: 'Choose'
                                });
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });
            }

            // UPDATE CONSUMABLE PURCHASE DATA
            function update_purchase_data_con(ele, ele2, ele3){
                let purchase_data_id = ele,
                    qty = $(ele2).closest('tr').find('td:eq(1)').find('input').val(),
                    price = $(ele2).closest('tr').find('td:eq(2)').find('input').val(),
                    party = $(ele2).closest('tr').find('td:eq(3)').find('select').val(),
                    gate_no = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    challan_no = $(ele2).closest('tr').find('td:eq(5)').find('input').val(),
                    challan_photo = $(ele2).closest('tr').find('td:eq(6)').find('input').prop('files')[0],
                    bill_photo = $(ele2).closest('tr').find('td:eq(7)').find('input').prop('files')[0],
                    challan_file_data = '',
                    bill_file_data = '',
                    purchase_date = $(ele2).closest('tr').find('td:eq(8)').find('input').val();

                if(qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || purchase_date === ''){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    if(challan_photo != undefined)
                        challan_file_data = challan_photo.name;

                    if(bill_photo != undefined)
                        bill_file_data = bill_photo.name;

                    if(challan_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', challan_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    if(bill_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', bill_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    $.ajax({
                        url: '../../api/interactionController',
                        method: 'post',
                        data: {
                            interact_type: 'update',
                            interact: 'purchase_data',
                            id: purchase_data_id,
                            purchase_id: ele3,
                            qty: qty,
                            price: price,
                            party: party,
                            gate_no: gate_no,
                            challan_no: challan_no,
                            challan_photo: challan_file_data,
                            bill_photo: bill_file_data,
                            purchase_date: purchase_date
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let t;

                                Swal.fire({
                                    title: 'Updating Purchase Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    Swal.fire({
                                        title: 'Success',
                                        text: data.Reply,
                                        type: 'success',
                                        width: 450,
                                        showCloseButton: false,
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    }).then((result) => {
                                        if(result.value){
                                            window.location.reload(true);
                                        }
                                    });
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Something went wrong.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });
                }
            }

            // VIEW SPARE PURCHASE
            function view_purchase_spr(ele){
                let requisition_id = ele;

                $.ajax({
                    url: '../../api/purchase',
                    method: 'post',
                    data: {
                        purchase_data_type: 'fetch_purchase_spr',
                        requisition_id: requisition_id
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Purchase Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            }).then(function(){
                                let trHTML = '',
                                    required_for = '',
                                    parts_unit = '',
                                    user_category = '<?= $user_category ?>';

                                $.each(data.Reply1, function(i, purchase_item_1){
                                    if(purchase_item_1.required_for == 1)
                                        required_for = 'BCP-CCM';
                                    else if(purchase_item_1.required_for == 2)
                                        required_for = 'BCP-Furnace';
                                    else if(purchase_item_1.required_for == 3)
                                        required_for = 'Concast-CCM';
                                    else if(purchase_item_1.required_for == 4)
                                        required_for = 'Concast-Furnace';
                                    else if(purchase_item_1.required_for == 5)
                                        required_for = 'HRM';
                                    else if(purchase_item_1.required_for == 6)
                                        required_for = 'HRM Unit-2';
                                    else if(purchase_item_1.required_for == 7)
                                        required_for = 'Lal Masjid';
                                    else if(purchase_item_1.required_for == 8)
                                        required_for = 'Sonargaon';
                                    else if(purchase_item_1.required_for == 9)
                                        required_for = 'General';

                                    if(purchase_item_1.parts_unit == 1)
                                        parts_unit = 'Bag';
                                    else if(purchase_item_1.parts_unit == 2)
                                        parts_unit = 'Box';
                                    else if(purchase_item_1.parts_unit == 3)
                                        parts_unit = 'Box/Pcs';
                                    else if(purchase_item_1.parts_unit == 4)
                                        parts_unit = 'Bun';
                                    else if(purchase_item_1.parts_unit == 5)
                                        parts_unit = 'Bundle';
                                    else if(purchase_item_1.parts_unit == 6)
                                        parts_unit = 'Can';
                                    else if(purchase_item_1.parts_unit == 7)
                                        parts_unit = 'Cartoon';
                                    else if(purchase_item_1.parts_unit == 8)
                                        parts_unit = 'Challan';
                                    else if(purchase_item_1.parts_unit == 9)
                                        parts_unit = 'Coil';
                                    else if(purchase_item_1.parts_unit == 10)
                                        parts_unit = 'Drum';
                                    else if(purchase_item_1.parts_unit == 11)
                                        parts_unit = 'Feet';
                                    else if(purchase_item_1.parts_unit == 12)
                                        parts_unit = 'Gallon';
                                    else if(purchase_item_1.parts_unit == 13)
                                        parts_unit = 'Item';
                                    else if(purchase_item_1.parts_unit == 14)
                                        parts_unit = 'Job';
                                    else if(purchase_item_1.parts_unit == 15)
                                        parts_unit = 'Kg';
                                    else if(purchase_item_1.parts_unit == 16)
                                        parts_unit = 'Kg/Bundle';
                                    else if(purchase_item_1.parts_unit == 17)
                                        parts_unit = 'Kv';
                                    else if(purchase_item_1.parts_unit == 18)
                                        parts_unit = 'Lbs';
                                    else if(purchase_item_1.parts_unit == 19)
                                        parts_unit = 'Ltr';
                                    else if(purchase_item_1.parts_unit == 20)
                                        parts_unit = 'Mtr';
                                    else if(purchase_item_1.parts_unit == 21)
                                        parts_unit = 'Pack';
                                    else if(purchase_item_1.parts_unit == 22)
                                        parts_unit = 'Pack/Pcs';
                                    else if(purchase_item_1.parts_unit == 23)
                                        parts_unit = 'Pair';
                                    else if(purchase_item_1.parts_unit == 24)
                                        parts_unit = 'Pcs';
                                    else if(purchase_item_1.parts_unit == 25)
                                        parts_unit = 'Pound';
                                    else if(purchase_item_1.parts_unit == 26)
                                        parts_unit = 'Qty';
                                    else if(purchase_item_1.parts_unit == 27)
                                        parts_unit = 'Roll';
                                    else if(purchase_item_1.parts_unit == 28)
                                        parts_unit = 'Set';
                                    else if(purchase_item_1.parts_unit == 29)
                                        parts_unit = 'Truck';
                                    else if(purchase_item_1.parts_unit == 30)
                                        parts_unit = 'Unit';
                                    else if(purchase_item_1.parts_unit == 31)
                                        parts_unit = 'Yeard';
                                    else if(purchase_item_1.parts_unit == 32)
                                        parts_unit = '(Unit Unknown)';
                                    else if(purchase_item_1.parts_unit == 33)
                                        parts_unit = 'SFT';
                                    else if(purchase_item_1.parts_unit == 34)
                                        parts_unit = 'RFT';
                                    else if(purchase_item_1.parts_unit == 35)
                                        parts_unit = 'CFT';

                                    trHTML += '<tr>';
                                        trHTML += '<td class="text-left alert-info" colspan="10">\
                                                        <i class="mdi mdi-drag-variant"></i> Required For: ' + required_for + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Parts Name: ' + purchase_item_1.parts_name + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Old Spares Details:  ' + purchase_item_1.old_spare_details + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Status:  ' + purchase_item_1.status + '&emsp;&emsp;\
                                                        <i class="mdi mdi-drag-variant"></i> Remarks: ' + purchase_item_1.remarks + '\
                                                        \
                                                        <span class="float-right">(' + purchase_item_1.purchased_parts_qty + '/' + purchase_item_1.requisitioned_parts_qty + ' purchased)</span>\
                                                        <div class="progress mt-1 mr-1 progress-sm w-25 float-right" style="background: #fff;">\
                                                            <div class="progress-bar bg-primary" role="progressbar" style="width: ' + ((purchase_item_1.purchased_parts_qty / purchase_item_1.requisitioned_parts_qty) * 100) + '%;" aria-valuenow="' + ((purchase_item_1.purchased_parts_qty / purchase_item_1.requisitioned_parts_qty) * 100) + '" aria-valuemin="0" aria-valuemax="100"></div>\
                                                        </div>\
                                                        \
                                                    </td>';
                                    trHTML += '</tr>';

                                    $.each(data.Reply2, function(j, purchase_item_2){
                                        if(purchase_item_2.parts_id == purchase_item_1.parts_id){
                                            trHTML += '<tr>';
                                                trHTML += '<td class="align-middle text-center">' + (j+1) + '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.parts_qty + ' ' + parts_unit + '</span>';

                                                    trHTML += '<div class="input-group data-input d-none">';
                                                        trHTML += '<input type="number" class="form-control" placeholder="Insert" value="'+purchase_item_2.parts_qty+'" oninput="qty_2(this, ' + purchase_item_2.parts_qty + ', ' + purchase_item_1.requisitioned_parts_qty + ', ' + purchase_item_1.purchase_indx_f + ', ' + purchase_item_1.purchase_indx_l + ')">';
                                                        trHTML += '<div class="input-group-prepend">';
                                                            trHTML += '<div class="input-group-text">' + parts_unit + '</div>';
                                                        trHTML += '</div>';
                                                    trHTML += '</div>';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span"><i class="mdi mdi-currency-bdt"></i>' + purchase_item_2.price + '</span>';

                                                    trHTML += '<input type="number" class="form-control data-input d-none" placeholder="Insert" value="'+purchase_item_2.price+'" oninput="price_2(this, ' + purchase_item_2.price + ')">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.party_name + '</span>';

                                                    trHTML += '<div class="data-input d-none">';
                                                        let party_name_option = '';
                                                        $.ajax({
                                                            url: '../../api/party',
                                                            method: 'post',
                                                            data: {
                                                                party_data_type: 'fetch_all'
                                                            },
                                                            dataType: 'json',
                                                            cache: false,
                                                            async: false,
                                                            success: function(data){
                                                                if(data.Type == 'success'){
                                                                    $.each(data.Reply, function(i, party_item){
                                                                        let selected = (party_item.party_id == purchase_item_2.party_id) ? 'selected' : '';

                                                                        party_name_option += '<option value="'+party_item.party_id+'|'+party_item.party_remarks+'" ' + selected + '>'+party_item.party_name+'</option>';
                                                                    });
                                                                } else if(data.Type == 'error'){
                                                                    Swal.fire({
                                                                        title: 'Error',
                                                                        text: data.Reply,
                                                                        type: 'error',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                                                        footer: 'Please try again.'
                                                                    });
                                                                } else{
                                                                    Swal.fire({
                                                                        title: 'Info',
                                                                        text: 'Server is under maintenance. Please try again later!',
                                                                        type: 'info',
                                                                        width: 450,
                                                                        showCloseButton: true,
                                                                        confirmButtonColor: '#5cb85c',
                                                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                                                    });
                                                                }

                                                                return false;
                                                            }
                                                        });

                                                        trHTML += '<select class="select-b" onchange="party_name_2(this)">';
                                                            trHTML += '<option value="">Choose</option>';
                                                            trHTML += party_name_option;
                                                        trHTML += '</select>';
                                                        trHTML += '<span class="float-left mt-1 text-primary remarks"></span>';
                                                    trHTML += '</div>';    
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.gate_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+purchase_item_2.gate_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.challan_no + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" placeholder="Insert" value="'+purchase_item_2.challan_no+'">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(purchase_item_2.challan_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + purchase_item_2.challan_photo + '" target="_blank">' + purchase_item_2.challan_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';
                                                
                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">';
                                                        if(purchase_item_2.bill_photo)
                                                            trHTML += '<a title="View photo" href="../../assets/images/uploads/' + purchase_item_2.bill_photo + '" target="_blank">' + purchase_item_2.bill_photo + '</a>';
                                                        else
                                                            trHTML += '';
                                                    trHTML += '</span>';

                                                    trHTML += '<input type="file" class="form-control data-input d-none" accept="image/*">';
                                                trHTML += '</td>';

                                                trHTML += '<td class="align-middle text-center">';
                                                    trHTML += '<span class="data-span">' + purchase_item_2.purchase_date + '</span>';

                                                    trHTML += '<input type="text" class="form-control data-input d-none" oninput="purchase_date_2(this)" data-id="'+ purchase_item_2.parts_id +'" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" value="'+purchase_item_2.purchase_date+'">';
                                                trHTML += '</td>';

                                                if(user_category != 3){
                                                    trHTML += '<td class="align-middle text-center">';
                                                        trHTML += '<a title="Edit" href="javascript:void(0)" class="btn btn-xs btn-info edt-btn" style="margin-right: 5px;" onclick="edit_btn(this)"><i class="mdi mdi-pencil"></i></a>';
                                                        trHTML += '<a title="Cancel" href="javascript:void(0)" class="btn btn-xs btn-danger d-none cncl-btn" style="margin-right: 5px;" onclick="cancel_btn(this)"><i class="mdi mdi-cancel"></i></a>';
                                                        trHTML += '<a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none upd-btn" onclick="update_purchase_data_spr(' + purchase_item_2.purchase_data_id + ', this, ' + purchase_item_1.purchase_id + ')"><i class="mdi mdi-arrow-up-bold-outline"></i></a>';
                                                    trHTML += '</td>';
                                                }
                                            trHTML += '</tr>';
                                        }
                                    });
                                });

                                $('.purchase-records').empty().append(trHTML);

                                $('.select-b').select2({
                                    width: '100%',
                                    placeholder: 'Choose'
                                });
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });
            }

            // UPDATE SPARE PURCHASE DATA
            function update_purchase_data_spr(ele, ele2, ele3){
                let purchase_data_id = ele,
                    qty = $(ele2).closest('tr').find('td:eq(1)').find('input').val(),
                    price = $(ele2).closest('tr').find('td:eq(2)').find('input').val(),
                    party = $(ele2).closest('tr').find('td:eq(3)').find('select').val(),
                    gate_no = $(ele2).closest('tr').find('td:eq(4)').find('input').val(),
                    challan_no = $(ele2).closest('tr').find('td:eq(5)').find('input').val(),
                    challan_photo = $(ele2).closest('tr').find('td:eq(6)').find('input').prop('files')[0],
                    bill_photo = $(ele2).closest('tr').find('td:eq(7)').find('input').prop('files')[0],
                    challan_file_data = '',
                    bill_file_data = '',
                    purchase_date = $(ele2).closest('tr').find('td:eq(8)').find('input').val();

                if(qty === '' || price === '' || party === '' || gate_no === '' || challan_no === '' || purchase_date === ''){
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty table row data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please fill all data in the table row.'
                    });
                } else{
                    if(challan_photo != undefined)
                        challan_file_data = challan_photo.name;

                    if(bill_photo != undefined)
                        bill_file_data = bill_photo.name;

                    if(challan_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', challan_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    if(bill_photo != undefined){
                        let form_data = new FormData();
                        form_data.append('file', bill_photo);

                        $.ajax({
                            url: '../../api/uploadImage',
                            method: 'post',
                            data: form_data,
                            dataType: 'json',
                            contentType : false,
                            processData: false,
                            cache: false,
                            async: false,
                            success: function(data){
                                if(data.Type === 'error'){
                                    flag = 0;
                                    msg = data.Reply;
                                }
                            }
                        });

                        if(flag == 0)
                            return false;
                    }

                    $.ajax({
                        url: '../../api/interactionController',
                        method: 'post',
                        data: {
                            interact_type: 'update',
                            interact: 'purchase_data2',
                            id: purchase_data_id,
                            purchase_id: ele3,
                            qty: qty,
                            price: price,
                            party: party,
                            gate_no: gate_no,
                            challan_no: challan_no,
                            challan_photo: challan_file_data,
                            bill_photo: bill_file_data,
                            purchase_date: purchase_date
                        },
                        dataType: 'json',
                        cache: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let t;

                                Swal.fire({
                                    title: 'Updating Purchase Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    Swal.fire({
                                        title: 'Success',
                                        text: data.Reply,
                                        type: 'success',
                                        width: 450,
                                        showCloseButton: false,
                                        allowOutsideClick: false,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                    }).then((result) => {
                                        if(result.value){
                                            window.location.reload(true);
                                        }
                                    });
                                });
                            } else if(data.Type == 'error'){
                                Swal.fire({
                                    title: 'Error',
                                    text: data.Reply,
                                    type: 'error',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                    footer: 'Something went wrong.'
                                });
                            } else{
                                Swal.fire({
                                    title: 'Info',
                                    text: 'Server is under maintenance. Please try again later!',
                                    type: 'info',
                                    width: 450,
                                    showCloseButton: true,
                                    confirmButtonColor: '#5cb85c',
                                    confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                });
                            }

                            return false;
                        }
                    });
                }
            }
        </script>
    </body>
</html>