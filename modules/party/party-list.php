<?php 
    require_once('../session.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RRM Inventory by RRM Gorup | Party</title>

        <?php 
            require_once('../../sub-page-header.php');

            // PERMISSION
            $user_categories = [1, 3, 4];
            if(!in_array($user_category, $user_categories)){
                header('location: ../dashboard');
            }
        ?>

        <style type="text/css">
            .payment{
                width: 120px;
                display: inline-block;
                text-align: center;
                height: 30px;
                margin-right: 0;
            }
            .pay{
                height: 30px;
                display: none;
            }
        </style>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">
            <!-- Topbar Start -->
            <?php include('../../topbar-for-sub-page.php'); ?>
            <!-- end Topbar -->

            <?php include('../../navbar-for-sub-page.php'); ?>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="wrapper full-width-background">
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Party</a></li>
                                    <li class="breadcrumb-item active">Party List</li>
                                </ol>
                            </div>
                            <h4 class="page-title"><i class="mdi mdi-account-multiple-outline"></i> Party List</h4>
                        </div>
                    </div>
                </div>     
                <!-- end page title -->
                
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <!-- Start Modals For Add Party -->
                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Add Party</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        
                                        <form action="javascript:void(0);" method="post" name="formAddParty" class="add-party-form" enctype="multipart/form-data" data-parsley-validate>
                                            <div class="modal-body" style="overflow: scroll; height: 500px;">
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div class="card" style="margin-bottom: 0">
                                                            <div class="card-body">
                                                                <div class="alert alert-success add-party-success d-none fade show">
                                                                    <h4 class="mt-0">Success</h4>
                                                                    <p class="mb-0">All the required fields are filled!</p>
                                                                </div>

                                                                <div class="alert alert-danger add-party-danger d-none fade show">
                                                                    <h4 class="mt-0">Error</h4>
                                                                    <p class="mb-0">Please fill all the required fields!</p>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label for="party_name">Party Name <span style="color: #f0643b">*</span></label>
                                                                            <input type="text" class="form-control" name="party_name" id="party_name" placeholder="Insert" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label for="mobile">Mobile</label>
                                                                            <input type="number" class="form-control" name="mobile" id="mobile" placeholder="Insert" required>
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label for="address">Address</label>
                                                                            <input type="text" class="form-control" name="address" placeholder="Insert" id="address">
                                                                        </div>
                                                                    </div>
                                                                </div> 

                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label for="opening_ledger_balance">Opening Ledger Balance <span style="color: #f0643b">*</span></label><i class="fa fa-info-circle float-right mt-1" data-toggle="tooltip" data-placement="bottom" data-original-title="Insert positive value, if its Debit; Insert negative value, if its Credit."></i>
                                                                            <input type="number" class="form-control" name="opening_ledger_balance" id="opening_ledger_balance" placeholder="Insert" required>
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label for="remarks">Remarks</label>
                                                                            <input type="textarea" class="form-control" name="remarks" id="remarks" placeholder="Insert">
                                                                        </div>
                                                                    </div>
                                                                </div> 
                                                            </div> <!-- end card-body-->
                                                        </div> <!-- end card-->
                                                    </div> <!-- end col -->
                                                </div> 
                                            </div>

                                            <div class="modal-footer justify-content-center">
                                                <div class="row justify-content-center">
                                                    <button type="submit" class="btn btn-success waves-effect waves-light"><span class="btn-label"><i class="fas fa-plus"></i></span>Add</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <!-- End Modals For Add Party -->

                            <!-- Start Modals For Update Party -->
                            <div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Update Party</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>

                                        <form action="javascript:void(0);" method="post" name="formUpdateParty" class="update-party-form" enctype="multipart/form-data" data-parsley-validate>
                                            <div class="modal-body" style="overflow: scroll; height: 500px;">
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div class="card" style="margin-bottom: 0">
                                                            <div class="card-body">
                                                                <div class="alert alert-success update-party-success d-none fade show">
                                                                    <h4 class="mt-0">Success</h4>
                                                                    <p class="mb-0">All the required fields are filled!</p>
                                                                </div>

                                                                <div class="alert alert-danger update-party-danger d-none fade show">
                                                                    <h4 class="mt-0">Error</h4>
                                                                    <p class="mb-0">Please fill all the required fields!</p>
                                                                </div>

                                                                <input type="hidden" name="party_id" id="party_id">

                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label for="upd_party_name">Party Name <span style="color: #f0643b">*</span></label>
                                                                            <input type="text" class="form-control" name="upd_party_name" id="upd_party_name" placeholder="Insert" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label for="upd_mobile">Mobile</label>
                                                                            <input type="number" class="form-control" name="upd_mobile" id="upd_mobile" placeholder="Insert" required>
                                                                        </div>
                                                                    </div> 

                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label for="upd_address">Address</label>
                                                                            <input type="text" class="form-control" name="upd_address" id="upd_address" placeholder="Insert">
                                                                        </div>
                                                                    </div>
                                                                </div> 

                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <label for="upd_remarks">Remarks</label>
                                                                            <input type="textarea" class="form-control" name="upd_remarks" id="upd_remarks" placeholder="Insert">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> <!-- end card-body-->
                                                        </div> <!-- end card-->
                                                    </div> <!-- end col -->                  
                                                </div> 
                                            </div>

                                            <div class="modal-footer justify-content-center">
                                                <div class="row justify-content-center">
                                                    <button type="submit" class="btn btn-success waves-effect waves-light"><span class="btn-label"><i class="fas fa-edit"></i></span>Update</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <!-- End Modals For Update Party -->

                            <!-- Start Modals For Party Ledger -->
                            <div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg modal-dialog-centered" style="max-width: 1180px !important;">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myLargeModalLabel">Party Ledger</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        
                                        <div class="modal-body" style="overflow: scroll; height: 500px;">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="card" style="margin-bottom: 0">
                                                        <div class="card-body">
                                                            <table id="basic-datatable3" class="table w-100 nowrap cell-border">
                                                                <thead style="color: #fff; background-color: #5089de;">
                                                                    <tr>
                                                                        <th style="padding-right: 114px">SL.</th>
                                                                        <th style="padding-right: 114px">Ledger Description</th>
                                                                        <th style="padding-right: 114px">Debit Amount</th>
                                                                        <th style="padding-right: 114px">Credit Amount</th>
                                                                        <th style="padding-right: 114px">Total Balance</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="records">
                                                                </tbody>
                                                                <tfoot style="color: #fff; background-color: #5089de;">
                                                                    <tr>
                                                                        <th style="padding-right: 114px">SL.</th>
                                                                        <th style="padding-right: 114px">Ledger Description</th>
                                                                        <th style="padding-right: 114px">Debit Amount</th>
                                                                        <th style="padding-right: 114px">Credit Amount</th>
                                                                        <th style="padding-right: 114px">Total Balance</th>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div> <!-- end card-body-->
                                                    </div> <!-- end card-->
                                                </div> <!-- end col -->                  
                                            </div> 
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <!-- End Modals For Party Ledger -->

                            <div class="button-list">  
                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg"><span class="btn-label"><i class="mdi mdi-account-multiple-plus-outline" style=""></i></span>Add Party</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body" style="overflow-x: auto; overflow-y: auto;">
                                <table id="basic-datatable2" class="table w-100 nowrap cell-border">
                                    <thead style="color: #fff; background-color: #5089de;">
                                        <tr>
                                            <th class="text-center">SL.</th>
                                            <th class="text-center">Party Name</th>
                                            <th class="text-center">Mobile</th>
                                            <th class="text-center">Address</th>
                                            <th class="text-center">Opening Balance</th>
                                            <th class="text-center">Current Balance</th>
                                            <th class="text-center">Remarks</th>
                                            <th class="text-center">Payment</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $party_query = mysqli_query($conn, "SELECT * FROM rrmsteel_party");

                                            if(mysqli_num_rows($party_query) > 0){
                                                $i = 1;

                                                while($row = mysqli_fetch_assoc($party_query)){
                                                    $party_ledger_query = mysqli_query($conn, "SELECT * FROM rrmsteel_party_ledger WHERE party_id = '".$row['party_id']."'");

                                                    if(mysqli_num_rows($party_ledger_query) > 0){
                                                        $total_debit = 0;
                                                        $total_credit = 0;

                                                        while($row2 = mysqli_fetch_assoc($party_ledger_query)){
                                                            $total_debit += $row2['debit'];
                                                            $total_credit += $row2['credit'];
                                                        }

                                                        $current_balance = $total_debit - $total_credit;
                                                    }
                                        ?>
                                                    <tr>
                                                        <td class="text-center"><?= $i++; ?></td>
                                                        <td class="text-center"><?= $row['party_name']; ?></td>
                                                        <td class="text-center"><?= $row['party_mobile']; ?></td>
                                                        <td class="text-center"><?= $row['party_address']; ?></td>
                                                        <td class="text-center">
                                                            <?php
                                                                if($row['opening_ledger_balance'] < 0)
                                                                    echo '<span class="text-danger">' . number_format((float)abs($row['opening_ledger_balance']), 2, '.', '') . '</span>';
                                                                else
                                                                    echo '<span class="text-success">' . number_format((float)$row['opening_ledger_balance'], 2, '.', '') . '</span>';
                                                            ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php
                                                                if($current_balance < 0)
                                                                    echo '<span class="text-danger">' . number_format((float)abs($current_balance), 2, '.', ''). '</span>';
                                                                else
                                                                    echo '<span class="text-success">' . number_format((float)$current_balance, 2, '.', ''). '</span>';
                                                            ?>
                                                        </td>
                                                        <td class="text-center"><?= $row['party_remarks']; ?></td>
                                                        <td class="text-center">
                                                            <input type="number" class="form-control payment" name="payment" id="payment<?= $row['party_id'] ?>" min="0" max="<?= $current_balance; ?>" data-id="<?= $row['party_id'] ?>" placeholder="Insert" oninput="payment(<?= $row['party_id'] ?>)" onchange="payment(<?= $row['party_id'] ?>)">

                                                            <input type="hidden" class="temp-payment" id="temp_payment<?= $row['party_id'] ?>" value="<?= $current_balance; ?>">

                                                            &emsp;

                                                            <button title="Pay" type="button" class="btn btn-xs btn-info pay" id="pay<?= $row['party_id'] ?>" data-id="<?= $row['party_id'] ?>" onclick="pay(<?= $row['party_id'] ?>)"><i class="mdi mdi-currency-bdt"></i></button>
                                                        </td>
                                                        <td class="text-center">
                                                            <a title="Ledger Record" href="javascript:void(0)" class="btn btn-xs btn-secondary" data-toggle="modal" data-target=".bs-example-modal-lg3" data-id="<?= $row['party_id'] ?>" onclick="party_ledger(<?= $row['party_id'] ?>)"><i class="mdi mdi-format-list-bulleted-type"></i></a>
                                                            <a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success" data-toggle="modal" data-target=".bs-example-modal-lg2" data-id="<?= $row['party_id'] ?>" onclick="update_party(<?= $row['party_id'] ?>)"><i class="mdi mdi-pencil"></i>
                                                            </a>
                                                            <a title="Delete" href="javascript:void(0)" class="btn btn-xs btn-danger" data-id="<?= $row['party_id'] ?>" onclick="delete_party(<?= $row['party_id'] ?>)"><i class="mdi mdi-delete"></i></a>
                                                        </td>
                                                    </tr>
                                        <?php 
                                                }
                                            }
                                        ?>
                                    </tbody>
                                    <tfoot style="color: #fff; background-color: #5089de;">
                                        <tr>
                                            <th class="text-center">SL.</th>
                                            <th class="text-center">Party Name</th>
                                            <th class="text-center">Mobile</th>
                                            <th class="text-center">Address</th>
                                            <th class="text-center">Opening Balance</th>
                                            <th class="text-center">Current Balance</th>
                                            <th class="text-center">Remarks</th>
                                            <th class="text-center">Payment</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>
                <!-- end row-->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <?php require_once('../../footer-for-sub-page.php'); ?>

        <!-- Validation init js-->
        <script src="../../assets/js/pages/party-form-validation.init.js"></script>

        <!-- Custom js -->
        <script type="text/javascript">
            $(document).ready(function(){
                // PAYMENT
                $('.payment').attr({'min': 0});
                $('.pay').css('display', 'none');

                $('#basic-datatable2').DataTable({
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                    }
                });
            });

            // PAYMENT
            function payment(ele){
                let id = ele;

                $('#pay' + id).css('display', 'inline-block');

                let payment = $('#payment' + id).val();
                let temp_payment = $('#temp_payment' + id).val();

                if(parseInt(payment) < 0){
                    $('#payment' + id).val(0);
                    return false;
                } else if(parseInt(payment) > parseInt(temp_payment)){
                    $('#payment' + id).val($('#temp_payment' + id).val());
                    return false;
                } else if(payment === '' || parseInt(payment) === 0){
                    $('.pay').css('display', 'none');
                }
            }

            // UPDATE PARTY MODAL
            function update_party(ele){
                let party_id = ele;

                $.ajax({
                    url: '../../api/party',
                    method: 'post',
                    data: {
                        party_data_type: 'fetch',
                        party_id: party_id
                    },
                    dataType: 'json',
                    cache: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Party Data',
                                text: 'Please wait...',
                                timer: 1000,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 1000);
                                }
                            }).then(function(){
                                $('#party_id').val(data.Reply[0].party_id);
                                $('#upd_party_name').val(data.Reply[0].party_name);
                                $('#upd_mobile').val(data.Reply[0].party_mobile);
                                $('#upd_address').val(data.Reply[0].party_address);
                                $('#upd_remarks').val(data.Reply[0].party_remarks);
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });
            }

            // PARTY LEDGER
            function party_ledger(ele){
                let party_id = ele;

                let table = $('#basic-datatable3').DataTable();
                table.destroy();
                $('#records').empty();

                let trHTML = '';

                $.ajax({
                    url: '../../api/party',
                    method: 'post',
                    data: {
                        party_data_type: 'fetch_ledger',
                        party_id: party_id
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let t;

                            Swal.fire({
                                title: 'Fetching Ledger Data',
                                text: 'Please wait...',
                                timer: 1000,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 1000);
                                }
                            });

                            let total_balance = 0;
                            $.each(data.Reply, function(i, item){
                                let debit_class = '',
                                    credit_class = '',
                                    total_class = '';
                                if(item.debit > 0)
                                    debit_class = 'text-success';
                                if(item.credit > 0)
                                    credit_class = 'text-danger';

                                total_balance = +total_balance + +(item.debit - item.credit);
                                if(total_balance > 0)
                                    total_class = 'text-success';
                                else
                                    total_class = 'text-danger';

                                trHTML += '<tr>';
                                    trHTML += '<td style="padding-right: 114px;">' + (i+1) + '</td>';
                                    trHTML += '<td style="padding-right: 114px;">' + item.description + '</td>';
                                    trHTML += '<td style="padding-right: 114px;"><span class="' + debit_class + '">' + item.debit + '</span></td>';
                                    trHTML += '<td style="padding-right: 114px;"><span class="' + credit_class + '">' + item.credit + '</span></td>';
                                    trHTML += '<td style="padding-right: 114px;"><span class="' + total_class + '">' + Math.abs((total_balance)).toFixed(2) + '</span></td>';
                                trHTML += '</tr>';
                            });
                        } else if(data.Type == 'error'){
                            Swal.fire({
                                title: 'Error',
                                text: data.Reply,
                                type: 'error',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                footer: 'Please try again.'
                            });
                        } else{
                            Swal.fire({
                                title: 'Info',
                                text: 'Server is under maintenance. Please try again later!',
                                type: 'info',
                                width: 450,
                                showCloseButton: true,
                                confirmButtonColor: '#5cb85c',
                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                            });
                        }

                        return false;
                    }
                });

                $('#records').append(trHTML);

                $('#basic-datatable3').DataTable({
                    scrollX: !0,
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded')
                    }
                });
            }
        </script>
    </body>
</html>