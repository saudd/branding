<?php 
    require_once('../session.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RRM Inventory by RRM Gorup | Inventory</title>

        <?php 
            require_once('../../sub-page-header.php');

            // PERMISSION
            $user_categories = [1, 3];
            if(!in_array($user_category, $user_categories)){
                header('location: ../dashboard');
            }
        ?>

        <style type="text/css">
            #inventory_history > .row > .col-12 > .card > .card-body > .overflow > #basic-datatable_wrapper{
                width: 99%;
            }

            .action-type, .req-for, .qty, .price, .action-date{
                width: 160px;
                display: inline-block;
                text-align: center;
                height: 34px;
                margin-right: 5px;
            }

            .save{
                height: 32px;
                margin-left: 5px;
            }

            .table > thead > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th{
                text-align: center;
                vertical-align: middle;
            }
        </style>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">
            <!-- Topbar Start -->
            <?php include('../../topbar-for-sub-page.php'); ?>
            <!-- end Topbar -->

            <?php include('../../navbar-for-sub-page.php'); ?>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="wrapper full-width-background">   
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                    <li class="breadcrumb-item active">Inventory List</li>
                                </ol>
                            </div>
                            <h4 class="page-title"><i class="mdi mdi-shape-outline"></i> Inventory List</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card-box">
                            <ul class="nav nav-pills navtab-bg nav-justified">
                                <li class="nav-item">
                                    <a href="#inventory_summary" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                        <i class="fas fa-tasks mr-1"></i>
                                        <span class="d-none d-sm-inline">SUMMARY</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#inventory_history" data-toggle="tab" aria-expanded="true" class="nav-link">
                                        <i class="fas fa-history mr-1"></i>
                                        <span class="d-none d-sm-inline">HISTORY</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane show active" id="inventory_summary">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body" style="overflow-x: auto; overflow-y: auto;">
                                                    <table id="basic-datatable2" class="table w-100 nowrap cell-border">
                                                        <thead style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Parts Name</th>
                                                                <th>Parts Unit</th>
                                                                <th>Stock Quantity</th>
                                                                <th>Received Quantity</th>
                                                                <th>Issued Quantity</th>
                                                                <th>Average Rate</th>
                                                                <th class="alert-primary">Receive / Issue Parts</th>
                                                                <!-- <th class="alert-danger">Delete Received / Issued Records</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody class="summary-records">
                                                        </tbody>
                                                        <tfoot style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Parts Name</th>
                                                                <th>Parts Unit</th>
                                                                <th>Stock Quantity</th>
                                                                <th>Received Quantity</th>
                                                                <th>Issued Quantity</th>
                                                                <th>Average Rate</th>
                                                                <th class="alert-primary">Receive / Issue Parts</th>
                                                                <!-- <th class="alert-danger">Delete Received / Issued Records</th> -->
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div> <!-- end card body-->
                                            </div> <!-- end card -->
                                        </div><!-- end col-->
                                    </div>
                                </div>

                                <div class="tab-pane" id="inventory_history">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body" style="overflow-x: auto; overflow-y: auto;">
                                                    <table id="basic-datatable3" class="table w-100 nowrap cell-border">
                                                        <thead style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th><i class="mdi mdi-pencil"></i></th>
                                                                <th>Receive / Issue<br>Date</th>
                                                                <th>Parts<br>Name</th>
                                                                <th>Parts<br>Unit</th>
                                                                <th>Opening<br>Qty.</th>
                                                                <th>Opening<br>Value</th>
                                                                <th>Parts<br>Rate</th>
                                                                <th>Parts<br>Avg. Rate</th>
                                                                <th style="min-width: 130px">Required<br>For</th>
                                                                <th style="min-width: 70px">Received<br>Qty.</th>
                                                                <th style="min-width: 80px">Received<br>Value</th>
                                                                <th style="min-width: 70px">Issued<br>Qty.</th>
                                                                <th style="min-width: 80px">Issued<br>Value</th>
                                                                <th>Closing<br>Qty.</th>
                                                                <th>Closing<br>Value</th>
                                                                <th>Action<br>Datetime</th>
                                                                <th>Updated<br>By</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $inventory_history_query = mysqli_query($conn, "SELECT * FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id INNER JOIN rrmsteel_user u ON u.user_id = i.user_id WHERE (i.received_qty > 0 OR i.issued_qty > 0) ORDER BY i.inventory_history_created DESC");

                                                                if(mysqli_num_rows($inventory_history_query) > 0){
                                                                    while($row = mysqli_fetch_assoc($inventory_history_query)){
                                                            ?>
                                                                        <tr>   
                                                                            <td>
                                                                                <a title="Edit" href="javascript:void(0)" class="btn btn-xs btn-info edt-btn <?= (($row['received_qty'] == 0 && $row['issued_qty'] == 0) ? 'd-none' : '') ?>" style="margin-right: 5px;" onclick="edit_btn(this, <?= (($row['received_qty'] > 0) ? 1 : 2) ?>, <?= $row['parts_id'] ?>)"><i class="mdi mdi-pencil"></i></a>

                                                                                <a title="Cancel" href="javascript:void(0)" class="btn btn-xs btn-danger d-none cncl-btn" style="margin-right: 5px;" onclick="cancel_btn(this)"><i class="mdi mdi-cancel"></i></a>

                                                                                <a title="Update" href="javascript:void(0)" class="btn btn-xs btn-success d-none upd-btn" onclick="update_inventory_2(this, <?= $row['parts_id'] ?>, <?= $row['inventory_history_id'] ?>)"><i class="mdi mdi-arrow-up-bold-outline"></i></a>
                                                                            </td>
                                                                            <td>
                                                                                <?= '<span class="">' . $row['history_date'] . '</span>'; ?>
                                                                                
                                                                                <input type="text" class="form-control d-none action-date-h" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" onchange="action_date_h(this, <?= $row['parts_id'] ?>)" value="<?= $row['history_date'] ?>" oninput="action_date_2(this)">
                                                                            </td>
                                                                            <td><?= $row['parts_name'] ?></td>
                                                                            <td>
                                                                                <?php 
                                                                                    if($row['unit'] == 1) echo 'Bag';
                                                                                    elseif($row['unit'] == 2) echo 'Box';
                                                                                    elseif($row['unit'] == 3) echo 'Box/Pcs';
                                                                                    elseif($row['unit'] == 4) echo 'Bun';
                                                                                    elseif($row['unit'] == 5) echo 'Bundle';
                                                                                    elseif($row['unit'] == 6) echo 'Can';
                                                                                    elseif($row['unit'] == 7) echo 'Cartoon';
                                                                                    elseif($row['unit'] == 8) echo 'Challan';
                                                                                    elseif($row['unit'] == 9) echo 'Coil';
                                                                                    elseif($row['unit'] == 10) echo 'Drum';
                                                                                    elseif($row['unit'] == 11) echo 'Feet';
                                                                                    elseif($row['unit'] == 12) echo 'Gallon';
                                                                                    elseif($row['unit'] == 13) echo 'Item';
                                                                                    elseif($row['unit'] == 14) echo 'Job';
                                                                                    elseif($row['unit'] == 15) echo 'Kg';
                                                                                    elseif($row['unit'] == 16) echo 'Kg/Bundle';
                                                                                    elseif($row['unit'] == 17) echo 'Kv';
                                                                                    elseif($row['unit'] == 18) echo 'Lbs';
                                                                                    elseif($row['unit'] == 19) echo 'Ltr';
                                                                                    elseif($row['unit'] == 20) echo 'Mtr';
                                                                                    elseif($row['unit'] == 21) echo 'Pack';
                                                                                    elseif($row['unit'] == 22) echo 'Pack/Pcs';
                                                                                    elseif($row['unit'] == 23) echo 'Pair';
                                                                                    elseif($row['unit'] == 24) echo 'Pcs';
                                                                                    elseif($row['unit'] == 25) echo 'Pound';
                                                                                    elseif($row['unit'] == 26) echo 'Qty';
                                                                                    elseif($row['unit'] == 27) echo 'Roll';
                                                                                    elseif($row['unit'] == 28) echo 'Set';
                                                                                    elseif($row['unit'] == 29) echo 'Truck';
                                                                                    elseif($row['unit'] == 30) echo 'Unit';
                                                                                    elseif($row['unit'] == 31) echo 'Yeard';
                                                                                    elseif($row['unit'] == 32) echo '(Unit Unknown)';
                                                                                    elseif($row['unit'] == 33) echo 'SFT';
                                                                                    elseif($row['unit'] == 34) echo 'RFT';
                                                                                    elseif($row['unit'] == 35) echo 'CFT';
                                                                                ?>
                                                                            </td>
                                                                            <td><?= $row['opening_qty']; ?></td>
                                                                            <td><?= $row['opening_value']; ?></td>
                                                                            <td><?= $row['parts_rate']; ?></td>
                                                                            <td><?= $row['parts_avg_rate']; ?></td>
                                                                            <td>
                                                                                <span class="">
                                                                                    <?php 
                                                                                        if($row['required_for'] == 1)
                                                                                            echo 'BCP-CCM';
                                                                                        elseif($row['required_for'] == 2)
                                                                                            echo 'BCP-Furnace';
                                                                                        elseif($row['required_for'] == 3)
                                                                                            echo 'Concast-CCM';
                                                                                        elseif($row['required_for'] == 4)
                                                                                            echo 'Concast-Furnace';
                                                                                        elseif($row['required_for'] == 5)
                                                                                            echo 'HRM';
                                                                                        elseif($row['required_for'] == 6)
                                                                                            echo 'HRM Unit-2';
                                                                                        elseif($row['required_for'] == 7)
                                                                                            echo 'Lal Masjid';
                                                                                        elseif($row['required_for'] == 8)
                                                                                            echo 'Sonargaon';
                                                                                        elseif($row['required_for'] == 9)
                                                                                            echo 'General';
                                                                                    ?>
                                                                                </span>

                                                                                <select class="form-control d-none req-for-h" onchange="required_for_h(this, <?= $row['parts_id'] ?>)">
                                                                                    <option value="">Choose</option>
                                                                                    <option value="1" <?= ($row['required_for'] == 1 ? 'selected' : ''); ?>>BCP-CCM</option>
                                                                                    <option value="2" <?= ($row['required_for'] == 2 ? 'selected' : ''); ?>>BCP-Furnace</option>
                                                                                    <option value="3" <?= ($row['required_for'] == 3 ? 'selected' : ''); ?>>Concast-CCM</option>
                                                                                    <option value="4" <?= ($row['required_for'] == 4 ? 'selected' : ''); ?>>Concast-Furnace</option>
                                                                                    <option value="5" <?= ($row['required_for'] == 5 ? 'selected' : ''); ?>>HRM</option>
                                                                                    <option value="6" <?= ($row['required_for'] == 6 ? 'selected' : ''); ?>>HRM Unit-2</option>
                                                                                    <option value="7" <?= ($row['required_for'] == 7 ? 'selected' : ''); ?>>Lal Masjid</option>
                                                                                    <option value="8" <?= ($row['required_for'] == 8 ? 'selected' : ''); ?>>Sonargaon</option>
                                                                                    <option value="9" <?= ($row['required_for'] == 9 ? 'selected' : ''); ?>>General</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <?php 
                                                                                    if($row['received_qty'] > 0){
                                                                                        echo '<span class="data-span">' . $row['received_qty'] . '</span>';
                                                                                        echo '<input type="number" class="form-control d-none data-input rcv-qty" placeholder="Insert" value="'.$row['received_qty'].'" oninput="qty_h(this)"';
                                                                                    }
                                                                                ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php 
                                                                                    if($row['received_value'] > 0){
                                                                                        echo '<span class="data-span">' . $row['received_value'] . '</span>';
                                                                                        echo '<input type="number" class="form-control d-none data-input rcv-price" placeholder="Insert" value="'.$row['received_value'].'" oninput="price_h(this)"';
                                                                                    }
                                                                                ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php 
                                                                                    if($row['issued_qty'] > 0){
                                                                                        echo '<span class="data-span">' . $row['issued_qty'] . '</span>';
                                                                                        echo '<input type="number" class="form-control d-none data-input iss-qty" placeholder="Insert" value="'.$row['issued_qty'].'" oninput="qty_h(this)"';
                                                                                    }
                                                                                ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php 
                                                                                    if($row['issued_value'] > 0){
                                                                                        echo '<span class="data-span">' . $row['issued_value'] . '</span>';
                                                                                        echo '<input type="number" class="form-control d-none data-input iss-price" placeholder="Insert" value="'.$row['issued_value'].'" oninput="price_h(this)"';
                                                                                    }
                                                                                ?>
                                                                            </td>
                                                                            <td><?= $row['closing_qty']; ?></td>
                                                                            <td><?= $row['closing_value']; ?></td>
                                                                            <td><?= date('d M, Y\a\t h:i a', $row['inventory_history_created']); ?></td>
                                                                            <td><?= $row['user_fullname']; ?></td>
                                                                        </tr>
                                                            <?php 
                                                                    }
                                                                }
                                                            ?>
                                                        </tbody>
                                                        <tfoot style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th><i class="mdi mdi-pencil"></i></th>
                                                                <th>Receive / Issue<br>Date</th>
                                                                <th>Parts<br>Name</th>
                                                                <th>Parts<br>Unit</th>
                                                                <th>Opening<br>Qty.</th>
                                                                <th>Opening<br>Value</th>
                                                                <th>Parts<br>Rate</th>
                                                                <th>Parts<br>Avg. Rate</th>
                                                                <th>Required<br>For</th>
                                                                <th>Received<br>Qty.</th>
                                                                <th>Received<br>Value</th>
                                                                <th>Issued<br>Qty.</th>
                                                                <th>Issued<br>Value</th>
                                                                <th>Closing<br>Qty.</th>
                                                                <th>Closing<br>Value</th>
                                                                <th>Action<br>Datetime</th>
                                                                <th>Updated<br>By</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div> <!-- end card-body -->
                                            </div> <!-- end card -->
                                        </div> <!-- end col -->
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end card-box-->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer Start -->
        <?php require_once('../../footer-for-sub-page.php'); ?>
        <!-- end Footer -->

        <!-- Custom js -->
        <script type="text/javascript">
            $(document).ready(function(){
                // FETCH AND DISPLAY SUMMARY DATA
                $.ajax({
                    url: '../../api/inventory',
                    method: 'post',
                    data: {
                        inventory_data_type: 'inventory_summary_list'
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let table_data = '';

                            $.each(data.Reply, function(i, inventory_summary){
                                let tr_style = '';

                                if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty)
                                    tr_style = 'background: #fce0d8 !important';

                                table_data += '<tr style="'+tr_style+'">';
                                    table_data += '<td rowspan="2" class="align-middle">' + (i+1) + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.parts_name + '</td>';

                                    let parts_unit = '';

                                    if(inventory_summary.parts_unit == 1) parts_unit = 'Bag';
                                    else if(inventory_summary.parts_unit == 2) parts_unit = 'Box';
                                    else if(inventory_summary.parts_unit == 3) parts_unit = 'Box/Pcs';
                                    else if(inventory_summary.parts_unit == 4) parts_unit = 'Bun';
                                    else if(inventory_summary.parts_unit == 5) parts_unit = 'Bundle';
                                    else if(inventory_summary.parts_unit == 6) parts_unit = 'Can';
                                    else if(inventory_summary.parts_unit == 7) parts_unit = 'Cartoon';
                                    else if(inventory_summary.parts_unit == 8) parts_unit = 'Challan';
                                    else if(inventory_summary.parts_unit == 9) parts_unit = 'Coil';
                                    else if(inventory_summary.parts_unit == 10) parts_unit = 'Drum';
                                    else if(inventory_summary.parts_unit == 11) parts_unit = 'Feet';
                                    else if(inventory_summary.parts_unit == 12) parts_unit = 'Gallon';
                                    else if(inventory_summary.parts_unit == 13) parts_unit = 'Item';
                                    else if(inventory_summary.parts_unit == 14) parts_unit = 'Job';
                                    else if(inventory_summary.parts_unit == 15) parts_unit = 'Kg';
                                    else if(inventory_summary.parts_unit == 16) parts_unit = 'Kg/Bundle';
                                    else if(inventory_summary.parts_unit == 17) parts_unit = 'Kv';
                                    else if(inventory_summary.parts_unit == 18) parts_unit = 'Lbs';
                                    else if(inventory_summary.parts_unit == 19) parts_unit = 'Ltr';
                                    else if(inventory_summary.parts_unit == 20) parts_unit = 'Mtr';
                                    else if(inventory_summary.parts_unit == 21) parts_unit = 'Pack';
                                    else if(inventory_summary.parts_unit == 22) parts_unit = 'Pack/Pcs';
                                    else if(inventory_summary.parts_unit == 23) parts_unit = 'Pair';
                                    else if(inventory_summary.parts_unit == 24) parts_unit = 'Pcs';
                                    else if(inventory_summary.parts_unit == 25) parts_unit = 'Pound';
                                    else if(inventory_summary.parts_unit == 26) parts_unit = 'Qty';
                                    else if(inventory_summary.parts_unit == 27) parts_unit = 'Roll';
                                    else if(inventory_summary.parts_unit == 28) parts_unit = 'Set';
                                    else if(inventory_summary.parts_unit == 29) parts_unit = 'Truck';
                                    else if(inventory_summary.parts_unit == 30) parts_unit = 'Unit';
                                    else if(inventory_summary.parts_unit == 31) parts_unit = 'Yeard';
                                    else if(inventory_summary.parts_unit == 32) parts_unit = '(Unit Unknown)';
                                    else if(inventory_summary.parts_unit == 33) parts_unit = 'SFT';
                                    else if(inventory_summary.parts_unit == 34) parts_unit = 'RFT';
                                    else if(inventory_summary.parts_unit == 35) parts_unit = 'CFT';

                                    table_data += '<td class="align-middle">' + parts_unit + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.parts_qty + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.tot_rcv_qty + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.tot_iss_qty + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.parts_avg_rate + '</td>';

                                    let td_class = 'alert-primary';
                                    if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty)
                                        td_class = 'alert-danger';

                                    let req_field = '';
                                    if(inventory_summary.parts_qty == 0)
                                        req_field = 'disabled';

                                    table_data += '<td class="'+td_class+'">\
                                        \
                                        <select class="select-b action-type" onchange="action_type(this)">\
                                            <option value="">Choose type</option>\
                                            <option value="1">Receive</option>\
                                            <option value="2">Issue</option>\
                                        </select>\
                                        \
                                        <select class="d-none req-for" onchange="required_for(this, '+ inventory_summary.parts_id +')">\
                                            <option value="">Choose required for</option>\
                                            <option value="1">BCP-CCM</option>\
                                            <option value="2">BCP-Furnace</option>\
                                            <option value="3">Concast-CCM</option>\
                                            <option value="4">Concast-Furnace</option>\
                                            <option value="5">HRM</option>\
                                            <option value="6">HRM Unit-2</option>\
                                            <option value="7">Lal Masjid</option>\
                                            <option value="8">Sonargaon</option>\
                                            <option value="9">General</option>\
                                        </select>\
                                        \
                                        <input type="text" class="form-control d-none action-date" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" onchange="action_date(this, '+ inventory_summary.parts_id +')" oninput="action_date_2(this)">\
                                        \
                                        <input type="number" class="form-control d-none qty" placeholder="Insert qty." oninput="qty(this)">\
                                        \
                                        <input type="number" class="form-control d-none price" placeholder="Insert price" oninput="price(this)">';

                                        if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty){
                                            table_data += '<i class="fa fa-info-circle mt-1" title="Warning: Low quantity!"></i>';
                                        }

                                        table_data += '<button title="Save" type="button" class="btn btn-xs btn-info d-none save" onclick="update_inventory(this, '+ inventory_summary.parts_id +')"><i class="mdi mdi-content-save"></i></button>\
                                        \
                                        <br>\
                                        \
                                        <p class="mt-2 mb-0 badge badge-danger text-left warning d-none" style="font-size: 100%"></p>\
                                    </td>';
                                table_data += '</tr>';

                                table_data += '<tr style="'+tr_style+'">';
                                    table_data += '<td colspan="8" class="text-left">';
                                        table_data += '<span class="d-none">' + (i+1) + '.1' + ' ' + inventory_summary.parts_name + '</span>';

                                        table_data += '<strong>Pending Receive Till Date</strong>:&emsp; <span style="color: #0000ff">BCP-CCM: </span><span style="color: #0000ff">' + inventory_summary.bcp_ccm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff0000">BCP-FUR: </span><span style="color: #ff0000">' + inventory_summary.bcp_fur.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff00a5">CON-CCM: </span><span style="color: #ff00a5">' + inventory_summary.con_ccm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #00d0ff">CON-FUR: </span><span style="color: #00d0ff">' + inventory_summary.con_fur.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff8d00">HRM: </span><span style="color: #ff8d00">' + inventory_summary.hrm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #6200ff">HRM Unit-2: </span><span style="color: #6200ff">' + inventory_summary.hrm_2.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #0089ff">Lal Masjid: </span><span style="color: #0089ff">' + inventory_summary.lal_masjid.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #009612">Sonargaon: </span><span style="color: #009612">' + inventory_summary.sonargaon.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #960076">General: </span><span style="color: #960076">' + inventory_summary.general.toFixed(3) + '</span>';
                                    table_data += '</td>';
                                    table_data += '<td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td>';
                                table_data += '</tr>';
                            });

                            let t;

                            Swal.fire({
                                title: 'Loading Inventory Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            }).then(function(){
                                $('.summary-records').append(table_data);

                                $('#basic-datatable2').DataTable({
                                    'ordering': false,
                                    language: {
                                        paginate: {
                                            previous: '<i class="mdi mdi-chevron-left">',
                                            next: '<i class="mdi mdi-chevron-right">'
                                        }
                                    },
                                    drawCallback: function(){
                                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                    }
                                });
                            });
                        }
                        
                    }
                });

                $('#basic-datatable3').DataTable({
                    language: {
                        paginate: {
                            previous: '<i class="mdi mdi-chevron-left">',
                            next: '<i class="mdi mdi-chevron-right">'
                        }
                    },
                    drawCallback: function(){
                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                    }
                });
            });

            // ACTION TYPE
            function action_type(ele){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val();

                $(ele).closest('tr').find('td').find('.save').addClass('d-none');

                if(action_type == 1){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('').addClass('d-none');
                    $(ele).closest('tr').find('td').find('.req-for, .action-date').val('').removeClass('d-none');
                } else if(action_type == 2){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('').addClass('d-none');
                    $(ele).closest('tr').find('td').find('.req-for, .action-date').val('').removeClass('d-none');
                } else{
                    $(ele).closest('tr').find('td').find('.req-for, .qty, .price, .action-date').addClass('d-none');
                }
            }

            let max_parts_qty = 0;

            // REQUIRED FOR
            function required_for(ele, ele2){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    action_date = $(ele).closest('tr').find('td').find('.action-date').val(),
                    qty = $(ele).closest('tr').find('td').find('.qty').val(),
                    parts_id = ele2;

                if(action_type && $(ele).val() && action_date){
                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            parts_id: parts_id,
                            required_for: $(ele).val(),
                            action_date: action_date,
                            inventory_data_type: ((action_type == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                if(data.Reply[0].parts_qty == null){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'No purchase record found!' : 'No inventory / receive record found!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some before receive.' : 'Please receive some before issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else if(data.Reply[0].parts_qty <= 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'All purchased & borrowed parts have been received!' : 'All received parts have been issued!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some to receive.' : 'Please receive some to issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else{
                                    max_parts_qty = data.Reply[0].parts_qty;

                                    if(action_type == 1){
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val(data.Reply[0].parts_qty);
                                        $(ele).closest('tr').find('td').find('.price').removeClass('d-none').val(data.Reply[0].parts_price.toFixed(2));
                                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                                    } else{
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    }
                                }
                            }
                        }
                    });
                }

                if(!$(ele).val() || !action_date){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                }
            }

            // ACTION DATE
            function action_date(ele, ele2){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for').val(),
                    qty = $(ele).closest('tr').find('td').find('.qty').val();
                    parts_id = ele2;

                if(action_type && req_for && $(ele).val()){
                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            parts_id: parts_id,
                            required_for: req_for,
                            action_date: $(ele).val(),
                            inventory_data_type: ((action_type == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                if(data.Reply[0].parts_qty == null){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'No purchase record found!' : 'No inventory / receive record found!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some before receive.' : 'Please receive some before issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else if(data.Reply[0].parts_qty <= 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'All purchased & borrowed parts have been received!' : 'All received parts have been issued!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some to receive.' : 'Please receive some to issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else{
                                    max_parts_qty = data.Reply[0].parts_qty;

                                    if(action_type == 1){
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val(data.Reply[0].parts_qty);
                                        $(ele).closest('tr').find('td').find('.price').removeClass('d-none').val(data.Reply[0].parts_price.toFixed(2));
                                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                                    } else{
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    }
                                }
                            }
                        }
                    });
                }

                if(!$(ele).val() || !req_for){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                }
            }

            function action_date_2(ele){
                $(ele).closest('tr').find('td').find('.action-date').val('');
                $(ele).closest('tr').find('td').find('.action-date-h').val('');
            }

            // QTY
            function qty(ele){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    action_date = $(ele).closest('tr').find('td').find('.action-date').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for').val(),
                    qty = $(ele).val(),
                    price = $(ele).closest('tr').find('td').find('.price').val();

                if(!qty){
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                } else if(parseFloat(qty) <= 0){
                    $(ele).closest('tr').find('td').find('.qty').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                } else if(parseFloat(qty) > parseFloat(max_parts_qty)){
                    $(ele).closest('tr').find('td').find('.qty').val(parseFloat(max_parts_qty));
                } else{
                    if((action_type == 1 && action_date && req_for && price) || (action_type == 2 && action_date && req_for)){
                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                    }
                }
            }

            // PRICE
            function price(ele){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    action_date = $(ele).closest('tr').find('td').find('.action-date').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for').val(),
                    qty = $(ele).closest('tr').find('td').find('.qty').val(),
                    price = $(ele).val();

                if(!price){
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                } else if(price <= 0){
                    $(ele).closest('tr').find('td').find('.price').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');

                    return false;
                } else{
                    if((action_type == 1 && action_date && req_for && qty) || (action_type == 2 && action_date && qty)){
                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                    }
                }
            }

            // EDIT
            function edit_btn(ele, ele2, ele3){
                $(ele).closest('tr').find('.data-span').addClass('d-none');
                $(ele).closest('tr').find('.data-input').removeClass('d-none');
                $(ele).closest('tr').find('.edt-btn').addClass('d-none');
                $(ele).closest('tr').find('.cncl-btn').removeClass('d-none');
                $(ele).closest('tr').find('.upd-btn').removeClass('d-none');

                let parts_id = ele3,
                    action_date = $(ele).closest('tr').find('td:eq(1)').find('span').html(),
                    req_for = $(ele).closest('tr').find('td:eq(8)').find('span').html(),
                    qty = ((ele2 == 1) ? $(ele).closest('tr').find('td:eq(9)').find('span').html() : $(ele).closest('tr').find('td:eq(11)').find('span').html()),
                    price = ((ele2 == 1) ? $(ele).closest('tr').find('td:eq(10)').find('span').html() : $(ele).closest('tr').find('td:eq(12)').find('span').html());

                if(req_for.trim() == 'BCP-CCM')
                    req_for = 1;
                else if(req_for.trim() == 'BCP-Furnace')
                    req_for = 2;
                else if(req_for.trim() == 'Concast-CCM')
                    req_for = 3;
                else if(req_for.trim() == 'Concast-Furnace')
                    req_for = 4;
                else if(req_for.trim() == 'HRM')
                    req_for = 5;
                else if(req_for.trim() == 'HRM Unit-2')
                    req_for = 6;
                else if(req_for.trim() == 'Lal Masjid')
                    req_for = 7;
                else if(req_for.trim() == 'Sonargaon')
                    req_for = 8;
                else if(req_for.trim() == 'General')
                    req_for = 9;

                $(ele).closest('tr').find('td:eq(1)').find('input').val(action_date);
                $(ele).closest('tr').find('td:eq(8)').find('select').val(req_for);

                if(ele2 == 1){
                    $(ele).closest('tr').find('td:eq(9)').find('input').val(qty);
                    $(ele).closest('tr').find('td:eq(10)').find('input').val(price);
                } else{
                    $(ele).closest('tr').find('td:eq(11)').find('input').val(qty);
                    $(ele).closest('tr').find('td:eq(12)').find('input').val(price);
                }

                // SET MAX VALUE
                $.ajax({
                    url: '../../api/inventory',
                    method: 'post',
                    data: {
                        parts_id: parts_id,
                        required_for: req_for,
                        action_date: action_date,
                        inventory_data_type: ((ele2 == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let max_val = parseInt(data.Reply[0].parts_qty) + parseInt(qty);

                            (ele2 == 1) ? $(ele).closest('tr').find('td:eq(9)').find('input').attr('max', max_val) : $(ele).closest('tr').find('td:eq(11)').find('input').attr('max', max_val);
                        }
                    }
                });
            }

            // CANCEL
            function cancel_btn(ele){
                $(ele).closest('tr').find('.data-span').removeClass('d-none');
                $(ele).closest('tr').find('.data-input').addClass('d-none');
                
                $(ele).closest('tr').find('.edt-btn').removeClass('d-none');
                $(ele).closest('tr').find('.cncl-btn').addClass('d-none');
                $(ele).closest('tr').find('.upd-btn').addClass('d-none');
            }

            // ACTION DATE HISTORY
            function action_date_h(ele, ele2){
                let parts_id = ele2,
                    action_date = $(ele).closest('tr').find('td').find('.action-date-h').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for-h').val(),
                    action_type = (($(ele).closest('tr').find('td').find('.rcv-qty').val() == undefined) ? 2 : 1),
                    qty = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-qty').val() : $(ele).closest('tr').find('td').find('.iss-qty').val()),
                    price = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-price').val() : $(ele).closest('tr').find('td').find('.iss-price').val());

                if(!action_date){
                    $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                } else{
                    // SET MAX VALUE
                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            parts_id: parts_id,
                            required_for: req_for,
                            action_date: action_date,
                            inventory_data_type: ((ele2 == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let max_val = parseInt(data.Reply[0].parts_qty) + parseInt(qty);

                                if(isNaN(max_val)){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'No purchase record found!' : 'No receive record found!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some before receive.' : 'Please receive some before issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                                    });
                                } else if(parseInt(max_val) <= 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'All purchased & borrowed parts have been received!' : 'All received parts have been issued!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some to receive.' : 'Please receive some to issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                                    });
                                } else{
                                    (action_type == 1) ? $(ele).closest('tr').find('td:eq(9)').find('input').attr('max', max_val) : $(ele).closest('tr').find('td:eq(11)').find('input').attr('max', max_val);

                                    $(ele).closest('tr').find('td').find('.upd-btn').removeClass('d-none');
                                }
                            }
                        }
                    });

                    if(req_for && qty && price){
                        $(ele).closest('tr').find('td').find('.upd-btn').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                    }
                }
            }

            // REQUIRED FOR HISTORY
            function required_for_h(ele, ele2){
                let parts_id = ele2,
                    action_date = $(ele).closest('tr').find('td').find('.action-date-h').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for-h').val(),
                    action_type = (($(ele).closest('tr').find('td').find('.rcv-qty').val() == undefined) ? 2 : 1),
                    qty = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-qty').val() : $(ele).closest('tr').find('td').find('.iss-qty').val()),
                    price = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-price').val() : $(ele).closest('tr').find('td').find('.iss-price').val());

                if(!req_for){
                    $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                } else{
                    // SET MAX VALUE
                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            parts_id: parts_id,
                            required_for: req_for,
                            action_date: action_date,
                            inventory_data_type: ((action_type == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let max_val = parseInt(data.Reply[0].parts_qty) + parseInt(qty);

                                if(isNaN(max_val)){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'No purchase record found!' : 'No receive record found!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some before receive.' : 'Please receive some before issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                                    });
                                } else if(parseInt(max_val) <= 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'All purchased & borrowed parts have been received!' : 'All received parts have been issued!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some to receive.' : 'Please receive some to issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                                    });
                                } else{
                                    (action_type == 1) ? $(ele).closest('tr').find('td:eq(9)').find('input').attr('max', max_val) : $(ele).closest('tr').find('td:eq(11)').find('input').attr('max', max_val);

                                    $(ele).closest('tr').find('td').find('.upd-btn').removeClass('d-none');
                                }
                            }
                        }
                    });

                    if(action_date && qty && price){
                        $(ele).closest('tr').find('td').find('.upd-btn').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                    }
                }
            }

            // RECEIVE / ISSUE QTY
            function qty_h(ele){
                let action_date = $(ele).closest('tr').find('td').find('.action-date-h').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for-h').val(),
                    action_type = (($(ele).closest('tr').find('td').find('.rcv-qty').val() == undefined) ? 2 : 1),
                    qty = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-qty').val() : $(ele).closest('tr').find('td').find('.iss-qty').val()),
                    price = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-price').val() : $(ele).closest('tr').find('td').find('.iss-price').val());

                if(!qty){
                    $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                } else if(parseInt(qty) <= 0){
                    (action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-qty').val('') : $(ele).closest('tr').find('td').find('.iss-qty').val('');
                    $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');

                    return false;
                } else{
                    if(action_date && req_for && price){
                        $(ele).closest('tr').find('td').find('.upd-btn').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                    }
                }
            }

            // RECEIVE / ISSUE PRICE
            function price_h(ele){
                let action_date = $(ele).closest('tr').find('td').find('.action-date-h').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for-h').val(),
                    action_type = (($(ele).closest('tr').find('td').find('.rcv-qty').val() == undefined) ? 2 : 1),
                    qty = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-qty').val() : $(ele).closest('tr').find('td').find('.iss-qty').val()),
                    price = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-price').val() : $(ele).closest('tr').find('td').find('.iss-price').val());

                if(!price){
                    $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                } else if(parseInt(price) <= 0){
                    (action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-price').val('') : $(ele).closest('tr').find('td').find('.iss-price').val('');
                    $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');

                    return false;
                } else{
                    if(action_date && req_for && qty){
                        $(ele).closest('tr').find('td').find('.upd-btn').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.upd-btn').addClass('d-none');
                    }
                }
            }

            // UPDATE INVENTORY
            function update_inventory_2(ele, ele2, ele3){
                let parts_id = ele2,
                    inventory_history_id = ele3,
                    action_date = $(ele).closest('tr').find('td').find('.action-date-h').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for-h').val(),
                    action_type = (($(ele).closest('tr').find('td').find('.rcv-qty').val() == undefined) ? 2 : 1),
                    qty = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-qty').val() : $(ele).closest('tr').find('td').find('.iss-qty').val()),
                    price = ((action_type == 1) ? $(ele).closest('tr').find('td').find('.rcv-price').val() : $(ele).closest('tr').find('td').find('.iss-price').val());

                if(action_date && req_for && qty && price){
                    Swal.fire({
                        title: 'Are you sure?',
                        text: 'Inventory will be updated!',
                        type: 'warning',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonColor: '#5cb85c',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<i class="fas fa-check"></i>&nbsp;&nbsp; Yes',
                        cancelButtonText: '<i class="fas fa-times"></i>&nbsp;&nbsp; No'
                    }).then(function(result){
                        if(result.value){
                            $.ajax({
                                url: '../../api/interactionController',
                                method: 'post',
                                data: {
                                    interact_type: 'update',
                                    interact: ((action_type == 1) ? 'inv_receive' : 'inv_issue'),
                                    parts_id: parts_id,
                                    inventory_history_id: inventory_history_id,
                                    required_for: req_for,
                                    action_date: action_date,
                                    qty: qty,
                                    price: price
                                },
                                dataType: 'json',
                                cache: false,
                                success: function(data){
                                    if(data.Type == 'success'){
                                        let t;

                                        Swal.fire({
                                            title: 'Updating inventory data',
                                            text: 'Please wait...',
                                            timer: 100,
                                            allowOutsideClick: false,
                                            onBeforeOpen: function(){
                                                Swal.showLoading(), t = setInterval(function(){
                                                }, 100);
                                            }
                                        }).then(function(){
                                            Swal.fire({
                                                title: 'Success',
                                                text: data.Reply,
                                                type: 'success',
                                                width: 450,
                                                showCloseButton: false,
                                                allowOutsideClick: false,
                                                confirmButtonColor: '#5cb85c',
                                                confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                            }).then((result) => {
                                                if(result.value){
                                                    window.location.reload(true);
                                                }
                                            });
                                        });
                                    } else if(data.Type == 'error'){
                                        Swal.fire({
                                            title: 'Error',
                                            text: data.Reply,
                                            type: 'error',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                            footer: 'Please insert valid data.'
                                        });
                                    } else if(data.Type == 'invalid'){
                                        Swal.fire({
                                            title: 'Info',
                                            text: data.Reply,
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        }).then((result) => {
                                            if(result.value){
                                                window.location.reload(true);
                                            }
                                        });
                                    } else{
                                        Swal.fire({
                                            title: 'Info',
                                            text: 'Server is under maintenance. Please try again later!',
                                            type: 'info',
                                            width: 450,
                                            showCloseButton: true,
                                            confirmButtonColor: '#5cb85c',
                                            confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!'
                                        });
                                    }

                                    return false;
                                }
                            });
                        }
                    });
                } else{
                    Swal.fire({
                        title: 'Error',
                        text: 'Empty Field Data!',
                        type: 'error',
                        width: 450,
                        showCloseButton: true,
                        confirmButtonColor: '#5cb85c',
                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                        footer: 'Please insert all the field data.'
                    });
                }
            }
        </script>
    </body>
</html>