<?php 
    require_once('../session.php');
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>RRM Inventory by RRM Gorup | Inventory</title>

        <?php 
            require_once('../../sub-page-header.php');

            // PERMISSION
            $user_categories = [1, 3];
            if(!in_array($user_category, $user_categories)){
                header('location: ../dashboard');
            }
        ?>

        <style type="text/css">
            .action-type, .req-for, .qty, .price, .action-date{
                width: 160px;
                display: inline-block;
                text-align: center;
                height: 34px;
                margin-right: 5px;
            }

            .save{
                height: 32px;
                margin-left: 5px;
            }

            .table > thead > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th{
                text-align: center;
                vertical-align: middle;
            }
        </style>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">
            <!-- Topbar Start -->
            <?php include('../../topbar-for-sub-page.php'); ?>
            <!-- end Topbar -->

            <?php include('../../navbar-for-sub-page.php'); ?>
            <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <div class="wrapper full-width-background">   
            <div class="container-fluid">
                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                    <li class="breadcrumb-item active">Inventory List - Summary</li>
                                </ol>
                            </div>
                            <h4 class="page-title"><i class="mdi mdi-shape-outline"></i> Inventory List - Summary</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card-box">
                            <ul class="nav nav-pills navtab-bg nav-justified">
                                <li class="nav-item">
                                    <a href="#a" data-toggle="tab" aria-expanded="false" class="nav-link active summary-cat">
                                        <span class="d-none d-sm-inline">A</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#b" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">B</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#c" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">C</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#d" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">D</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#e" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">E</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#f" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">F</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#g" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">G</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#h" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">H</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#i" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">I</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#j" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">J</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#k" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">K</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#l" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">L</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#m" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">M</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#n" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">N</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#o" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">O</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#p" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">P</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#q" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">Q</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#r" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">R</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#s" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">S</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#t" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">T</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#u" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">U</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#v" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">V</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#w" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">W</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#x" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">X</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#y" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">Y</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#z" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">Z</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#9" data-toggle="tab" aria-expanded="true" class="nav-link summary-cat">
                                        <span class="d-none d-sm-inline">#</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane show active summary-div" id="a">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body" style="overflow-x: auto; overflow-y: auto; width: 99%">
                                                    <table id="basic-datatable2" class="table w-100 nowrap cell-border">
                                                        <thead style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Parts Name</th>
                                                                <th>Parts Unit</th>
                                                                <th>Stock Quantity</th>
                                                                <th>Received Quantity</th>
                                                                <th>Issued Quantity</th>
                                                                <th>Average Rate</th>
                                                                <th class="alert-primary">Receive / Issue Parts</th>
                                                                <!-- <th class="alert-danger">Delete Received / Issued Records</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody class="summary-records">
                                                        </tbody>
                                                        <tfoot style="color: #fff; background-color: #5089de;">
                                                            <tr>
                                                                <th>SL.</th>
                                                                <th>Parts Name</th>
                                                                <th>Parts Unit</th>
                                                                <th>Stock Quantity</th>
                                                                <th>Received Quantity</th>
                                                                <th>Issued Quantity</th>
                                                                <th>Average Rate</th>
                                                                <th class="alert-primary">Receive / Issue Parts</th>
                                                                <!-- <th class="alert-danger">Delete Received / Issued Records</th> -->
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div> <!-- end card body-->
                                            </div> <!-- end card -->
                                        </div><!-- end col-->
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end card-box-->
                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

        <!-- Footer Start -->
        <?php require_once('../../footer-for-sub-page.php'); ?>
        <!-- end Footer -->

        <!-- Custom js -->
        <script type="text/javascript">
            $(document).ready(function(){
                // FETCH SUMMARY DATA
                $.ajax({
                    url: '../../api/inventory',
                    method: 'post',
                    data: {
                        inventory_data_type: 'inventory_summary_list',
                        alpha: 'a'
                    },
                    dataType: 'json',
                    cache: false,
                    async: false,
                    success: function(data){
                        if(data.Type == 'success'){
                            let table_data = '';

                            $.each(data.Reply, function(i, inventory_summary){
                                let tr_style = '';

                                if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty)
                                    tr_style = 'background: #fce0d8 !important';

                                table_data += '<tr style="'+tr_style+'">';
                                    table_data += '<td rowspan="2" class="align-middle">' + (i+1) + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.parts_name + '</td>';

                                    let parts_unit = '';

                                    if(inventory_summary.parts_unit == 1) parts_unit = 'Bag';
                                    else if(inventory_summary.parts_unit == 2) parts_unit = 'Box';
                                    else if(inventory_summary.parts_unit == 3) parts_unit = 'Box/Pcs';
                                    else if(inventory_summary.parts_unit == 4) parts_unit = 'Bun';
                                    else if(inventory_summary.parts_unit == 5) parts_unit = 'Bundle';
                                    else if(inventory_summary.parts_unit == 6) parts_unit = 'Can';
                                    else if(inventory_summary.parts_unit == 7) parts_unit = 'Cartoon';
                                    else if(inventory_summary.parts_unit == 8) parts_unit = 'Challan';
                                    else if(inventory_summary.parts_unit == 9) parts_unit = 'Coil';
                                    else if(inventory_summary.parts_unit == 10) parts_unit = 'Drum';
                                    else if(inventory_summary.parts_unit == 11) parts_unit = 'Feet';
                                    else if(inventory_summary.parts_unit == 12) parts_unit = 'Gallon';
                                    else if(inventory_summary.parts_unit == 13) parts_unit = 'Item';
                                    else if(inventory_summary.parts_unit == 14) parts_unit = 'Job';
                                    else if(inventory_summary.parts_unit == 15) parts_unit = 'Kg';
                                    else if(inventory_summary.parts_unit == 16) parts_unit = 'Kg/Bundle';
                                    else if(inventory_summary.parts_unit == 17) parts_unit = 'Kv';
                                    else if(inventory_summary.parts_unit == 18) parts_unit = 'Lbs';
                                    else if(inventory_summary.parts_unit == 19) parts_unit = 'Ltr';
                                    else if(inventory_summary.parts_unit == 20) parts_unit = 'Mtr';
                                    else if(inventory_summary.parts_unit == 21) parts_unit = 'Pack';
                                    else if(inventory_summary.parts_unit == 22) parts_unit = 'Pack/Pcs';
                                    else if(inventory_summary.parts_unit == 23) parts_unit = 'Pair';
                                    else if(inventory_summary.parts_unit == 24) parts_unit = 'Pcs';
                                    else if(inventory_summary.parts_unit == 25) parts_unit = 'Pound';
                                    else if(inventory_summary.parts_unit == 26) parts_unit = 'Qty';
                                    else if(inventory_summary.parts_unit == 27) parts_unit = 'Roll';
                                    else if(inventory_summary.parts_unit == 28) parts_unit = 'Set';
                                    else if(inventory_summary.parts_unit == 29) parts_unit = 'Truck';
                                    else if(inventory_summary.parts_unit == 30) parts_unit = 'Unit';
                                    else if(inventory_summary.parts_unit == 31) parts_unit = 'Yeard';
                                    else if(inventory_summary.parts_unit == 32) parts_unit = '(Unit Unknown)';
                                    else if(inventory_summary.parts_unit == 33) parts_unit = 'SFT';
                                    else if(inventory_summary.parts_unit == 34) parts_unit = 'RFT';
                                    else if(inventory_summary.parts_unit == 35) parts_unit = 'CFT';

                                    table_data += '<td class="align-middle">' + parts_unit + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.parts_qty + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.tot_rcv_qty + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.tot_iss_qty + '</td>';
                                    table_data += '<td class="align-middle">' + inventory_summary.parts_avg_rate + '</td>';

                                    let td_class = 'alert-primary';
                                    if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty)
                                        td_class = 'alert-danger';

                                    let req_field = '';
                                    if(inventory_summary.parts_qty == 0)
                                        req_field = 'disabled';

                                    table_data += '<td class="'+td_class+'">\
                                        \
                                        <select class="select-b action-type" onchange="action_type(this)">\
                                            <option value="">Choose type</option>\
                                            <option value="1">Receive</option>\
                                            <option value="2">Issue</option>\
                                        </select>\
                                        \
                                        <select class="d-none req-for" onchange="required_for(this, '+ inventory_summary.parts_id +')">\
                                            <option value="">Choose required for</option>\
                                            <option value="1">BCP-CCM</option>\
                                            <option value="2">BCP-Furnace</option>\
                                            <option value="3">Concast-CCM</option>\
                                            <option value="4">Concast-Furnace</option>\
                                            <option value="5">HRM</option>\
                                            <option value="6">HRM Unit-2</option>\
                                            <option value="7">Lal Masjid</option>\
                                            <option value="8">Sonargaon</option>\
                                            <option value="9">General</option>\
                                        </select>\
                                        \
                                        <input type="text" class="form-control d-none action-date" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" onchange="action_date(this, '+ inventory_summary.parts_id +')" oninput="validate_action_date(this)">\
                                        \
                                        <input type="number" class="form-control d-none qty" placeholder="Insert qty." oninput="qty(this)">\
                                        \
                                        <input type="number" class="form-control d-none price" placeholder="Insert price" oninput="price(this)">';

                                        if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty){
                                            table_data += '<i class="fa fa-info-circle mt-1" title="Warning: Low quantity!"></i>';
                                        }

                                        table_data += '<button title="Save" type="button" class="btn btn-xs btn-info d-none save" onclick="update_inventory(this, '+ inventory_summary.parts_id +')"><i class="mdi mdi-content-save"></i></button>\
                                        \
                                        <br>\
                                        \
                                        <p class="mt-2 mb-0 badge badge-danger text-left warning d-none" style="font-size: 100%"></p>\
                                    </td>';
                                table_data += '</tr>';

                                table_data += '<tr style="'+tr_style+'">';
                                    table_data += '<td colspan="8" class="text-left">';
                                        table_data += '<span class="d-none">' + (i+1) + '.1' + ' ' + inventory_summary.parts_name + '</span>';

                                        table_data += '<strong>Pending Receive Till Date</strong>:&emsp; <span style="color: #0000ff">BCP-CCM: </span><span style="color: #0000ff">' + inventory_summary.bcp_ccm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff0000">BCP-FUR: </span><span style="color: #ff0000">' + inventory_summary.bcp_fur.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff00a5">CON-CCM: </span><span style="color: #ff00a5">' + inventory_summary.con_ccm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #00d0ff">CON-FUR: </span><span style="color: #00d0ff">' + inventory_summary.con_fur.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff8d00">HRM: </span><span style="color: #ff8d00">' + inventory_summary.hrm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #6200ff">HRM Unit-2: </span><span style="color: #6200ff">' + inventory_summary.hrm_2.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #0089ff">Lal Masjid: </span><span style="color: #0089ff">' + inventory_summary.lal_masjid.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #009612">Sonargaon: </span><span style="color: #009612">' + inventory_summary.sonargaon.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #960076">General: </span><span style="color: #960076">' + inventory_summary.general.toFixed(3) + '</span>';
                                    table_data += '</td>';
                                    table_data += '<td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td>';
                                table_data += '</tr>';
                            });

                            let t;

                            Swal.fire({
                                title: 'Loading Inventory Data',
                                text: 'Please wait...',
                                timer: 100,
                                allowOutsideClick: false,
                                onBeforeOpen: function(){
                                    Swal.showLoading(), t = setInterval(function(){
                                    }, 100);
                                }
                            }).then(function(){
                                $('.summary-records').append(table_data);

                                $('#basic-datatable2').DataTable({
                                    'ordering': false,
                                    language: {
                                        paginate: {
                                            previous: '<i class="mdi mdi-chevron-left">',
                                            next: '<i class="mdi mdi-chevron-right">'
                                        }
                                    },
                                    drawCallback: function(){
                                        $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                    }
                                });
                            });
                        }
                    }
                });

                $('.summary-cat').on('click', function(){
                    $('.summary-div').attr('id', $(this).attr('href')[1]);

                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            inventory_data_type: 'inventory_summary_list',
                            alpha: $(this).attr('href')[1]
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                let table_data = '';

                                $.each(data.Reply, function(i, inventory_summary){
                                    let tr_style = '';

                                    if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty)
                                        tr_style = 'background: #fce0d8 !important';

                                    table_data += '<tr style="'+tr_style+'">';
                                        table_data += '<td rowspan="2" class="align-middle">' + (i+1) + '</td>';
                                        table_data += '<td class="align-middle">' + inventory_summary.parts_name + '</td>';

                                        let parts_unit = '';

                                        if(inventory_summary.parts_unit == 1) parts_unit = 'Bag';
                                        else if(inventory_summary.parts_unit == 2) parts_unit = 'Box';
                                        else if(inventory_summary.parts_unit == 3) parts_unit = 'Box/Pcs';
                                        else if(inventory_summary.parts_unit == 4) parts_unit = 'Bun';
                                        else if(inventory_summary.parts_unit == 5) parts_unit = 'Bundle';
                                        else if(inventory_summary.parts_unit == 6) parts_unit = 'Can';
                                        else if(inventory_summary.parts_unit == 7) parts_unit = 'Cartoon';
                                        else if(inventory_summary.parts_unit == 8) parts_unit = 'Challan';
                                        else if(inventory_summary.parts_unit == 9) parts_unit = 'Coil';
                                        else if(inventory_summary.parts_unit == 10) parts_unit = 'Drum';
                                        else if(inventory_summary.parts_unit == 11) parts_unit = 'Feet';
                                        else if(inventory_summary.parts_unit == 12) parts_unit = 'Gallon';
                                        else if(inventory_summary.parts_unit == 13) parts_unit = 'Item';
                                        else if(inventory_summary.parts_unit == 14) parts_unit = 'Job';
                                        else if(inventory_summary.parts_unit == 15) parts_unit = 'Kg';
                                        else if(inventory_summary.parts_unit == 16) parts_unit = 'Kg/Bundle';
                                        else if(inventory_summary.parts_unit == 17) parts_unit = 'Kv';
                                        else if(inventory_summary.parts_unit == 18) parts_unit = 'Lbs';
                                        else if(inventory_summary.parts_unit == 19) parts_unit = 'Ltr';
                                        else if(inventory_summary.parts_unit == 20) parts_unit = 'Mtr';
                                        else if(inventory_summary.parts_unit == 21) parts_unit = 'Pack';
                                        else if(inventory_summary.parts_unit == 22) parts_unit = 'Pack/Pcs';
                                        else if(inventory_summary.parts_unit == 23) parts_unit = 'Pair';
                                        else if(inventory_summary.parts_unit == 24) parts_unit = 'Pcs';
                                        else if(inventory_summary.parts_unit == 25) parts_unit = 'Pound';
                                        else if(inventory_summary.parts_unit == 26) parts_unit = 'Qty';
                                        else if(inventory_summary.parts_unit == 27) parts_unit = 'Roll';
                                        else if(inventory_summary.parts_unit == 28) parts_unit = 'Set';
                                        else if(inventory_summary.parts_unit == 29) parts_unit = 'Truck';
                                        else if(inventory_summary.parts_unit == 30) parts_unit = 'Unit';
                                        else if(inventory_summary.parts_unit == 31) parts_unit = 'Yeard';
                                        else if(inventory_summary.parts_unit == 32) parts_unit = '(Unit Unknown)';
                                        else if(inventory_summary.parts_unit == 33) parts_unit = 'SFT';
                                        else if(inventory_summary.parts_unit == 34) parts_unit = 'RFT';
                                        else if(inventory_summary.parts_unit == 35) parts_unit = 'CFT';

                                        table_data += '<td class="align-middle">' + parts_unit + '</td>';
                                        table_data += '<td class="align-middle">' + inventory_summary.parts_qty + '</td>';
                                        table_data += '<td class="align-middle">' + inventory_summary.tot_rcv_qty + '</td>';
                                        table_data += '<td class="align-middle">' + inventory_summary.tot_iss_qty + '</td>';
                                        table_data += '<td class="align-middle">' + inventory_summary.parts_avg_rate + '</td>';

                                        let td_class = 'alert-primary';
                                        if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty)
                                            td_class = 'alert-danger';

                                        let req_field = '';
                                        if(inventory_summary.parts_qty == 0)
                                            req_field = 'disabled';

                                        table_data += '<td class="'+td_class+'">\
                                            \
                                            <select class="select-b action-type" onchange="action_type(this)">\
                                                <option value="">Choose type</option>\
                                                <option value="1">Receive</option>\
                                                <option value="2">Issue</option>\
                                            </select>\
                                            \
                                            <select class="d-none req-for" onchange="required_for(this, '+ inventory_summary.parts_id +')">\
                                                <option value="">Choose required for</option>\
                                                <option value="1">BCP-CCM</option>\
                                                <option value="2">BCP-Furnace</option>\
                                                <option value="3">Concast-CCM</option>\
                                                <option value="4">Concast-Furnace</option>\
                                                <option value="5">HRM</option>\
                                                <option value="6">HRM Unit-2</option>\
                                                <option value="7">Lal Masjid</option>\
                                                <option value="8">Sonargaon</option>\
                                                <option value="9">General</option>\
                                            </select>\
                                            \
                                            <input type="text" class="form-control d-none action-date" data-provide="datepicker" data-date-autoclose="true" data-date-format="yyyy-mm-dd" data-date-start-date="" data-date-end-date="1d" placeholder="Insert date" onchange="action_date(this, '+ inventory_summary.parts_id +')" oninput="validate_action_date(this)">\
                                            \
                                            <input type="number" class="form-control d-none qty" placeholder="Insert qty." oninput="qty(this)">\
                                            \
                                            <input type="number" class="form-control d-none price" placeholder="Insert price" oninput="price(this)">';

                                            if(inventory_summary.parts_qty <= inventory_summary.parts_alert_qty){
                                                table_data += '<i class="fa fa-info-circle mt-1" title="Warning: Low quantity!"></i>';
                                            }

                                            table_data += '<button title="Save" type="button" class="btn btn-xs btn-info d-none save" onclick="update_inventory(this, '+ inventory_summary.parts_id +')"><i class="mdi mdi-content-save"></i></button>\
                                            \
                                            <br>\
                                            \
                                            <p class="mt-2 mb-0 badge badge-danger text-left warning d-none" style="font-size: 100%"></p>\
                                        </td>';
                                    table_data += '</tr>';

                                    table_data += '<tr style="'+tr_style+'">';
                                        table_data += '<td colspan="8" class="text-left">';
                                            table_data += '<span class="d-none">' + (i+1) + '.1' + ' ' + inventory_summary.parts_name + '</span>';

                                            table_data += '<strong>Pending Receive Till Date</strong>:&emsp; <span style="color: #0000ff">BCP-CCM: </span><span style="color: #0000ff">' + inventory_summary.bcp_ccm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff0000">BCP-FUR: </span><span style="color: #ff0000">' + inventory_summary.bcp_fur.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff00a5">CON-CCM: </span><span style="color: #ff00a5">' + inventory_summary.con_ccm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #00d0ff">CON-FUR: </span><span style="color: #00d0ff">' + inventory_summary.con_fur.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #ff8d00">HRM: </span><span style="color: #ff8d00">' + inventory_summary.hrm.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #6200ff">HRM Unit-2: </span><span style="color: #6200ff">' + inventory_summary.hrm_2.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #0089ff">Lal Masjid: </span><span style="color: #0089ff">' + inventory_summary.lal_masjid.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #009612">Sonargaon: </span><span style="color: #009612">' + inventory_summary.sonargaon.toFixed(3) + '</span> &nbsp;|&nbsp; <span style="color: #960076">General: </span><span style="color: #960076">' + inventory_summary.general.toFixed(3) + '</span>';
                                        table_data += '</td>';
                                        table_data += '<td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td><td class="d-none"></td>';
                                    table_data += '</tr>';
                                });

                                let t;

                                Swal.fire({
                                    title: 'Loading Inventory Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    let table = $('#basic-datatable2').DataTable();

                                    table.destroy();

                                    $('.summary-records').empty().append(table_data);

                                    $('#basic-datatable2').DataTable({
                                        'ordering': false,
                                        language: {
                                            paginate: {
                                                previous: '<i class="mdi mdi-chevron-left">',
                                                next: '<i class="mdi mdi-chevron-right">'
                                            }
                                        },
                                        drawCallback: function(){
                                            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                        }
                                    });
                                });
                            } else{
                                let t;

                                Swal.fire({
                                    title: 'Loading Inventory Data',
                                    text: 'Please wait...',
                                    timer: 100,
                                    allowOutsideClick: false,
                                    onBeforeOpen: function(){
                                        Swal.showLoading(), t = setInterval(function(){
                                        }, 100);
                                    }
                                }).then(function(){
                                    let table = $('#basic-datatable2').DataTable();

                                    table.destroy();
                        
                                    $('.summary-records').empty();

                                    $('#basic-datatable2').DataTable({
                                        'ordering': false,
                                        language: {
                                            paginate: {
                                                previous: '<i class="mdi mdi-chevron-left">',
                                                next: '<i class="mdi mdi-chevron-right">'
                                            }
                                        },
                                        drawCallback: function(){
                                            $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                                        }
                                    });
                                });
                            }
                        }
                    });
                });
            });

            // ACTION TYPE
            function action_type(ele){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val();

                $(ele).closest('tr').find('td').find('.save').addClass('d-none');

                if(action_type == 1){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('').addClass('d-none');
                    $(ele).closest('tr').find('td').find('.req-for, .action-date').val('').removeClass('d-none');
                } else if(action_type == 2){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('').addClass('d-none');
                    $(ele).closest('tr').find('td').find('.req-for, .action-date').val('').removeClass('d-none');
                } else{
                    $(ele).closest('tr').find('td').find('.req-for, .qty, .price, .action-date').addClass('d-none');
                }
            }

            let max_parts_qty = 0;

            // REQUIRED FOR
            function required_for(ele, ele2){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    action_date = $(ele).closest('tr').find('td').find('.action-date').val(),
                    qty = $(ele).closest('tr').find('td').find('.qty').val(),
                    parts_id = ele2;

                if(action_type && $(ele).val() && action_date){
                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            parts_id: parts_id,
                            required_for: $(ele).val(),
                            action_date: action_date,
                            inventory_data_type: ((action_type == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            if(data.Type == 'success'){
                                if(data.Reply[0].parts_qty == null){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'No purchase record found!' : 'No inventory / receive record found!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some before receive.' : 'Please receive some before issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else if(data.Reply[0].parts_qty <= 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'All purchased & borrowed parts have been received!' : 'All received parts have been issued!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some to receive.' : 'Please receive some to issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else{
                                    max_parts_qty = data.Reply[0].parts_qty;

                                    if(action_type == 1){
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val(data.Reply[0].parts_qty);
                                        $(ele).closest('tr').find('td').find('.price').removeClass('d-none').val(data.Reply[0].parts_price.toFixed(2));
                                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                                    } else{
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    }
                                }
                            }
                        }
                    });
                }

                if(!$(ele).val() || !action_date){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                }
            }

            // ACTION DATE
            function action_date(ele, ele2){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for').val(),
                    qty = $(ele).closest('tr').find('td').find('.qty').val();
                    parts_id = ele2;

                if(action_type && req_for && $(ele).val()){
                    $.ajax({
                        url: '../../api/inventory',
                        method: 'post',
                        data: {
                            parts_id: parts_id,
                            required_for: req_for,
                            action_date: $(ele).val(),
                            inventory_data_type: ((action_type == 1) ? 'inventory_parts_receive' : 'inventory_parts_issue')
                        },
                        dataType: 'json',
                        cache: false,
                        async: false,
                        success: function(data){
                            console.log(data.Reply[0].parts_qty);
                            if(data.Type == 'success'){
                                if(data.Reply[0].parts_qty == null){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'No purchase record found!' : 'No inventory / receive record found!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some before receive.' : 'Please receive some before issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else if(data.Reply[0].parts_qty <= 0){
                                    Swal.fire({
                                        title: 'Error',
                                        text: ((action_type == 1) ? 'All purchased & borrowed parts have been received!' : 'All received parts have been issued!'),
                                        type: 'error',
                                        width: 450,
                                        showCloseButton: true,
                                        confirmButtonColor: '#5cb85c',
                                        confirmButtonText: '<i class="fas fa-thumbs-up"></i>&nbsp;&nbsp; Okay!',
                                        footer: ((action_type == 1) ? 'Please purchase some to receive.' : 'Please receive some to issue.')
                                    }).then(function(){
                                        $(ele).closest('tr').find('td').find('.qty').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.price').addClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    });
                                } else{
                                    max_parts_qty = data.Reply[0].parts_qty;

                                    if(action_type == 1){
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val(data.Reply[0].parts_qty);
                                        $(ele).closest('tr').find('td').find('.price').removeClass('d-none').val(data.Reply[0].parts_price.toFixed(2));
                                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                                    } else{
                                        $(ele).closest('tr').find('td').find('.qty').removeClass('d-none').val('');
                                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                                    }
                                }
                            }
                        }
                    });
                }

                if(!$(ele).val() || !req_for){
                    $(ele).closest('tr').find('td').find('.qty, .price').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                }
            }

            // VALIDATE ACTION DATE FIELD
            function validate_action_date(ele){
                $(ele).closest('tr').find('td').find('.action-date').val('');
            }

            // QTY
            function qty(ele){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    action_date = $(ele).closest('tr').find('td').find('.action-date').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for').val(),
                    qty = $(ele).val(),
                    price = $(ele).closest('tr').find('td').find('.price').val();

                if(!qty){
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                } else if(parseFloat(qty) <= 0){
                    $(ele).closest('tr').find('td').find('.qty').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                } else if(parseFloat(qty) > parseFloat(max_parts_qty)){
                    $(ele).closest('tr').find('td').find('.qty').val(parseFloat(max_parts_qty));
                } else{
                    if((action_type == 1 && action_date && req_for && price) || (action_type == 2 && action_date && req_for)){
                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                    }
                }
            }

            // PRICE
            function price(ele){
                let action_type = $(ele).closest('tr').find('td').find('.action-type').val(),
                    action_date = $(ele).closest('tr').find('td').find('.action-date').val(),
                    req_for = $(ele).closest('tr').find('td').find('.req-for').val(),
                    qty = $(ele).closest('tr').find('td').find('.qty').val(),
                    price = $(ele).val();

                if(!price){
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                } else if(price <= 0){
                    $(ele).closest('tr').find('td').find('.price').val('');
                    $(ele).closest('tr').find('td').find('.save').addClass('d-none');

                    return false;
                } else{
                    if((action_type == 1 && action_date && req_for && qty) || (action_type == 2 && action_date && qty)){
                        $(ele).closest('tr').find('td').find('.save').removeClass('d-none');
                    } else{
                        $(ele).closest('tr').find('td').find('.save').addClass('d-none');
                    }
                }
            }
        </script>
    </body>
</html>