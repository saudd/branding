<?php
    namespace Loan;

    class Loan{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        // FETCH A CONSUMABLE REQUISITION
        function fetch_requisition_con($requisition_id){
            $requisition_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_requisition WHERE requisition_id = '$requisition_id'");

            if(mysqli_num_rows($requisition_query) > 0){
                $requisition_info = mysqli_fetch_assoc($requisition_query);

                $requisition_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks, r.parts_qty AS r_qty, i.parts_qty AS i_qty, i.parts_rate AS price FROM rrmsteel_con_requisition_data r INNER JOIN rrmsteel_parts p ON p.parts_id = r.parts_id INNER JOIN rrmsteel_inv_summary i ON i.parts_id = r.parts_id WHERE r.requisition_id = '$requisition_id' AND r.loan = 1");

                if(mysqli_num_rows($requisition_data_query) > 0){
                    while($row = mysqli_fetch_assoc($requisition_data_query)){
                        $loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT rd.parts_id AS borrowed_parts_id, SUM(rd.parts_qty) AS borrowed_parts_qty FROM rrmsteel_con_loan_data rd INNER JOIN rrmsteel_con_loan r ON r.loan_id = rd.loan_id WHERE r.requisition_id = '$requisition_id' AND rd.parts_id = '".$row['parts_id']."' LIMIT 1"));

                        if(isset($loan_data_info)){
                            $borrowed_parts_id = $loan_data_info['borrowed_parts_id'] == null ? 0 : $loan_data_info['borrowed_parts_id'];
                            $borrowed_parts_qty = $loan_data_info['borrowed_parts_qty'] == null ? 0 : $loan_data_info['borrowed_parts_qty'];
                        } else{
                            $borrowed_parts_id = 0;
                            $borrowed_parts_qty = 0;
                        }

                        $data[] = [
                            'p_approval_status' => $requisition_info['p_approval_status'],
                            'approval_status' => $requisition_info['approval_status'],
                            'required_for' => $row['required_for'],
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'r_qty' => $row['r_qty'],
                            'parts_unit' => $row['unit'],
                            'i_qty' => $row['i_qty'],
                            'price' => $row['price'],
                            'parts_usage' => $row['parts_usage'],
                            'remarks' => $row['r_remarks'],
                            'requisition_id' => $row['requisition_id'],
                            'borrowed_parts_id' => $borrowed_parts_id,
                            'borrowed_parts_qty' => $borrowed_parts_qty
                        ];
                    }

                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No loan data found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No loan data found !'
                )));
            }
        }

        // FETCH A SPARE REQUISITION
        function fetch_requisition_spr($requisition_id){
            $requisition_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_requisition WHERE requisition_id = '$requisition_id'");

            if(mysqli_num_rows($requisition_query) > 0){
                $requisition_info = mysqli_fetch_assoc($requisition_query);

                $requisition_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks, r.parts_qty AS r_qty, i.parts_qty AS i_qty, i.parts_rate AS price FROM rrmsteel_spr_requisition_data r INNER JOIN rrmsteel_parts p ON p.parts_id = r.parts_id INNER JOIN rrmsteel_inv_summary i ON i.parts_id = r.parts_id WHERE r.requisition_id = '$requisition_id' AND r.loan = 1");

                if(mysqli_num_rows($requisition_data_query) > 0){
                    while($row = mysqli_fetch_assoc($requisition_data_query)){
                        $loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT rd.parts_id AS borrowed_parts_id, SUM(rd.parts_qty) AS borrowed_parts_qty FROM rrmsteel_spr_loan_data rd INNER JOIN rrmsteel_spr_loan r ON r.loan_id = rd.loan_id WHERE r.requisition_id = '$requisition_id' AND rd.parts_id = '".$row['parts_id']."' LIMIT 1"));

                        if(isset($loan_data_info)){
                            $borrowed_parts_id = $loan_data_info['borrowed_parts_id'] == null ? 0 : $loan_data_info['borrowed_parts_id'];
                            $borrowed_parts_qty = $loan_data_info['borrowed_parts_qty'] == null ? 0 : $loan_data_info['borrowed_parts_qty'];
                        } else{
                            $borrowed_parts_id = 0;
                            $borrowed_parts_qty = 0;
                        }

                        $data[] = [
                            'p_approval_status' => $requisition_info['p_approval_status'],
                            'approval_status' => $requisition_info['approval_status'],
                            'required_for' => $row['required_for'],
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'r_qty' => $row['r_qty'],
                            'parts_unit' => $row['unit'],
                            'i_qty' => $row['i_qty'],
                            'price' => $row['price'],
                            'old_spare_details' => $row['old_spare_details'],
                            'status' => $row['status'],
                            'remarks' => $row['r_remarks'],
                            'requisition_id' => $row['requisition_id'],
                            'borrowed_parts_id' => $borrowed_parts_id,
                            'borrowed_parts_qty' => $borrowed_parts_qty
                        ];
                    }

                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No loan data found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No loan data found !'
                )));
            }
        }

        // FETCH A CONSUMABLE LOAN
        function fetch_loan_con($requisition_id){
            $loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan WHERE requisition_id = '$requisition_id' LIMIT 1");

            if(mysqli_num_rows($loan_query) > 0){
                $loan_info = mysqli_fetch_assoc($loan_query);
                $loan_id = $loan_info['loan_id'];

                $loan_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks FROM rrmsteel_con_loan_data r INNER JOIN rrmsteel_parts pr ON pr.parts_id = r.parts_id INNER JOIN rrmsteel_party pt ON pt.party_id = r.party_id WHERE loan_id = '$loan_id' ORDER BY r.parts_id");

                while($row = mysqli_fetch_assoc($loan_data_query)){
                    $parts_id = $row['parts_id'];

                    $requisition_data_query = mysqli_query($this->conn, "SELECT parts_qty FROM rrmsteel_con_requisition_data WHERE requisition_id = '$requisition_id' AND parts_id = '$parts_id' AND loan = 1 LIMIT 1");
                    $requisition_info = mysqli_fetch_assoc($requisition_data_query);
                    $requisitioned_parts_qty = $requisition_info['parts_qty'];

                    $loan_data_query_2 = mysqli_query($this->conn, "SELECT COUNT(*) AS loan_count, SUM(parts_qty) AS borrowed_parts_qty FROM rrmsteel_con_loan_data r WHERE loan_id = '$loan_id' AND parts_id = '$parts_id'");
                    $loan_data_info = mysqli_fetch_assoc($loan_data_query_2);
                    $borrowed_parts_qty = $loan_data_info['borrowed_parts_qty'];
                    $loan_count = $loan_data_info['loan_count'];

                    $data1[] = [
                        'loan_id' => $loan_id,
                        'required_for' => $row['required_for'],
                        'parts_id' => $parts_id,
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'requisitioned_parts_qty' => $requisitioned_parts_qty,
                        'borrowed_parts_qty' => $borrowed_parts_qty,
                        'loan_indx_f' => 1,
                        'loan_indx_l' => (int)$loan_count,
                        'parts_usage' => $row['parts_usage'],
                        'remarks' => $row['r_remarks']
                    ];

                    $data2[] = [
                        'loan_data_id' => $row['loan_data_id'],
                        'parts_id' => $parts_id,
                        'parts_qty' => $row['parts_qty'],
                        'price' => $row['price'],
                        'party_id' => $row['party_id'],
                        'party_name' => $row['party_name'],
                        'gate_no' => $row['gate_no'],
                        'challan_no' => $row['challan_no'],
                        'challan_photo' => $row['challan_photo'],
                        'bill_photo' => $row['bill_photo'],
                        'loan_date' => $row['loan_date']
                    ];
                }

                $data1 = array_values(array_unique($data1, SORT_REGULAR));

                foreach($data1 as $key => $value){
                    if($key == 0){
                        $loan_indx_f = $value['loan_indx_f'];
                        $loan_indx_l = $value['loan_indx_l'];
                    } else{
                        $loan_indx_f = $loan_indx_l + 2;
                        $loan_indx_l = $loan_indx_f + $value['loan_indx_l'] - 1;
                    }

                    $data1[$key]['loan_indx_f'] = $loan_indx_f;
                    $data1[$key]['loan_indx_l'] = $loan_indx_l;
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply1' => $data1,
                    'Reply2' => $data2
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No loan data found !'
                )));
            }
        }

        // FETCH A SPARE LOAN
        function fetch_loan_spr($requisition_id){
            $loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan WHERE requisition_id = '$requisition_id' LIMIT 1");

            if(mysqli_num_rows($loan_query) > 0){
                $loan_info = mysqli_fetch_assoc($loan_query);
                $loan_id = $loan_info['loan_id'];

                $loan_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks FROM rrmsteel_spr_loan_data r INNER JOIN rrmsteel_parts pr ON pr.parts_id = r.parts_id INNER JOIN rrmsteel_party pt ON pt.party_id = r.party_id WHERE loan_id = '$loan_id' ORDER BY r.parts_id");

                while($row = mysqli_fetch_assoc($loan_data_query)){
                    $parts_id = $row['parts_id'];

                    $requisition_data_query = mysqli_query($this->conn, "SELECT parts_qty FROM rrmsteel_spr_requisition_data WHERE requisition_id = '$requisition_id' AND parts_id = '$parts_id' AND loan = 1 LIMIT 1");
                    $requisition_info = mysqli_fetch_assoc($requisition_data_query);
                    $requisitioned_parts_qty = $requisition_info['parts_qty'];

                    $loan_data_query_2 = mysqli_query($this->conn, "SELECT COUNT(*) AS loan_count, SUM(parts_qty) AS borrowed_parts_qty FROM rrmsteel_spr_loan_data r WHERE loan_id = '$loan_id' AND parts_id = '$parts_id'");
                    $loan_data_info = mysqli_fetch_assoc($loan_data_query_2);
                    $borrowed_parts_qty = $loan_data_info['borrowed_parts_qty'];
                    $loan_count = $loan_data_info['loan_count'];

                    $data1[] = [
                        'loan_id' => $loan_id,
                        'required_for' => $row['required_for'],
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'requisitioned_parts_qty' => $requisitioned_parts_qty,
                        'borrowed_parts_qty' => $borrowed_parts_qty,
                        'loan_indx_f' => 1,
                        'loan_indx_l' => (int)$loan_count,
                        'old_spare_details' => $row['old_spare_details'],
                        'status' => (($row['status'] == 1) ? 'Repairable' : 'Unusual'),
                        'remarks' => $row['r_remarks']
                    ];

                    $data2[] = [
                        'loan_data_id' => $row['loan_data_id'],
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty'],
                        'price' => $row['price'],
                        'party_id' => $row['party_id'],
                        'party_name' => $row['party_name'],
                        'gate_no' => $row['gate_no'],
                        'challan_no' => $row['challan_no'],
                        'challan_photo' => $row['challan_photo'],
                        'bill_photo' => $row['bill_photo'],
                        'loan_date' => $row['loan_date']
                    ];
                }

                $data1 = array_values(array_unique($data1, SORT_REGULAR));

                foreach($data1 as $key => $value){
                    if($key == 0){
                        $loan_indx_f = $value['loan_indx_f'];
                        $loan_indx_l = $value['loan_indx_l'];
                    } else{
                        $loan_indx_f = $loan_indx_l + 2;
                        $loan_indx_l = $loan_indx_f + $value['loan_indx_l'] - 1;
                    }

                    $data1[$key]['loan_indx_f'] = $loan_indx_f;
                    $data1[$key]['loan_indx_l'] = $loan_indx_l;
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply1' => $data1,
                    'Reply2' => $data2
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No loan data found !'
                )));
            }
        }

        // FETCH FILTERED LOAN
        function fetch_filtered_loan($type, $party_id, $parts_id){
            $data1 = []; $data2 = []; $data3 = []; $data4 = []; $flag = 1;

            if($type == 1){
                if(!$party_id && !$parts_id){
                    $con_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan_data pr INNER JOIN rrmsteel_con_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id");
                    $spr_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan_data pr INNER JOIN rrmsteel_spr_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id");

                    if(mysqli_num_rows($con_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($con_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }
                } elseif($party_id && !$parts_id){
                    $con_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan_data pr INNER JOIN rrmsteel_con_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id'");
                    $spr_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan_data pr INNER JOIN rrmsteel_spr_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id'");

                    if(mysqli_num_rows($con_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($con_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }
                } elseif(!$party_id && $parts_id){
                    $con_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan_data pr INNER JOIN rrmsteel_con_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.parts_id = '$parts_id'");
                    $spr_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan_data pr INNER JOIN rrmsteel_spr_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.parts_id = '$parts_id'");

                    if(mysqli_num_rows($con_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($con_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }
                } elseif($party_id && $parts_id){
                    $con_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan_data pr INNER JOIN rrmsteel_con_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.parts_id = '$parts_id'");
                    $spr_loan_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan_data pr INNER JOIN rrmsteel_spr_loan p ON p.loan_id = pr.loan_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.parts_id = '$parts_id'");

                    if(mysqli_num_rows($con_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($con_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_loan_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_loan_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'loan_data_id' => $row['loan_data_id'],
                                'loan_id' => $row['loan_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'loan_date' => $row['loan_date']
                            ];
                        }
                    }
                } else{
                    $flag = 0;
                }
            } else{
                if($parts_id){
                    $con_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_con_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.parts_id = '$parts_id' AND rd.loan = 1 AND NOT EXISTS (SELECT pr.requisition_id FROM rrmsteel_con_loan pr WHERE pr.requisition_id = rd.requisition_id)");
                    $spr_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_spr_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.parts_id = '$parts_id' AND rd.loan = 1 AND NOT EXISTS (SELECT pr.requisition_id FROM rrmsteel_spr_loan pr WHERE pr.requisition_id = rd.requisition_id)");

                    if(mysqli_num_rows($con_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($con_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }

                    $con_loan_data_query = mysqli_query($this->conn, "SELECT prd.*, p.parts_name, p.unit, pr.requisition_id FROM rrmsteel_con_loan_data prd INNER JOIN rrmsteel_parts p ON p.parts_id = prd.parts_id INNER JOIN rrmsteel_con_loan pr ON pr.loan_id = prd.loan_id WHERE prd.parts_id = '$parts_id' GROUP BY prd.loan_id, prd.parts_id");
                    $spr_loan_data_query = mysqli_query($this->conn, "SELECT prd.*, p.parts_name, p.unit, pr.requisition_id FROM rrmsteel_spr_loan_data prd INNER JOIN rrmsteel_parts p ON p.parts_id = prd.parts_id INNER JOIN rrmsteel_spr_loan pr ON pr.loan_id = prd.loan_id WHERE prd.parts_id = '$parts_id' GROUP BY prd.loan_id, prd.parts_id");

                    if(mysqli_num_rows($con_loan_data_query) > 0){
                        while($row = mysqli_fetch_assoc($con_loan_data_query)){
                            $con_loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT (req_qty - SUM(parts_qty)) AS rem_qty FROM rrmsteel_con_loan_data WHERE loan_id = '".$row['loan_id']."' GROUP BY parts_id"));
                            $rem_qty = $con_loan_data_info['rem_qty'];

                            if($rem_qty > 0){
                                $required_for = $row['required_for'];

                                if($required_for == 1)
                                    $required_for = 'BCP-CCM';
                                elseif($required_for == 2)
                                    $required_for = 'BCP-Furnace';
                                elseif($required_for == 3)
                                    $required_for = 'Concast-CCM';
                                elseif($required_for == 4)
                                    $required_for = 'Concast-Furnace';
                                elseif($required_for == 5)
                                    $required_for = 'HRM';
                                elseif($required_for == 6)
                                    $required_for = 'HRM Unit-2';
                                elseif($required_for == 7)
                                    $required_for = 'Lal Masjid';
                                elseif($required_for == 8)
                                    $required_for = 'Sonargaon';
                                elseif($required_for == 9)
                                    $required_for = 'General';

                                $parts_unit = $row['unit'];

                                if($row['unit'] == 1)
                                    $parts_unit = 'Bag';
                                elseif($row['unit'] == 2)
                                    $parts_unit = 'Box';
                                elseif($row['unit'] == 3)
                                    $parts_unit = 'Box/Pcs';
                                elseif($row['unit'] == 4)
                                    $parts_unit = 'Bun';
                                elseif($row['unit'] == 5)
                                    $parts_unit = 'Bundle';
                                elseif($row['unit'] == 6)
                                    $parts_unit = 'Can';
                                elseif($row['unit'] == 7)
                                    $parts_unit = 'Cartoon';
                                elseif($row['unit'] == 8)
                                    $parts_unit = 'Challan';
                                elseif($row['unit'] == 9)
                                    $parts_unit = 'Coil';
                                elseif($row['unit'] == 10)
                                    $parts_unit = 'Drum';
                                elseif($row['unit'] == 11)
                                    $parts_unit = 'Feet';
                                elseif($row['unit'] == 12)
                                    $parts_unit = 'Gallon';
                                elseif($row['unit'] == 13)
                                    $parts_unit = 'Item';
                                elseif($row['unit'] == 14)
                                    $parts_unit = 'Job';
                                elseif($row['unit'] == 15)
                                    $parts_unit = 'Kg';
                                elseif($row['unit'] == 16)
                                    $parts_unit = 'Kg/Bundle';
                                elseif($row['unit'] == 17)
                                    $parts_unit = 'Kv';
                                elseif($row['unit'] == 18)
                                    $parts_unit = 'Lbs';
                                elseif($row['unit'] == 19)
                                    $parts_unit = 'Ltr';
                                elseif($row['unit'] == 20)
                                    $parts_unit = 'Mtr';
                                elseif($row['unit'] == 21)
                                    $parts_unit = 'Pack';
                                elseif($row['unit'] == 22)
                                    $parts_unit = 'Pack/Pcs';
                                elseif($row['unit'] == 23)
                                    $parts_unit = 'Pair';
                                elseif($row['unit'] == 24)
                                    $parts_unit = 'Pcs';
                                elseif($row['unit'] == 25)
                                    $parts_unit = 'Pound';
                                elseif($row['unit'] == 26)
                                    $parts_unit = 'Qty';
                                elseif($row['unit'] == 27)
                                    $parts_unit = 'Roll';
                                elseif($row['unit'] == 28)
                                    $parts_unit = 'Set';
                                elseif($row['unit'] == 29)
                                    $parts_unit = 'Truck';
                                elseif($row['unit'] == 30)
                                    $parts_unit = 'Unit';
                                elseif($row['unit'] == 31)
                                    $parts_unit = 'Yeard';
                                elseif($row['unit'] == 32)
                                    $parts_unit = '(Unit Unknown)';
                                elseif($row['unit'] == 33)
                                    $parts_unit = 'SFT';
                                elseif($row['unit'] == 34)
                                    $parts_unit = 'RFT';
                                elseif($row['unit'] == 35)
                                    $parts_unit = 'CFT';

                                $data3[] = [
                                    'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                    'required_for' => $required_for,
                                    'parts_id' => $row['parts_id'],
                                    'parts_name' => $row['parts_name'],
                                    'parts_unit' => $parts_unit,
                                    'parts_qty' => $rem_qty,
                                    'date' => $row['loan_date']
                                ];
                            }
                        }
                    }

                    if(mysqli_num_rows($spr_loan_data_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_loan_data_query)){
                            $spr_loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT (req_qty - SUM(parts_qty)) AS rem_qty FROM rrmsteel_spr_loan_data WHERE loan_id = '".$row['loan_id']."' GROUP BY parts_id"));
                            $rem_qty = $spr_loan_data_info['rem_qty'];

                            if($rem_qty > 0){
                                $required_for = $row['required_for'];

                                if($required_for == 1)
                                    $required_for = 'BCP-CCM';
                                elseif($required_for == 2)
                                    $required_for = 'BCP-Furnace';
                                elseif($required_for == 3)
                                    $required_for = 'Concast-CCM';
                                elseif($required_for == 4)
                                    $required_for = 'Concast-Furnace';
                                elseif($required_for == 5)
                                    $required_for = 'HRM';
                                elseif($required_for == 6)
                                    $required_for = 'HRM Unit-2';
                                elseif($required_for == 7)
                                    $required_for = 'Lal Masjid';
                                elseif($required_for == 8)
                                    $required_for = 'Sonargaon';
                                elseif($required_for == 9)
                                    $required_for = 'General';

                                $parts_unit = $row['unit'];

                                if($row['unit'] == 1)
                                    $parts_unit = 'Bag';
                                elseif($row['unit'] == 2)
                                    $parts_unit = 'Box';
                                elseif($row['unit'] == 3)
                                    $parts_unit = 'Box/Pcs';
                                elseif($row['unit'] == 4)
                                    $parts_unit = 'Bun';
                                elseif($row['unit'] == 5)
                                    $parts_unit = 'Bundle';
                                elseif($row['unit'] == 6)
                                    $parts_unit = 'Can';
                                elseif($row['unit'] == 7)
                                    $parts_unit = 'Cartoon';
                                elseif($row['unit'] == 8)
                                    $parts_unit = 'Challan';
                                elseif($row['unit'] == 9)
                                    $parts_unit = 'Coil';
                                elseif($row['unit'] == 10)
                                    $parts_unit = 'Drum';
                                elseif($row['unit'] == 11)
                                    $parts_unit = 'Feet';
                                elseif($row['unit'] == 12)
                                    $parts_unit = 'Gallon';
                                elseif($row['unit'] == 13)
                                    $parts_unit = 'Item';
                                elseif($row['unit'] == 14)
                                    $parts_unit = 'Job';
                                elseif($row['unit'] == 15)
                                    $parts_unit = 'Kg';
                                elseif($row['unit'] == 16)
                                    $parts_unit = 'Kg/Bundle';
                                elseif($row['unit'] == 17)
                                    $parts_unit = 'Kv';
                                elseif($row['unit'] == 18)
                                    $parts_unit = 'Lbs';
                                elseif($row['unit'] == 19)
                                    $parts_unit = 'Ltr';
                                elseif($row['unit'] == 20)
                                    $parts_unit = 'Mtr';
                                elseif($row['unit'] == 21)
                                    $parts_unit = 'Pack';
                                elseif($row['unit'] == 22)
                                    $parts_unit = 'Pack/Pcs';
                                elseif($row['unit'] == 23)
                                    $parts_unit = 'Pair';
                                elseif($row['unit'] == 24)
                                    $parts_unit = 'Pcs';
                                elseif($row['unit'] == 25)
                                    $parts_unit = 'Pound';
                                elseif($row['unit'] == 26)
                                    $parts_unit = 'Qty';
                                elseif($row['unit'] == 27)
                                    $parts_unit = 'Roll';
                                elseif($row['unit'] == 28)
                                    $parts_unit = 'Set';
                                elseif($row['unit'] == 29)
                                    $parts_unit = 'Truck';
                                elseif($row['unit'] == 30)
                                    $parts_unit = 'Unit';
                                elseif($row['unit'] == 31)
                                    $parts_unit = 'Yeard';
                                elseif($row['unit'] == 32)
                                    $parts_unit = '(Unit Unknown)';
                                elseif($row['unit'] == 33)
                                    $parts_unit = 'SFT';
                                elseif($row['unit'] == 34)
                                    $parts_unit = 'RFT';
                                elseif($row['unit'] == 35)
                                    $parts_unit = 'CFT';

                                $data4[] = [
                                    'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                    'required_for' => $required_for,
                                    'parts_id' => $row['parts_id'],
                                    'parts_name' => $row['parts_name'],
                                    'parts_unit' => $parts_unit,
                                    'parts_qty' => $rem_qty,
                                    'date' => $row['loan_date']
                                ];
                            }
                        }
                    }
                } else{
                    $con_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_con_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.loan = 1 AND NOT EXISTS (SELECT pr.requisition_id FROM rrmsteel_con_loan pr WHERE pr.requisition_id = rd.requisition_id)");
                    $spr_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_spr_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.loan = 1 AND NOT EXISTS (SELECT pr.requisition_id FROM rrmsteel_spr_loan pr WHERE pr.requisition_id = rd.requisition_id)");

                    if(mysqli_num_rows($con_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($con_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }

                    $con_loan_data_query = mysqli_query($this->conn, "SELECT prd.*, p.parts_name, p.unit, pr.requisition_id FROM rrmsteel_con_loan_data prd INNER JOIN rrmsteel_parts p ON p.parts_id = prd.parts_id INNER JOIN rrmsteel_con_loan pr ON pr.loan_id = prd.loan_id GROUP BY prd.loan_id, prd.parts_id");
                    $spr_loan_data_query = mysqli_query($this->conn, "SELECT prd.*, p.parts_name, p.unit, pr.requisition_id FROM rrmsteel_spr_loan_data prd INNER JOIN rrmsteel_parts p ON p.parts_id = prd.parts_id INNER JOIN rrmsteel_spr_loan pr ON pr.loan_id = prd.loan_id GROUP BY prd.loan_id, prd.parts_id");

                    if(mysqli_num_rows($con_loan_data_query) > 0){
                        while($row = mysqli_fetch_assoc($con_loan_data_query)){
                            $con_loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT (req_qty - SUM(parts_qty)) AS rem_qty FROM rrmsteel_con_loan_data WHERE parts_id = '".$row['parts_id']."' AND loan_id = '".$row['loan_id']."' GROUP BY parts_id"));
                            $rem_qty = $con_loan_data_info['rem_qty'];

                            if($rem_qty > 0){
                                $required_for = $row['required_for'];

                                if($required_for == 1)
                                    $required_for = 'BCP-CCM';
                                elseif($required_for == 2)
                                    $required_for = 'BCP-Furnace';
                                elseif($required_for == 3)
                                    $required_for = 'Concast-CCM';
                                elseif($required_for == 4)
                                    $required_for = 'Concast-Furnace';
                                elseif($required_for == 5)
                                    $required_for = 'HRM';
                                elseif($required_for == 6)
                                    $required_for = 'HRM Unit-2';
                                elseif($required_for == 7)
                                    $required_for = 'Lal Masjid';
                                elseif($required_for == 8)
                                    $required_for = 'Sonargaon';
                                elseif($required_for == 9)
                                    $required_for = 'General';

                                $parts_unit = $row['unit'];

                                if($row['unit'] == 1)
                                    $parts_unit = 'Bag';
                                elseif($row['unit'] == 2)
                                    $parts_unit = 'Box';
                                elseif($row['unit'] == 3)
                                    $parts_unit = 'Box/Pcs';
                                elseif($row['unit'] == 4)
                                    $parts_unit = 'Bun';
                                elseif($row['unit'] == 5)
                                    $parts_unit = 'Bundle';
                                elseif($row['unit'] == 6)
                                    $parts_unit = 'Can';
                                elseif($row['unit'] == 7)
                                    $parts_unit = 'Cartoon';
                                elseif($row['unit'] == 8)
                                    $parts_unit = 'Challan';
                                elseif($row['unit'] == 9)
                                    $parts_unit = 'Coil';
                                elseif($row['unit'] == 10)
                                    $parts_unit = 'Drum';
                                elseif($row['unit'] == 11)
                                    $parts_unit = 'Feet';
                                elseif($row['unit'] == 12)
                                    $parts_unit = 'Gallon';
                                elseif($row['unit'] == 13)
                                    $parts_unit = 'Item';
                                elseif($row['unit'] == 14)
                                    $parts_unit = 'Job';
                                elseif($row['unit'] == 15)
                                    $parts_unit = 'Kg';
                                elseif($row['unit'] == 16)
                                    $parts_unit = 'Kg/Bundle';
                                elseif($row['unit'] == 17)
                                    $parts_unit = 'Kv';
                                elseif($row['unit'] == 18)
                                    $parts_unit = 'Lbs';
                                elseif($row['unit'] == 19)
                                    $parts_unit = 'Ltr';
                                elseif($row['unit'] == 20)
                                    $parts_unit = 'Mtr';
                                elseif($row['unit'] == 21)
                                    $parts_unit = 'Pack';
                                elseif($row['unit'] == 22)
                                    $parts_unit = 'Pack/Pcs';
                                elseif($row['unit'] == 23)
                                    $parts_unit = 'Pair';
                                elseif($row['unit'] == 24)
                                    $parts_unit = 'Pcs';
                                elseif($row['unit'] == 25)
                                    $parts_unit = 'Pound';
                                elseif($row['unit'] == 26)
                                    $parts_unit = 'Qty';
                                elseif($row['unit'] == 27)
                                    $parts_unit = 'Roll';
                                elseif($row['unit'] == 28)
                                    $parts_unit = 'Set';
                                elseif($row['unit'] == 29)
                                    $parts_unit = 'Truck';
                                elseif($row['unit'] == 30)
                                    $parts_unit = 'Unit';
                                elseif($row['unit'] == 31)
                                    $parts_unit = 'Yeard';
                                elseif($row['unit'] == 32)
                                    $parts_unit = '(Unit Unknown)';
                                elseif($row['unit'] == 33)
                                    $parts_unit = 'SFT';
                                elseif($row['unit'] == 34)
                                    $parts_unit = 'RFT';
                                elseif($row['unit'] == 35)
                                    $parts_unit = 'CFT';

                                $data3[] = [
                                    'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                    'required_for' => $required_for,
                                    'parts_id' => $row['parts_id'],
                                    'parts_name' => $row['parts_name'],
                                    'parts_unit' => $parts_unit,
                                    'parts_qty' => $rem_qty,
                                    'date' => $row['loan_date']
                                ];
                            }
                        }
                    }

                    if(mysqli_num_rows($spr_loan_data_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_loan_data_query)){
                            $spr_loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT (req_qty - SUM(parts_qty)) AS rem_qty FROM rrmsteel_spr_loan_data WHERE parts_id = '".$row['parts_id']."' AND loan_id = '".$row['loan_id']."' GROUP BY parts_id"));
                            $rem_qty = $spr_loan_data_info['rem_qty'];

                            if($rem_qty > 0){
                                $required_for = $row['required_for'];

                                if($required_for == 1)
                                    $required_for = 'BCP-CCM';
                                elseif($required_for == 2)
                                    $required_for = 'BCP-Furnace';
                                elseif($required_for == 3)
                                    $required_for = 'Concast-CCM';
                                elseif($required_for == 4)
                                    $required_for = 'Concast-Furnace';
                                elseif($required_for == 5)
                                    $required_for = 'HRM';
                                elseif($required_for == 6)
                                    $required_for = 'HRM Unit-2';
                                elseif($required_for == 7)
                                    $required_for = 'Lal Masjid';
                                elseif($required_for == 8)
                                    $required_for = 'Sonargaon';
                                elseif($required_for == 9)
                                    $required_for = 'General';

                                $parts_unit = $row['unit'];

                                if($row['unit'] == 1)
                                    $parts_unit = 'Bag';
                                elseif($row['unit'] == 2)
                                    $parts_unit = 'Box';
                                elseif($row['unit'] == 3)
                                    $parts_unit = 'Box/Pcs';
                                elseif($row['unit'] == 4)
                                    $parts_unit = 'Bun';
                                elseif($row['unit'] == 5)
                                    $parts_unit = 'Bundle';
                                elseif($row['unit'] == 6)
                                    $parts_unit = 'Can';
                                elseif($row['unit'] == 7)
                                    $parts_unit = 'Cartoon';
                                elseif($row['unit'] == 8)
                                    $parts_unit = 'Challan';
                                elseif($row['unit'] == 9)
                                    $parts_unit = 'Coil';
                                elseif($row['unit'] == 10)
                                    $parts_unit = 'Drum';
                                elseif($row['unit'] == 11)
                                    $parts_unit = 'Feet';
                                elseif($row['unit'] == 12)
                                    $parts_unit = 'Gallon';
                                elseif($row['unit'] == 13)
                                    $parts_unit = 'Item';
                                elseif($row['unit'] == 14)
                                    $parts_unit = 'Job';
                                elseif($row['unit'] == 15)
                                    $parts_unit = 'Kg';
                                elseif($row['unit'] == 16)
                                    $parts_unit = 'Kg/Bundle';
                                elseif($row['unit'] == 17)
                                    $parts_unit = 'Kv';
                                elseif($row['unit'] == 18)
                                    $parts_unit = 'Lbs';
                                elseif($row['unit'] == 19)
                                    $parts_unit = 'Ltr';
                                elseif($row['unit'] == 20)
                                    $parts_unit = 'Mtr';
                                elseif($row['unit'] == 21)
                                    $parts_unit = 'Pack';
                                elseif($row['unit'] == 22)
                                    $parts_unit = 'Pack/Pcs';
                                elseif($row['unit'] == 23)
                                    $parts_unit = 'Pair';
                                elseif($row['unit'] == 24)
                                    $parts_unit = 'Pcs';
                                elseif($row['unit'] == 25)
                                    $parts_unit = 'Pound';
                                elseif($row['unit'] == 26)
                                    $parts_unit = 'Qty';
                                elseif($row['unit'] == 27)
                                    $parts_unit = 'Roll';
                                elseif($row['unit'] == 28)
                                    $parts_unit = 'Set';
                                elseif($row['unit'] == 29)
                                    $parts_unit = 'Truck';
                                elseif($row['unit'] == 30)
                                    $parts_unit = 'Unit';
                                elseif($row['unit'] == 31)
                                    $parts_unit = 'Yeard';
                                elseif($row['unit'] == 32)
                                    $parts_unit = '(Unit Unknown)';
                                elseif($row['unit'] == 33)
                                    $parts_unit = 'SFT';
                                elseif($row['unit'] == 34)
                                    $parts_unit = 'RFT';
                                elseif($row['unit'] == 35)
                                    $parts_unit = 'CFT';

                                $data4[] = [
                                    'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                    'required_for' => $required_for,
                                    'parts_id' => $row['parts_id'],
                                    'parts_name' => $row['parts_name'],
                                    'parts_unit' => $parts_unit,
                                    'parts_qty' => $rem_qty,
                                    'date' => $row['loan_date']
                                ];
                            }
                        }
                    }
                } 
            }

            if($flag = 1){
                if($type == 1)
                    $merged = array_merge($data1, $data2);
                else
                    $merged = array_merge($data1, $data2, $data3, $data4);
                    
                $reply = array(
                    'Type' => 'success',
                    'Reply' => $merged
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No loan data found !'
                )));
            }
        }

        // FETCH PARTS LOAN DATA
        function fetch_parts_loan_data($parts_id, $parts_cat){
            if($parts_cat == 1){
                $spr_loan_data_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan_data ln INNER JOIN rrmsteel_parts p ON p.parts_id = ln.parts_id INNER JOIN rrmsteel_party pr ON pr.party_id = ln.party_id WHERE ln.parts_id = '$parts_id' ORDER BY ln.loan_data_created DESC");

                if(mysqli_num_rows($spr_loan_data_query) > 0){
                    while($row = mysqli_fetch_assoc($spr_loan_data_query)){
                        $required_for = $row['required_for'];

                        if($required_for == 1)
                            $required_for = 'BCP-CCM';
                        elseif($required_for == 2)
                            $required_for = 'BCP-Furnace';
                        elseif($required_for == 3)
                            $required_for = 'Concast-CCM';
                        elseif($required_for == 4)
                            $required_for = 'Concast-Furnace';
                        elseif($required_for == 5)
                            $required_for = 'HRM';
                        elseif($required_for == 6)
                            $required_for = 'HRM Unit-2';
                        elseif($required_for == 7)
                            $required_for = 'Lal Masjid';
                        elseif($required_for == 8)
                            $required_for = 'Sonargaon';
                        elseif($required_for == 9)
                            $required_for = 'General';

                        $parts_unit = $row['unit'];

                        if($row['unit'] == 1)
                            $parts_unit = 'Bag';
                        elseif($row['unit'] == 2)
                            $parts_unit = 'Box';
                        elseif($row['unit'] == 3)
                            $parts_unit = 'Box/Pcs';
                        elseif($row['unit'] == 4)
                            $parts_unit = 'Bun';
                        elseif($row['unit'] == 5)
                            $parts_unit = 'Bundle';
                        elseif($row['unit'] == 6)
                            $parts_unit = 'Can';
                        elseif($row['unit'] == 7)
                            $parts_unit = 'Cartoon';
                        elseif($row['unit'] == 8)
                            $parts_unit = 'Challan';
                        elseif($row['unit'] == 9)
                            $parts_unit = 'Coil';
                        elseif($row['unit'] == 10)
                            $parts_unit = 'Drum';
                        elseif($row['unit'] == 11)
                            $parts_unit = 'Feet';
                        elseif($row['unit'] == 12)
                            $parts_unit = 'Gallon';
                        elseif($row['unit'] == 13)
                            $parts_unit = 'Item';
                        elseif($row['unit'] == 14)
                            $parts_unit = 'Job';
                        elseif($row['unit'] == 15)
                            $parts_unit = 'Kg';
                        elseif($row['unit'] == 16)
                            $parts_unit = 'Kg/Bundle';
                        elseif($row['unit'] == 17)
                            $parts_unit = 'Kv';
                        elseif($row['unit'] == 18)
                            $parts_unit = 'Lbs';
                        elseif($row['unit'] == 19)
                            $parts_unit = 'Ltr';
                        elseif($row['unit'] == 20)
                            $parts_unit = 'Mtr';
                        elseif($row['unit'] == 21)
                            $parts_unit = 'Pack';
                        elseif($row['unit'] == 22)
                            $parts_unit = 'Pack/Pcs';
                        elseif($row['unit'] == 23)
                            $parts_unit = 'Pair';
                        elseif($row['unit'] == 24)
                            $parts_unit = 'Pcs';
                        elseif($row['unit'] == 25)
                            $parts_unit = 'Pound';
                        elseif($row['unit'] == 26)
                            $parts_unit = 'Qty';
                        elseif($row['unit'] == 27)
                            $parts_unit = 'Roll';
                        elseif($row['unit'] == 28)
                            $parts_unit = 'Set';
                        elseif($row['unit'] == 29)
                            $parts_unit = 'Truck';
                        elseif($row['unit'] == 30)
                            $parts_unit = 'Unit';
                        elseif($row['unit'] == 31)
                            $parts_unit = 'Yeard';
                        elseif($row['unit'] == 32)
                            $parts_unit = '(Unit Unknown)';
                        elseif($row['unit'] == 33)
                            $parts_unit = 'SFT';
                        elseif($row['unit'] == 34)
                            $parts_unit = 'RFT';
                        elseif($row['unit'] == 35)
                            $parts_unit = 'CFT';

                        $data[] = [
                            'loan_data_id' => $row['loan_data_id'],
                            'loan_id' => $row['loan_id'],
                            'req_for' => $row['required_for'],
                            'required_for' => $required_for,
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'parts_unit' => $parts_unit,
                            'parts_cat' => $parts_cat,
                            'parts_qty' => $row['parts_qty'],
                            'req_qty' => $row['req_qty'],
                            'repay_qty' => $row['repay_qty'],
                            'price' => $row['price'],
                            'party_id' => $row['party_id'],
                            'party_name' => $row['party_name'],
                            'loan_date' => $row['loan_date']
                        ];
                    }
                }
            } else{
                $con_loan_data_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan_data ln INNER JOIN rrmsteel_parts p ON p.parts_id = ln.parts_id INNER JOIN rrmsteel_party pr ON pr.party_id = ln.party_id WHERE ln.parts_id = '$parts_id' ORDER BY ln.loan_data_created DESC");

                if(mysqli_num_rows($con_loan_data_query) > 0){
                    while($row = mysqli_fetch_assoc($con_loan_data_query)){
                        $required_for = $row['required_for'];

                        if($required_for == 1)
                            $required_for = 'BCP-CCM';
                        elseif($required_for == 2)
                            $required_for = 'BCP-Furnace';
                        elseif($required_for == 3)
                            $required_for = 'Concast-CCM';
                        elseif($required_for == 4)
                            $required_for = 'Concast-Furnace';
                        elseif($required_for == 5)
                            $required_for = 'HRM';
                        elseif($required_for == 6)
                            $required_for = 'HRM Unit-2';
                        elseif($required_for == 7)
                            $required_for = 'Lal Masjid';
                        elseif($required_for == 8)
                            $required_for = 'Sonargaon';
                        elseif($required_for == 9)
                            $required_for = 'General';

                        $parts_unit = $row['unit'];

                        if($row['unit'] == 1)
                            $parts_unit = 'Bag';
                        elseif($row['unit'] == 2)
                            $parts_unit = 'Box';
                        elseif($row['unit'] == 3)
                            $parts_unit = 'Box/Pcs';
                        elseif($row['unit'] == 4)
                            $parts_unit = 'Bun';
                        elseif($row['unit'] == 5)
                            $parts_unit = 'Bundle';
                        elseif($row['unit'] == 6)
                            $parts_unit = 'Can';
                        elseif($row['unit'] == 7)
                            $parts_unit = 'Cartoon';
                        elseif($row['unit'] == 8)
                            $parts_unit = 'Challan';
                        elseif($row['unit'] == 9)
                            $parts_unit = 'Coil';
                        elseif($row['unit'] == 10)
                            $parts_unit = 'Drum';
                        elseif($row['unit'] == 11)
                            $parts_unit = 'Feet';
                        elseif($row['unit'] == 12)
                            $parts_unit = 'Gallon';
                        elseif($row['unit'] == 13)
                            $parts_unit = 'Item';
                        elseif($row['unit'] == 14)
                            $parts_unit = 'Job';
                        elseif($row['unit'] == 15)
                            $parts_unit = 'Kg';
                        elseif($row['unit'] == 16)
                            $parts_unit = 'Kg/Bundle';
                        elseif($row['unit'] == 17)
                            $parts_unit = 'Kv';
                        elseif($row['unit'] == 18)
                            $parts_unit = 'Lbs';
                        elseif($row['unit'] == 19)
                            $parts_unit = 'Ltr';
                        elseif($row['unit'] == 20)
                            $parts_unit = 'Mtr';
                        elseif($row['unit'] == 21)
                            $parts_unit = 'Pack';
                        elseif($row['unit'] == 22)
                            $parts_unit = 'Pack/Pcs';
                        elseif($row['unit'] == 23)
                            $parts_unit = 'Pair';
                        elseif($row['unit'] == 24)
                            $parts_unit = 'Pcs';
                        elseif($row['unit'] == 25)
                            $parts_unit = 'Pound';
                        elseif($row['unit'] == 26)
                            $parts_unit = 'Qty';
                        elseif($row['unit'] == 27)
                            $parts_unit = 'Roll';
                        elseif($row['unit'] == 28)
                            $parts_unit = 'Set';
                        elseif($row['unit'] == 29)
                            $parts_unit = 'Truck';
                        elseif($row['unit'] == 30)
                            $parts_unit = 'Unit';
                        elseif($row['unit'] == 31)
                            $parts_unit = 'Yeard';
                        elseif($row['unit'] == 32)
                            $parts_unit = '(Unit Unknown)';
                        elseif($row['unit'] == 33)
                            $parts_unit = 'SFT';
                        elseif($row['unit'] == 34)
                            $parts_unit = 'RFT';
                        elseif($row['unit'] == 35)
                            $parts_unit = 'CFT';

                        $data[] = [
                            'loan_data_id' => $row['loan_data_id'],
                            'loan_id' => $row['loan_id'],
                            'req_for' => $row['required_for'],
                            'required_for' => $required_for,
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'parts_unit' => $parts_unit,
                            'parts_cat' => $parts_cat,
                            'parts_qty' => $row['parts_qty'],
                            'req_qty' => $row['req_qty'],
                            'repay_qty' => $row['repay_qty'],
                            'price' => $row['price'],
                            'party_id' => $row['party_id'],
                            'party_name' => $row['party_name'],
                            'loan_date' => $row['loan_date']
                        ];
                    }
                }
            }

            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }

        // FETCH PARTS LOAN REPAY HISTORY DATA
        function fetch_parts_loan_repay_history_data($parts_id, $parts_cat){
            if($parts_cat == 1){
                $spr_loan_repay_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_loan_repay ln INNER JOIN rrmsteel_parts p ON p.parts_id = ln.parts_id INNER JOIN rrmsteel_party pr ON pr.party_id = ln.party_id WHERE ln.parts_id = '$parts_id' ORDER BY ln.loan_repay_data_created DESC");

                if(mysqli_num_rows($spr_loan_repay_query) > 0){
                    while($row = mysqli_fetch_assoc($spr_loan_repay_query)){
                        $parts_unit = $row['unit'];

                        if($row['unit'] == 1)
                            $parts_unit = 'Bag';
                        elseif($row['unit'] == 2)
                            $parts_unit = 'Box';
                        elseif($row['unit'] == 3)
                            $parts_unit = 'Box/Pcs';
                        elseif($row['unit'] == 4)
                            $parts_unit = 'Bun';
                        elseif($row['unit'] == 5)
                            $parts_unit = 'Bundle';
                        elseif($row['unit'] == 6)
                            $parts_unit = 'Can';
                        elseif($row['unit'] == 7)
                            $parts_unit = 'Cartoon';
                        elseif($row['unit'] == 8)
                            $parts_unit = 'Challan';
                        elseif($row['unit'] == 9)
                            $parts_unit = 'Coil';
                        elseif($row['unit'] == 10)
                            $parts_unit = 'Drum';
                        elseif($row['unit'] == 11)
                            $parts_unit = 'Feet';
                        elseif($row['unit'] == 12)
                            $parts_unit = 'Gallon';
                        elseif($row['unit'] == 13)
                            $parts_unit = 'Item';
                        elseif($row['unit'] == 14)
                            $parts_unit = 'Job';
                        elseif($row['unit'] == 15)
                            $parts_unit = 'Kg';
                        elseif($row['unit'] == 16)
                            $parts_unit = 'Kg/Bundle';
                        elseif($row['unit'] == 17)
                            $parts_unit = 'Kv';
                        elseif($row['unit'] == 18)
                            $parts_unit = 'Lbs';
                        elseif($row['unit'] == 19)
                            $parts_unit = 'Ltr';
                        elseif($row['unit'] == 20)
                            $parts_unit = 'Mtr';
                        elseif($row['unit'] == 21)
                            $parts_unit = 'Pack';
                        elseif($row['unit'] == 22)
                            $parts_unit = 'Pack/Pcs';
                        elseif($row['unit'] == 23)
                            $parts_unit = 'Pair';
                        elseif($row['unit'] == 24)
                            $parts_unit = 'Pcs';
                        elseif($row['unit'] == 25)
                            $parts_unit = 'Pound';
                        elseif($row['unit'] == 26)
                            $parts_unit = 'Qty';
                        elseif($row['unit'] == 27)
                            $parts_unit = 'Roll';
                        elseif($row['unit'] == 28)
                            $parts_unit = 'Set';
                        elseif($row['unit'] == 29)
                            $parts_unit = 'Truck';
                        elseif($row['unit'] == 30)
                            $parts_unit = 'Unit';
                        elseif($row['unit'] == 31)
                            $parts_unit = 'Yeard';
                        elseif($row['unit'] == 32)
                            $parts_unit = '(Unit Unknown)';
                        elseif($row['unit'] == 33)
                            $parts_unit = 'SFT';
                        elseif($row['unit'] == 34)
                            $parts_unit = 'RFT';
                        elseif($row['unit'] == 35)
                            $parts_unit = 'CFT';

                        $data[] = [
                            'loan_data_id' => $row['loan_data_id'],
                            'loan_id' => $row['loan_id'],
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'parts_unit' => $parts_unit,
                            'parts_cat' => $parts_cat,
                            'repaid_qty' => $row['repaid_qty'],
                            'repay_date' => $row['repay_date'],
                            'party_id' => $row['party_id'],
                            'party_name' => $row['party_name']
                        ];
                    }
                }
            } else{
                $con_loan_repay_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_loan_repay ln INNER JOIN rrmsteel_parts p ON p.parts_id = ln.parts_id INNER JOIN rrmsteel_party pr ON pr.party_id = ln.party_id WHERE ln.parts_id = '$parts_id' ORDER BY ln.loan_repay_data_created DESC");

                if(mysqli_num_rows($con_loan_repay_query) > 0){
                    while($row = mysqli_fetch_assoc($con_loan_repay_query)){
                        $parts_unit = $row['unit'];

                        if($row['unit'] == 1)
                            $parts_unit = 'Bag';
                        elseif($row['unit'] == 2)
                            $parts_unit = 'Box';
                        elseif($row['unit'] == 3)
                            $parts_unit = 'Box/Pcs';
                        elseif($row['unit'] == 4)
                            $parts_unit = 'Bun';
                        elseif($row['unit'] == 5)
                            $parts_unit = 'Bundle';
                        elseif($row['unit'] == 6)
                            $parts_unit = 'Can';
                        elseif($row['unit'] == 7)
                            $parts_unit = 'Cartoon';
                        elseif($row['unit'] == 8)
                            $parts_unit = 'Challan';
                        elseif($row['unit'] == 9)
                            $parts_unit = 'Coil';
                        elseif($row['unit'] == 10)
                            $parts_unit = 'Drum';
                        elseif($row['unit'] == 11)
                            $parts_unit = 'Feet';
                        elseif($row['unit'] == 12)
                            $parts_unit = 'Gallon';
                        elseif($row['unit'] == 13)
                            $parts_unit = 'Item';
                        elseif($row['unit'] == 14)
                            $parts_unit = 'Job';
                        elseif($row['unit'] == 15)
                            $parts_unit = 'Kg';
                        elseif($row['unit'] == 16)
                            $parts_unit = 'Kg/Bundle';
                        elseif($row['unit'] == 17)
                            $parts_unit = 'Kv';
                        elseif($row['unit'] == 18)
                            $parts_unit = 'Lbs';
                        elseif($row['unit'] == 19)
                            $parts_unit = 'Ltr';
                        elseif($row['unit'] == 20)
                            $parts_unit = 'Mtr';
                        elseif($row['unit'] == 21)
                            $parts_unit = 'Pack';
                        elseif($row['unit'] == 22)
                            $parts_unit = 'Pack/Pcs';
                        elseif($row['unit'] == 23)
                            $parts_unit = 'Pair';
                        elseif($row['unit'] == 24)
                            $parts_unit = 'Pcs';
                        elseif($row['unit'] == 25)
                            $parts_unit = 'Pound';
                        elseif($row['unit'] == 26)
                            $parts_unit = 'Qty';
                        elseif($row['unit'] == 27)
                            $parts_unit = 'Roll';
                        elseif($row['unit'] == 28)
                            $parts_unit = 'Set';
                        elseif($row['unit'] == 29)
                            $parts_unit = 'Truck';
                        elseif($row['unit'] == 30)
                            $parts_unit = 'Unit';
                        elseif($row['unit'] == 31)
                            $parts_unit = 'Yeard';
                        elseif($row['unit'] == 32)
                            $parts_unit = '(Unit Unknown)';
                        elseif($row['unit'] == 33)
                            $parts_unit = 'SFT';
                        elseif($row['unit'] == 34)
                            $parts_unit = 'RFT';
                        elseif($row['unit'] == 35)
                            $parts_unit = 'CFT';

                        $data[] = [
                            'loan_data_id' => $row['loan_data_id'],
                            'loan_id' => $row['loan_id'],
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'parts_unit' => $parts_unit,
                            'parts_cat' => $parts_cat,
                            'repaid_qty' => $row['repaid_qty'],
                            'repay_date' => $row['repay_date'],
                            'party_id' => $row['party_id'],
                            'party_name' => $row['party_name']
                        ];
                    }
                }
            }
            
            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }
    }
?>