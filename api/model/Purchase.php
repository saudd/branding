<?php
    namespace Purchase;

    class Purchase{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        // FETCH A CONSUMABLE REQUISITION
        function fetch_requisition_con($requisition_id){
            $requisition_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_requisition WHERE requisition_id = '$requisition_id'");

            if(mysqli_num_rows($requisition_query) > 0){
                $requisition_info = mysqli_fetch_assoc($requisition_query);

                $requisition_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks, r.parts_qty AS r_qty, i.parts_qty AS i_qty, i.parts_rate AS price FROM rrmsteel_con_requisition_data r INNER JOIN rrmsteel_parts p ON p.parts_id = r.parts_id INNER JOIN rrmsteel_inv_summary i ON i.parts_id = r.parts_id WHERE r.requisition_id = '$requisition_id' AND r.loan = 0");

                if(mysqli_num_rows($requisition_data_query) > 0){
                    while($row = mysqli_fetch_assoc($requisition_data_query)){
                        $purchase_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT rd.parts_id AS purchased_parts_id, SUM(rd.parts_qty) AS purchased_parts_qty FROM rrmsteel_con_purchase_data rd INNER JOIN rrmsteel_con_purchase r ON r.purchase_id = rd.purchase_id WHERE r.requisition_id = '$requisition_id' AND rd.parts_id = '".$row['parts_id']."' LIMIT 1"));

                        if(isset($purchase_data_info)){
                            $purchased_parts_id = $purchase_data_info['purchased_parts_id'] == null ? 0 : $purchase_data_info['purchased_parts_id'];
                            $purchased_parts_qty = $purchase_data_info['purchased_parts_qty'] == null ? 0 : $purchase_data_info['purchased_parts_qty'];
                        } else{
                            $purchased_parts_id = 0;
                            $purchased_parts_qty = 0;
                        }

                        $data[] = [
                            'p_approval_status' => $requisition_info['p_approval_status'],
                            'approval_status' => $requisition_info['approval_status'],
                            'required_for' => $row['required_for'],
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'r_qty' => $row['r_qty'],
                            'parts_unit' => $row['unit'],
                            'i_qty' => $row['i_qty'],
                            'price' => $row['price'],
                            'parts_usage' => $row['parts_usage'],
                            'remarks' => $row['r_remarks'],
                            'requisition_id' => $row['requisition_id'],
                            'purchased_parts_id' => $purchased_parts_id,
                            'purchased_parts_qty' => $purchased_parts_qty
                        ];
                    }

                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No purchase data found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No purchase data found !'
                )));
            }
        }

        // FETCH A SPARE REQUISITION
        function fetch_requisition_spr($requisition_id){
            $requisition_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_requisition WHERE requisition_id = '$requisition_id'");

            if(mysqli_num_rows($requisition_query) > 0){
                $requisition_info = mysqli_fetch_assoc($requisition_query);

                $requisition_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks, r.parts_qty AS r_qty, i.parts_qty AS i_qty, i.parts_rate AS price FROM rrmsteel_spr_requisition_data r INNER JOIN rrmsteel_parts p ON p.parts_id = r.parts_id INNER JOIN rrmsteel_inv_summary i ON i.parts_id = r.parts_id WHERE r.requisition_id = '$requisition_id' AND r.loan = 0");

                if(mysqli_num_rows($requisition_data_query) > 0){
                    while($row = mysqli_fetch_assoc($requisition_data_query)){
                        $purchase_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT rd.parts_id AS purchased_parts_id, SUM(rd.parts_qty) AS purchased_parts_qty FROM rrmsteel_spr_purchase_data rd INNER JOIN rrmsteel_spr_purchase r ON r.purchase_id = rd.purchase_id WHERE r.requisition_id = '$requisition_id' AND rd.parts_id = '".$row['parts_id']."' LIMIT 1"));

                        if(isset($purchase_data_info)){
                            $purchased_parts_id = $purchase_data_info['purchased_parts_id'] == null ? 0 : $purchase_data_info['purchased_parts_id'];
                            $purchased_parts_qty = $purchase_data_info['purchased_parts_qty'] == null ? 0 : $purchase_data_info['purchased_parts_qty'];
                        } else{
                            $purchased_parts_id = 0;
                            $purchased_parts_qty = 0;
                        }

                        $data[] = [
                            'p_approval_status' => $requisition_info['p_approval_status'],
                            'approval_status' => $requisition_info['approval_status'],
                            'required_for' => $row['required_for'],
                            'parts_id' => $row['parts_id'],
                            'parts_name' => $row['parts_name'],
                            'r_qty' => $row['r_qty'],
                            'parts_unit' => $row['unit'],
                            'i_qty' => $row['i_qty'],
                            'price' => $row['price'],
                            'old_spare_details' => $row['old_spare_details'],
                            'status' => $row['status'],
                            'remarks' => $row['r_remarks'],
                            'requisition_id' => $row['requisition_id'],
                            'purchased_parts_id' => $purchased_parts_id,
                            'purchased_parts_qty' => $purchased_parts_qty
                        ];
                    }

                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No purchase data found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No purchase data found !'
                )));
            }
        }

        // FETCH A CONSUMABLE PURCHASE
        function fetch_purchase_con($requisition_id){
            $purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase WHERE requisition_id = '$requisition_id' LIMIT 1");

            if(mysqli_num_rows($purchase_query) > 0){
                $purchase_info = mysqli_fetch_assoc($purchase_query);
                $purchase_id = $purchase_info['purchase_id'];

                $purchase_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks FROM rrmsteel_con_purchase_data r INNER JOIN rrmsteel_parts pr ON pr.parts_id = r.parts_id INNER JOIN rrmsteel_party pt ON pt.party_id = r.party_id WHERE purchase_id = '$purchase_id' ORDER BY r.parts_id");

                while($row = mysqli_fetch_assoc($purchase_data_query)){
                    $parts_id = $row['parts_id'];

                    $requisition_data_query = mysqli_query($this->conn, "SELECT parts_qty FROM rrmsteel_con_requisition_data WHERE requisition_id = '$requisition_id' AND parts_id = '$parts_id' AND loan = 0 LIMIT 1");
                    $requisition_info = mysqli_fetch_assoc($requisition_data_query);
                    $requisitioned_parts_qty = $requisition_info['parts_qty'];

                    $purchase_data_query_2 = mysqli_query($this->conn, "SELECT COUNT(*) AS purchase_count, SUM(parts_qty) AS purchased_parts_qty FROM rrmsteel_con_purchase_data r WHERE purchase_id = '$purchase_id' AND parts_id = '$parts_id'");
                    $purchase_data_info = mysqli_fetch_assoc($purchase_data_query_2);
                    $purchased_parts_qty = $purchase_data_info['purchased_parts_qty'];
                    $purchase_count = $purchase_data_info['purchase_count'];

                    $data1[] = [
                        'purchase_id' => $purchase_id,
                        'required_for' => $row['required_for'],
                        'parts_id' => $parts_id,
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'requisitioned_parts_qty' => $requisitioned_parts_qty,
                        'purchased_parts_qty' => $purchased_parts_qty,
                        'purchase_indx_f' => 1,
                        'purchase_indx_l' => (int)$purchase_count,
                        'parts_usage' => $row['parts_usage'],
                        'remarks' => $row['r_remarks']
                    ];

                    $data2[] = [
                        'purchase_data_id' => $row['purchase_data_id'],
                        'parts_id' => $parts_id,
                        'parts_qty' => $row['parts_qty'],
                        'price' => $row['price'],
                        'party_id' => $row['party_id'],
                        'party_name' => $row['party_name'],
                        'gate_no' => $row['gate_no'],
                        'challan_no' => $row['challan_no'],
                        'challan_photo' => $row['challan_photo'],
                        'bill_photo' => $row['bill_photo'],
                        'purchase_date' => $row['purchase_date']
                    ];
                }

                $data1 = array_values(array_unique($data1, SORT_REGULAR));

                foreach($data1 as $key => $value){
                    if($key == 0){
                        $purchase_indx_f = $value['purchase_indx_f'];
                        $purchase_indx_l = $value['purchase_indx_l'];
                    } else{
                        $purchase_indx_f = $purchase_indx_l + 2;
                        $purchase_indx_l = $purchase_indx_f + $value['purchase_indx_l'] - 1;
                    }

                    $data1[$key]['purchase_indx_f'] = $purchase_indx_f;
                    $data1[$key]['purchase_indx_l'] = $purchase_indx_l;
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply1' => $data1,
                    'Reply2' => $data2
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No purchase data found !'
                )));
            }
        }

        // FETCH A SPARE PURCHASE
        function fetch_purchase_spr($requisition_id){
            $purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase WHERE requisition_id = '$requisition_id' LIMIT 1");

            if(mysqli_num_rows($purchase_query) > 0){
                $purchase_info = mysqli_fetch_assoc($purchase_query);
                $purchase_id = $purchase_info['purchase_id'];

                $purchase_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks FROM rrmsteel_spr_purchase_data r INNER JOIN rrmsteel_parts pr ON pr.parts_id = r.parts_id INNER JOIN rrmsteel_party pt ON pt.party_id = r.party_id WHERE purchase_id = '$purchase_id' ORDER BY r.parts_id");

                while($row = mysqli_fetch_assoc($purchase_data_query)){
                    $parts_id = $row['parts_id'];

                    $requisition_data_query = mysqli_query($this->conn, "SELECT parts_qty FROM rrmsteel_spr_requisition_data WHERE requisition_id = '$requisition_id' AND parts_id = '$parts_id' AND loan = 0 LIMIT 1");
                    $requisition_info = mysqli_fetch_assoc($requisition_data_query);
                    $requisitioned_parts_qty = $requisition_info['parts_qty'];

                    $purchase_data_query_2 = mysqli_query($this->conn, "SELECT COUNT(*) AS purchase_count, SUM(parts_qty) AS purchased_parts_qty FROM rrmsteel_spr_purchase_data r WHERE purchase_id = '$purchase_id' AND parts_id = '$parts_id'");
                    $purchase_data_info = mysqli_fetch_assoc($purchase_data_query_2);
                    $purchased_parts_qty = $purchase_data_info['purchased_parts_qty'];
                    $purchase_count = $purchase_data_info['purchase_count'];

                    $data1[] = [
                        'purchase_id' => $purchase_id,
                        'required_for' => $row['required_for'],
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'requisitioned_parts_qty' => $requisitioned_parts_qty,
                        'purchased_parts_qty' => $purchased_parts_qty,
                        'purchase_indx_f' => 1,
                        'purchase_indx_l' => (int)$purchase_count,
                        'old_spare_details' => $row['old_spare_details'],
                        'status' => (($row['status'] == 1) ? 'Repairable' : 'Unusual'),
                        'remarks' => $row['r_remarks']
                    ];

                    $data2[] = [
                        'purchase_data_id' => $row['purchase_data_id'],
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty'],
                        'price' => $row['price'],
                        'party_id' => $row['party_id'],
                        'party_name' => $row['party_name'],
                        'gate_no' => $row['gate_no'],
                        'challan_no' => $row['challan_no'],
                        'challan_photo' => $row['challan_photo'],
                        'bill_photo' => $row['bill_photo'],
                        'purchase_date' => $row['purchase_date']
                    ];
                }

                $data1 = array_values(array_unique($data1, SORT_REGULAR));

                foreach($data1 as $key => $value){
                    if($key == 0){
                        $purchase_indx_f = $value['purchase_indx_f'];
                        $purchase_indx_l = $value['purchase_indx_l'];
                    } else{
                        $purchase_indx_f = $purchase_indx_l + 2;
                        $purchase_indx_l = $purchase_indx_f + $value['purchase_indx_l'] - 1;
                    }

                    $data1[$key]['purchase_indx_f'] = $purchase_indx_f;
                    $data1[$key]['purchase_indx_l'] = $purchase_indx_l;
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply1' => $data1,
                    'Reply2' => $data2
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No purchase data found !'
                )));
            }
        }

        // FETCH FILTERED PURCHASE
        function fetch_filtered_purchase($type, $party_id, $parts_id, $date_range = null){
            $data1 = []; $data2 = []; $flag = 1;

            if($date_range){
                $date_range = explode(' to ', $date_range);
                $start_date = $date_range[0];
                $end_date = $date_range[1];
            }

            if($type == 1){
                if(!$party_id && !$parts_id){
                    if($date_range == null){
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id");
                    } else{
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                    }

                    if(mysqli_num_rows($con_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($con_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }
                } elseif($party_id && !$parts_id){
                    if($date_range == null){
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id'");
                    } else{
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                    }

                    if(mysqli_num_rows($con_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($con_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }
                } elseif(!$party_id && $parts_id){
                    if($date_range == null){
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.parts_id = '$parts_id'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.parts_id = '$parts_id'");
                    } else{
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.parts_id = '$parts_id' AND pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.parts_id = '$parts_id' AND pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                    }

                    if(mysqli_num_rows($con_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($con_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }
                } elseif($party_id && $parts_id){
                    if($date_range == null){
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.parts_id = '$parts_id'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.parts_id = '$parts_id'");
                    } else{
                        $con_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_purchase_data pr INNER JOIN rrmsteel_con_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.parts_id = '$parts_id' AND pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                        $spr_purchase_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_purchase_data pr INNER JOIN rrmsteel_spr_purchase p ON p.purchase_id = pr.purchase_id INNER JOIN rrmsteel_parts pt ON pt.parts_id = pr.parts_id INNER JOIN rrmsteel_party py ON py.party_id = pr.party_id WHERE pr.party_id = '$party_id' AND pr.parts_id = '$parts_id' AND pr.purchase_date >= '$start_date' AND pr.purchase_date <= '$end_date'");
                    }

                    if(mysqli_num_rows($con_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($con_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Consumable',
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_purchase_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_purchase_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'purchase_data_id' => $row['purchase_data_id'],
                                'purchase_id' => $row['purchase_id'],
                                'type' => 'Spare',
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'price' => $row['price'],
                                'party_id' => $row['party_id'],
                                'party_name' => $row['party_name'],
                                'gate_no' => $row['gate_no'],
                                'challan_no' => $row['challan_no'],
                                'purchase_date' => $row['purchase_date']
                            ];
                        }
                    }
                } else{
                    $flag = 0;
                }
            } else{
                if($date_range){
                    $start_date_timestamp = strtotime($start_date);
                    $end_date_timestamp = strtotime($end_date);
                }

                if($parts_id){
                    if($date_range == null){
                        $con_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_con_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.parts_id = '$parts_id' AND rd.loan = 0 AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_con_purchase_data prd INNER JOIN rrmsteel_con_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                        $spr_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_spr_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.parts_id = '$parts_id' AND rd.loan = 0 AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_spr_purchase_data prd INNER JOIN rrmsteel_spr_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                    } else{
                        $con_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_con_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.parts_id = '$parts_id' AND rd.loan = 0 AND rd.requisition_data_created >= '$start_date_timestamp' AND rd.requisition_data_created <= '$end_date_timestamp' AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_con_purchase_data prd INNER JOIN rrmsteel_con_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                        $spr_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_spr_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.parts_id = '$parts_id' AND rd.loan = 0 AND rd.requisition_data_created >= '$start_date_timestamp' AND rd.requisition_data_created <= '$end_date_timestamp' AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_spr_purchase_data prd INNER JOIN rrmsteel_spr_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                    }

                    if(mysqli_num_rows($con_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($con_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }
                } else{
                    if($date_range == null){
                        $con_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_con_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.loan = 0 AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_con_purchase_data prd INNER JOIN rrmsteel_con_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                        $spr_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_spr_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.loan = 0 AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_spr_purchase_data prd INNER JOIN rrmsteel_spr_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                    } else{
                        $con_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_con_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.loan = 0 AND rd.requisition_data_created >= '$start_date_timestamp' AND rd.requisition_data_created <= '$end_date_timestamp' AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_con_purchase_data prd INNER JOIN rrmsteel_con_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                        $spr_requisition_data_query = mysqli_query($this->conn, "SELECT rd.*, p.parts_name, p.unit FROM rrmsteel_spr_requisition_data rd INNER JOIN rrmsteel_parts p ON p.parts_id = rd.parts_id WHERE rd.loan = 0 AND rd.requisition_data_created >= '$start_date_timestamp' AND rd.requisition_data_created <= '$end_date_timestamp' AND (NOT EXISTS (SELECT prd.parts_id FROM rrmsteel_spr_purchase_data prd INNER JOIN rrmsteel_spr_purchase pr ON pr.purchase_id = prd.purchase_id WHERE pr.requisition_id = rd.requisition_id AND prd.parts_id = rd.parts_id AND prd.parts_qty >= rd.parts_qty))");
                    }

                    if(mysqli_num_rows($con_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($con_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data1[] = [
                                'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }

                    if(mysqli_num_rows($spr_requisition_data_query) > 0){
                        while($row = mysqli_fetch_assoc($spr_requisition_data_query)){
                            $required_for = $row['required_for'];

                            if($required_for == 1)
                                $required_for = 'BCP-CCM';
                            elseif($required_for == 2)
                                $required_for = 'BCP-Furnace';
                            elseif($required_for == 3)
                                $required_for = 'Concast-CCM';
                            elseif($required_for == 4)
                                $required_for = 'Concast-Furnace';
                            elseif($required_for == 5)
                                $required_for = 'HRM';
                            elseif($required_for == 6)
                                $required_for = 'HRM Unit-2';
                            elseif($required_for == 7)
                                $required_for = 'Lal Masjid';
                            elseif($required_for == 8)
                                $required_for = 'Sonargaon';
                            elseif($required_for == 9)
                                $required_for = 'General';

                            $parts_unit = $row['unit'];

                            if($row['unit'] == 1)
                                $parts_unit = 'Bag';
                            elseif($row['unit'] == 2)
                                $parts_unit = 'Box';
                            elseif($row['unit'] == 3)
                                $parts_unit = 'Box/Pcs';
                            elseif($row['unit'] == 4)
                                $parts_unit = 'Bun';
                            elseif($row['unit'] == 5)
                                $parts_unit = 'Bundle';
                            elseif($row['unit'] == 6)
                                $parts_unit = 'Can';
                            elseif($row['unit'] == 7)
                                $parts_unit = 'Cartoon';
                            elseif($row['unit'] == 8)
                                $parts_unit = 'Challan';
                            elseif($row['unit'] == 9)
                                $parts_unit = 'Coil';
                            elseif($row['unit'] == 10)
                                $parts_unit = 'Drum';
                            elseif($row['unit'] == 11)
                                $parts_unit = 'Feet';
                            elseif($row['unit'] == 12)
                                $parts_unit = 'Gallon';
                            elseif($row['unit'] == 13)
                                $parts_unit = 'Item';
                            elseif($row['unit'] == 14)
                                $parts_unit = 'Job';
                            elseif($row['unit'] == 15)
                                $parts_unit = 'Kg';
                            elseif($row['unit'] == 16)
                                $parts_unit = 'Kg/Bundle';
                            elseif($row['unit'] == 17)
                                $parts_unit = 'Kv';
                            elseif($row['unit'] == 18)
                                $parts_unit = 'Lbs';
                            elseif($row['unit'] == 19)
                                $parts_unit = 'Ltr';
                            elseif($row['unit'] == 20)
                                $parts_unit = 'Mtr';
                            elseif($row['unit'] == 21)
                                $parts_unit = 'Pack';
                            elseif($row['unit'] == 22)
                                $parts_unit = 'Pack/Pcs';
                            elseif($row['unit'] == 23)
                                $parts_unit = 'Pair';
                            elseif($row['unit'] == 24)
                                $parts_unit = 'Pcs';
                            elseif($row['unit'] == 25)
                                $parts_unit = 'Pound';
                            elseif($row['unit'] == 26)
                                $parts_unit = 'Qty';
                            elseif($row['unit'] == 27)
                                $parts_unit = 'Roll';
                            elseif($row['unit'] == 28)
                                $parts_unit = 'Set';
                            elseif($row['unit'] == 29)
                                $parts_unit = 'Truck';
                            elseif($row['unit'] == 30)
                                $parts_unit = 'Unit';
                            elseif($row['unit'] == 31)
                                $parts_unit = 'Yeard';
                            elseif($row['unit'] == 32)
                                $parts_unit = '(Unit Unknown)';
                            elseif($row['unit'] == 33)
                                $parts_unit = 'SFT';
                            elseif($row['unit'] == 34)
                                $parts_unit = 'RFT';
                            elseif($row['unit'] == 35)
                                $parts_unit = 'CFT';

                            $data2[] = [
                                'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-' . $row['requisition_id'],
                                'required_for' => $required_for,
                                'parts_id' => $row['parts_id'],
                                'parts_name' => $row['parts_name'],
                                'parts_unit' => $parts_unit,
                                'parts_qty' => $row['parts_qty'],
                                'date' => date('Y-m-d', $row['requisition_data_created'])
                            ];
                        }
                    }
                } 
            }

            if($flag = 1){
                $merged = array_merge($data1, $data2);
                    
                $reply = array(
                    'Type' => 'success',
                    'Reply' => $merged
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No purchase data found !'
                )));
            }
        }

        // FETCH TOTAL PURCHASE AGAINST REQUISITION
        function fetch_tot_purchase_against_requisition(){
            // CONSUMABLE
            $con_requisition_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");
            $con_purchase_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE()) AND YEAR(purchase_date) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");

            $data1 = [];
            $data2 = [];

            if(mysqli_num_rows($con_requisition_query) > 0){
                while($row = mysqli_fetch_assoc($con_requisition_query)){
                    $data1[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_purchase_query) > 0){
                while($row = mysqli_fetch_assoc($con_purchase_query)){
                    $data2[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data3 = [];
            $data4 = [];

            foreach($data1 as $key => $value){
                array_push($data3, $value['parts_id']);

                foreach($data2 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data4, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $con_requisition_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $con_purchase_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");

            $data11 = [];
            $data22 = [];

            if(mysqli_num_rows($con_requisition_query2) > 0){
                while($row = mysqli_fetch_assoc($con_requisition_query2)){
                    $data11[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_purchase_query2) > 0){
                while($row = mysqli_fetch_assoc($con_purchase_query2)){
                    $data22[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data33 = [];
            $data44 = [];

            foreach($data11 as $key => $value){
                array_push($data33, $value['parts_id']);

                foreach($data22 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data44, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $con_requisition_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $con_purchase_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY parts_id ORDER BY parts_id");

            $data111 = [];
            $data222 = [];

            if(mysqli_num_rows($con_requisition_query3) > 0){
                while($row = mysqli_fetch_assoc($con_requisition_query3)){
                    $data111[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_purchase_query3) > 0){
                while($row = mysqli_fetch_assoc($con_purchase_query3)){
                    $data222[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data333 = [];
            $data444 = [];

            foreach($data111 as $key => $value){
                array_push($data333, $value['parts_id']);

                foreach($data222 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data444, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            // SPARE
            $spr_requisition_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");
            $spr_purchase_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE()) AND YEAR(purchase_date) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");

            $data5 = [];
            $data6 = [];

            if(mysqli_num_rows($spr_requisition_query) > 0){
                while($row = mysqli_fetch_assoc($spr_requisition_query)){
                    $data5[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_purchase_query) > 0){
                while($row = mysqli_fetch_assoc($spr_purchase_query)){
                    $data6[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data7 = [];
            $data8 = [];

            foreach($data5 as $key => $value){
                array_push($data7, $value['parts_id']);

                foreach($data6 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data8, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $spr_requisition_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $spr_purchase_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");

            $data55 = [];
            $data66 = [];

            if(mysqli_num_rows($spr_requisition_query2) > 0){
                while($row = mysqli_fetch_assoc($spr_requisition_query2)){
                    $data55[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_purchase_query2) > 0){
                while($row = mysqli_fetch_assoc($spr_purchase_query2)){
                    $data66[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data77 = [];
            $data88 = [];

            foreach($data55 as $key => $value){
                array_push($data77, $value['parts_id']);

                foreach($data66 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data88, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $spr_requisition_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $spr_purchase_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY parts_id ORDER BY parts_id");

            $data555 = [];
            $data666 = [];

            if(mysqli_num_rows($spr_requisition_query3) > 0){
                while($row = mysqli_fetch_assoc($spr_requisition_query3)){
                    $data555[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_purchase_query3) > 0){
                while($row = mysqli_fetch_assoc($spr_purchase_query3)){
                    $data666[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data777 = [];
            $data888 = [];

            foreach($data555 as $key => $value){
                array_push($data777, $value['parts_id']);

                foreach($data666 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data888, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data9[] = [
                'tot_requisition' => (count($data3) + count($data7)),
                'tot_purchase' => (count($data4) + count($data8)),
                'tot_requisition2' => (count($data33) + count($data77)),
                'tot_purchase2' => (count($data44) + count($data88)),
                'tot_requisition3' => (count($data333) + count($data777)),
                'tot_purchase3' => (count($data444) + count($data888))
            ];

            $reply = array(
                'Type' => 'success',
                'Reply' => $data9
            );

            exit(json_encode($reply));
        }
    }
?>