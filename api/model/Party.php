<?php
    namespace Party;

    class Party{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        // FETCH A PARTY
        function fetch($party_id){
            $party_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_party WHERE party_id = '$party_id' LIMIT 1");

            if(mysqli_num_rows($party_query) > 0){
            	while($row = mysqli_fetch_assoc($party_query)){
	                $data[] = [
	                	'party_id' => $row['party_id'],
	                    'party_name' => $row['party_name'],
	                    'party_mobile' => $row['party_mobile'],
	                    'party_address' => $row['party_address'],
	                    'party_remarks' => $row['party_remarks']
	                ];
	            }

                $reply = array(
	                'Type' => 'success',
	                'Reply' => $data
	            );

	            exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No party found !'
                )));
            }
        }

        // FETCH ALL PARTY
        function fetch_all(){
            $party_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_party");

            if(mysqli_num_rows($party_query) > 0){
                while($row = mysqli_fetch_assoc($party_query)){
                    $data[] = [
                        'party_id' => $row['party_id'],
                        'party_name' => $row['party_name'],
                        'party_mobile' => $row['party_mobile'],
                        'party_address' => $row['party_address'],
                        'party_remarks' => $row['party_remarks']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No party found !'
                )));
            }
        }

        // FETCH A PARTY LEDGER
        function fetch_party_ledger($party_id){
            $party_ledger_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_party_ledger WHERE party_id = '$party_id'");

            if(mysqli_num_rows($party_ledger_query) > 0){
                while($row = mysqli_fetch_assoc($party_ledger_query)){
                    $data[] = [
                        'party_ledger_id' => $row['party_ledger_id'],
                        'party_id' => $row['party_id'],
                        'description' => $row['description'],
                        'debit' => $row['debit'],
                        'credit' => $row['credit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No party ledger found !'
                )));
            }
        }
    }
?>