<?php
    namespace Requisition;

    class Requisition{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        // FETCH A CONSUMABLE REQUISITION
        function fetch_consumable($requisition_id){
            $requisition_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_con_requisition WHERE requisition_id = '$requisition_id'");

            if(mysqli_num_rows($requisition_query) > 0){
                $requisition_info = mysqli_fetch_assoc($requisition_query);

                $user_query = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['requisition_by']."' LIMIT 1");
                $user_info = mysqli_fetch_assoc($user_query);

                $user_query2 = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['approved_by']."' LIMIT 1");
                $user_info2 = mysqli_fetch_assoc($user_query2);
                if(isset($user_info2)){
                    $approved_by = $user_info2['user_fullname'];
                } else{
                    $approved_by = 'None';
                }

                $user_query3 = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['s_approved_by']."' LIMIT 1");
                $user_info3 = mysqli_fetch_assoc($user_query3);
                if(isset($user_info3)){
                    $s_approved_by = $user_info3['user_fullname'];
                } else{
                    $s_approved_by = 'None';
                }

                $user_query4 = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['p_approved_by']."' LIMIT 1");
                $user_info4 = mysqli_fetch_assoc($user_query4);
                if(isset($user_info4)){
                    $p_approved_by = $user_info4['user_fullname'];
                } else{
                    $p_approved_by = 'None';
                }

                $requisition_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks, r.parts_qty AS r_qty, i.parts_qty AS i_qty, i.parts_rate AS price FROM rrmsteel_con_requisition_data r INNER JOIN rrmsteel_parts p ON p.parts_id = r.parts_id INNER JOIN rrmsteel_inv_summary i ON i.parts_id = r.parts_id WHERE requisition_id = '$requisition_id'");

                while($row = mysqli_fetch_assoc($requisition_data_query)){
                    $data[] = [
                        'reference' => 'RRM\\CONSUMABLE-REQUISITION\\' . date('Y') . '-',
                        'requisition_by' => $user_info['user_fullname'],
                        's_approval_status' => $requisition_info['s_approval_status'],
                        's_approved_by' => $s_approved_by,
                        'p_approval_status' => $requisition_info['p_approval_status'],
                        'p_approved_by' => $p_approved_by,
                        'approval_status' => $requisition_info['approval_status'],
                        'approved_by' => $approved_by,
                        'requisition_created' => date('d M, Y', $requisition_info['requisition_created']),
                        'requisition_data_id' => $row['requisition_data_id'],
                        'required_for' => $row['required_for'],
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'r_qty' => $row['r_qty'],
                        'parts_unit' => $row['unit'],
                        'i_qty' => $row['i_qty'],
                        'price' => $row['price'],
                        'parts_usage' => $row['parts_usage'],
                        'remarks' => $row['r_remarks'],
                        'loan' => $row['loan'],
                        'requisition_id' => $row['requisition_id']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No requisition data found !'
                )));
            }
        }

        // FETCH A SPARE REQUISITION
        function fetch_spare($requisition_id){
            $requisition_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_spr_requisition WHERE requisition_id = '$requisition_id'");

            if(mysqli_num_rows($requisition_query) > 0){
                $requisition_info = mysqli_fetch_assoc($requisition_query);

                $user_query = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['requisition_by']."' LIMIT 1");
                $user_info = mysqli_fetch_assoc($user_query);

                $user_query2 = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['approved_by']."' LIMIT 1");
                $user_info2 = mysqli_fetch_assoc($user_query2);
                if(isset($user_info2)){
                    $approved_by = $user_info2['user_fullname'];
                } else{
                    $approved_by = 'None';
                }

                $user_query3 = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['s_approved_by']."' LIMIT 1");
                $user_info3 = mysqli_fetch_assoc($user_query3);
                if(isset($user_info3)){
                    $s_approved_by = $user_info3['user_fullname'];
                } else{
                    $s_approved_by = 'None';
                }

                $user_query4 = mysqli_query($this->conn, "SELECT user_fullname FROM rrmsteel_user WHERE user_id = '".$requisition_info['p_approved_by']."' LIMIT 1");
                $user_info4 = mysqli_fetch_assoc($user_query4);
                if(isset($user_info4)){
                    $p_approved_by = $user_info4['user_fullname'];
                } else{
                    $p_approved_by = 'None';
                }

                $requisition_data_query = mysqli_query($this->conn, "SELECT *, r.remarks AS r_remarks, r.parts_qty AS r_qty, i.parts_qty AS i_qty, i.parts_rate AS price FROM rrmsteel_spr_requisition_data r INNER JOIN rrmsteel_parts p ON p.parts_id = r.parts_id INNER JOIN rrmsteel_inv_summary i ON i.parts_id = r.parts_id WHERE requisition_id = '$requisition_id'");

                while($row = mysqli_fetch_assoc($requisition_data_query)){
                    $data[] = [
                        'reference' => 'RRM\\SPARE-REQUISITION\\' . date('Y') . '-',
                        'requisition_by' => $user_info['user_fullname'],
                        's_approval_status' => $requisition_info['s_approval_status'],
                        's_approved_by' => $s_approved_by,
                        'p_approval_status' => $requisition_info['p_approval_status'],
                        'p_approved_by' => $p_approved_by,
                        'approval_status' => $requisition_info['approval_status'],
                        'approved_by' => $approved_by,
                        'requisition_created' => date('d M, Y', $requisition_info['requisition_created']),
                        'requisition_data_id' => $row['requisition_data_id'],
                        'required_for' => $row['required_for'],
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'r_qty' => $row['r_qty'],
                        'parts_unit' => $row['unit'],
                        'i_qty' => $row['i_qty'],
                        'price' => $row['price'],
                        'old_spare_details' => $row['old_spare_details'],
                        'status' => $row['status'],
                        'remarks' => $row['r_remarks'],
                        'loan' => $row['loan'],
                        'requisition_id' => $row['requisition_id']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No requisition data found !'
                )));
            }
        }

        // FETCH REQUISITION NUMBER
        function fetch_requisition_num(){
            // CURRENT MONTH
            $requisition_info_1 = mysqli_fetch_array(mysqli_query($this->conn, "SELECT SUM(CASE WHEN approval_status = 0 THEN 1 Else 0 End) tot_pending, SUM(CASE WHEN approval_status = 1 THEN 1 Else 0 End) tot_approved, SUM(CASE WHEN approval_status = 2 THEN 1 Else 0 End) tot_rejected FROM rrmsteel_con_requisition WHERE MONTH(FROM_UNIXTIME(requisition_created)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(requisition_created)) = YEAR(CURDATE())"));

            $requisition_info_2 = mysqli_fetch_array(mysqli_query($this->conn, "SELECT SUM(CASE WHEN approval_status = 0 THEN 1 Else 0 End) tot_pending, SUM(CASE WHEN approval_status = 1 THEN 1 Else 0 End) tot_approved, SUM(CASE WHEN approval_status = 2 THEN 1 Else 0 End) tot_rejected FROM rrmsteel_spr_requisition WHERE MONTH(FROM_UNIXTIME(requisition_created)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(requisition_created)) = YEAR(CURDATE())"));

            $tot_pending = 0; $tot_approved = 0; $tot_rejected = 0;

            if(isset($requisition_info_1['tot_pending']))
                $tot_pending += $requisition_info_1['tot_pending'];
            if(isset($requisition_info_1['tot_approved']))
                $tot_approved += $requisition_info_1['tot_approved'];
            if(isset($requisition_info_1['tot_rejected']))
                $tot_rejected += $requisition_info_1['tot_rejected'];

            if(isset($requisition_info_2['tot_pending']))
                $tot_pending += $requisition_info_2['tot_pending'];
            if(isset($requisition_info_2['tot_approved']))
                $tot_approved += $requisition_info_2['tot_approved'];
            if(isset($requisition_info_2['tot_rejected']))
                $tot_rejected += $requisition_info_2['tot_rejected'];

            // PREVIOUS MONTH
            $requisition_info_3 = mysqli_fetch_array(mysqli_query($this->conn, "SELECT SUM(CASE WHEN approval_status = 0 THEN 1 Else 0 End) tot_pending, SUM(CASE WHEN approval_status = 1 THEN 1 Else 0 End) tot_approved, SUM(CASE WHEN approval_status = 2 THEN 1 Else 0 End) tot_rejected FROM rrmsteel_con_requisition WHERE MONTH(FROM_UNIXTIME(requisition_created)) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(FROM_UNIXTIME(requisition_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH)"));

            $requisition_info_4 = mysqli_fetch_array(mysqli_query($this->conn, "SELECT SUM(CASE WHEN approval_status = 0 THEN 1 Else 0 End) tot_pending, SUM(CASE WHEN approval_status = 1 THEN 1 Else 0 End) tot_approved, SUM(CASE WHEN approval_status = 2 THEN 1 Else 0 End) tot_rejected FROM rrmsteel_spr_requisition WHERE MONTH(FROM_UNIXTIME(requisition_created)) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(FROM_UNIXTIME(requisition_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH)"));

            $tot_pending2 = 0; $tot_approved2 = 0; $tot_rejected2 = 0;

            if(isset($requisition_info_3['tot_pending']))
                $tot_pending2 += $requisition_info_3['tot_pending'];
            if(isset($requisition_info_3['tot_approved']))
                $tot_approved2 += $requisition_info_3['tot_approved'];
            if(isset($requisition_info_3['tot_rejected']))
                $tot_rejected2 += $requisition_info_3['tot_rejected'];

            if(isset($requisition_info_4['tot_pending']))
                $tot_pending2 += $requisition_info_4['tot_pending'];
            if(isset($requisition_info_4['tot_approved']))
                $tot_approved2 += $requisition_info_4['tot_approved'];
            if(isset($requisition_info_4['tot_rejected']))
                $tot_rejected2 += $requisition_info_4['tot_rejected'];

            // PREVIOUS TO PREVIOUS MONTH
            $requisition_info_5 = mysqli_fetch_array(mysqli_query($this->conn, "SELECT SUM(CASE WHEN approval_status = 0 THEN 1 Else 0 End) tot_pending, SUM(CASE WHEN approval_status = 1 THEN 1 Else 0 End) tot_approved, SUM(CASE WHEN approval_status = 2 THEN 1 Else 0 End) tot_rejected FROM rrmsteel_con_requisition WHERE MONTH(FROM_UNIXTIME(requisition_created)) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(FROM_UNIXTIME(requisition_created)) = YEAR(CURDATE() - INTERVAL 2 MONTH)"));

            $requisition_info_6 = mysqli_fetch_array(mysqli_query($this->conn, "SELECT SUM(CASE WHEN approval_status = 0 THEN 1 Else 0 End) tot_pending, SUM(CASE WHEN approval_status = 1 THEN 1 Else 0 End) tot_approved, SUM(CASE WHEN approval_status = 2 THEN 1 Else 0 End) tot_rejected FROM rrmsteel_spr_requisition WHERE MONTH(FROM_UNIXTIME(requisition_created)) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(FROM_UNIXTIME(requisition_created)) = YEAR(CURDATE() - INTERVAL 2 MONTH)"));

            $tot_pending3 = 0; $tot_approved3 = 0; $tot_rejected3 = 0;

            if(isset($requisition_info_5['tot_pending']))
                $tot_pending3 += $requisition_info_5['tot_pending'];
            if(isset($requisition_info_5['tot_approved']))
                $tot_approved3 += $requisition_info_5['tot_approved'];
            if(isset($requisition_info_5['tot_rejected']))
                $tot_rejected3 += $requisition_info_5['tot_rejected'];

            if(isset($requisition_info_6['tot_pending']))
                $tot_pending3 += $requisition_info_6['tot_pending'];
            if(isset($requisition_info_6['tot_approved']))
                $tot_approved3 += $requisition_info_6['tot_approved'];
            if(isset($requisition_info_6['tot_rejected']))
                $tot_rejected3 += $requisition_info_6['tot_rejected'];

            $data[] = [
                'tot_pending' => $tot_pending,
                'tot_approved' => $tot_approved,
                'tot_rejected' => $tot_rejected,
                'tot_pending2' => $tot_pending2,
                'tot_approved2' => $tot_approved2,
                'tot_rejected2' => $tot_rejected2,
                'tot_pending3' => $tot_pending3,
                'tot_approved3' => $tot_approved3,
                'tot_rejected3' => $tot_rejected3
            ];

            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }
    }
?>