<?php
    namespace Parts;

    class Parts{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        // FETCH A PARTS
        function fetch($parts_id){
            $parts_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_parts WHERE parts_id = '$parts_id' LIMIT 1");

            if(mysqli_num_rows($parts_query) > 0){
            	while($row = mysqli_fetch_assoc($parts_query)){
	                $data[] = [
	                	'parts_id' => $row['parts_id'],
	                    'parts_name' => $row['parts_name'],
                        'parts_nickname' => $row['parts_nickname'],
	                    'parts_category' => $row['category'],
                        'parts_subcategory' => $row['subcategory'],
                        'parts_subcategory_2' => $row['subcategory_2'],
                        'parts_type' => $row['type'],
                        'parts_group' => $row['apply_group'],
                        'parts_inv_type' => $row['inv_type'],
	                    'parts_unit' => $row['unit'],
                        'parts_alert_qty' => $row['alert_qty'],
                        'parts_remarks' => $row['remarks']
	                ];
	            }

                $reply = array(
	                'Type' => 'success',
	                'Reply' => $data
	            );

	            exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No parts found !'
                )));
            }
        }

        // FETCH ALL PARTS
        function fetch_all(){
            $parts_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id");

            if(mysqli_num_rows($parts_query) > 0){
                while($row = mysqli_fetch_assoc($parts_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_nickname' => $row['parts_nickname'],
                        'parts_category' => $row['category'],
                        'parts_subcategory' => $row['subcategory'],
                        'parts_subcategory_2' => $row['subcategory_2'],
                        'parts_type' => $row['type'],
                        'parts_group' => $row['apply_group'],
                        'parts_inv_type' => $row['inv_type'],
                        'parts_unit' => $row['unit'],
                        'parts_alert_qty' => $row['alert_qty'],
                        'parts_image' => $row['parts_image'],
                        'parts_remarks' => $row['remarks'],
                        'parts_qty' => $row['parts_qty'],
                        'parts_avg_rate' => $row['parts_avg_rate']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No parts found !'
                )));
            }
        }
    }
?>