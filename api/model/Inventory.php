<?php
    namespace Inventory;

    class Inventory{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        // FETCH TOTAL RECEIVE AGAINST PURCHASE
        function fetch_tot_receive_against_purchase(){
            // CONSUMABLE
            $con_requisition_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");
            $con_purchase_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) AS parts_qty FROM rrmsteel_con_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE()) AND YEAR(purchase_date) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");
            $con_receive_query = mysqli_query($this->conn, "SELECT i.parts_id, SUM(i.received_qty) AS parts_qty FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE p.category = 2 AND i.source = 1 AND MONTH(i.history_date) = MONTH(CURDATE()) AND YEAR(i.history_date) = YEAR(CURDATE()) GROUP BY i.parts_id ORDER BY i.parts_id");

            $data1 = [];
            $data2 = [];
            $data3 = [];

            if(mysqli_num_rows($con_requisition_query) > 0){
                while($row = mysqli_fetch_assoc($con_requisition_query)){
                    $data1[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_purchase_query) > 0){
                while($row = mysqli_fetch_assoc($con_purchase_query)){
                    $data2[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_receive_query) > 0){
                while($row = mysqli_fetch_assoc($con_receive_query)){
                    $data3[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data5 = [];

            foreach($data1 as $key => $value){
                foreach($data2 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            $data5[] = [
                                'parts_id' => $value2['parts_id'],
                                'parts_qty' => $value2['parts_qty']
                            ];
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data7 = [];

            foreach($data5 as $key => $value){
                foreach($data3 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] == $value['parts_qty']){
                            array_push($data7, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $con_requisition_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $con_purchase_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) AS parts_qty FROM rrmsteel_con_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $con_receive_query2 = mysqli_query($this->conn, "SELECT i.parts_id, SUM(i.received_qty) AS parts_qty FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE p.category = 2 AND i.source = 1 AND MONTH(i.history_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(i.history_date) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY i.parts_id ORDER BY i.parts_id");

            $data11 = [];
            $data22 = [];
            $data33 = [];

            if(mysqli_num_rows($con_requisition_query2) > 0){
                while($row = mysqli_fetch_assoc($con_requisition_query2)){
                    $data11[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_purchase_query2) > 0){
                while($row = mysqli_fetch_assoc($con_purchase_query2)){
                    $data22[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_receive_query2) > 0){
                while($row = mysqli_fetch_assoc($con_receive_query2)){
                    $data33[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data55 = [];

            foreach($data11 as $key => $value){
                foreach($data22 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            $data55[] = [
                                'parts_id' => $value2['parts_id'],
                                'parts_qty' => $value2['parts_qty']
                            ];
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data77 = [];

            foreach($data55 as $key => $value){
                foreach($data33 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            array_push($data77, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $con_requisition_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_con_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $con_purchase_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) AS parts_qty FROM rrmsteel_con_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $con_receive_query3 = mysqli_query($this->conn, "SELECT i.parts_id, SUM(i.received_qty) AS parts_qty FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE p.category = 2 AND i.source = 1 AND MONTH(i.history_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(i.history_date) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY i.parts_id ORDER BY i.parts_id");

            $data111 = [];
            $data222 = [];
            $data333 = [];

            if(mysqli_num_rows($con_requisition_query3) > 0){
                while($row = mysqli_fetch_assoc($con_requisition_query3)){
                    $data111[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_purchase_query3) > 0){
                while($row = mysqli_fetch_assoc($con_purchase_query3)){
                    $data222[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($con_receive_query3) > 0){
                while($row = mysqli_fetch_assoc($con_receive_query3)){
                    $data333[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data555 = [];

            foreach($data111 as $key => $value){
                foreach($data222 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            $data555[] = [
                                'parts_id' => $value2['parts_id'],
                                'parts_qty' => $value2['parts_qty']
                            ];
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data777 = [];

            foreach($data555 as $key => $value){
                foreach($data333 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] == $value['parts_qty']){
                            array_push($data777, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            // SPARE
            $spr_requisition_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");
            $spr_purchase_query = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) AS parts_qty FROM rrmsteel_spr_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE()) AND YEAR(purchase_date) = YEAR(CURDATE()) GROUP BY parts_id ORDER BY parts_id");
            $spr_receive_query = mysqli_query($this->conn, "SELECT i.parts_id, SUM(i.received_qty) AS parts_qty FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE p.category = 1 AND i.source = 1 AND MONTH(i.history_date) = MONTH(CURDATE()) AND YEAR(i.history_date) = YEAR(CURDATE()) GROUP BY i.parts_id ORDER BY i.parts_id");

            $data8 = [];
            $data9 = [];
            $data10 = [];

            if(mysqli_num_rows($spr_requisition_query) > 0){
                while($row = mysqli_fetch_assoc($spr_requisition_query)){
                    $data8[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_purchase_query) > 0){
                while($row = mysqli_fetch_assoc($spr_purchase_query)){
                    $data9[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_receive_query) > 0){
                while($row = mysqli_fetch_assoc($spr_receive_query)){
                    $data10[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data12 = [];

            foreach($data8 as $key => $value){
                foreach($data9 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            $data12[] = [
                                'parts_id' => $value2['parts_id'],
                                'parts_qty' => $value2['parts_qty']
                            ];
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data14 = [];

            foreach($data12 as $key => $value){
                foreach($data10 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] == $value['parts_qty']){
                            array_push($data14, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $spr_requisition_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $spr_purchase_query2 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) AS parts_qty FROM rrmsteel_spr_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $spr_receive_query2 = mysqli_query($this->conn, "SELECT i.parts_id, SUM(i.received_qty) AS parts_qty FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE p.category = 1 AND i.source = 1 AND MONTH(i.history_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(i.history_date) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY i.parts_id ORDER BY i.parts_id");

            $data88 = [];
            $data99 = [];
            $data1010 = [];

            if(mysqli_num_rows($spr_requisition_query2) > 0){
                while($row = mysqli_fetch_assoc($spr_requisition_query2)){
                    $data88[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_purchase_query2) > 0){
                while($row = mysqli_fetch_assoc($spr_purchase_query2)){
                    $data99[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_receive_query2) > 0){
                while($row = mysqli_fetch_assoc($spr_receive_query2)){
                    $data1010[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data1212 = [];

            foreach($data88 as $key => $value){
                foreach($data99 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            $data1212[] = [
                                'parts_id' => $value2['parts_id'],
                                'parts_qty' => $value2['parts_qty']
                            ];
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data1414 = [];

            foreach($data1212 as $key => $value){
                foreach($data1010 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] == $value['parts_qty']){
                            array_push($data1414, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $spr_requisition_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) as parts_qty FROM rrmsteel_spr_requisition_data WHERE MONTH(FROM_UNIXTIME(requisition_data_created)) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(FROM_UNIXTIME(requisition_data_created)) = YEAR(CURDATE() - INTERVAL 1 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $spr_purchase_query3 = mysqli_query($this->conn, "SELECT parts_id, SUM(parts_qty) AS parts_qty FROM rrmsteel_spr_purchase_data WHERE MONTH(purchase_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(purchase_date) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY parts_id ORDER BY parts_id");
            $spr_receive_query3 = mysqli_query($this->conn, "SELECT i.parts_id, SUM(i.received_qty) AS parts_qty FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE p.category = 1 AND i.source = 1 AND MONTH(i.history_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(i.history_date) = YEAR(CURDATE() - INTERVAL 2 MONTH) GROUP BY i.parts_id ORDER BY i.parts_id");

            $data888 = [];
            $data999 = [];
            $data101010 = [];

            if(mysqli_num_rows($spr_requisition_query3) > 0){
                while($row = mysqli_fetch_assoc($spr_requisition_query3)){
                    $data888[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_purchase_query3) > 0){
                while($row = mysqli_fetch_assoc($spr_purchase_query3)){
                    $data999[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            if(mysqli_num_rows($spr_receive_query3) > 0){
                while($row = mysqli_fetch_assoc($spr_receive_query3)){
                    $data101010[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_qty' => $row['parts_qty']
                    ];
                }
            }

            $data121212 = [];

            foreach($data888 as $key => $value){
                foreach($data999 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] >= $value['parts_qty']){
                            $data121212[] = [
                                'parts_id' => $value2['parts_id'],
                                'parts_qty' => $value2['parts_qty']
                            ];
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data141414 = [];

            foreach($data121212 as $key => $value){
                foreach($data101010 as $key2 => $value2){
                    if($value2['parts_id'] == $value['parts_id']){
                        if($value2['parts_qty'] == $value['parts_qty']){
                            array_push($data141414, $value2['parts_id']);
                        }
                    } else{
                        continue;
                    }
                }
            }

            $data15[] = [
                'tot_purchase' => (count($data5) + count($data12)),
                'tot_receive' => (count($data7) + count($data14)),
                'tot_purchase2' => (count($data55) + count($data1212)),
                'tot_receive2' => (count($data77) + count($data1414)),
                'tot_purchase3' => (count($data555) + count($data121212)),
                'tot_receive3' => (count($data777) + count($data141414))
            ];

            $reply = array(
                'Type' => 'success',
                'Reply' => $data15
            );

            exit(json_encode($reply));
        }

        // INVENTORY PARTS RECEIVE
        function inventory_parts_receive($parts_id, $required_for, $action_date){
            // INVENTORY HISTORY DATA
            $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date < '$action_date'");
            $inventory_history_query_rows = mysqli_num_rows($inventory_history_query);

            if($inventory_history_query_rows == 0){
                $parts_qty = null;

                $data[] = [
                    'parts_qty' => $parts_qty
                ];
            } else{
                // PARTS CATEGORY
                $parts_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT category FROM rrmsteel_parts WHERE parts_id = '$parts_id' LIMIT 1"));
                $parts_category = $parts_data_info['category'];

                // PURCHASED & BORROWED PARTS
                if($parts_category == 1){
                    $purchase_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT SUM(parts_qty) AS purchased_parts_qty, SUM(price) AS purchased_parts_price FROM rrmsteel_spr_purchase_data WHERE parts_id = '$parts_id' AND required_for = '$required_for' AND purchase_date <= '$action_date'"));

                    $loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT SUM(parts_qty) AS borrowed_parts_qty, SUM(price) AS borrowed_parts_price FROM rrmsteel_spr_loan_data WHERE parts_id = '$parts_id' AND required_for = '$required_for' AND loan_date <= '$action_date'"));
                } else{
                    $purchase_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT SUM(parts_qty) AS purchased_parts_qty, SUM(price) AS purchased_parts_price FROM rrmsteel_con_purchase_data WHERE parts_id = '$parts_id' AND required_for = '$required_for' AND purchase_date <= '$action_date'"));

                    $loan_data_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT SUM(parts_qty) AS borrowed_parts_qty, SUM(price) AS borrowed_parts_price FROM rrmsteel_con_loan_data WHERE parts_id = '$parts_id' AND required_for = '$required_for' AND loan_date <= '$action_date'"));
                }

                $tot_parts_qty = $purchase_data_info['purchased_parts_qty'] + $loan_data_info['borrowed_parts_qty'];
                $tot_parts_price = $purchase_data_info['purchased_parts_price'] + $loan_data_info['borrowed_parts_price'];

                if($tot_parts_qty == 0)
                    $parts_price = null;
                elseif($tot_parts_qty > 0)
                    $parts_price = $tot_parts_price / $tot_parts_qty;

                // RECEIVED PARTS
                $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT SUM(received_qty) AS received_parts_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND required_for = '$required_for' AND source = 1 AND history_date <= '$action_date'"));
                $received_parts_qty = $inventory_history_info['received_parts_qty'];

                if($tot_parts_qty != null && $received_parts_qty == null)
                    $parts_qty = $tot_parts_qty;
                elseif($tot_parts_qty != null && $received_parts_qty != null)
                    $parts_qty = $tot_parts_qty - $received_parts_qty;
                else
                    $parts_qty = null;

                $data[] = [
                    'parts_qty' => $parts_qty,
                    'parts_price' => ($parts_price * $parts_qty)
                ];
            }

            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }

        // INVENTORY PARTS ISSUE
        function inventory_parts_issue($parts_id, $required_for, $action_date){
            // INVENTORY HISTORY DATA
            $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date < '$action_date'");

            if(mysqli_num_rows($inventory_history_query) == 0){
                $parts_qty = null;
            } else{
                // AVAILABLE FOR SELECTED
                $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT parts_avg_rate, closing_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date = '$action_date' LIMIT 1"));

                // NOT AVAILABLE FOR SELECTED
                $inventory_history_info1 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT parts_avg_rate, closing_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date <= '$action_date' ORDER BY history_date DESC, inventory_history_created DESC LIMIT 1"));
                
                if(isset($inventory_history_info)){
                    $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT parts_avg_rate, closing_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date >= '$action_date' AND closing_qty = (SELECT MIN(closing_qty) AS min_closing_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date >= '$action_date') ORDER BY history_date, inventory_history_created LIMIT 1"));
                    $parts_qty = $inventory_history_info2['closing_qty'];
                    $parts_price = $inventory_history_info2['parts_avg_rate'];
                } else{
                    $parts_qty = $inventory_history_info1['closing_qty'];
                    $parts_price = $inventory_history_info1['parts_avg_rate'];
                }
            }

            $data[] = [
                'parts_qty' => $parts_qty,
                'parts_price' => ($parts_qty * $parts_price)
            ];

            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }

        // INVENTORY PARTS TOTAL RECEIVE & ISSUE QTY. & VALUE
        function inventory_parts_tot_rcv_iss_qty_val(){
            $tot_issued_qty = 0;
            $tot_received_qty = 0;
            $tot_issued_val = 0;
            $tot_received_val = 0;

            $flag1 = 0;
            $flag2 = 0;
            $flag3 = 0;

            $inventory_history_query = mysqli_query($this->conn, "SELECT SUM(received_qty) AS received_qty, SUM(issued_qty) AS issued_qty, SUM(received_value) AS received_value, SUM(issued_value) AS issued_value FROM rrmsteel_inv_history WHERE source > 0 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE())");

            if(mysqli_num_rows($inventory_history_query) > 0){
                $flag1 = 1;

                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    $tot_received_qty += $row['received_qty'];
                    $tot_issued_qty += $row['issued_qty'];
                    $tot_received_val += $row['received_value'];
                    $tot_issued_val += $row['issued_value'];
                }
            }

            $inventory_history_query2 = mysqli_query($this->conn, "SELECT SUM(received_qty) AS received_qty, SUM(issued_qty) AS issued_qty, SUM(received_value) AS received_value, SUM(issued_value) AS issued_value FROM rrmsteel_inv_history WHERE source > 0 AND MONTH(history_date) = MONTH(CURDATE() - INTERVAL 1 MONTH) AND YEAR(history_date) = YEAR(CURDATE() - INTERVAL 1 MONTH)");

            $tot_issued_qty2 = 0;
            $tot_received_qty2 = 0;
            $tot_issued_val2 = 0;
            $tot_received_val2 = 0;

            if(mysqli_num_rows($inventory_history_query2) > 0){
                $flag2 = 1;

                while($row = mysqli_fetch_assoc($inventory_history_query2)){
                    $tot_received_qty2 += $row['received_qty'];
                    $tot_issued_qty2 += $row['issued_qty'];
                    $tot_received_val2 += $row['received_value'];
                    $tot_issued_val2 += $row['issued_value'];
                }
            }

            $inventory_history_query3 = mysqli_query($this->conn, "SELECT SUM(received_qty) AS received_qty, SUM(issued_qty) AS issued_qty, SUM(received_value) AS received_value, SUM(issued_value) AS issued_value FROM rrmsteel_inv_history WHERE source > 0 AND MONTH(history_date) = MONTH(CURDATE() - INTERVAL 2 MONTH) AND YEAR(history_date) = YEAR(CURDATE() - INTERVAL 2 MONTH)");

            $tot_issued_qty3 = 0;
            $tot_received_qty3 = 0;
            $tot_issued_val3 = 0;
            $tot_received_val3 = 0;

            if(mysqli_num_rows($inventory_history_query3) > 0){
                $flag3 = 1;

                while($row = mysqli_fetch_assoc($inventory_history_query3)){
                    $tot_received_qty3 += $row['received_qty'];
                    $tot_issued_qty3 += $row['issued_qty'];
                    $tot_received_val3 += $row['received_value'];
                    $tot_issued_val3 += $row['issued_value'];
                }
            }

            if($flag1 == 1 || $flag2 == 1 || $flag3 == 1){
                $data[] = [
                    'tot_received_qty' => $tot_received_qty,
                    'tot_issued_qty' => $tot_issued_qty,
                    'tot_received_val' => $tot_received_val,
                    'tot_issued_val' => $tot_issued_val,
                    'tot_received_qty2' => $tot_received_qty2,
                    'tot_issued_qty2' => $tot_issued_qty2,
                    'tot_received_val2' => $tot_received_val2,
                    'tot_issued_val2' => $tot_issued_val2,
                    'tot_received_qty3' => $tot_received_qty3,
                    'tot_issued_qty3' => $tot_issued_qty3,
                    'tot_received_val3' => $tot_received_val3,
                    'tot_issued_val3' => $tot_issued_val3
                ];

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // INVENTORY SUMMARY LIST
        function inventory_summary_list($alpha){
            if($alpha == 9)
                $where = "p.parts_name regexp '^[0-9]+'";
            else
                $where = "p.parts_name LIKE '".$alpha."%'";

            $inventory_history_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit, p.category, p.alert_qty, i.inventory_id, i.parts_qty AS parts_qty, i.parts_avg_rate, SUM(h.received_qty) AS tot_rcv_qty, SUM(h.issued_qty) AS tot_iss_qty FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE " . $where . " GROUP BY p.parts_id");

            if(mysqli_num_rows($inventory_history_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    if($row['category'] == 2){
                        $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM (SELECT parts_id AS pur_parts_id, SUM(CASE WHEN required_for = 1 THEN parts_qty ELSE 0 END) AS tot_pur_1, SUM(CASE WHEN required_for = 2 THEN parts_qty ELSE 0 END) AS tot_pur_2, SUM(CASE WHEN required_for = 3 THEN parts_qty ELSE 0 END) AS tot_pur_3, SUM(CASE WHEN required_for = 4 THEN parts_qty ELSE 0 END) AS tot_pur_4, SUM(CASE WHEN required_for = 5 THEN parts_qty ELSE 0 END) AS tot_pur_5, SUM(CASE WHEN required_for = 6 THEN parts_qty ELSE 0 END) AS tot_pur_6, SUM(CASE WHEN required_for = 7 THEN parts_qty ELSE 0 END) AS tot_pur_7, SUM(CASE WHEN required_for = 8 THEN parts_qty ELSE 0 END) AS tot_pur_8, SUM(CASE WHEN required_for = 9 THEN parts_qty ELSE 0 END) AS tot_pur_9 FROM rrmsteel_con_purchase_data WHERE parts_id = '".$row['parts_id']."') p LEFT JOIN (SELECT parts_id AS inv_parts_id, SUM(CASE WHEN required_for = 1 THEN received_qty ELSE 0 END) AS tot_rcv_1, SUM(CASE WHEN required_for = 2 THEN received_qty ELSE 0 END) AS tot_rcv_2, SUM(CASE WHEN required_for = 3 THEN received_qty ELSE 0 END) AS tot_rcv_3, SUM(CASE WHEN required_for = 4 THEN received_qty ELSE 0 END) AS tot_rcv_4, SUM(CASE WHEN required_for = 5 THEN received_qty ELSE 0 END) AS tot_rcv_5, SUM(CASE WHEN required_for = 6 THEN received_qty ELSE 0 END) AS tot_rcv_6, SUM(CASE WHEN required_for = 7 THEN received_qty ELSE 0 END) AS tot_rcv_7, SUM(CASE WHEN required_for = 8 THEN received_qty ELSE 0 END) AS tot_rcv_8, SUM(CASE WHEN required_for = 9 THEN received_qty ELSE 0 END) AS tot_rcv_9 FROM rrmsteel_inv_history WHERE parts_id = '".$row['parts_id']."') i ON p.pur_parts_id = i.inv_parts_id"));

                        $inventory_history_info3 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM (SELECT parts_id AS loan_parts_id, SUM(CASE WHEN required_for = 1 THEN parts_qty ELSE 0 END) AS tot_loan_1, SUM(CASE WHEN required_for = 2 THEN parts_qty ELSE 0 END) AS tot_loan_2, SUM(CASE WHEN required_for = 3 THEN parts_qty ELSE 0 END) AS tot_loan_3, SUM(CASE WHEN required_for = 4 THEN parts_qty ELSE 0 END) AS tot_loan_4, SUM(CASE WHEN required_for = 5 THEN parts_qty ELSE 0 END) AS tot_loan_5, SUM(CASE WHEN required_for = 6 THEN parts_qty ELSE 0 END) AS tot_loan_6, SUM(CASE WHEN required_for = 7 THEN parts_qty ELSE 0 END) AS tot_loan_7, SUM(CASE WHEN required_for = 8 THEN parts_qty ELSE 0 END) AS tot_loan_8, SUM(CASE WHEN required_for = 9 THEN parts_qty ELSE 0 END) AS tot_loan_9 FROM rrmsteel_con_loan_data WHERE parts_id = '".$row['parts_id']."') p LEFT JOIN (SELECT parts_id AS inv_parts_id, SUM(CASE WHEN required_for = 1 THEN received_qty ELSE 0 END) AS tot_rcv_1, SUM(CASE WHEN required_for = 2 THEN received_qty ELSE 0 END) AS tot_rcv_2, SUM(CASE WHEN required_for = 3 THEN received_qty ELSE 0 END) AS tot_rcv_3, SUM(CASE WHEN required_for = 4 THEN received_qty ELSE 0 END) AS tot_rcv_4, SUM(CASE WHEN required_for = 5 THEN received_qty ELSE 0 END) AS tot_rcv_5, SUM(CASE WHEN required_for = 6 THEN received_qty ELSE 0 END) AS tot_rcv_6, SUM(CASE WHEN required_for = 7 THEN received_qty ELSE 0 END) AS tot_rcv_7, SUM(CASE WHEN required_for = 8 THEN received_qty ELSE 0 END) AS tot_rcv_8, SUM(CASE WHEN required_for = 9 THEN received_qty ELSE 0 END) AS tot_rcv_9 FROM rrmsteel_inv_history WHERE parts_id = '".$row['parts_id']."') i ON p.loan_parts_id = i.inv_parts_id"));
                    } else{
                        $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM (SELECT parts_id AS pur_parts_id, SUM(CASE WHEN required_for = 1 THEN parts_qty ELSE 0 END) AS tot_pur_1, SUM(CASE WHEN required_for = 2 THEN parts_qty ELSE 0 END) AS tot_pur_2, SUM(CASE WHEN required_for = 3 THEN parts_qty ELSE 0 END) AS tot_pur_3, SUM(CASE WHEN required_for = 4 THEN parts_qty ELSE 0 END) AS tot_pur_4, SUM(CASE WHEN required_for = 5 THEN parts_qty ELSE 0 END) AS tot_pur_5, SUM(CASE WHEN required_for = 6 THEN parts_qty ELSE 0 END) AS tot_pur_6, SUM(CASE WHEN required_for = 7 THEN parts_qty ELSE 0 END) AS tot_pur_7, SUM(CASE WHEN required_for = 8 THEN parts_qty ELSE 0 END) AS tot_pur_8, SUM(CASE WHEN required_for = 9 THEN parts_qty ELSE 0 END) AS tot_pur_9 FROM rrmsteel_spr_purchase_data WHERE parts_id = '".$row['parts_id']."') p LEFT JOIN (SELECT parts_id AS inv_parts_id, SUM(CASE WHEN required_for = 1 THEN received_qty ELSE 0 END) AS tot_rcv_1, SUM(CASE WHEN required_for = 2 THEN received_qty ELSE 0 END) AS tot_rcv_2, SUM(CASE WHEN required_for = 3 THEN received_qty ELSE 0 END) AS tot_rcv_3, SUM(CASE WHEN required_for = 4 THEN received_qty ELSE 0 END) AS tot_rcv_4, SUM(CASE WHEN required_for = 5 THEN received_qty ELSE 0 END) AS tot_rcv_5, SUM(CASE WHEN required_for = 6 THEN received_qty ELSE 0 END) AS tot_rcv_6, SUM(CASE WHEN required_for = 7 THEN received_qty ELSE 0 END) AS tot_rcv_7, SUM(CASE WHEN required_for = 8 THEN received_qty ELSE 0 END) AS tot_rcv_8, SUM(CASE WHEN required_for = 9 THEN received_qty ELSE 0 END) AS tot_rcv_9 FROM rrmsteel_inv_history WHERE parts_id = '".$row['parts_id']."') i ON p.pur_parts_id = i.inv_parts_id"));

                        $inventory_history_info3 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT * FROM (SELECT parts_id AS loan_parts_id, SUM(CASE WHEN required_for = 1 THEN parts_qty ELSE 0 END) AS tot_loan_1, SUM(CASE WHEN required_for = 2 THEN parts_qty ELSE 0 END) AS tot_loan_2, SUM(CASE WHEN required_for = 3 THEN parts_qty ELSE 0 END) AS tot_loan_3, SUM(CASE WHEN required_for = 4 THEN parts_qty ELSE 0 END) AS tot_loan_4, SUM(CASE WHEN required_for = 5 THEN parts_qty ELSE 0 END) AS tot_loan_5, SUM(CASE WHEN required_for = 6 THEN parts_qty ELSE 0 END) AS tot_loan_6, SUM(CASE WHEN required_for = 7 THEN parts_qty ELSE 0 END) AS tot_loan_7, SUM(CASE WHEN required_for = 8 THEN parts_qty ELSE 0 END) AS tot_loan_8, SUM(CASE WHEN required_for = 9 THEN parts_qty ELSE 0 END) AS tot_loan_9 FROM rrmsteel_spr_loan_data WHERE parts_id = '".$row['parts_id']."') p LEFT JOIN (SELECT parts_id AS inv_parts_id, SUM(CASE WHEN required_for = 1 THEN received_qty ELSE 0 END) AS tot_rcv_1, SUM(CASE WHEN required_for = 2 THEN received_qty ELSE 0 END) AS tot_rcv_2, SUM(CASE WHEN required_for = 3 THEN received_qty ELSE 0 END) AS tot_rcv_3, SUM(CASE WHEN required_for = 4 THEN received_qty ELSE 0 END) AS tot_rcv_4, SUM(CASE WHEN required_for = 5 THEN received_qty ELSE 0 END) AS tot_rcv_5, SUM(CASE WHEN required_for = 6 THEN received_qty ELSE 0 END) AS tot_rcv_6, SUM(CASE WHEN required_for = 7 THEN received_qty ELSE 0 END) AS tot_rcv_7, SUM(CASE WHEN required_for = 8 THEN received_qty ELSE 0 END) AS tot_rcv_8, SUM(CASE WHEN required_for = 9 THEN received_qty ELSE 0 END) AS tot_rcv_9 FROM rrmsteel_inv_history WHERE parts_id = '".$row['parts_id']."') i ON p.loan_parts_id = i.inv_parts_id"));
                    }

                    $bcp_ccm = ($inventory_history_info2['tot_pur_1'] - $inventory_history_info2['tot_rcv_1']) + ($inventory_history_info3['tot_loan_1'] - $inventory_history_info3['tot_rcv_1']);
                    $bcp_fur = ($inventory_history_info2['tot_pur_2'] - $inventory_history_info2['tot_rcv_2']) + ($inventory_history_info3['tot_loan_2'] - $inventory_history_info3['tot_rcv_2']);
                    $con_ccm = ($inventory_history_info2['tot_pur_3'] - $inventory_history_info2['tot_rcv_3']) + ($inventory_history_info3['tot_loan_3'] - $inventory_history_info3['tot_rcv_3']);
                    $con_fur = ($inventory_history_info2['tot_pur_4'] - $inventory_history_info2['tot_rcv_4']) + ($inventory_history_info3['tot_loan_4'] - $inventory_history_info3['tot_rcv_4']);
                    $hrm = ($inventory_history_info2['tot_pur_5'] - $inventory_history_info2['tot_rcv_5']) + ($inventory_history_info3['tot_loan_5'] - $inventory_history_info3['tot_rcv_5']);
                    $hrm_2 = ($inventory_history_info2['tot_pur_6'] - $inventory_history_info2['tot_rcv_6']) + ($inventory_history_info3['tot_loan_6'] - $inventory_history_info3['tot_rcv_6']);
                    $sonargaon = ($inventory_history_info2['tot_pur_7'] - $inventory_history_info2['tot_rcv_7']) + ($inventory_history_info3['tot_loan_7'] - $inventory_history_info3['tot_rcv_7']);
                    $lal_masjid = ($inventory_history_info2['tot_pur_8'] - $inventory_history_info2['tot_rcv_8']) + ($inventory_history_info3['tot_loan_8'] - $inventory_history_info3['tot_rcv_8']);
                    $general = ($inventory_history_info2['tot_pur_9'] - $inventory_history_info2['tot_rcv_9']) + ($inventory_history_info3['tot_loan_9'] - $inventory_history_info3['tot_rcv_9']);

                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'parts_qty' => $row['parts_qty'],
                        'parts_alert_qty' => $row['alert_qty'],
                        'tot_rcv_qty' => $row['tot_rcv_qty'],
                        'tot_iss_qty' => $row['tot_iss_qty'],
                        'parts_avg_rate' => $row['parts_avg_rate'],
                        'inventory_id' => $row['inventory_id'],
                        'bcp_ccm' => $bcp_ccm,
                        'bcp_fur' => $bcp_fur,
                        'con_ccm' => $con_ccm,
                        'con_fur' => $con_fur,
                        'hrm' => $hrm,
                        'hrm_2' => $hrm_2,
                        'sonargaon' => $sonargaon,
                        'lal_masjid' => $lal_masjid,
                        'general' => $general
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No summary data found !'
                )));
            }
        }

        // INVENTORY HISTORY LIST
        function inventory_history_list(){
            $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id INNER JOIN rrmsteel_user u ON u.user_id = i.user_id WHERE i.source > 0 ORDER BY i.history_date DESC, i.inventory_history_created DESC");

            if(mysqli_num_rows($inventory_history_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    $parts_unit = '';

                    if($row['unit'] == 1) $parts_unit = 'Bag';
                    elseif($row['unit'] == 2) $parts_unit = 'Box';
                    elseif($row['unit'] == 3) $parts_unit = 'Box/Pcs';
                    elseif($row['unit'] == 4) $parts_unit = 'Bun';
                    elseif($row['unit'] == 5) $parts_unit = 'Bundle';
                    elseif($row['unit'] == 6) $parts_unit = 'Can';
                    elseif($row['unit'] == 7) $parts_unit = 'Cartoon';
                    elseif($row['unit'] == 8) $parts_unit = 'Challan';
                    elseif($row['unit'] == 9) $parts_unit = 'Coil';
                    elseif($row['unit'] == 10) $parts_unit = 'Drum';
                    elseif($row['unit'] == 11) $parts_unit = 'Feet';
                    elseif($row['unit'] == 12) $parts_unit = 'Gallon';
                    elseif($row['unit'] == 13) $parts_unit = 'Item';
                    elseif($row['unit'] == 14) $parts_unit = 'Job';
                    elseif($row['unit'] == 15) $parts_unit = 'Kg';
                    elseif($row['unit'] == 16) $parts_unit = 'Kg/Bundle';
                    elseif($row['unit'] == 17) $parts_unit = 'Kv';
                    elseif($row['unit'] == 18) $parts_unit = 'Lbs';
                    elseif($row['unit'] == 19) $parts_unit = 'Ltr';
                    elseif($row['unit'] == 20) $parts_unit = 'Mtr';
                    elseif($row['unit'] == 21) $parts_unit = 'Pack';
                    elseif($row['unit'] == 22) $parts_unit = 'Pack/Pcs';
                    elseif($row['unit'] == 23) $parts_unit = 'Pair';
                    elseif($row['unit'] == 24) $parts_unit = 'Pcs';
                    elseif($row['unit'] == 25) $parts_unit = 'Pound';
                    elseif($row['unit'] == 26) $parts_unit = 'Qty';
                    elseif($row['unit'] == 27) $parts_unit = 'Roll';
                    elseif($row['unit'] == 28) $parts_unit = 'Set';
                    elseif($row['unit'] == 29) $parts_unit = 'Truck';
                    elseif($row['unit'] == 30) $parts_unit = 'Unit';
                    elseif($row['unit'] == 31) $parts_unit = 'Yeard';
                    elseif($row['unit'] == 32) $parts_unit = '(Unit Unknown)';
                    elseif($row['unit'] == 33) $parts_unit = 'SFT';
                    elseif($row['unit'] == 34) $parts_unit = 'RFT';
                    elseif($row['unit'] == 35) $parts_unit = 'CFT';

                    $required_for = '';

                    if($row['required_for'] == 1)
                        $required_for = 'BCP-CCM';
                    elseif($row['required_for'] == 2)
                        $required_for = 'BCP-Furnace';
                    elseif($row['required_for'] == 3)
                        $required_for = 'Concast-CCM';
                    elseif($row['required_for'] == 4)
                        $required_for = 'Concast-Furnace';
                    elseif($row['required_for'] == 5)
                        $required_for = 'HRM';
                    elseif($row['required_for'] == 6)
                        $required_for = 'HRM Unit-2';
                    elseif($row['required_for'] == 7)
                        $required_for = 'Lal Masjid';
                    elseif($row['required_for'] == 8)
                        $required_for = 'Sonargaon';
                    elseif($row['required_for'] == 9)
                        $required_for = 'General';

                    $data[] = [
                        'inventory_history_id' => $row['inventory_history_id'],
                        'source' => $row['source'],
                        'required_for' => $required_for,
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $parts_unit,
                        'parts_rate' => $row['parts_rate'],
                        'parts_avg_rate' => $row['parts_avg_rate'],
                        'opening_qty' => $row['opening_qty'],
                        'opening_value' => $row['opening_value'],
                        'received_qty' => $row['received_qty'],
                        'received_value' => $row['received_value'],
                        'issued_qty' => $row['issued_qty'],
                        'issued_value' => $row['issued_value'],
                        'closing_qty' => $row['closing_qty'],
                        'closing_value' => $row['closing_value'],
                        'history_date' => $row['history_date'],
                        'inventory_history_created' => date('d M, Y \a\t h:i a', $row['inventory_history_created']),
                        'user_fullname' => $row['user_fullname']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH INVENTORY PARTS DATE VALIDITY
        function fetch_parts_date_validity($parts_id){
            $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT history_date FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' ORDER BY history_date DESC LIMIT 1"));

            $start_date = '';
            $history_date = '';

            if(isset($inventory_history_info['history_date'])){
                $history_date = $inventory_history_info['history_date'];
                $date_diff = time() - strtotime($history_date);
                $days_num = round($date_diff / (60 * 60 * 24));

                if($days_num > 29)
                    $start_date = -29 . 'd';
                else
                    $start_date = -$days_num . 'd';
            } else{
                $history_date = '';
            }

            $data[] = [
                'start_date' => $start_date,
                'latest_date' => $history_date
            ];

            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }

        // FETCH ALL INVENTORY PARTS INFO
        function fetch_all_parts_info($type, $required_for){
            if($type == 1){
                $inventory_history_query = mysqli_query($this->conn, "SELECT *, i.parts_avg_rate AS p_avg FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source > 1 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) ORDER BY h.history_date");
            } else{
                $inventory_history_query = mysqli_query($this->conn, "SELECT *, i.parts_avg_rate AS p_avg FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source = 1 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) ORDER BY h.history_date");
            }

            if(mysqli_num_rows($inventory_history_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'parts_qty' => $row['parts_qty'],
                        'parts_avg_rate' => $row['p_avg']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS DATE
        function fetch_all_parts_date($type, $required_for){
            if($type == 1){
                $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source > 1 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) ORDER BY h.history_date");
            } else{
                $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source = 1 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) ORDER BY h.history_date");
            }

            if(mysqli_num_rows($inventory_history_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    if($type == 1){
                        $qty = $row['issued_qty'];
                    } else{
                        $qty = $row['received_qty'];
                    }

                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'issued_qty' => $qty,
                        'history_date' => date('Y-m-d', strtotime($row['history_date']))
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS INFO
        function fetch_filtered_parts_info($type, $parts, $date_range, $required_for){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($type == 1){
                if($parts == 0){
                    $inventory_history_query = mysqli_query($this->conn, "SELECT *, i.parts_avg_rate AS p_avg FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source > 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                } else{
                    $inventory_history_query = mysqli_query($this->conn, "SELECT *, i.parts_avg_rate AS p_avg FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.parts_id = '$parts' AND h.source > 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                }
            } else{
                if($parts == 0){
                    $inventory_history_query = mysqli_query($this->conn, "SELECT *, i.parts_avg_rate AS p_avg FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source = 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                } else{
                    $inventory_history_query = mysqli_query($this->conn, "SELECT *, i.parts_avg_rate AS p_avg FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.parts_id = '$parts' AND h.source = 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                }
            }

            if(mysqli_num_rows($inventory_history_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit'],
                        'parts_qty' => $row['parts_qty'],
                        'parts_avg_rate' => $row['p_avg']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS DATE
        function fetch_filtered_parts_date($type, $parts, $date_range, $required_for){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($type == 1){
                if($parts == 0){
                    $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source > 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                } else{
                    $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.parts_id = '$parts' AND h.source > 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                }
            } else{
                if($parts == 0){
                    $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.source = 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date");
                } else{
                   $inventory_history_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_inv_history h ON h.parts_id = i.parts_id INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE h.required_for = '$required_for' AND h.parts_id = '$parts' AND h.source = 1 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' ORDER BY h.history_date"); 
                }
            }

            if(mysqli_num_rows($inventory_history_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    if($type == 1){
                        $qty = $row['issued_qty'];
                    } else{
                        $qty = $row['received_qty'];
                    }

                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'issued_qty' => $qty,
                        'history_date' => date('Y-m-d', strtotime($row['history_date']))
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS STOCK INFO
        function fetch_all_parts_stock_info(){
            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS STOCK DATE
        function fetch_all_parts_stock_date(){
            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];
                    $parts_name = $row['parts_name'];
                    $parts_unit = $row['unit'];

                    $inventory_history_query = mysqli_query($this->conn, "SELECT SUM(issued_qty) AS tot_issued_qty, SUM(received_qty) AS tot_received_qty, history_date FROM rrmsteel_inv_history WHERE source > 0 AND parts_id = '$parts_id' AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) GROUP BY history_date ORDER BY history_date");

                    if(mysqli_num_rows($inventory_history_query) > 0){
                        $flag = 1;

                        while($row2 = mysqli_fetch_assoc($inventory_history_query)){
                            $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.closing_qty, i.closing_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE MONTH(history_date) < MONTH(CURDATE()) GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND MONTH(i.history_date) < MONTH(CURDATE()) ORDER BY i.history_date DESC, i.inventory_history_created DESC LIMIT 1"));

                            if(isset($inventory_history_info)){
                                $opening_qty = $inventory_history_info['closing_qty'];
                            } else{
                                $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.opening_qty, i.opening_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE MONTH(history_date) = MONTH(CURDATE()) GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND MONTH(i.history_date) = MONTH(CURDATE()) ORDER BY i.history_date, i.inventory_history_created LIMIT 1"));

                                if(isset($inventory_history_info2)){
                                    $opening_qty = $inventory_history_info2['opening_qty'];
                                } else{
                                    $opening_qty = 0;
                                }
                            }

                            $data[] = [
                                'parts_id' => $parts_id,
                                'parts_name' => $parts_name,
                                'parts_unit' => $parts_unit,
                                'opening_qty' => $opening_qty,
                                'received_qty' => $row2['tot_received_qty'],
                                'issued_qty' => $row2['tot_issued_qty'],
                                'history_date' => $row2['history_date']
                            ];
                        }
                    }
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS STOCK DATE 2
        function fetch_all_parts_stock_date_2($type){
            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];
                    $parts_name = $row['parts_name'];
                    $parts_unit = $row['unit'];
                    $parts_avg_rate = $row['parts_avg_rate'];

                    if($type == 1){
                        $inventory_history_query = mysqli_query($this->conn, "SELECT history_date, SUM(issued_qty) AS tot_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND source > 1 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) GROUP BY history_date ORDER BY history_date");
                    } else{
                        $inventory_history_query = mysqli_query($this->conn, "SELECT history_date, SUM(received_qty) AS tot_qty FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND source = 1 AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) GROUP BY history_date ORDER BY history_date");
                    }
                    
                    if(mysqli_num_rows($inventory_history_query) > 0){
                        $flag = 1;

                        while($row2 = mysqli_fetch_assoc($inventory_history_query)){
                            $data[] = [
                                'parts_id' => $parts_id,
                                'parts_name' => $parts_name,
                                'parts_unit' => $parts_unit,
                                'avg_parts_rate' => $parts_avg_rate,
                                'issued_qty' => $row2['tot_qty'],
                                'parts_val' => $parts_avg_rate * $row2['tot_qty'],
                                'history_date' => date('Y-m-d', strtotime($row2['history_date']))
                            ];
                        }
                    }
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS STOCK INFO
        function fetch_filtered_parts_stock_info($parts){
            if($parts == 0){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");
            } else{
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.parts_id = '$parts'");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS STOCK DATE
        function fetch_filtered_parts_stock_date($parts, $date_range){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($parts == 0){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");
            } else{
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.parts_id = '$parts'");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];
                    $parts_name = $row['parts_name'];
                    $parts_unit = $row['unit'];

                    $inventory_history_query = mysqli_query($this->conn, "SELECT SUM(issued_qty) AS tot_issued_qty, SUM(received_qty) AS tot_received_qty, history_date FROM rrmsteel_inv_history WHERE source > 0 AND parts_id = '$parts_id' AND history_date >= '$start_date' AND history_date <= '$end_date' GROUP BY history_date ORDER BY history_date");

                    if(mysqli_num_rows($inventory_history_query) > 0){
                        $flag = 1;

                        while($row2 = mysqli_fetch_assoc($inventory_history_query)){
                            $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.closing_qty, i.closing_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE history_date < '$start_date' GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND i.history_date < '$start_date' ORDER BY i.history_date DESC, i.inventory_history_created DESC LIMIT 1"));

                            if(isset($inventory_history_info)){
                                $opening_qty = $inventory_history_info['closing_qty'];
                            } else{
                                $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.opening_qty, i.opening_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE history_date = '$start_date' GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND i.history_date = '$start_date' ORDER BY i.history_date, i.inventory_history_created LIMIT 1"));

                                if(isset($inventory_history_info2)){
                                    $opening_qty = $inventory_history_info2['opening_qty'];
                                } else{
                                    $opening_qty = 0;
                                }
                            }
                            
                            $data[] = [
                                'parts_id' => $parts_id,
                                'parts_name' => $parts_name,
                                'parts_unit' => $parts_unit,
                                'opening_qty' => $opening_qty,
                                'received_qty' => $row2['tot_received_qty'],
                                'issued_qty' => $row2['tot_issued_qty'],
                                'history_date' => $row2['history_date']
                            ];
                        }
                    }
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS STOCK DATE 2
        function fetch_filtered_parts_stock_date_2($type, $parts, $date_range){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($parts == 0){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");
            } else{
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.parts_id = '$parts'");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];
                    $parts_name = $row['parts_name'];
                    $parts_unit = $row['unit'];

                    if($type == 1){
                        $inventory_history_query = mysqli_query($this->conn, "SELECT AVG(parts_avg_rate) AS avg_rate, SUM(issued_qty) AS tot_qty, history_date FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND source > 1  AND history_date >= '$start_date' AND history_date <= '$end_date' GROUP BY history_date ORDER BY history_date");
                    } else{
                        $inventory_history_query = mysqli_query($this->conn, "SELECT AVG(parts_avg_rate) AS avg_rate, SUM(received_qty) AS tot_qty, history_date FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND source = 1  AND history_date >= '$start_date' AND history_date <= '$end_date' GROUP BY history_date ORDER BY history_date");
                    }
                    
                    if(mysqli_num_rows($inventory_history_query) > 0){
                        $flag = 1;

                        while($row2 = mysqli_fetch_assoc($inventory_history_query)){
                            $data[] = [
                                'parts_id' => $parts_id,
                                'parts_name' => $parts_name,
                                'parts_unit' => $parts_unit,
                                'avg_parts_rate' => $row2['avg_rate'],
                                'issued_qty' => $row2['tot_qty'],
                                'parts_val' => $row2['avg_rate'] * $row2['tot_qty'],
                                'history_date' => date('Y-m-d', strtotime($row2['history_date']))
                            ];
                        }
                    }
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS OVERALL INFO
        function fetch_all_parts_overall_info(){
            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_nickname' => $row['parts_nickname'],
                        'parts_category' => $row['category'],
                        'parts_subcategory' => $row['subcategory'],
                        'parts_unit' => $row['unit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS OVERALL DETAILS
        function fetch_all_parts_overall_details(){
            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];
                    $parts_rate = $row['parts_rate'];
                    $parts_avg_rate = $row['parts_avg_rate'];
                    $flag = 1;

                    $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.closing_qty, i.closing_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE MONTH(history_date) < MONTH(CURDATE()) GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND MONTH(i.history_date) < MONTH(CURDATE()) ORDER BY i.history_date DESC, i.inventory_history_created DESC LIMIT 1"));

                    if(isset($inventory_history_info)){
                        $opening_qty = $inventory_history_info['closing_qty'];
                        $opening_val = $inventory_history_info['closing_value'];
                    } else{
                        $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.opening_qty, i.opening_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE MONTH(history_date) = MONTH(CURDATE()) GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND MONTH(i.history_date) = MONTH(CURDATE()) ORDER BY i.history_date, i.inventory_history_created LIMIT 1"));

                        if(isset($inventory_history_info2)){
                            $opening_qty = $inventory_history_info2['opening_qty'];
                            $opening_val = $inventory_history_info2['opening_value'];
                        } else{
                            $opening_qty = 0;
                            $opening_val = 0;
                        }
                    }

                    $inventory_history_query = mysqli_query($this->conn, "SELECT received_qty, received_value, issued_qty, issued_value FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE())");

                    $received_qty = 0;
                    $received_val = 0;
                    $issued_qty = 0;
                    $issued_val = 0;

                    while($row = mysqli_fetch_assoc($inventory_history_query)){
                        $received_qty += $row['received_qty'];
                        $received_val += $row['received_value'];
                        $issued_qty += $row['issued_qty'];
                        $issued_val += $row['issued_value'];
                    }

                    $closing_qty = $opening_qty + $received_qty - $issued_qty;
                    $closing_val = $opening_val + $received_val - $issued_val;
                    
                    $data[] = [
                        'parts_id' => $parts_id,
                        'opening_qty' => $opening_qty,
                        'opening_value' => $opening_val,
                        'parts_rate' => $parts_rate,
                        'parts_avg_rate' => $parts_avg_rate,
                        'received_qty' => $received_qty,
                        'received_value' => $received_val,
                        'issued_qty' => $issued_qty,
                        'issued_value' => $issued_val,
                        'closing_qty' => $closing_qty,
                        'closing_value' => $closing_val
                    ];
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS OVERALL INFO
        function fetch_filtered_parts_overall_info($parts){
            if($parts){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.parts_id = '$parts'");
            } else{
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_nickname' => $row['parts_nickname'],
                        'parts_category' => $row['category'],
                        'parts_subcategory' => $row['subcategory'],
                        'parts_unit' => $row['unit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS OVERALL DETAILS
        function fetch_filtered_parts_overall_details($parts, $date_range){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($parts == 0){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");
            } else{
                $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.parts_id = '$parts'");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];
                    $parts_name = $row['parts_name'];
                    $parts_nickname = $row['parts_nickname'];
                    $parts_category = $row['category'];
                    $parts_subcategory = $row['subcategory'];
                    $parts_unit = $row['unit'];
                    $flag = 1;

                    $inventory_history_info = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.parts_rate, i.closing_qty, i.closing_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE history_date < '$start_date' GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND i.history_date < '$start_date' ORDER BY i.history_date DESC, i.inventory_history_created DESC LIMIT 1"));

                    if(isset($inventory_history_info)){
                        $opening_qty = $inventory_history_info['closing_qty'];
                        $opening_val = $inventory_history_info['closing_value'];
                        $parts_rate = $inventory_history_info['parts_rate'];
                    } else{
                        $inventory_history_info2 = mysqli_fetch_assoc(mysqli_query($this->conn, "SELECT i.parts_id, i.opening_qty, i.opening_value, i.parts_rate FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE history_date = '$start_date' GROUP BY parts_id) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE i.parts_id = '$parts_id' AND i.history_date = '$start_date' ORDER BY i.history_date, i.inventory_history_created LIMIT 1"));

                        if(isset($inventory_history_info2)){
                            $opening_qty = $inventory_history_info2['opening_qty'];
                            $opening_val = $inventory_history_info2['opening_value'];
                            $parts_rate = $inventory_history_info2['parts_rate'];
                        } else{
                            $opening_qty = 0;
                            $opening_val = 0;
                            $parts_rate = 0;
                        }
                    }

                    $inventory_history_query = mysqli_query($this->conn, "SELECT received_qty, received_value, issued_qty, issued_value FROM rrmsteel_inv_history WHERE parts_id = '$parts_id' AND history_date >= '$start_date' AND history_date <= '$end_date'");

                    $received_qty = 0;
                    $received_val = 0;
                    $issued_qty = 0;
                    $issued_val = 0;

                    while($row = mysqli_fetch_assoc($inventory_history_query)){
                        $received_qty += $row['received_qty'];
                        $received_val += $row['received_value'];
                        $issued_qty += $row['issued_qty'];
                        $issued_val += $row['issued_value'];
                    }

                    $parts_avg_rate = ($opening_qty + $received_qty) > 0 ? (($opening_val + $received_val) / ($opening_qty + $received_qty)) : 0;

                    $closing_qty = $opening_qty + $received_qty - $issued_qty;
                    $closing_val = $opening_val + $received_val - $issued_val;
                    
                    $data[] = [
                        'parts_id' => $parts_id,
                        'parts_name' => $parts_name,
                        'parts_nickname' => $parts_nickname,
                        'parts_category' => $parts_category,
                        'parts_subcategory' => $parts_subcategory,
                        'parts_unit' => $parts_unit,
                        'opening_qty' => $opening_qty,
                        'opening_value' => $opening_val,
                        'parts_rate' => $parts_rate,
                        'parts_avg_rate' => $parts_avg_rate,
                        'received_qty' => $received_qty,
                        'received_value' => $received_val,
                        'issued_qty' => $issued_qty,
                        'issued_value' => $issued_val,
                        'closing_qty' => $closing_qty,
                        'closing_value' => $closing_val
                    ];
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH INVENTORY SPECIFIC PARTS INFO
        function fetch_specific_parts_info($required_for){
            if($required_for == 14){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 15){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 16){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 17){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 18){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 19){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 20){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 21){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 22){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 23){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 24){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 25){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 26){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 27){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 28){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 29){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 30){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 31){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH INVENTORY SPECIFIC PARTS DETAILS
        function fetch_specific_parts_details($required_for){
            if($required_for == 14){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 15){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 16){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 17){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 18){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 19){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 20){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 21){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 22){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 23){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 24){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 25){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 26){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 27){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND h.required_for = 5 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 28){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 29){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 30){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            } elseif($required_for == 31){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 6 AND MONTH(h.history_date) = MONTH(CURDATE()) AND YEAR(h.history_date) = YEAR(CURDATE()) GROUP BY p.parts_id");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $flag = 1;

                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'issued_qty' => $row['tot_issued'],
                        'parts_avg_rate' => $row['parts_avg_rate'],
                        'parts_val' => $row['parts_avg_rate'] * $row['tot_issued']
                    ];
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY SPECIFIC PARTS INFO
        function fetch_filtered_specific_parts_info($date_range, $required_for){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($required_for == 14){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 15){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 16){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 17){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 18){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 19){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 20){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 21){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 22){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 23){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 24){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 25){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 26){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 27){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 28){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 29){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 30){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 31){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, p.parts_name, p.unit FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'parts_name' => $row['parts_name'],
                        'parts_unit' => $row['unit']
                    ];
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY SPECIFIC PARTS DETAILS
        function fetch_filtered_specific_parts_details($date_range, $required_for){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            if($required_for == 14){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 15){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 16){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 17){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 18){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 1 OR h.required_for = 2) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 19){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 20){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 21){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 22){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 23){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND (h.required_for = 3 OR h.required_for = 4) AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 24){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 25){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 26){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 27){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 4 AND h.source > 1 AND h.required_for = 5 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 28){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 3 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 29){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 2 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 30){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 1 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            } elseif($required_for == 31){
                $inventory_summary_query = mysqli_query($this->conn, "SELECT p.parts_id, i.parts_avg_rate, SUM(h.issued_qty) AS tot_issued FROM rrmsteel_parts p INNER JOIN rrmsteel_inv_summary i ON i.parts_id = p.parts_id INNER JOIN rrmsteel_inv_history h ON h.parts_id = p.parts_id WHERE p.apply_group = 7 AND h.source > 1 AND h.required_for = 6 AND h.history_date >= '$start_date' AND h.history_date <= '$end_date' GROUP BY p.parts_id");
            }

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $flag = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $flag = 1;

                    $data[] = [
                        'parts_id' => $row['parts_id'],
                        'issued_qty' => $row['tot_issued'],
                        'parts_avg_rate' => $row['parts_avg_rate'],
                        'parts_val' => $row['parts_avg_rate'] * $row['tot_issued']
                    ];
                }

                if($flag == 1){
                    $reply = array(
                        'Type' => 'success',
                        'Reply' => $data
                    );

                    exit(json_encode($reply));
                } else{
                    exit(json_encode(array(
                        'Type' => 'error',
                        'Reply' => 'No history found !'
                    )));
                }
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ALL INVENTORY PARTS SUMMARY DETAILS
        function fetch_all_parts_summary_details($curr_mon_status){
            $required_for_val_arr = [];

            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $tot_opening_val1 = 0;
                $tot_opening_val2 = 0;
                $tot_opening_val3 = 0;
                $tot_opening_val4 = 0;
                $tot_opening_val5 = 0;
                $tot_opening_val6 = 0;
                $tot_opening_val7 = 0;

                $tot_received_val1 = 0;
                $tot_received_val2 = 0;
                $tot_received_val3 = 0;
                $tot_received_val4 = 0;
                $tot_received_val5 = 0;
                $tot_received_val6 = 0;
                $tot_received_val7 = 0;

                $tot_issued_val1 = 0;
                $tot_issued_val2 = 0;
                $tot_issued_val3 = 0;
                $tot_issued_val4 = 0;
                $tot_issued_val5 = 0;
                $tot_issued_val6 = 0;
                $tot_issued_val7 = 0;

                $tot_closing_val1 = 0;
                $tot_closing_val2 = 0;
                $tot_closing_val3 = 0;
                $tot_closing_val4 = 0;
                $tot_closing_val5 = 0;
                $tot_closing_val6 = 0;
                $tot_closing_val7 = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];

                    $inventory_history_query = mysqli_query($this->conn, "SELECT i.parts_id, i.required_for, i.closing_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE source > 0 AND MONTH(history_date) < MONTH(CURDATE()) GROUP BY parts_id, required_for) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE source > 0 AND MONTH(i.history_date) < MONTH(CURDATE()) AND i.parts_id = '$parts_id' ORDER BY i.required_for, i.history_date DESC, i.inventory_history_created DESC");

                    if(mysqli_num_rows($inventory_history_query) > 0){
                        $required_for = 0;

                        while($row = mysqli_fetch_assoc($inventory_history_query)){
                            if($required_for != $row['required_for']){
                                switch($row['required_for']){
                                    case 1:
                                        $tot_opening_val1 += $row['closing_value'];
                                        break;
                                    case 2:
                                        $tot_opening_val1 += $row['closing_value'];
                                        break;
                                    case 3:
                                        $tot_opening_val2 += $row['closing_value'];
                                        break;
                                    case 4:
                                        $tot_opening_val2 += $row['closing_value'];
                                        break;
                                    case 5:
                                        $tot_opening_val3 += $row['closing_value'];
                                        break;
                                    case 6:
                                        $tot_opening_val4 += $row['closing_value'];
                                        break;
                                    case 7:
                                        $tot_opening_val5 += $row['closing_value'];
                                        break;
                                    case 8:
                                        $tot_opening_val6 += $row['closing_value'];
                                        break;
                                    case 9:
                                        $tot_opening_val7 += $row['closing_value'];
                                        break;
                                }
                                
                                $required_for = $row['required_for'];
                            } else{
                                continue;
                            }
                        }
                    } else{
                        $inventory_history_query2 = mysqli_query($this->conn, "SELECT i.parts_id, i.required_for, i.opening_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history WHERE source > 0 AND MONTH(history_date) = MONTH(CURDATE()) GROUP BY parts_id, required_for) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE source > 0 AND MONTH(i.history_date) = MONTH(CURDATE()) AND i.parts_id = '$parts_id' ORDER BY i.required_for, i.history_date DESC, i.inventory_history_created DESC");

                        if(mysqli_num_rows($inventory_history_query2) > 0){
                            $required_for = 0;

                            while($row = mysqli_fetch_assoc($inventory_history_query2)){
                                if($required_for != $row['required_for']){
                                    switch($row['required_for']){
                                        case 1:
                                            $tot_opening_val1 += $row['opening_value'];
                                            break;
                                        case 2:
                                            $tot_opening_val1 += $row['opening_value'];
                                            break;
                                        case 3:
                                            $tot_opening_val2 += $row['opening_value'];
                                            break;
                                        case 4:
                                            $tot_opening_val2 += $row['opening_value'];
                                            break;
                                        case 5:
                                            $tot_opening_val3 += $row['opening_value'];
                                            break;
                                        case 6:
                                            $tot_opening_val4 += $row['opening_value'];
                                            break;
                                        case 7:
                                            $tot_opening_val5 += $row['opening_value'];
                                            break;
                                        case 8:
                                            $tot_opening_val6 += $row['opening_value'];
                                            break;
                                        case 9:
                                            $tot_opening_val7 += $row['opening_value'];
                                            break;
                                    }
                                    
                                    $required_for = $row['required_for'];
                                } else{
                                    continue;
                                }
                            }
                        } else{
                            $tot_opening_val1 += 0;
                            $tot_opening_val2 += 0;
                            $tot_opening_val3 += 0;
                            $tot_opening_val4 += 0;
                            $tot_opening_val5 += 0;
                            $tot_opening_val6 += 0;
                            $tot_opening_val7 += 0;
                        }
                    }

                    $inventory_history_query = mysqli_query($this->conn, "SELECT required_for, SUM(received_value) AS received_value, SUM(issued_value) AS issued_value FROM rrmsteel_inv_history WHERE source > 0 AND parts_id = '$parts_id' AND MONTH(history_date) = MONTH(CURDATE()) AND YEAR(history_date) = YEAR(CURDATE()) GROUP BY required_for ORDER BY required_for");

                    while($row = mysqli_fetch_assoc($inventory_history_query)){
                        switch($row['required_for']){
                            case 1:
                                $tot_received_val1 += $row['received_value'];
                                $tot_issued_val1 += $row['issued_value'];
                                break;
                            case 2:
                                $tot_received_val1 += $row['received_value'];
                                $tot_issued_val1 += $row['issued_value'];
                                break;
                            case 3:
                                $tot_received_val2 += $row['received_value'];
                                $tot_issued_val2 += $row['issued_value'];
                                break;
                            case 4:
                                $tot_received_val2 += $row['received_value'];
                                $tot_issued_val2 += $row['issued_value'];
                                break;
                            case 5:
                                $tot_received_val3 += $row['received_value'];
                                $tot_issued_val3 += $row['issued_value'];
                                break;
                            case 6:
                                $tot_received_val4 += $row['received_value'];
                                $tot_issued_val4 += $row['issued_value'];
                                break;
                            case 7:
                                $tot_received_val5 += $row['received_value'];
                                $tot_issued_val5 += $row['issued_value'];
                                break;
                            case 8:
                                $tot_received_val6 += $row['received_value'];
                                $tot_issued_val6 += $row['issued_value'];
                                break;
                            case 9:
                                $tot_received_val7 += $row['received_value'];
                                $tot_issued_val7 += $row['issued_value'];
                                break;
                        }
                    }
                }

                $tot_closing_val1 = $tot_opening_val1 + $tot_received_val1 - $tot_issued_val1;
                $tot_closing_val2 = $tot_opening_val2 + $tot_received_val2 - $tot_issued_val2;
                $tot_closing_val3 = $tot_opening_val3 + $tot_received_val3 - $tot_issued_val3;
                $tot_closing_val4 = $tot_opening_val4 + $tot_received_val4 - $tot_issued_val4;
                $tot_closing_val5 = $tot_opening_val5 + $tot_received_val5 - $tot_issued_val5;
                $tot_closing_val6 = $tot_opening_val6 + $tot_received_val6 - $tot_issued_val6;
                $tot_closing_val7 = $tot_opening_val7 + $tot_received_val7 - $tot_issued_val7;

                for($i=1; $i<=7; $i++){
                    switch($i){
                        case 1:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val1,
                                'tot_received_val' => $tot_received_val1,
                                'tot_issued_val' => $tot_issued_val1,
                                'tot_closing_val' => $tot_closing_val1
                            ]);
                            break;
                        case 2:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val2,
                                'tot_received_val' => $tot_received_val2,
                                'tot_issued_val' => $tot_issued_val2,
                                'tot_closing_val' => $tot_closing_val2
                            ]);
                            break;
                        case 3:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val3,
                                'tot_received_val' => $tot_received_val3,
                                'tot_issued_val' => $tot_issued_val3,
                                'tot_closing_val' => $tot_closing_val3
                            ]);
                            break;
                        case 4:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val4,
                                'tot_received_val' => $tot_received_val4,
                                'tot_issued_val' => $tot_issued_val4,
                                'tot_closing_val' => $tot_closing_val4
                            ]);
                            break;
                        case 5:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val5,
                                'tot_received_val' => $tot_received_val5,
                                'tot_issued_val' => $tot_issued_val5,
                                'tot_closing_val' => $tot_closing_val5
                            ]);
                            break;
                        case 6:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val6,
                                'tot_received_val' => $tot_received_val6,
                                'tot_issued_val' => $tot_issued_val6,
                                'tot_closing_val' => $tot_closing_val6
                            ]);
                            break;
                        case 7:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val7,
                                'tot_received_val' => $tot_received_val7,
                                'tot_issued_val' => $tot_issued_val7,
                                'tot_closing_val' => $tot_closing_val7
                            ]);
                            break;
                    }
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $required_for_val_arr
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH FILTERED INVENTORY PARTS SUMMARY DETAILS
        function fetch_filtered_parts_summary_details($date_range){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            $required_for_val_arr = [];

            $inventory_summary_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_inv_summary i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id");

            if(mysqli_num_rows($inventory_summary_query) > 0){
                $tot_opening_val1 = 0;
                $tot_opening_val2 = 0;
                $tot_opening_val3 = 0;
                $tot_opening_val4 = 0;
                $tot_opening_val5 = 0;
                $tot_opening_val6 = 0;
                $tot_opening_val7 = 0;

                $tot_received_val1 = 0;
                $tot_received_val2 = 0;
                $tot_received_val3 = 0;
                $tot_received_val4 = 0;
                $tot_received_val5 = 0;
                $tot_received_val6 = 0;
                $tot_received_val7 = 0;

                $tot_issued_val1 = 0;
                $tot_issued_val2 = 0;
                $tot_issued_val3 = 0;
                $tot_issued_val4 = 0;
                $tot_issued_val5 = 0;
                $tot_issued_val6 = 0;
                $tot_issued_val7 = 0;

                $tot_closing_val1 = 0;
                $tot_closing_val2 = 0;
                $tot_closing_val3 = 0;
                $tot_closing_val4 = 0;
                $tot_closing_val5 = 0;
                $tot_closing_val6 = 0;
                $tot_closing_val7 = 0;

                while($row = mysqli_fetch_assoc($inventory_summary_query)){
                    $parts_id = $row['parts_id'];

                    $inventory_history_query = mysqli_query($this->conn, "SELECT i.parts_id, i.required_for, i.closing_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history GROUP BY parts_id, required_for) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE source > 0 AND i.history_date < '$start_date' AND i.parts_id = '$parts_id' ORDER BY i.required_for, i.history_date DESC, i.inventory_history_created DESC");

                    if(mysqli_num_rows($inventory_history_query) > 0){
                        $required_for = 0;

                        while($row = mysqli_fetch_assoc($inventory_history_query)){
                            if($required_for != $row['required_for']){
                                switch($row['required_for']){
                                    case 1:
                                        $tot_opening_val1 += $row['closing_value'];
                                        break;
                                    case 2:
                                        $tot_opening_val1 += $row['closing_value'];
                                        break;
                                    case 3:
                                        $tot_opening_val2 += $row['closing_value'];
                                        break;
                                    case 4:
                                        $tot_opening_val2 += $row['closing_value'];
                                        break;
                                    case 5:
                                        $tot_opening_val3 += $row['closing_value'];
                                        break;
                                    case 6:
                                        $tot_opening_val4 += $row['closing_value'];
                                        break;
                                    case 7:
                                        $tot_opening_val5 += $row['closing_value'];
                                        break;
                                    case 8:
                                        $tot_opening_val6 += $row['closing_value'];
                                        break;
                                    case 9:
                                        $tot_opening_val7 += $row['closing_value'];
                                        break;
                                }
                                
                                $required_for = $row['required_for'];
                            } else{
                                continue;
                            }
                        }
                    } else{
                        $inventory_history_query2 = mysqli_query($this->conn, "SELECT i.parts_id, i.required_for, i.opening_value FROM (SELECT parts_id, MAX(history_date) AS max_date FROM rrmsteel_inv_history GROUP BY parts_id, required_for) i2 INNER JOIN rrmsteel_inv_history i ON i.parts_id = i2.parts_id AND i.history_date = i2.max_date WHERE source > 0 AND i.history_date = '$start_date' AND i.parts_id = '$parts_id' ORDER BY i.required_for, i.history_date DESC, i.inventory_history_created DESC");

                        if(mysqli_num_rows($inventory_history_query2) > 0){
                            $required_for = 0;

                            while($row = mysqli_fetch_assoc($inventory_history_query2)){
                                if($required_for != $row['required_for']){
                                    switch($row['required_for']){
                                        case 1:
                                            $tot_opening_val1 += $row['opening_value'];
                                            break;
                                        case 2:
                                            $tot_opening_val1 += $row['opening_value'];
                                            break;
                                        case 3:
                                            $tot_opening_val2 += $row['opening_value'];
                                            break;
                                        case 4:
                                            $tot_opening_val2 += $row['opening_value'];
                                            break;
                                        case 5:
                                            $tot_opening_val3 += $row['opening_value'];
                                            break;
                                        case 6:
                                            $tot_opening_val4 += $row['opening_value'];
                                            break;
                                        case 7:
                                            $tot_opening_val5 += $row['opening_value'];
                                            break;
                                        case 8:
                                            $tot_opening_val6 += $row['opening_value'];
                                            break;
                                        case 9:
                                            $tot_opening_val7 += $row['opening_value'];
                                            break;
                                    }
                                    
                                    $required_for = $row['required_for'];
                                } else{
                                    continue;
                                }
                            }
                        } else{
                            $tot_opening_val1 += 0;
                            $tot_opening_val2 += 0;
                            $tot_opening_val3 += 0;
                            $tot_opening_val4 += 0;
                            $tot_opening_val5 += 0;
                            $tot_opening_val6 += 0;
                            $tot_opening_val7 += 0;
                        }
                    }

                    $inventory_history_query = mysqli_query($this->conn, "SELECT required_for, SUM(received_value) AS received_value, SUM(issued_value) AS issued_value FROM rrmsteel_inv_history WHERE source > 0 AND parts_id = '$parts_id' AND history_date >= '$start_date' AND history_date <= '$end_date' GROUP BY required_for ORDER BY required_for");

                    while($row = mysqli_fetch_assoc($inventory_history_query)){
                        switch($row['required_for']){
                            case 1:
                                $tot_received_val1 += $row['received_value'];
                                $tot_issued_val1 += $row['issued_value'];
                                break;
                            case 2:
                                $tot_received_val1 += $row['received_value'];
                                $tot_issued_val1 += $row['issued_value'];
                                break;
                            case 3:
                                $tot_received_val2 += $row['received_value'];
                                $tot_issued_val2 += $row['issued_value'];
                                break;
                            case 4:
                                $tot_received_val2 += $row['received_value'];
                                $tot_issued_val2 += $row['issued_value'];
                                break;
                            case 5:
                                $tot_received_val3 += $row['received_value'];
                                $tot_issued_val3 += $row['issued_value'];
                                break;
                            case 6:
                                $tot_received_val4 += $row['received_value'];
                                $tot_issued_val4 += $row['issued_value'];
                                break;
                            case 7:
                                $tot_received_val5 += $row['received_value'];
                                $tot_issued_val5 += $row['issued_value'];
                                break;
                            case 8:
                                $tot_received_val6 += $row['received_value'];
                                $tot_issued_val6 += $row['issued_value'];
                                break;
                            case 9:
                                $tot_received_val7 += $row['received_value'];
                                $tot_issued_val7 += $row['issued_value'];
                                break;
                        }
                    }
                }

                $tot_closing_val1 = $tot_opening_val1 + $tot_received_val1 - $tot_issued_val1;
                $tot_closing_val2 = $tot_opening_val2 + $tot_received_val2 - $tot_issued_val2;
                $tot_closing_val3 = $tot_opening_val3 + $tot_received_val3 - $tot_issued_val3;
                $tot_closing_val4 = $tot_opening_val4 + $tot_received_val4 - $tot_issued_val4;
                $tot_closing_val5 = $tot_opening_val5 + $tot_received_val5 - $tot_issued_val5;
                $tot_closing_val6 = $tot_opening_val6 + $tot_received_val6 - $tot_issued_val6;
                $tot_closing_val7 = $tot_opening_val7 + $tot_received_val7 - $tot_issued_val7;

                for($i=1; $i<=7; $i++){
                    switch($i){
                        case 1:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val1,
                                'tot_received_val' => $tot_received_val1,
                                'tot_issued_val' => $tot_issued_val1,
                                'tot_closing_val' => $tot_closing_val1
                            ]);
                            break;
                        case 2:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val2,
                                'tot_received_val' => $tot_received_val2,
                                'tot_issued_val' => $tot_issued_val2,
                                'tot_closing_val' => $tot_closing_val2
                            ]);
                            break;
                        case 3:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val3,
                                'tot_received_val' => $tot_received_val3,
                                'tot_issued_val' => $tot_issued_val3,
                                'tot_closing_val' => $tot_closing_val3
                            ]);
                            break;
                        case 4:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val4,
                                'tot_received_val' => $tot_received_val4,
                                'tot_issued_val' => $tot_issued_val4,
                                'tot_closing_val' => $tot_closing_val4
                            ]);
                            break;
                        case 5:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val5,
                                'tot_received_val' => $tot_received_val5,
                                'tot_issued_val' => $tot_issued_val5,
                                'tot_closing_val' => $tot_closing_val5
                            ]);
                            break;
                        case 6:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val6,
                                'tot_received_val' => $tot_received_val6,
                                'tot_issued_val' => $tot_issued_val6,
                                'tot_closing_val' => $tot_closing_val6
                            ]);
                            break;
                        case 7:
                            array_push($required_for_val_arr, (object)[
                                'tot_opening_val' => $tot_opening_val7,
                                'tot_received_val' => $tot_received_val7,
                                'tot_issued_val' => $tot_issued_val7,
                                'tot_closing_val' => $tot_closing_val7
                            ]);
                            break;
                    }
                }

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $required_for_val_arr
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }

        // FETCH ISSUED INVENTORY PARTS SUMMARY DETAILS
        function fetch_issued_parts_summary_details(){
            $inventory_history_query = mysqli_query($this->conn, "SELECT i.required_for, SUM(CASE WHEN p.apply_group = 1 THEN i.issued_value ELSE 0 END) AS tot_mechanical, SUM(CASE WHEN p.apply_group = 2 THEN i.issued_value ELSE 0 END) AS tot_electrical, SUM(CASE WHEN p.apply_group = 3 THEN i.issued_value ELSE 0 END) AS tot_chemical, SUM(CASE WHEN p.apply_group = 4 THEN i.issued_value ELSE 0 END) AS tot_machinery, SUM(CASE WHEN p.apply_group = 7 THEN i.issued_value ELSE 0 END) AS tot_general FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.source > 1 AND i.required_for <= 6 AND MONTH(i.history_date) = MONTH(CURDATE()) GROUP BY i.required_for ORDER BY i.required_for");

            $bcp_mechanical = 0;
            $bcp_electrical = 0;
            $bcp_chemical = 0;
            $bcp_machinery = 0;
            $bcp_general = 0;

            $con_mechanical = 0;
            $con_electrical = 0;
            $con_chemical = 0;
            $con_machinery = 0;
            $con_general = 0;

            $hrm_mechanical = 0;
            $hrm_electrical = 0;
            $hrm_chemical = 0;
            $hrm_machinery = 0;
            $hrm_general = 0;

            $hrm2_mechanical = 0;
            $hrm2_electrical = 0;
            $hrm2_chemical = 0;
            $hrm2_machinery = 0;
            $hrm2_general = 0;
            
            if(mysqli_num_rows($inventory_history_query) > 0){
                $i = 1;
                
                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    if($i == 1 || $i == 2){
                        $bcp_mechanical += $row['tot_mechanical'];
                        $bcp_electrical += $row['tot_electrical'];
                        $bcp_chemical += $row['tot_chemical'];
                        $bcp_machinery += $row['tot_machinery'];
                        $bcp_general += $row['tot_general'];
                    } elseif($i == 3 || $i == 4){
                        $con_mechanical += $row['tot_mechanical'];
                        $con_electrical += $row['tot_electrical'];
                        $con_chemical += $row['tot_chemical'];
                        $con_machinery += $row['tot_machinery'];
                        $con_general += $row['tot_general'];
                    } elseif($i == 5){
                        $hrm_mechanical = $row['tot_mechanical'];
                        $hrm_electrical = $row['tot_electrical'];
                        $hrm_chemical = $row['tot_chemical'];
                        $hrm_machinery = $row['tot_machinery'];
                        $hrm_general = $row['tot_general'];
                    } else{
                        $hrm2_mechanical = $row['tot_mechanical'];
                        $hrm2_electrical = $row['tot_electrical'];
                        $hrm2_chemical = $row['tot_chemical'];
                        $hrm2_machinery = $row['tot_machinery'];
                        $hrm2_general = $row['tot_general'];
                    }

                    $i++;
                }
            }

            $bcp['mechanical'] = $bcp_mechanical;
            $bcp['electrical'] = $bcp_electrical;
            $bcp['chemical'] = $bcp_chemical;
            $bcp['machinery'] = $bcp_machinery;
            $bcp['general'] = $bcp_general;

            $con['mechanical'] = $con_mechanical;
            $con['electrical'] = $con_electrical;
            $con['chemical'] = $con_chemical;
            $con['machinery'] = $con_machinery;
            $con['general'] = $con_general;

            $hrm['mechanical'] = $hrm_mechanical;
            $hrm['electrical'] = $hrm_electrical;
            $hrm['chemical'] = $hrm_chemical;
            $hrm['machinery'] = $hrm_machinery;
            $hrm['general'] = $hrm_general;

            $hrm2['mechanical'] = $hrm2_mechanical;
            $hrm2['electrical'] = $hrm2_electrical;
            $hrm2['chemical'] = $hrm2_chemical;
            $hrm2['machinery'] = $hrm2_machinery;
            $hrm2['general'] = $hrm2_general;

            $data[] = [
                'bcp' => $bcp,
                'con' => $con,
                'hrm' => $hrm,
                'hrm2' => $hrm2
            ];

            $reply = array(
                'Type' => 'success',
                'Reply' => $data
            );

            exit(json_encode($reply));
        }

        function fetch_filtered_issued_parts_summary_details($date_range){
            $date_range = explode(' to ', $date_range);
            $start_date = $date_range[0];
            $end_date = $date_range[1];

            $inventory_history_query = mysqli_query($this->conn, "SELECT i.required_for, SUM(CASE WHEN p.apply_group = 1 THEN i.issued_value ELSE 0 END) AS tot_mechanical, SUM(CASE WHEN p.apply_group = 2 THEN i.issued_value ELSE 0 END) AS tot_electrical, SUM(CASE WHEN p.apply_group = 3 THEN i.issued_value ELSE 0 END) AS tot_chemical, SUM(CASE WHEN p.apply_group = 4 THEN i.issued_value ELSE 0 END) AS tot_machinery, SUM(CASE WHEN p.apply_group = 7 THEN i.issued_value ELSE 0 END) AS tot_general FROM rrmsteel_inv_history i INNER JOIN rrmsteel_parts p ON p.parts_id = i.parts_id WHERE i.source > 1 AND i.required_for <= 6 AND i.history_date >= '$start_date' AND i.history_date <= '$end_date' GROUP BY i.required_for ORDER BY i.required_for");

            if(mysqli_num_rows($inventory_history_query) > 0){
                $bcp_mechanical = 0;
                $bcp_electrical = 0;
                $bcp_chemical = 0;
                $bcp_machinery = 0;
                $bcp_general = 0;

                $con_mechanical = 0;
                $con_electrical = 0;
                $con_chemical = 0;
                $con_machinery = 0;
                $con_general = 0;

                $hrm_mechanical = 0;
                $hrm_electrical = 0;
                $hrm_machinery = 0;
                $hrm_general = 0;

                $hrm2_mechanical = 0;
                $hrm2_electrical = 0;
                $hrm2_machinery = 0;
                $hrm2_general = 0;

                $i = 1;

                while($row = mysqli_fetch_assoc($inventory_history_query)){
                    if($i == 1 || $i == 2){
                        $bcp_mechanical += $row['tot_mechanical'];
                        $bcp_electrical += $row['tot_electrical'];
                        $bcp_chemical += $row['tot_chemical'];
                        $bcp_machinery += $row['tot_machinery'];
                        $bcp_general += $row['tot_general'];
                    } elseif($i == 3 || $i == 4){
                        $con_mechanical += $row['tot_mechanical'];
                        $con_electrical += $row['tot_electrical'];
                        $con_chemical += $row['tot_chemical'];
                        $con_machinery += $row['tot_machinery'];
                        $con_general += $row['tot_general'];
                    } elseif($i == 5){
                        $hrm_mechanical = $row['tot_mechanical'];
                        $hrm_electrical = $row['tot_electrical'];
                        $hrm_chemical = $row['tot_chemical'];
                        $hrm_machinery = $row['tot_machinery'];
                        $hrm_general = $row['tot_general'];
                    } else{
                        $hrm2_mechanical = $row['tot_mechanical'];
                        $hrm2_electrical = $row['tot_electrical'];
                        $hrm2_chemical = $row['tot_chemical'];
                        $hrm2_machinery = $row['tot_machinery'];
                        $hrm2_general = $row['tot_general'];
                    }

                    $i++;
                }

                $bcp['mechanical'] = $bcp_mechanical;
                $bcp['electrical'] = $bcp_electrical;
                $bcp['chemical'] = $bcp_chemical;
                $bcp['machinery'] = $bcp_machinery;
                $bcp['general'] = $bcp_general;

                $con['mechanical'] = $con_mechanical;
                $con['electrical'] = $con_electrical;
                $con['chemical'] = $con_chemical;
                $con['machinery'] = $con_machinery;
                $con['general'] = $con_general;

                $hrm['mechanical'] = $hrm_mechanical;
                $hrm['electrical'] = $hrm_electrical;
                $hrm['chemical'] = $hrm_chemical;
                $hrm['machinery'] = $hrm_machinery;
                $hrm['general'] = $hrm_general;

                $hrm2['mechanical'] = $hrm2_mechanical;
                $hrm2['electrical'] = $hrm2_electrical;
                $hrm2['chemical'] = $hrm2_chemical;
                $hrm2['machinery'] = $hrm2_machinery;
                $hrm2['general'] = $hrm2_general;

                $data[] = [
                    'bcp' => $bcp,
                    'con' => $con,
                    'hrm' => $hrm,
                    'hrm2' => $hrm2
                ];

                $reply = array(
                    'Type' => 'success',
                    'Reply' => $data
                );

                exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No history found !'
                )));
            }
        }
    }
?>