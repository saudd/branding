<?php
    namespace User;

    class User{
        private $conn;

        function __construct(){
            $this->conn = $GLOBALS['conn'];
        }

        function fetch($user_id){
            $user_query = mysqli_query($this->conn, "SELECT * FROM rrmsteel_user WHERE user_id = '$user_id' LIMIT 1");

            if(mysqli_num_rows($user_query) > 0){
            	while($row = mysqli_fetch_assoc($user_query)){
	                $data[] = [
	                	'user_id' => $row['user_id'],
	                    'user_fullname' => $row['user_fullname'],
	                    'user_email' => $row['user_email'],
	                    'user_mobile' => $row['user_mobile'],
	                    'user_designation' => $row['user_designation'],
	                    'user_department' => $row['user_department'],
	                    'user_status' => $row['user_status'],
	                    'user_category' => $row['user_category']
	                ];
	            }

                $reply = array(
	                'Type' => 'success',
	                'Reply' => $data
	            );

	            exit(json_encode($reply));
            } else{
                exit(json_encode(array(
                    'Type' => 'error',
                    'Reply' => 'No user found !'
                )));
            }
        }
    }
?>