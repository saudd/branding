<?php
    use Purchase\Purchase;

    require_once('connection.php');
    require_once('model/Purchase.php');
    
    if($_POST['purchase_data_type'] == 'fetch_requisition_con'){
        if(!empty($_POST['requisition_id'])){
            $purchase = new Purchase();
            echo $purchase->fetch_requisition_con(mysqli_real_escape_string($conn, $_POST['requisition_id']));
        }
    } elseif($_POST['purchase_data_type'] == 'fetch_requisition_spr'){
        if(!empty($_POST['requisition_id'])){
            $purchase = new Purchase();
            echo $purchase->fetch_requisition_spr(mysqli_real_escape_string($conn, $_POST['requisition_id']));
        }
    } elseif($_POST['purchase_data_type'] == 'fetch_purchase_con'){
        if(!empty($_POST['requisition_id'])){
            $purchase = new Purchase();
            echo $purchase->fetch_purchase_con(mysqli_real_escape_string($conn, $_POST['requisition_id']));
        }
    } elseif($_POST['purchase_data_type'] == 'fetch_purchase_spr'){
        if(!empty($_POST['requisition_id'])){
            $purchase = new Purchase();
            echo $purchase->fetch_purchase_spr(mysqli_real_escape_string($conn, $_POST['requisition_id']));
        }
    } elseif($_POST['purchase_data_type'] == 'fetch_filtered_purchase'){
        if(!empty($_POST['type']) || !empty($_POST['party_id']) || !empty($_POST['parts_id'])){
            $purchase = new Purchase();

            if(isset($_POST['date_range'])){
                echo $purchase->fetch_filtered_purchase(mysqli_real_escape_string($conn, $_POST['type']), mysqli_real_escape_string($conn, $_POST['party_id']), mysqli_real_escape_string($conn, $_POST['parts_id']), mysqli_real_escape_string($conn, $_POST['date_range']));
            } else{
               echo $purchase->fetch_filtered_purchase(mysqli_real_escape_string($conn, $_POST['type']), mysqli_real_escape_string($conn, $_POST['party_id']), mysqli_real_escape_string($conn, $_POST['parts_id'])); 
            }
        }
    } elseif($_POST['purchase_data_type'] == 'fetch_tot_purchase_against_requisition'){
        $purchase = new Purchase();
        echo $purchase->fetch_tot_purchase_against_requisition();
    }
?>